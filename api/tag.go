package api

import (
	"reflect"
	"strconv"
	"strings"
)

// Tag is a struct tag that belongs to a particular
// package. By convention, the Tag is either:
//
//	(A) a value followed by comma-separated flags, or
//	(B) a value followed by a sequence of space-seperated values.
//
// values that the package will read when importing or exporting
// the API. The first value is considered the primary value with
// subsequent comma-separated values acting as a collection of
// 'flags' for the package to understand. If you are creating
// a new package tag and need to support commas, then use a
// sequence of spaces separated values instead and call Slice.
type Tag string

// Value returns the first/primary value of the Tag without any
// subsequent comma-separated values.
func (t Tag) Value() string {
	return strings.Split(strings.Split(string(t), ",")[0], " ")[0]
}

// Slice returns the slice of space-separated values.
func (t Tag) Slice() []string {
	return strings.Split(string(t), " ")
}

// Index returns the  i'th separated value in the sequence.
func (t Tag) Index(i int) string {
	slice := t.Slice()
	if len(slice) > i {
		return slice[i]
	}
	return ""
}

// Has returns true when the Tag has the given flag value as
// one of the subsequent comma-separated values.
func (t Tag) Has(flag string) bool {
	for _, v := range strings.Split(string(t), ",")[1:] {
		if v == flag {
			return true
		}
	}
	return false
}

// Tags are the tags associated with the different packages.
type Tags reflect.StructTag

// Iter returns an iterator over the tags list, allowing
// each tag to be iteratated over in order.
func (l Tags) Iter() TagsIterator {
	return TagsIterator{list: l}
}

// Get returns the Tag associated with the given transport.
func (l Tags) Get(pkg string) Tag {
	return Tag(reflect.StructTag(l).Get(string(pkg)))
}

// Doc returns the doc string associated with the tags.
// The doc string begins after the first newline of the
// tag and ignores any tab characters inside it.
func (l Tags) Doc() string {
	splits := strings.SplitN(string(l), "\n", 2)

	if len(splits) > 1 {

		// determine the indentation on the first line
		var indentation int
		for _, char := range splits[1] {
			if char != '\t' {
				break
			}
			indentation++
		}

		var sequence = strings.Repeat("\t", indentation)

		return strings.ReplaceAll("\n"+splits[1], "\n"+sequence, "\n")[1:]
	}

	return ""
}

// TagsIterator contains the state for an iteration over a qtags list.
// Next must be called before
type TagsIterator struct {
	list Tags

	name  string
	value Tag
}

// Key returns the iterators current Tag and it's name.
func (i *TagsIterator) Value() (string, Tag) {
	return i.name, i.value
}

// Next advances the iterator and returns true if a value was found.
func (iter *TagsIterator) Next() bool {

	tag := iter.list
	if tag == "" {
		return false
	}

	// Skip leading space.
	i := 0
	for i < len(tag) && tag[i] == ' ' {
		i++
	}
	tag = tag[i:]
	if tag == "" {
		return false
	}

	// Scan to colon. A space, a quote or a control character is a syntax error.
	// Strictly speaking, control chars include the range [0x7f, 0x9f], not just
	// [0x00, 0x1f], but in practice, we ignore the multi-byte control characters
	// as it is simpler to inspect the tag's bytes than the tag's runes.
	i = 0
	for i < len(tag) && tag[i] > ' ' && tag[i] != ':' && tag[i] != '"' && tag[i] != 0x7f {
		i++
	}
	if i == 0 || i+1 >= len(tag) || tag[i] != ':' || tag[i+1] != '"' {
		return false
	}
	name := string(tag[:i])
	tag = tag[i+1:]

	// Scan quoted string to find value.
	i = 1
	for i < len(tag) && tag[i] != '"' {
		if tag[i] == '\\' {
			i++
		}
		i++
	}
	if i >= len(tag) {
		return false
	}
	qvalue := string(tag[:i+1])
	tag = tag[i+1:]

	iter.name = name

	unquoted, err := strconv.Unquote(qvalue)
	if err != nil {
		return false
	}
	iter.value = Tag(unquoted)

	iter.list = tag

	return true
}
