package api_test

import (
	"testing"

	"pkg.iqhive.com/iq/api"
)

func TestIterator(t *testing.T) {

	var example api.Tags = `a:"1" b:"2" c:"3"`

	iter := example.Iter()

	if iter.Next() != true {
		t.Fatal("expected true")
	}
	if v, tag := iter.Value(); v != "a" || tag != "1" {
		t.Fatal("expected a")
	}
	if iter.Next() != true {
		t.Fatal("expected true")
	}
	if v, tag := iter.Value(); v != "b" || tag != "2" {
		t.Fatal("expected b")
	}
	if iter.Next() != true {
		t.Fatal("expected true")
	}
	if v, tag := iter.Value(); v != "c" || tag != "3" {
		t.Fatal("expected c")
	}
}
