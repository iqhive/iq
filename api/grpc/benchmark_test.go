package grpc_test

import (
	"context"
	"net"
	"os"
	"testing"

	google_grpc "google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/grpc"

	mysvc "github.com/neocortical/mysvc/grpc"
)

// userServiceController implements the gRPC UserServiceServer interface.
type MyServiceServer struct{}

// GetUsers calls the core service's GetUsers method and maps the result to a grpc service response.
func (MyServiceServer) GetUsers(ctx context.Context, req *mysvc.GetUsersRequest) (resp *mysvc.GetUsersResponse, err error) {

	var users []*mysvc.User

	for i := 0; i < 100; i++ {
		users = append(users, &mysvc.User{
			Name: "Bob",
		})
	}

	return &mysvc.GetUsersResponse{
		Users: users,
	}, nil
}

type GetUsersRequest struct {
	Ids []int64 `json:"ids" pb:"1"`
}

type User struct {
	ID   int64  `json:"id" pb:"1"`
	Name string `json:"name" pb:"2"`
}

type GetUsersResponse struct {
	Users []User `json:"users" pb:"1"`
}

type BenchmarkAPI struct {
	api.Specification `grpc:"grpc.UserService"`

	GetUsers func(GetUsersRequest) (GetUsersResponse, error)
}

func (b *BenchmarkAPI) Implement() {
	b.GetUsers = func(req GetUsersRequest) (GetUsersResponse, error) {

		var users []User

		for i := 0; i < 100; i++ {
			users = append(users, User{
				ID:   int64(i),
				Name: "Bob",
			})
		}

		return GetUsersResponse{
			Users: users,
		}, nil
	}
}

var BenchmarkClient BenchmarkAPI

var GoogleClient mysvc.UserServiceClient

func TestMain(t *testing.M) {

	server := google_grpc.NewServer()
	mysvc.RegisterUserServiceServer(server, MyServiceServer{})
	reflection.Register(server)

	con, err := net.Listen("tcp", "localhost:5555")
	if err != nil {
		panic(err)
	}

	go func() {
		err = server.Serve(con)
		if err != nil {
			panic(err)
		}
	}()

	conn, err := google_grpc.Dial("localhost:5555", google_grpc.WithInsecure())
	if err != nil {
		panic(err)
	}

	GoogleClient = mysvc.NewUserServiceClient(conn)

	s := new(BenchmarkAPI)
	s.Implement()

	go grpc.ListenAndServe(":7777", s)

	if err := grpc.Set(&BenchmarkClient, "localhost:7777", google_grpc.WithInsecure()); err != nil {
		panic(err)
	}

	os.Exit(t.Run())
}

func TestUsers(t *testing.T) {
	resp, err := BenchmarkClient.GetUsers(GetUsersRequest{[]int64{1, 2, 3, 4}})
	if err != nil {
		panic(err)
	}
	if len(resp.Users) != 100 {
		t.Errorf("Expected 100 users, got %d", len(resp.Users))
	}
}

func BenchmarkGoogle(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, err := GoogleClient.GetUsers(context.Background(), &mysvc.GetUsersRequest{
			Ids: []int64{1, 2, 3, 4},
		})
		if err != nil {
			panic(err)
		}
	}

}

func BenchmarkIQ(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if _, err := BenchmarkClient.GetUsers(GetUsersRequest{[]int64{1, 2, 3, 4}}); err != nil {
			panic(err)
		}
	}
}
