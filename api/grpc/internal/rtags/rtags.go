package rtags

import (
	"errors"
	"reflect"
	"strconv"
	"strings"

	"pkg.iqhive.com/iq/api"
)

// IntoThree splits a string into three strings, based on
// the given separator.
func intoThree(str string, separator string) (string, string, string) {
	switch splits := strings.SplitN(str, separator, 3); len(splits) {
	case 1:
		return splits[0], "", ""
	case 2:
		return splits[0], splits[1], ""
	default:
		return splits[0], splits[1], splits[2]
	}
}

func WireNumberOf(field reflect.StructField) (int32, error) {
	ftag, ok := field.Tag.Lookup("pb")
	if !ok {

		//We can use the protoc tag if it exists.
		ptag, ok := field.Tag.Lookup("protobuf")
		if !ok {
			return 0, errors.New("all struct fields must have a pb tag")
		}

		_, ftag, _ = intoThree(ptag, ",")
	}

	number, err := strconv.Atoi(ftag)
	if err != nil {
		return 0, errors.New("all struct fields must have numerical pb tags")
	}

	return int32(number), nil
}

func FunctionNameOf(fn api.Function) string {
	tag := string(fn.Tags.Get("grpc"))

	var name string

parser:
	for _, r := range tag {
		switch r {
		case ',', ' ', '(':
			break parser
		default:
			name += string(r)
		}
	}

	if name == "" {
		return fn.Name
	}

	_, err := strconv.Atoi(name)
	if err == nil {
		return fn.Name
	}

	return name
}

type message struct {
	Type        reflect.Type
	Value       reflect.Value
	passthrough bool //if true, value is not a virtual message, and the type is passed through.
}

func (v *message) Interface() interface{} {
	return v.Value.Addr().Interface()
}

func (v *message) Zero() {
	v.Value = reflect.New(v.Type).Elem()
}

func (v *message) Load(args []reflect.Value) {
	v.Value = reflect.New(v.Type).Elem()

	if v.passthrough {
		v.Value.Set(args[0])
		return
	}

	if len(args) != v.Type.NumField() {
		panic("argument count mismatch")
	}

	for i := 0; i < v.Type.NumField(); i++ {
		v.Value.Field(i).Set(args[i])
	}
}

func (v *message) Scan(args []reflect.Value) {
	if v.passthrough {
		args[0] = v.Value
		return
	}

	if len(args) != v.Type.NumField() {
		panic("argument count mismatch")
	}

	for i := 0; i < v.Type.NumField(); i++ {
		args[i] = v.Value.Field(i)
	}
}

// ResultRules returns the result rules of the grpc tag.
func resultRulesOf(t string) []string {
	splits := strings.Split(string(t), " ")
	last := splits[len(splits)-1]

	if strings.Contains(last, "(") {
		return nil
	}
	if strings.Contains(last, ",") {
		return strings.Split(last, ",")
	}

	_, err := strconv.Atoi(last)
	if err == nil {
		return strings.Split(last, ",")
	}

	return nil
}

// ArgumentRulesOf returns the argument rules of the grpc tag.
func argumentRulesOf(tag string) []string {
	var rules string
	var found bool

	for _, r := range tag {
		switch r {
		case '(':
			found = true
			continue
		}

		if found {
			switch r {
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',':
				rules += string(r)
				continue
			}

			if r == ')' {
				found = false
				break
			}

			return nil
		}
	}

	if rules == "" || found {
		return nil
	}

	return strings.Split(rules, ",")
}

// create a virtual request/response types if the function has multiple arguments.
// we use the argument rules to construct this, as specified in
// the package documentation.
func MakeRequestAndResponse(fn api.Function) (req, resp message) {

	in := fn.Type.NumIn()
	out := fn.Type.NumOut()

	//Request
	var rules = argumentRulesOf(string(fn.Tags.Get("grpc")))
	switch {
	case len(rules) == 0 && in == 1:
		req.passthrough = true
		req.Type = fn.Type.In(0)
	case len(rules) != in:
		panic("argument rules do not match number of arguments for " + fn.Name)
	default:
		args := make([]reflect.StructField, in)
		for i := 0; i < in; i++ {
			args[i] = reflect.StructField{
				Name: "Field" + strconv.Itoa(i+1),
				Type: fn.Type.In(i),
				Tag:  reflect.StructTag(`pb:"` + rules[i] + `"`),
			}
		}
		req.Type = reflect.StructOf(args)
	}

	//Response
	rules = resultRulesOf(string(fn.Tags.Get("grpc")))
	switch {
	case len(rules) == 0 && out == 1:
		resp.passthrough = true
		resp.Type = fn.Type.Out(0)
	case len(rules) != out:
		panic("result rules do not match number of results for " + fn.Name)
	default:
		results := make([]reflect.StructField, out)
		for i := 0; i < out; i++ {
			results[i] = reflect.StructField{
				Name: "Field" + strconv.Itoa(i+1),
				Type: fn.Type.Out(i),
				Tag:  reflect.StructTag(`pb:"` + rules[i] + `"`),
			}
		}
		resp.Type = reflect.StructOf(results)
	}

	return req, resp
}
