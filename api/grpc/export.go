package grpc

import (
	"context"
	"net"
	"reflect"

	google_grpc "google.golang.org/grpc"
	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/grpc/internal/rtags"
	"pkg.iqhive.com/iq/wire/proto"
)

// Server is a grpc server.
type Server struct {
	server *google_grpc.Server
}

// NewServer creates a new grpc server, out of the
// given specifications.
func NewServer(opts ...google_grpc.ServerOption) *Server {
	return &Server{google_grpc.NewServer(opts...)}
}

func (s Server) ListenAndServe(addr string, implementations ...api.WithSpecification) error {
	for _, grpc := range implementations {
		spec, err := api.SpecificationOf(grpc)
		if err != nil {
			return err
		}

		services, err := servicesOf(spec)
		if err != nil {
			return err
		}

		for i := range services {
			s.server.RegisterService(&services[i], nil)
		}
	}

	lis, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	return s.server.Serve(lis)
}

// ListenAndServe listens on the TCP network address addr and serves the given grpc
// specification, options can be passed through to the grpc listener.
func ListenAndServe(addr string, implementations ...api.WithSpecification) error {
	return NewServer().ListenAndServe(addr, implementations...)
}

func servicesOf(spec api.Specification) ([]google_grpc.ServiceDesc, error) {

	var services []google_grpc.ServiceDesc

	var service = google_grpc.ServiceDesc{
		ServiceName: spec.Tags.Get("grpc").Value(),
	}

	for i := range spec.Functions {
		fn := &spec.Functions[i]

		if fn.Tags.Get("grpc").Value() == "-" {
			continue
		}

		reqV, respV := rtags.MakeRequestAndResponse(*fn)

		service.Methods = append(service.Methods, google_grpc.MethodDesc{
			MethodName: rtags.FunctionNameOf(*fn),
			Handler: func(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor google_grpc.UnaryServerInterceptor) (interface{}, error) {
				var req, resp = reqV, respV

				req.Zero()

				in, err := proto.MessageOf(req.Interface())
				if err != nil {
					return nil, err
				}
				if err := dec(in); err != nil {
					return nil, err
				}

				results, err := fn.FuncValue.Call(ctx, func(keys []api.Key) error {
					return nil
				}, func(args []reflect.Value) error {
					req.Scan(args)
					return nil
				})
				if err != nil {
					return nil, err
				}

				resp.Load(results)

				result, err := proto.MessageOf(resp.Interface())
				if err != nil {
					return nil, err
				}

				return result, nil
			},
		})
	}

	services = append(services, service)

	for _, section := range spec.Sections {
		children, err := servicesOf(section)
		if err != nil {
			return nil, err
		}
		services = append(services, children...)
	}

	return services, nil
}
