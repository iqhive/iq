package grpc

import (
	"context"
	"reflect"
	"strings"

	google_grpc "google.golang.org/grpc"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/grpc/internal/rtags"
	"pkg.iqhive.com/iq/wire/proto"
)

// Set sets the given API specification structure to operate over
// grpc, it conects to the given host using the given DialOptions.
func Set(grpc api.WithSpecification, host string, opts ...google_grpc.DialOption) error {
	spec, err := api.SpecificationOf(grpc)
	if err != nil {
		return err
	}

	conn, err := google_grpc.Dial(host, opts...)
	if err != nil {
		return err
	}

	if err := imp(spec, conn); err != nil {
		return err
	}

	return nil
}

func imp(def api.Specification, conn *google_grpc.ClientConn) error {
	for i := range def.Functions {
		fn := &def.Functions[i]

		if fn.Tags.Get("grpc").Value() == "-" {
			continue
		}

		reqV, respV := rtags.MakeRequestAndResponse(*fn)

		name := rtags.FunctionNameOf(*fn)

		service := strings.Split(string(def.Tags.Get("grpc")), " ")[0]

		fn.FuncValue.Set(func(ctx context.Context, keys []api.Key, args []reflect.Value) (results []reflect.Value, err error) {
			var req, resp = reqV, respV

			req.Load(args)

			in, err := proto.MessageOf(req.Interface())
			if err != nil {
				return nil, err
			}

			resp.Zero()

			out, err := proto.MessageOf(resp.Interface())
			if err != nil {
				return nil, err
			}

			err = conn.Invoke(ctx, service+"/"+name, in, out)
			if err != nil {
				return nil, err
			}

			results = make([]reflect.Value, fn.Type.NumOut())
			resp.Scan(results)

			return results, nil
		})
	}

	for _, section := range def.Sections {
		if err := imp(section, conn); err != nil {
			return err
		}
	}

	return nil
}
