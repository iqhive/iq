// Package proto enables the generation of .proto files for an API.
package proto

import (
	"bytes"
	"fmt"
	"io"
	"path"
	"reflect"
	"strings"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/grpc/internal/rtags"
)

type messageWriter map[reflect.Type]string

// writeMessagesOf writes all the Go types that the function uses
// (recursively) as proto messages into the buffer.
func (state messageWriter) writeMessagesOf(w io.Writer, fn api.Function) error {
	req, resp := rtags.MakeRequestAndResponse(fn)

	if err := state.writeMessage(w, fn.Name+"Request", req.Type); err != nil {
		return err
	}

	if err := state.writeMessage(w, fn.Name+"Response", resp.Type); err != nil {
		return err
	}

	return nil
}

func (state messageWriter) goTypeToProto(t reflect.Type) string {
	switch t.Kind() {
	case reflect.String:
		return "string"
	case reflect.Int8, reflect.Int16, reflect.Int32:
		return "int32"
	case reflect.Int, reflect.Int64:
		return "int64"
	case reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return "uint32"
	case reflect.Uint, reflect.Uint64, reflect.Uintptr:
		return "uint64"
	case reflect.Float32:
		return "float"
	case reflect.Float64:
		return "double"
	case reflect.Bool:
		return "bool"

	case reflect.Struct:
		return state[t]
	case reflect.Ptr:
		return state.goTypeToProto(t.Elem())
	case reflect.Slice, reflect.Array:
		return "repeated " + state.goTypeToProto(t.Elem())
	case reflect.Map:
		return "map<" + state.goTypeToProto(t.Key()) + "," + state.goTypeToProto(t.Elem()) + ">"
	default:
		panic(fmt.Sprintf("unsupported type %v", t))
	}
}

// writeMessage writes a message to the buffer, using the provided Go type.
// if it has already been written, then it is skipped. The provided name
// will be used if the Type doesn't already have a name.
func (state messageWriter) writeMessage(w io.Writer, name string, rtype reflect.Type) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()

	if state[rtype] != "" {
		return nil
	}

	switch rtype.Kind() {
	case reflect.String, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Float32, reflect.Float64, reflect.Bool, reflect.Uintptr:
		return nil //basic types don't need their own message, they are builtin.
	case reflect.Map:
		if err := state.writeMessage(w, name, rtype.Key()); err != nil {
			return err
		}
		if err := state.writeMessage(w, name, rtype.Elem()); err != nil {
			return err
		}
		return nil //builtin collections.
	case reflect.Slice, reflect.Ptr:
		if err := state.writeMessage(w, name, rtype.Elem()); err != nil {
			return err
		}
		return nil //builtin collections.
	}

	if rtype.Kind() != reflect.Struct {
		return fmt.Errorf("converting %v kind types to proto is undefined", rtype.Kind())
	}

	//handle nested messages
	//FIXME handle un-named types.
	for i := 0; i < rtype.NumField(); i++ {
		if !rtype.Field(i).IsExported() {
			continue
		}
		if err := state.writeMessage(w, name, rtype.Field(i).Type); err != nil {
			return err
		}
	}

	if rtype.Name() != "" {
		name = rtype.Name()
	}
	state[rtype] = name

	fmt.Fprintf(w, "\nmessage %s {\n", name)
	for i := 0; i < rtype.NumField(); i++ {
		if !rtype.Field(i).IsExported() {
			continue
		}
		number, err := rtags.WireNumberOf(rtype.Field(i))
		if err != nil {
			return err
		}

		fmt.Fprintf(w, "\t%s %s = %d;\n", state.goTypeToProto(rtype.Field(i).Type), rtype.Field(i).Name, number)
	}
	fmt.Fprintf(w, "}\n")

	return nil
}

// writeFunction writes an RPC entry to the buffer.
func writeFunction(w io.Writer, f api.Function) error {

	req, resp := rtags.MakeRequestAndResponse(f)

	fmt.Fprintf(w, "\trpc %s(%s) returns (%s) {}\n", f.Name, req.Type.Name(), resp.Type.Name())

	return nil
}

func FileOf(grpc api.WithSpecification) ([]byte, error) {
	spec, err := api.SpecificationOf(grpc)
	if err != nil {
		return nil, err
	}

	var buffer bytes.Buffer
	fmt.Fprintln(&buffer, `syntax = "proto3";`)

	//we can use reflection to
	//look at the package name of the spec
	//and use this for the proto package.
	rtype := reflect.TypeOf(grpc)
	pkg := path.Base(rtype.PkgPath())
	svc := rtype.Name()

	pair := strings.SplitN(spec.Tags.Get("grpc").Value(), ".", 2)
	if pkg == "." {
		if len(pair) < 2 {
			return nil, fmt.Errorf(`%v is missing a package name, please add a grpc:"PackageName.ServiceName" tag`, rtype)
		}

		pkg = pair[0]
	}

	if len(pair) > 1 && pair[1] != "" {
		svc = pair[1]
	}
	if len(pair) == 1 && pair[0] != "" {
		svc = pair[0]
	}

	fmt.Fprintf(&buffer, "\npackage %s;\n", pkg)

	var state = make(messageWriter)

	//Loop through the functions in the
	//spec and extract all the function
	//types. We will create messages for
	//them.
	for _, f := range spec.Functions {
		if err := state.writeMessagesOf(&buffer, f); err != nil {
			return nil, err
		}
	}

	//Now that the messages have been
	//written, we can create the service.
	//We will use the name of the spec.
	fmt.Fprintf(&buffer, "\nservice %s {\n", svc)

	for _, f := range spec.Functions {
		if err := writeFunction(&buffer, f); err != nil {
			return nil, err
		}
	}

	fmt.Fprintf(&buffer, `}`)

	return buffer.Bytes(), nil
}
