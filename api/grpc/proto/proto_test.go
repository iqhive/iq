package proto_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/grpc/proto"
)

type DoesSomethingRequest struct {
	Name   string   `pb:"1"`
	Value  int32    `pb:"2"`
	Extras []string `pb:"3"`
}

type DoesSomethingResponse struct {
	Result string `pb:"1"`
}

var API struct {
	api.Specification `grpc:"proto_test.Service"`

	DoesSomething func(DoesSomethingRequest) (DoesSomethingResponse, error)
}

func TestProto(t *testing.T) {
	b, err := proto.FileOf(&API)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(string(b))
}
