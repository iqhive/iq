package grpc_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/grpc"
	"pkg.iqhive.com/iq/wire/proto"

	google_grpc "google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type SPID int32

func (s *SPID) String() string {
	return "SPID: " + fmt.Sprint(int32(*s))
}

type Nested struct {
	Value string `pb:"1"`
}

type HistogramRequest struct {
	SPID SPID `pb:"1"`

	Columns []string `pb:"2"`

	Complex complex64 `pb:"3"`

	Nested Nested `pb:"4"`

	Mapping map[string]string `pb:"5"`
}

type HistogramResponse struct {
	Data int32 `pb:"1"`
}

type API struct {
	api.Specification `grpc:"tktusageproto.DataUsage"`

	Histogram func(context.Context, HistogramRequest) (HistogramResponse, error)

	Complex func(a, b float32) (c complex64, err error) `grpc:"(1,2)"`
}

func (api *API) Implement() {
	api.Histogram = func(ctx context.Context, req HistogramRequest) (HistogramResponse, error) {
		return HistogramResponse{Data: 100}, nil
	}
	api.Complex = func(a, b float32) (c complex64, err error) {
		return complex(a, b), nil
	}
}

func TestGRPC(t *testing.T) {

	req := HistogramRequest{
		SPID:    22,
		Columns: []string{"Hello", "World"},
		Complex: complex(10, 22),
		Nested:  Nested{Value: "Hello"},
		Mapping: map[string]string{"Hello": "World"},
	}

	b, err := proto.Marshal(req)
	if err != nil {
		t.Fatal(err)
	}

	var copy HistogramRequest
	if err := proto.Unmarshal(b, &copy); err != nil {

		t.Fatal(err)
	}

	go func() {
		s := new(API)
		s.Implement()
		if err := grpc.ListenAndServe("localhost:1234", s); err != nil {
			panic(err)
		}
	}()

	time.Sleep(50 * time.Millisecond)

	var local API

	if err := grpc.Set(&local, "localhost:1234", google_grpc.WithTransportCredentials(insecure.NewCredentials())); err != nil {
		t.Fatal(err)
	}

	//Histogram
	resp, err := local.Histogram(context.Background(), req)
	if err != nil {
		t.Fatal(err)
	}

	if resp.Data != 100 {
		t.Fatal("Invalid response")
	}

	//Complex
	result, err := local.Complex(1, 2)
	if err != nil {
		t.Fatal(err)
	}

	if result != complex(1, 2) {
		t.Fatal("Invalid response")
	}
}
