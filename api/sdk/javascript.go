package sdk

import (
	"fmt"
	"strconv"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/oops"
)

type Javascript struct{}

func (Javascript) generate(w Writer, spec api.Specification) error {
	fmt.Fprintf(w, "window.%s = {\n", spec.Name)
	for i, fn := range spec.Functions {
		if fn.Tags.Get("rest") == "-" || fn.Tags.Get("swagger") == "-" {
			continue
		}
		fmt.Println(fn.Tags)

		fmt.Fprintf(w, "%s: function(", fn.Name)
		for i := 0; i < fn.Type.NumIn(); i++ {
			if i > 0 {
				fmt.Fprintf(w, ", ")
			}
			fmt.Fprintf(w, "%s", string([]rune{rune('a' + i)}))
		}
		fmt.Fprintf(w, ") {\n")
		fmt.Fprintf(w, "\t\t\t\treturn transport.Call([")
		for i := 0; i < fn.Type.NumIn(); i++ {
			if i > 0 {
				fmt.Fprintf(w, ", ")
			}
			fmt.Fprintf(w, "%s", string([]rune{rune('a' + i)}))
		}
		fmt.Fprintf(w, "], new Map<string, string>([")
		iter := fn.Tags.Iter()
		var next = ""
		for iter.Next() {
			pkg, tag := iter.Value()
			fmt.Fprintf(w, "%s[%s,%s]", next, strconv.Quote(pkg), strconv.Quote(string(tag)))
			next = ", "
		}
		fmt.Fprintf(w, "]))\n")
		fmt.Fprintf(w, "\t\t\t}")
		if i < len(spec.Functions)-1 {
			fmt.Fprintf(w, ",\n")
		}
	}
	fmt.Fprintf(w, "};\n")

	return nil
}

func (js Javascript) Generate(w Writer, specs ...api.WithSpecification) error {
	for _, each := range specs {
		spec, err := api.SpecificationOf(each)
		if err != nil {
			return oops.New(err)
		}
		if err := js.generate(w, spec); err != nil {
			return oops.New(err)
		}
		for _, each := range spec.Sections {
			if err := js.generate(w, each); err != nil {
				return oops.New(err)
			}
		}
	}
	return nil
}
