// Package sdk provides a way to generate SDKs from an API specification.
package sdk

import (
	"archive/zip"
	"io"
)

type Writer interface {
	io.Writer

	NewFile(name string) error
}

type writer struct {
	io.Writer

	zipped *zip.Writer
}

func (w *writer) NewFile(name string) (err error) {
	if w.zipped == nil {
		w.zipped = zip.NewWriter(w.Writer)
	} else {
		w.zipped.Flush()
	}
	w.Writer, err = w.zipped.Create(name)
	return
}

func NewWriter(w io.Writer) Writer {
	return &writer{
		Writer: w,
	}
}
