package sdk

import (
	"fmt"
	"path"
	"reflect"
	"strconv"
	"strings"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/oops"
)

type namespaceMap map[string]map[reflect.Type]bool

func (n namespaceMap) Add(T reflect.Type) bool {
	if n == nil {
		return false
	}
	pkg := T.PkgPath()
	if n[pkg] == nil {
		n[pkg] = make(map[reflect.Type]bool)
	}
	if _, ok := n[pkg][T]; ok {
		return false
	}
	n[pkg][T] = true
	return true
}

type Typescript struct{}

func (t Typescript) typeOf(T reflect.Type, types namespaceMap) string {
	switch T.Kind() {
	case reflect.Bool:
		return "boolean"
	case reflect.Int8, reflect.Int16, reflect.Int32:
		return "number"
	case reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return "number"
	case reflect.Uint, reflect.Uint64, reflect.Int, reflect.Int64:
		return "bigint"
	case reflect.Float32, reflect.Float64:
		return "number"
	case reflect.String:
		return "string"
	case reflect.Struct:
		if types.Add(T) {
			for i := 0; i < T.NumField(); i++ {
				if !T.Field(i).IsExported() {
					continue
				}
				t.typeOf(T.Field(i).Type, types)
			}
		}
		return path.Base(T.PkgPath()) + "." + T.Name()
	case reflect.Interface:
		return "any"
	case reflect.Ptr:
		return t.typeOf(T.Elem(), types)
	case reflect.Slice:
		return t.typeOf(T.Elem(), types) + "[]"
	default:
		panic(fmt.Errorf("unknown type: %s", T))
	}
}

func (t Typescript) writeFunctionHeader(w Writer, fn api.Function, name string, namespace namespaceMap) string {
	fmt.Fprintf(w, "%s(", name)
	for i := 0; i < fn.Type.NumIn(); i++ {
		if i > 0 {
			fmt.Fprintf(w, ", ")
		}
		fmt.Fprintf(w, "%s: %s", string([]rune{rune('a' + i)}), t.typeOf(fn.Type.In(i), namespace))
	}
	switch fn.Type.NumOut() {
	case 0:
		fmt.Fprintf(w, "): Promise<void>")
		return "Promise<void>"
	case 1:
		fmt.Fprintf(w, "): Promise<%s>", t.typeOf(fn.Type.Out(0), namespace))
		return "Promise<" + t.typeOf(fn.Type.Out(0), namespace) + ">"
	default:
		panic("not implemented")
	}
}

func (t Typescript) Generate(w Writer, implementations ...api.WithSpecification) error {
	fmt.Fprintf(w, `
export namespace api {
	export Transport interface {
		Call(args: any, tags: string): Promise<any>;
	}
}
`)

	var namespace = make(namespaceMap)

	for _, impl := range implementations {
		spec, err := api.SpecificationOf(impl)
		if err != nil {
			return oops.New(err)
		}

		fmt.Fprintf(w, "export namespace %s {\n", path.Base(reflect.TypeOf(impl).Elem().PkgPath()))

		fmt.Fprintf(w, "\texport var API: Implementation;\n\n")

		// create an interface for the implementation
		fmt.Fprintf(w, "\texport interface Implementation {\n")
		for _, fn := range spec.Functions {
			doc := fn.Name + " " + fn.Tags.Doc()
			for _, line := range strings.Split(doc, "\n") {
				fmt.Fprintf(w, "\t\t// %s\n", line)
			}
			fmt.Fprintf(w, "\t\t")
			t.writeFunctionHeader(w, fn, fn.Name, namespace)
			fmt.Fprintf(w, ";\n")
		}
		fmt.Fprintf(w, "\t}\n\n")

		// create a function to create an implementation
		// out of a transport
		fmt.Fprintf(w, "\texport function New(transport: api.Transport): Implementation {\n")
		fmt.Fprintf(w, "\t\treturn {\n")
		for _, fn := range spec.Functions {
			fmt.Fprintf(w, "\t\t\t%s: ", fn.Name)
			result := t.writeFunctionHeader(w, fn, "function", namespace)
			fmt.Fprintf(w, "{\n")
			fmt.Fprintf(w, "\t\t\t\treturn transport.Call([")
			for i := 0; i < fn.Type.NumIn(); i++ {
				if i > 0 {
					fmt.Fprintf(w, ", ")
				}
				fmt.Fprintf(w, "%s", string([]rune{rune('a' + i)}))
			}
			fmt.Fprintf(w, "], new Map<string, string>([")
			iter := fn.Tags.Iter()
			var next = ""
			for iter.Next() {
				pkg, tag := iter.Value()
				fmt.Fprintf(w, "%s[%s,%s]", next, strconv.Quote(pkg), strconv.Quote(string(tag)))
				next = ", "
			}
			fmt.Fprintf(w, "])) as %v;\n", result)
			fmt.Fprintf(w, "\t\t\t},\n")
		}
		fmt.Fprintf(w, "\t\t}\n")
		fmt.Fprintf(w, "\t}\n")
		fmt.Fprintf(w, "}\n")
	}

	for pkg, types := range namespace {
		fmt.Fprintf(w, "export namespace %s {\n", path.Base(pkg))
		for T := range types {
			fmt.Fprintf(w, "\texport interface %s {\n", T.Name())
			for i := 0; i < T.NumField(); i++ {
				field := T.Field(i)
				if field.IsExported() {
					continue
				}
				fmt.Fprintf(w, "\t\t%s: %s;\n", field.Name, t.typeOf(field.Type, nil))
			}
			fmt.Fprintf(w, "\t}\n")
		}
		fmt.Fprintf(w, "}\n")
	}

	return nil
}
