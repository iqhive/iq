# The Hive Go Compiler

Additions to the Go Specification:

1. no writes and/or writable references may be made to global
   variables after init and never write to exported variables
   in a different package.
2. pointer values and reference types (including any structs
   that contain them) may not be passed in a channel or as an
   argument to a goroutine.
3. all goroutine functions and main must immediately defer a 
   runtime.Goexit.
4. all channel reads/writes must be placed in a select with
   ctx.Done() or a default case, context.Background() and 
   context.TODO() not allowed outside of init and main.

## Implementation Notes

* goroutines may be launched on a different machine, or Go process.
* panics raised in one goroutine will not affect other goroutines.

## Magic comments:

`go:endpoint` marks a function with read-only serialisable arguments 
as an API endpoint that can either be imported and/or exported over HTTP.
Arguments must be read-only as if they were globals.

```go   
    //go:endpoint GET /hello-world
    func HelloWorld(ctx context.Context) (string, error) {
        return "Hello World", nil
    }
```

`go:database` marks a global map variable with a `comparable` key and value type to be backed by a database. These maps are permitted to be written to by functions
called by a primary goroutine (such as main, Test, or an api:endpoint).

```go
    type User struct {
        ID string
    }

    //go:database users /users/{ID}
    var users = make(map[string]User)
```

`go:handling` can mark a function with serialisable arguments as a message queue
subscriber capable of processing messages on the queue.
```go
    type Event struct {
        Type string
    }

    //go:handling hello-events
    func HandleEvent(event Event) {
        fmt.Println("Received event:", event.Type)
    }
```

`go:schedule` can mark a function to be called periodically via a crontab.
```go

    //go:schedule * * * * *
    func CleanupThings(now time.Time) {
        // ...
    }
```