package soap

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"reflect"

	"encoding/xml"

	http_api "pkg.iqhive.com/iq/api/internal/http"
	"pkg.iqhive.com/iq/oops"

	"pkg.iqhive.com/iq/api"
)

// Set sets the host of the given soap api.
func Set(soap api.WithSpecification, host string) error {
	spec, err := api.SpecificationOf(soap)
	if err != nil {
		return err
	}

	if err := imp(spec, host); err != nil {
		return err
	}

	return nil
}

func imp(def api.Specification, host string) error {

	for i := range def.Functions {
		fn := &def.Functions[i]

		if fn.Tags.Get("soap").Value() == "-" {
			continue
		}

		//Endpoint is extracted out of the rest tag for this field.
		//It uses the fmt formatting syntax.
		tag := fn.Tags.Get("soap")

		//Determine the SOAP action of this request.
		action := actionOf(string(tag))
		suffix := suffixOf(string(tag))

		//Create an implementation of the function that calls the REST
		//endpoint over the network and returns the results.
		fn.FuncValue.Set(func(ctx context.Context, keys []api.Key, args []reflect.Value) (results []reflect.Value, err error) {
			results = make([]reflect.Value, fn.Type.NumOut())

			//body buffers what we will be sending to the endpoint.
			var body bytes.Buffer
			if len(args) > 0 {
				if err := xml.NewEncoder(&body).Encode(envelope{
					XMLName: xml.Name{Local: "soap:Envelope"},
					NS:      "http://schemas.xmlsoap.org/soap/envelope/",
					Body: soapBody{
						XMLName: xml.Name{Local: "soap:Body"},
						Content: args[0].Interface(),
					},
				}); err != nil {
					return nil, oops.Fixme("make sure %T type can become xml", args[0].Interface())
				}
			}

			endpoint := host + suffix
			endpoint = http_api.AttachQueryKeys("soap", keys, endpoint)

			//Debug the url.
			if debug {
				fmt.Println(action, host+suffix)
			}

			req, err := http.NewRequestWithContext(ctx, "POST", endpoint, &body)
			if err != nil {
				return nil, oops.Wrap(err)

			}

			if err := http_api.AttachKeys("soap", keys, req); err != nil {
				return nil, oops.Fixme("make sure that soap/http tags have been configured for %v's authentication: %v", fn.Name, err)
			}

			req.Header.Set("SOAPAction", `"`+action+`"`)
			req.Header.Set("Content-Type", "application/soap+xml")

			if debug {
				fmt.Println("request:")
				b, _ := httputil.DumpRequest(req, true)
				fmt.Println(string(b))
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				return nil, oops.Wrap(err)

			}
			respBytes, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				return nil, oops.Wrap(err)
			}
			if err := resp.Body.Close(); err != nil {
				return nil, oops.Wrap(err)
			}

			//Debug the reponse.
			if debug {
				fmt.Println("body:\n", body.String())
				fmt.Println("response:")
				b, _ := httputil.DumpResponse(resp, true)
				fmt.Println(string(b))
			}

			if err := http_api.ResponseError(resp, respBytes); err != nil {
				return nil, err
			}

			//Zero out the results.
			for i := 0; i < fn.Type.NumOut(); i++ {
				t := fn.Type.Out(i)
				switch t.Kind() {
				case reflect.Ptr:
					results[i] = reflect.New(t.Elem())
				case reflect.Struct:
					results[i] = reflect.New(t).Elem()
				default:
					return nil, oops.Fixme("implement type %s", fn.Type)
				}
			}

			if len(results) > 0 {
				var val = results[0]

				if val.Type().Kind() != reflect.Ptr {
					val = val.Addr()
				}

				if err := xml.NewDecoder(bytes.NewReader(respBytes)).Decode(&envelope{Body: soapBody{Content: val.Interface()}}); err != nil {
					log.Print(ctx, err)
					return nil, oops.Wrap(err)
				}

			}

			return results, nil
		})
	}

	for _, section := range def.Sections {
		if err := imp(section, host); err != nil {
			return err
		}
	}

	return nil
}
