/*
Package soap provides a SOAP api driver.

Tags

Each API function can have a soap tag that specifies
the SOAP action for that function.

	SOAP_ACTION /path/to/endpoint
	[ACTION] [PATH]
*/
package soap

import (
	"encoding/xml"

	"pkg.iqhive.com/iq/api"
)

const debug = false

//specification is unexported so that only drivers will define this interface.
type specification interface {
	SetSpecification(api.Specification) error
}

type envelope struct {
	XMLName xml.Name // http://schemas.xmlsoap.org/soap/envelope/
	NS      string   `xml:"xmlns:soap,attr,omitempty"`
	Body    soapBody
}

type soapBody struct {
	XMLName xml.Name   // http://schemas.xmlsoap.org/soap/envelope/
	Fault   *soapFault `xml:",omitempty"`
	Content interface{}
}

type soapFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope/ Fault"`

	Code   string `xml:"faultcode,omitempty"`
	String string `xml:"faultstring,omitempty"`
	Actor  string `xml:"faultactor,omitempty"`
	Detail string `xml:"detail,omitempty"`
}

// This fn solely exists so that Content is decoded correctly into its struct.
// It is a copy paste from gowsdl.
func (b *soapBody) UnmarshalXML(d *xml.Decoder, _ xml.StartElement) error {
	if b.Content == nil {
		return xml.UnmarshalError("Content must be a pointer to a struct")
	}

	var (
		token    xml.Token
		err      error
		consumed bool
	)

Loop:
	for {
		if token, err = d.Token(); err != nil {
			return err
		}

		if token == nil {
			break
		}

		switch se := token.(type) {
		case xml.StartElement:
			if consumed {
				return xml.UnmarshalError("Found multiple elements inside SOAP body; not wrapped-document/literal WS-I compliant")
			} else if se.Name.Space == "http://schemas.xmlsoap.org/soap/envelope/" && se.Name.Local == "Fault" {
				b.Fault = &soapFault{}
				b.Content = nil

				err = d.DecodeElement(b.Fault, &se)
				if err != nil {
					return err
				}

				consumed = true
			} else {
				if err = d.DecodeElement(b.Content, &se); err != nil {
					return err
				}

				consumed = true
			}
		case xml.EndElement:
			break Loop
		}
	}

	return nil
}

/*

HTTP REQ: POST /apiv2/services/service-order-003?wsdl HTTP/1.1
Host: ipms-dev.tcf.org.nz
Connection: close
Authorization: Basic SVFfSElWRV9TUF9kZXYkMzpHcjNlblRyZSY=
Content-Type: text/xml; charset="utf-8"
Soapaction: ''
User-Agent: gowsdl/0.1

<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ns1="http://transferobjects.ipms.tcf.org.nz">
	<SOAP-ENV:Body>
		<ns1:getRequestedPorts>
			<detail>true</detail>
			<statusList><status>Awaiting LSP Response</status></statusList>
			<statusList><status>Awaiting GSP Approval</status></statusList>
			<statusList><status>Cancelled</status></statusList>
			<statusList><status>Rejected</status></statusList>
			<statusList><status>Request Expired</status></statusList>
			<statusList><status>Approved</status></statusList>
			<filter>All</filter>
			<som>0</som>
		</ns1:getRequestedPorts>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>


HTTP RESP: HTTP/1.1 200
Connection: close
Transfer-Encoding: chunked
Content-Type: text/xml;charset=UTF-8
Date: Fri, 18 Jun 2021 03:29:52 GMT
Set-Cookie: visid_incap_2017870=/ZgvuMzRTcayRbWVVCW+RC8TzGAAAAAAQUIPAAAAAABRFdcsZdAu4rrpFhuO+bL5; expires=Fri, 17 Jun 2022 07:43:20 GMT; HttpOnly; path=/; Domain=.tcf.org.nz
Set-Cookie: incap_ses_998_2017870=IiXyCFyKZR/GGQ0iJpzZDTATzGAAAAAAhHZ0AasVfLix7JpPr1l4zg==; path=/; Domain=.tcf.org.nz
X-Cdn: Imperva
X-Iinfo: 7-21568234-21568235 NNYN CT(5 11 0) RT(1623986991977 10) q(0 0 1 1) r(1 1) U6

145
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<ns2:getRequestedPortsResponse xmlns:ns2="http://transferobjects.ipms.tcf.org.nz">
			<return>
				<success>true</success>
				<serverDateTime>2021-06-18T15:29:52.090+12:00</serverDateTime>
			</return>
		</ns2:getRequestedPortsResponse>
	</soap:Body>
</soap:Envelope>

*/
