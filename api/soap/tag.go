package soap

import (
	"strings"

	"pkg.iqhive.com/iq/api"
)

/*
Each API function can have a soap tag that specifies
the SOAP action for that function.

	SOAP_ACTION /path/to/endpoint
	[ACTION] [PATH]
*/
type tag api.Tag

// Method returns the method of the tag.
func actionOf(t string) string {
	return strings.Split(string(t), " ")[0]
}

// Path returns the path of the tag.
func suffixOf(t string) string {
	_, suffix, _ := strings.Cut(string(t), " ")
	return suffix
}
