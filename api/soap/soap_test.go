package soap

import (
	"encoding/xml"
	"testing"
)

func TestSoapEncode(t *testing.T) {
	payload := struct {
		XMLName    xml.Name
		NS         string `xml:"xmlns:tran,attr"`
		ReportName string `xml:"reportName"`
		FileRoot   string `xml:"fileRoot"`
		ReportType string `xml:"reportType"`
	}{
		XMLName:    xml.Name{Local: "tran:downloadReport"},
		NS:         "http://transferobjects.ipms.tcf.org.nz",
		ReportName: "CarrierDailyChanges",
		FileRoot:   "CarrierDailyChanges_Extract_20210618",
		ReportType: "Extract",
	}

	envelope := envelope{
		XMLName: xml.Name{Local: "soap:Envelope"},
		NS:      "http://transferobjects.ipms.tcf.org.nz",
		Body: soapBody{
			XMLName: xml.Name{Local: "soap:Body"},
			Content: payload,
		},
	}

	_, err := xml.Marshal(envelope)
	if err != nil {
		t.Fatal(err)
	}
}

type testResponse struct {
	XMLName xml.Name `xml:"getRequestedPortsResponse"`
	Test    bool     `xml:"test"`
	Return  struct {
		Success        bool   `xml:"success"`
		ServerDateTime string `xml:"serverDateTime"`
	} `xml:"return"`
}

func TestSoapDecode(t *testing.T) {
	body := testResponse{}

	envelope := envelope{Body: soapBody{Content: &body}}

	/*if err := xml.Unmarshal([]byte(`<ns2:getRequestedPortsResponse xmlns:ns2="http://transferobjects.ipms.tcf.org.nz" test="true">
		<test>true</test>
		<return>
			<success>true</success>
			<serverDateTime>2021-06-18T15:29:52.090+12:00</serverDateTime>
		</return>
	</ns2:getRequestedPortsResponse>`), &body); err != nil {
		t.Fatal(err)
	}*/

	b, err := xml.Marshal(envelope)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%s\n\n", b)

	if err := xml.Unmarshal([]byte( /**/ `<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			<test>true</test>
			<soap:Body>
				<ns2:getRequestedPortsResponse xmlns:ns2="http://transferobjects.ipms.tcf.org.nz" test="true">
					<test>true</test>
					<return>
						<success>true</success>
						<serverDateTime>2021-06-18T15:29:52.090+12:00</serverDateTime>
					</return>
				</ns2:getRequestedPortsResponse>
			</soap:Body>
		</soap:Envelope>`), &envelope); err != nil {
		t.Fatal(err)
	}

	//t.Fatalf("%v", tr.Test)

	if !body.Test {
		t.Fatalf("Fail Test3: %v", body)
	}
	if !body.Return.Success {
		t.Fatalf("Unsuccessful: %s", body.Return.ServerDateTime)
	}
}

/*

$ curl -X POST -d '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tran="http://transferobjects.ipms.tcf.org.nz">
   <soapenv:Header/>
   <soapenv:Body>
      <tran:downloadReport>
         <reportName>CarrierDailyChanges</reportName>
         <fileRoot>CarrierDailyChanges_Extract_20210617</fileRoot>
         <reportType>Extract</reportType>
      </tran:downloadReport>
   </soapenv:Body>
</soapenv:Envelope>' -H 'Authorization: Basic SVEtSGl2ZS1jYXJyaWVyLVJPJDM6Q0BiYkBnM1RyMzM=' https://ipms-dev.tcf.org.nz/apiv2/services/report-003

*/

func TestDownloadReport(t *testing.T) {

}
