// Package swagger can generate the documentation for a REST api.
package swagger

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/google/uuid"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/rest"
	"pkg.iqhive.com/iq/enum"
)

func typeOf[T any]() reflect.Type {
	return reflect.TypeOf([0]T{}).Elem()
}

type flag uint64

const (
	flagString flag = 1 << iota
)

// DocumentationOf returns the swagger documentation of the
// given specifications. The first specification is treated
// as the core api and the tags and host will be used.
func DocumentationOf(s api.WithSpecification) (openapi3.T, error) {
	spec, err := rest.SpecificationOf(s)
	if err != nil {
		return openapi3.T{}, err
	}

	var title string = spec.Name
	var version string = spec.Version
	if override := spec.Tags.Get("swagger"); override != "" {
		title, version, _ = strings.Cut(string(override), ",")
	}

	var docs string = title + " " + spec.Tags.Doc()

	var document func(api.Specification)
	document = func(spec api.Specification) {
		for _, section := range spec.Sections {
			if section.Tags.Get("swagger") == "-" {
				continue
			}

			doc := section.Tags.Doc()
			if doc != "" {
				url := fmt.Sprintf("[%s](/#/%s)", section.Name, url.PathEscape(section.Name+" API"))

				docs += "\n ## " + url + " \nThe " + section.Name + " API " + section.Tags.Doc() + "\n"
			}
			document(section)
		}
	}
	document(spec.Specification)

	if title == spec.Name {
		title += " API"
	}

	var swag = newSwagger()
	swag.Info = &openapi3.Info{
		Title:       title,
		Version:     version,
		Description: docs,
	}

	if spec.Host != "" {
		swag.Servers = []*openapi3.Server{
			{
				URL: spec.Host,
			},
		}
	}

	swag.addSpecification(spec)

	return swag.T, nil
}

type swagger struct {
	openapi3.T

	schemaDictionary map[reflect.Type]*openapi3.SchemaRef
}

func newSwagger() *swagger {
	var swag swagger
	swag.OpenAPI = "3.0.3"
	swag.Paths = make(openapi3.Paths)
	swag.schemaDictionary = make(map[reflect.Type]*openapi3.SchemaRef)
	swag.Components = new(openapi3.Components)
	swag.Components.Schemas = make(openapi3.Schemas)
	return &swag
}

func (swag *swagger) addSpecification(spec rest.Specification) {
	for path, resource := range spec.Resources {
		for method := range resource.Operations {
			fn := resource.Operations[method]

			doc := fn.Name + " " + fn.Tags.Doc()

			if fn.Tags.Get("swagger").Value() == "-" {
				continue
			}

			endpoint := swag.Paths[path]
			if endpoint == nil {
				endpoint = &openapi3.PathItem{}
			}

			operation := openapi3.NewOperation()
			operation.OperationID = strings.Join(append(fn.Path, fn.Name), ".")
			operation.Summary = fn.Name
			operation.Description = doc
			operation.Responses = swag.responsesFor(spec, fn)

			if fn.Path == nil {
				operation.Tags = []string{"endpoints"}
			} else {
				var title string = strings.Join(fn.Path, "") + " API"
				if override := fn.From.Name; override != "" {
					title = override + " API"
				}
				operation.Tags = []string{title}
			}

			operation.Security = swag.addSecurity(fn.Access.Authenticators, fn.Access.Keys)

			endpoint.Parameters, operation.Parameters, operation.RequestBody = swag.parametersFor(spec, fn)

			var nobody *openapi3.RequestBodyRef

			switch method {
			case "GET":
				endpoint.Get = operation
				nobody = endpoint.Get.RequestBody
			case "PUT":
				endpoint.Put = operation
			case "POST":
				endpoint.Post = operation
			case "DELETE":
				endpoint.Delete = operation
				nobody = endpoint.Delete.RequestBody
			case "PATCH":
				endpoint.Patch = operation
			case "CONNECT":
				endpoint.Connect = operation
			case "HEAD":
				endpoint.Head = operation
			default:
				panic("rest.SwaggerFor: unsupported method: " + method)
			}

			//We need to move the body into the parameters.
			if nobody != nil {
				body := swag.getSchemaValue(nobody.Value.Content["application/json"].Schema)
				for name, field := range body.Properties {
					var required = false
					for _, requirement := range body.Required {
						if requirement == name {
							required = true
							break
						}
					}
					endpoint.Parameters = append(endpoint.Parameters, &openapi3.ParameterRef{
						Value: &openapi3.Parameter{
							Name:     name,
							In:       "query",
							Required: required,
							Schema:   field,
						},
					})
				}
				operation.RequestBody = nil
			}

			swag.Paths[strings.ReplaceAll(path, "=%v", "")] = endpoint
		}
	}
}

// splitIntoTwo splits a string into two strings, based on
// the given seperator.
func splitIntoTwo(str string, separator string) (string, string) {
	s := strings.SplitN(str, separator, 2)
	if len(s) > 1 {
		return s[0], s[1]
	}
	return s[0], ""
}

func (swag *swagger) addSecurity(auth []api.Authenticator, keys []api.Key) (sec *openapi3.SecurityRequirements) {
	if swag.Components.SecuritySchemes == nil {
		swag.Components.SecuritySchemes = make(openapi3.SecuritySchemes)
	}

	sec = new(openapi3.SecurityRequirements)

	documentKey := func(key api.Key) {
		name, in := splitIntoTwo(string(key.Tags.Get("rest")), ",")
		if name == "" {
			name = key.Name
		}

		if key.Tags.Get("rest").Has("basic") {
			swag.Components.SecuritySchemes[key.Name] = &openapi3.SecuritySchemeRef{
				Value: &openapi3.SecurityScheme{
					Type:        "http",
					Scheme:      "basic",
					Description: key.Tags.Doc(),
				},
			}
		} else {
			swag.Components.SecuritySchemes[key.Name] = &openapi3.SecuritySchemeRef{
				Value: &openapi3.SecurityScheme{
					Type:        "apiKey",
					Name:        name,
					In:          in,
					Description: key.Tags.Doc(),
				},
			}
		}

		*sec = append(*sec, openapi3.SecurityRequirement{
			key.Name: {},
		})
	}

	for _, auth := range auth {
		k, _ := api.KeysOf(auth)
		for _, key := range k {
			documentKey(key)
		}
	}

	for _, key := range keys {
		documentKey(key)
	}

	if len(*sec) == 0 {
		return nil
	}

	return sec
}

func (swag *swagger) addFieldsToSchema(schema *openapi3.Schema, T reflect.Type) {
	name := swag.nameOf(T)

	for i := 0; i < T.NumField(); i++ {
		field := T.Field(i)

		if !field.IsExported() {
			continue
		}

		if field.Anonymous {
			if docs := api.Tags(field.Tag).Doc(); docs != "" {
				schema.Description = name + " " + docs
			}

			if field.Type.Kind() == reflect.Struct {
				swag.addFieldsToSchema(schema, field.Type)
			}
			continue
		}

		var flags flag

		var name = field.Name
		tag := api.Tags(field.Tag).Get("json")
		if tag != "" {
			if tag == "-" {
				continue
			}

			if base := tag.Value(); base != "" {
				name = base
			}
			if tag.Has("string") {
				flags |= flagString
			}
		}

		var required = true
		for field.Type.Kind() == reflect.Ptr {
			field.Type = field.Type.Elem()
			required = false
		}
		if tag.Has("omitempty") {
			required = false
		}

		schema.Properties[name] = swag.schemaFor(field.Type, field.Name+" "+api.Tags(field.Tag).Doc(), flags)

		if required {
			schema.Required = append(schema.Required, name)
		}
	}
}

func (swag *swagger) getSchemaValue(ref *openapi3.SchemaRef) *openapi3.Schema {
	if ref.Ref != "" {
		return swag.Components.Schemas[strings.TrimPrefix(ref.Ref, "#/components/schemas/")].Value
	}
	return ref.Value
}

func (swag *swagger) nameOf(T reflect.Type) string {
	var pkg = path.Base(T.PkgPath())
	if pkg == "main" {
		pkg = "."
	}
	var (
		name = T.Name()
	)
	if pkg == "enum" {
		_, name, _ = strings.Cut(name, "[")
		name, _, _ = strings.Cut(name, "]")
		name = path.Base(name)
		pkg, name, _ = strings.Cut(name, ".")
	}
	if pkg != "." {
		name = strings.Title(pkg) + name
	}
	if strings.Contains(name, "[") {
		container, details, _ := strings.Cut(name, "[")
		details, _, _ = strings.Cut(details, "]")
		details = path.Base(details)
		pkg, details, _ = strings.Cut(details, ".")
		if pkg != "." {
			details = strings.Title(pkg) + details
		}
		name = container + details
	}
	return name
}

func (swag *swagger) schemaFor(T reflect.Type, doc string, flags flag) *openapi3.SchemaRef {
	name := swag.nameOf(T)

	existing, ok := swag.Components.Schemas[name]
	if ok {
		if doc == "" || (existing.Value != nil && doc == existing.Value.Description) {
			return existing
		}
		return &openapi3.SchemaRef{
			Value: &openapi3.Schema{
				AllOf:       []*openapi3.SchemaRef{{Ref: "#/components/schemas/" + name}},
				Description: doc,
			},
		}
	}

	if name != "" {
		swag.Components.Schemas[name] = &openapi3.SchemaRef{
			Value: new(openapi3.Schema),
		}
	}

	schema := swag.newSchemaFor(T, flags)

	if name == "" {
		schema.Description = doc
		return &openapi3.SchemaRef{
			Value: schema,
		}
	}

	swag.Components.Schemas[name] = &openapi3.SchemaRef{
		Value: schema,
	}

	return &openapi3.SchemaRef{
		Value: &openapi3.Schema{
			AllOf:       []*openapi3.SchemaRef{{Ref: "#/components/schemas/" + name}},
			Description: doc,
		},
	}
}

func (swag *swagger) newSchemaFor(T reflect.Type, flags flag) (schema *openapi3.Schema) {
	type jsonType interface {
		TypeJSON() reflect.Type
	}
	for T.Implements(reflect.TypeOf((*jsonType)(nil)).Elem()) {
		override := reflect.New(T).Elem().Interface().(jsonType)

		T = override.TypeJSON()
	}

	name := swag.nameOf(T)

	// add type Doc() string as the documentation.
	defer func() {
		type documentable interface {
			Doc() string
		}
		type exemplar interface {
			Example()
		}

		if schema != nil {
			if T.Implements(reflect.TypeOf([0]documentable{}).Elem()) {
				schema.Description = name + " " + api.Tags(reflect.Zero(T).Interface().(documentable).Doc()).Doc()
			}
			if T.Implements(reflect.TypeOf([0]exemplar{}).Elem()) {
				var value = reflect.New(T.Elem()).Interface().(exemplar)
				value.Example()
				schema.Example = value
			}
			if reflect.PointerTo(T).Implements(reflect.TypeOf([0]exemplar{}).Elem()) {
				var value = reflect.New(T).Interface().(exemplar)
				value.Example()
				schema.Example = value
			}
		}

	}()

	//Handle enum types
	etype, ok := enum.TypeOf(T)
	if ok {
		if etype.Tags.Get("json").Value() == "string" {
			var values []interface{}

			var description strings.Builder
			for _, value := range etype.Values {
				var name = value.Name
				if tag := value.Tags.Get("text").Value(); tag != "" {
					name = strings.Split(tag, ",")[0]
					if tag == "-" {
						name = `""`
					}
				}
				if tag := value.Tags.Get("json").Value(); tag != "" {
					name = strings.Split(tag, ",")[0]
					if tag == "-" {
						name = `""`
					}
				}
				if name != "-" {
					values = append(values, name)
				}
				fmt.Fprintf(&description, "* %s: %s\n", name, value.Tags.Doc())
			}

			return &openapi3.Schema{
				Type:        "string",
				Description: description.String(),
				Enum:        values,
			}
		}
	}

	switch T {
	case typeOf[json.RawMessage]():
		// Return empty schema for RawMessage, essentially
		// this means we can accept any value.
		return openapi3.NewObjectSchema()
	case typeOf[time.Time]():
		return &openapi3.Schema{
			Type:   "string",
			Format: "date-time",
		}
	case typeOf[uuid.UUID]():
		return &openapi3.Schema{
			Type:   "string",
			Format: "uuid",
		}
	}

	switch T.Kind() {
	case reflect.String:
		return openapi3.NewStringSchema()
	case reflect.Int32:
		return openapi3.NewInt32Schema()
	case reflect.Int8, reflect.Int16, reflect.Int, reflect.Int64:
		return openapi3.NewInt64Schema()

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint64, reflect.Uint32:
		schema := openapi3.NewInt64Schema()
		min := 0.0
		schema.Min = &min
		return schema

	case reflect.Float32, reflect.Float64:
		return openapi3.NewFloat64Schema()

	case reflect.Bool:
		if flags&flagString != 0 {
			schema := openapi3.NewStringSchema()
			schema.Enum = []interface{}{"true", "false"}
			return schema
		}
		return openapi3.NewBoolSchema()

	case reflect.Slice, reflect.Array:
		var schema = openapi3.NewArraySchema()
		schema.Items = swag.schemaFor(T.Elem(), "", flags)

		if T.Kind() == reflect.Array {
			max := uint64(T.Len())
			schema.MinItems = uint64(T.Len())
			schema.MaxItems = &max
		}

		return schema

	case reflect.Struct:
		var schema = openapi3.NewObjectSchema()
		swag.addFieldsToSchema(schema, T)
		return schema

	case reflect.Ptr:
		return swag.newSchemaFor(T.Elem(), flags)

	case reflect.Interface:
		return openapi3.NewSchema()

	case reflect.Map:
		schema := openapi3.NewObjectSchema()
		schema.AdditionalProperties = openapi3.AdditionalProperties{
			Schema: swag.schemaFor(T.Elem(), "", flags),
		}
		return schema

	default:
		panic("rest.schemaFor: unsupported type: " + T.String())
	}
}

func (swag *swagger) responsesFor(spec rest.Specification, fn rest.Operation) (responses openapi3.Responses) {
	responses = openapi3.NewResponses()

	for code, response := range fn.Responses {
		desc := http.StatusText(code)
		responses[strconv.Itoa(code)] = &openapi3.ResponseRef{
			Value: &openapi3.Response{
				Description: &desc,
				Content: openapi3.Content{
					"application/json": &openapi3.MediaType{
						Schema: swag.schemaFor(response, "", 0),
					},
				},
			},
		}
	}

	return responses
}

func (swag *swagger) parametersFor(def rest.Specification, fn rest.Operation) (inpath, params openapi3.Parameters, inbody *openapi3.RequestBodyRef) {
	if fn.Body != nil {
		inbody = &openapi3.RequestBodyRef{
			Value: &openapi3.RequestBody{
				Content: openapi3.Content{
					"application/json": &openapi3.MediaType{
						Schema: swag.schemaFor(fn.Body, "", 0),
					},
				},
			},
		}
	}
	for _, param := range fn.Parameters {
		switch param.Location {
		case rest.ParameterInBody:
			if fn.Body != nil {
				continue
			}
			if param.Type.Implements(typeOf[io.Reader]()) {
				mime := `application/octet-stream`
				if val := fn.Tags.Get("mime").Value(); val != "" {
					mime = val
				}
				inbody = &openapi3.RequestBodyRef{
					Value: &openapi3.RequestBody{
						Content: openapi3.Content{
							mime: &openapi3.MediaType{},
						},
					},
				}
			} else {
				inbody = &openapi3.RequestBodyRef{
					Value: &openapi3.RequestBody{
						Content: openapi3.Content{
							"application/json": &openapi3.MediaType{
								Schema: swag.schemaFor(param.Type, "", 0),
							},
						},
					},
				}
			}
		case rest.ParameterInPath:
			inpath = append(inpath, &openapi3.ParameterRef{
				Value: &openapi3.Parameter{
					Name:     param.Name,
					In:       "path",
					Required: true,
					Schema:   swag.schemaFor(param.Type, "", 0),
				},
			})
		case rest.ParameterInQuery:
			params = append(params, &openapi3.ParameterRef{
				Value: &openapi3.Parameter{
					Name:     param.Name,
					In:       "query",
					Required: param.Type.Kind() != reflect.Ptr && !param.Tags.Get("json").Has("omitempty"),
					Schema:   swag.schemaFor(param.Type, "", 0),
				},
			})
		}
	}
	return
}
