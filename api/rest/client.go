package rest

import (
	"bytes"
	"context"
	"encoding"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"reflect"
	"strconv"
	"strings"

	"bitbucket.org/iqhive/apierror/v3"

	http_api "pkg.iqhive.com/iq/api/internal/http"
	"pkg.iqhive.com/iq/api/rest/internal/rtags"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"

	"pkg.iqhive.com/iq/api"
)

func Import[T api.WithSpecification](impl T, host string) T {
	Set(impl, host)
	return impl
}

// Set sets the host of the given rest api.
func Set(rest api.WithSpecification, host string) error {
	spec, err := SpecificationOf(rest)
	if err != nil {
		return oops.Wrap(err)
	}
	if err := imp(spec, host); err != nil {
		return oops.Wrap(err)
	}
	return nil
}

func (op Operation) encodeQuery(name string, query url.Values, rvalue reflect.Value) {
	if rvalue.IsValid() && !rvalue.IsZero() {
		if rvalue.Type().Implements(reflect.TypeOf([0]encoding.TextMarshaler{}).Elem()) {
			b, _ := rvalue.Interface().(encoding.TextMarshaler).MarshalText()
			query.Add(name, string(b))
		} else if rvalue.Type().Implements(reflect.TypeOf((*json.Marshaler)(nil)).Elem()) {
			b, _ := rvalue.Interface().(json.Marshaler).MarshalJSON()
			val := string(b)
			if val[0] == '"' {
				val, _ = strconv.Unquote(val)
			}
			query.Add(name, val)
		} else {
			if rvalue.Kind() == reflect.Slice {
				for i := 0; i < rvalue.Len(); i++ {
					op.encodeQuery(name+"[]", query, rvalue.Index(i))
				}
				return
			}
			query.Add(name, fmt.Sprintf("%v", rvalue.Interface()))
		}
	}
}

func (op Operation) clientWrite(path string, args []reflect.Value, body io.Writer, indent bool) (endpoint string) {

	encoder := json.NewEncoder(body)
	if indent {
		encoder.SetIndent("", "\t")
	}

	var mapping map[string]interface{}
	if op.argumentsNeedsMapping {
		mapping = make(map[string]interface{})
	}

	//deref is needed to prevent fmt from formatting the pointer as an address.
	deref := func(index []int) reflect.Value {

		value := args[index[0]]
		for value.Kind() == reflect.Ptr {
			value = value.Elem()
		}

		if len(index) > 1 {
			return FieldByIndex(value, index[1:])
		}

		return value
	}

	var query = make(url.Values)

	for _, param := range op.Parameters {
		if param.Location == ParameterInVoid {
			continue
		}

		if param.Location&ParameterInPath != 0 {
			path = strings.Replace(path, "{"+param.Name+"}", url.PathEscape(fmt.Sprintf("%v", deref(param.Index).Interface())), 1)
		}
		if param.Location&ParameterInQuery != 0 {
			op.encodeQuery(param.Name, query, deref(param.Index))
		}
		if param.Location == ParameterInBody {
			if op.argumentsNeedsMapping {
				mapping[param.Name] = deref(param.Index).Interface()
			} else {
				if err := encoder.Encode(deref(param.Index).Interface()); err != nil {
					panic(err)
				}
			}
		}
	}

	if op.argumentsNeedsMapping {
		if err := encoder.Encode(mapping); err != nil {
			panic(err)
		}

		if debug {
			b, _ := json.MarshalIndent(mapping, "", "\t")
			fmt.Println(string(b))
		}
	}

	if len(query) == 0 {
		return path
	}

	return path + "?" + query.Encode()
}

func toPtr(value *reflect.Value) interface{} {
	var ptr reflect.Value

	T := value.Type()

	if T.Kind() == reflect.Ptr {
		*value = reflect.New(T.Elem())
		ptr = *value
	} else {
		ptr = reflect.New(T)
		*value = ptr.Elem()
	}
	return ptr.Interface()
}

type copier struct {
	from io.Reader
}

func (c copier) WriteTo(w io.Writer) (n int64, err error) {
	return io.Copy(w, c.from)
}

// results argument need to be preallocated.
func (op Operation) clientRead(results []reflect.Value, response io.Reader, resultRules []string) (err error) {
	if len(results) == 0 {
		return nil
	}

	// we want to support IO types

	if op.Tags.Get("mime") != "" {
		switch v := toPtr(&results[0]).(type) {
		case *io.WriterTo:
			*v = copier{response}
		case *io.ReadCloser:
			*v = io.NopCloser(response)
		case *io.Reader:
			*v = response
		case *[]byte:
			*v, err = ioutil.ReadAll(response)
			if err != nil {
				return err
			}
		default:
			return oops.Fixme("%v: 'mime' tag is not compatible with result value of type %T",
				strings.Join(append(op.Path, op.Name), "."),
				v,
			)
		}
		return nil
	}

	responseNeedsMapping := len(resultRules) > 0

	decoder := json.NewDecoder(response)

	//If there are custom response mapping rules,
	//then we decode into a map here.
	var mapping map[string]json.RawMessage
	if responseNeedsMapping {
		mapping = make(map[string]json.RawMessage)

		if err := decoder.Decode(&mapping); err != nil {
			return err
		}

		if debug {
			for key := range mapping {
				fmt.Println(key)
			}
		}

		for i, rule := range resultRules {

			if debug {
				fmt.Println("copying", rule, "into", i, results[i].Type())
			}

			//Write into a return value (as usual)
			if err := json.Unmarshal(mapping[rule], toPtr(&results[i])); err != nil {
				return err
			}
		}
	} else {

		for i := range results {
			if err := decoder.Decode(toPtr(&results[i])); err != nil {
				return err
			}
		}
	}

	return nil
}

func imp(spec Specification, host string) error {

	for path, resource := range spec.Resources {
		for method, operation := range resource.Operations {

			op := operation
			fn := op.Function

			if debug {
				fmt.Println(path, fn.Name)
			}

			//Determine the HTTP method of this request.
			method := string(method)

			path := rtags.CleanupPattern(path)

			var resultRules = rtags.ResultRulesOf(string(fn.Tags.Get("rest")))

			//Create an implementation of the function that calls the REST
			//endpoint over the network and returns the results.
			fn.FuncValue.Set(func(ctx context.Context, keys []api.Key, args []reflect.Value) (results []reflect.Value, err error) {
				if host == "" {
					return nil, oops.Fixme("failed to call %v, %s host URL is empty", path, spec.Name)
				}

				results = make([]reflect.Value, fn.Type.NumOut())

				//body buffers what we will be sending to the endpoint.
				var body interface {
					io.ReadWriter
					fmt.Stringer
				} = new(bytes.Buffer)

				//Figure out the REST endpoint to send a request to.
				//args are interpolated into the path and query as
				//defined in the "rest" tag for this function.
				endpoint := op.clientWrite(path, args, body, false)
				endpoint = http_api.AttachQueryKeys("rest", keys, endpoint)

				//Debug the url.
				if debug {
					fmt.Println(method, host+endpoint)
					fmt.Println("body:\n", body.String())
				}

				// These methods should not have a body.
				switch method {
				case "GET", "HEAD", "DELETE", "OPTIONS", "TRACE":
					body = nil
				}

				var debugBody string
				if xray.Scanning(ctx) && body != nil {
					debugBody = body.String()
				}

				req, err := http.NewRequestWithContext(ctx, method, host+endpoint, body)
				if err != nil {
					return nil, oops.Wrap(err)

				}

				if err := http_api.AttachKeys("rest", keys, req); err != nil {
					return nil, oops.Fixme("make sure that rest/http tags have been configured for %v's authentication: %v", fn.Name, err)
				}

				//We are expecting JSON.
				req.Header.Set("Accept", "application/json")
				req.Header.Set("Content-Type", "application/json")

				if debug {
					fmt.Println("headers:\n", req.Header)
				}

				resp, err := http.DefaultClient.Do(req)
				if err != nil {
					return nil, oops.Wrap(err)

				}
				respBytes, err := io.ReadAll(resp.Body)
				if err != nil {
					return nil, oops.Wrap(err)
				}
				if err := resp.Body.Close(); err != nil {
					return nil, oops.Wrap(err)
				}

				if xray.Scanning(ctx) {
					xray.Scan(ctx, method+" "+host+endpoint, debugBody, string(respBytes))
				}

				//Debug the reponse.
				if debug {
					fmt.Println("response:")
					b, _ := httputil.DumpResponse(resp, true)
					fmt.Println(string(b))
				}

				if resp.StatusCode < 200 || resp.StatusCode > 299 {
					return nil, decodeError(req, resp, respBytes, spec, fn, err)
				}

				//Zero out the results.
				for i := 0; i < fn.Type.NumOut(); i++ {
					results[i] = reflect.Zero(fn.Type.Out(i))
				}

				if err := op.clientRead(results, bytes.NewReader(respBytes), resultRules); err != nil {
					return nil, err
				}

				// Custom Headers support.
				// TODO cache whether or not we need to do this loop?
				for i := range results {
					if results[i].Type().Implements(reflect.TypeOf((*http_api.HeaderReader)(nil)).Elem()) {
						if writer, ok := toPtr(&results[i]).(http_api.HeaderReader); ok {
							writer.ReadHeadersHTTP(resp.Header)
						}
					}
				}

				return results, nil
			})
		}
	}

	return nil
}

func decodeError(req *http.Request, resp *http.Response, body []byte, spec Specification, fn api.Function, err error) error {
	if bytes.Contains(body, []byte(`"err_code":`)) && bytes.Contains(body, []byte(`"err_message":`)) {
		ae, err := apierror.NewAPIErrorFromJSONBytes(body)
		if err == nil {
			return ae
		}
	}
	var wrap func(error) error = oops.New // we choose which api error to wrap with.
	if len(fn.Errors) > 0 {
		var generic map[string]any
	ErrorTypeMatching:
		for _, errType := range fn.Errors {
			rule := errType.Tags.Get("json")
			if rule != "" {
				if generic == nil {
					if err := json.Unmarshal(body, &generic); err != nil {
						continue
					}
				}
				var equals, contains bool
				path, value, equals := strings.Cut(string(rule), "=")
				if !equals {
					path, value, contains = strings.Cut(string(rule), "?")
				}
				var target any = generic
				for _, part := range strings.Split(path, ".") {
					object, ok := target.(map[string]any)
					if !ok {
						continue ErrorTypeMatching
					}
					target = object[part]
					if target == nil {
						continue ErrorTypeMatching
					}
				}
				if target != nil {
					switch true {
					case contains && strings.Contains(fmt.Sprintf("%v", target), value):
						fallthrough
					case equals && fmt.Sprintf("%v", target) == value:
						wrap = errType.New
					default:
						continue ErrorTypeMatching
					}
					break
				}
			}
			if bytes.Contains(body, []byte(errType.Tags.Doc())) {
				wrap = errType.New
				break
			}
		}
	}
	//If the API has an error structure configured for it, then we can populate it.
	if spec.ErrorSpec.Type != nil && resp.StatusCode != 401 && strings.HasPrefix(resp.Header.Get("Content-Type"), "application/json") {
		var val reflect.Value
		switch spec.ErrorSpec.Type.Kind() {
		case reflect.Ptr:
			val = reflect.New(spec.ErrorSpec.Type.Elem())
		default:
			val = reflect.New(spec.ErrorSpec.Type)
		}
		if err := json.NewDecoder(bytes.NewReader(body)).Decode(val.Interface()); err != nil {
			if err := http_api.ResponseError(resp, body); err != nil {
				return wrap(err)
			}
		} else {
			switch spec.ErrorSpec.Type.Kind() {
			case reflect.Ptr:
				i := val.Interface()
				if s, ok := i.(interface {
					SetStatusHTTP(int)
				}); ok {
					s.SetStatusHTTP(resp.StatusCode)
				}
				return wrap(i.(error))
			default:
				i := val.Elem().Interface()
				if s, ok := i.(interface {
					SetStatusHTTP(int)
				}); ok {
					s.SetStatusHTTP(resp.StatusCode)
				}
				return wrap(i.(error))
			}
		}
	} else {
		if resp.StatusCode == 404 {
			if req.Method == "DELETE" {
				return nil
			}
			return oops.NotFound()
		}
		if err := http_api.ResponseError(resp, body); err != nil {
			return wrap(err)
		}
	}
	return wrap(errors.New("unexpected status : " + resp.Status + " " + string(body)))
}
