package rest_test

import (
	"context"
	"crypto/subtle"
	"errors"
	"fmt"
	"net/http/httptest"
	"testing"
	"time"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/rest"
)

//FIXME this file is long and messy, all the tests are clumped together
//it would be wise to break them up into smaller focused tests.
//such as TestKeyAuthentication, TestEcho etc.

var API Specification

// Password is an example of an api.Authenticator.
// It authenticates based on a password provided
// inside of the REST request header.
type Password struct {
	context.Context

	Val   string `api:"key" rest:",header"`
	Other string `api:"key,omitempty" rest:",header"`
}

func (p *Password) Authenticate() error {
	if subtle.ConstantTimeCompare([]byte(p.Val), []byte("password")) == 1 {
		return nil
	}
	return errors.New("acccess denied")
}

// NestedPassword asserts that Authenticators can be nested and continue to function.
type NestedPassword struct {
	Password
}

// Query is an example of a query that
// is described by a struct. The fields
// will be inserted into the query of
// the REST request
type Query struct {
	String string `rest:",query,omitempty"`
	Count  int    `rest:"count,omitempty"`
}

// Object is a dummy object type.
type Object struct {
	ID, Value string
}

type DoSomething func(Object) error
type doSomethingREST func(string, Object) error

func (fn DoSomething) Transform_rest() doSomethingREST {
	return func(id string, obj Object) error {
		if id != "" {
			obj.ID = id
		}
		return fn(obj)
	}
}

func (fn doSomethingREST) TransformBack() DoSomething {
	return func(o Object) error {
		return fn(o.ID, o)
	}
}

type LowerByReference func(s *string) error
type lowerByReferenceREST func(string) (string, error)

func (fn LowerByReference) Transform_rest() lowerByReferenceREST {
	return func(s string) (string, error) {
		fn(&s)
		return s, nil
	}
}

func (fn lowerByReferenceREST) TransformBack() LowerByReference {
	return func(s *string) error {
		result, err := fn(*s)
		*s = result
		return err
	}
}

type Specification struct {
	api.Specification

	Basic struct {
		//Basic call.
		Print func() `rest:"GET /print"
			Print prints "Hello World" to the console.
		`

		//Path parameters.
		Echo func(string) string `rest:"GET /echo/{message=%v}"
			Echo returns the input message unchanged.
		`

		//Query parameters.
		Concat func(a, b string) string `rest:"GET /concat?a=%v&b=%v"
			Concat adds two strings together and returns the result.
		`

		//Returning an error.
		Error func() error `rest:"GET /error"
			Error always returns an error. 
		`
	}

	Auth struct {
		//Using an api.Authenticator for authentication.
		Secret func(Password) (string, error) `rest:"GET /secret"
			Secret returns a string when the provided credentials
			are correct.
		`

		NestedSecret func(NestedPassword) (string, error) `rest:"GET /nested/secret"
			Secret returns a string when the provided credentials
			are correct.
		`

		//Using an api.Key for authentication.
		WithKey struct {
			Token string `api:"key" rest:",query"`

			Print func() error `rest:"GET /key/print"
				Prints "Hello Keyholder" to the console.
			`
		}

		//Using an api.Key for authentication.
		WithToken struct {
			Token string `api:"key" rest:"x-auth-token,header"`

			Print func() error `rest:"GET /token/print"
				Prints "Hello Keyholder" to the console.
			`
		}

		//Using an implicit Authenticator for authentication.
		WithAuthenticator struct {
			Password Password

			Print func() error `rest:"GET /auth1/print"
				Prints "Hello Keyholder" to the console.
			`
		}

		//using api.Authentication
		WithAuthentication struct {
			api.Authentication

			Print func() error `rest:"GET /auth2/print"
				Prints "Hello Keyholder" to the console.
			`
		}
	}

	Context struct {
		Old func(ctx context.Context) error `rest:"GET /ctx/old"`
	}

	Advanced struct {
		//Struct parameters.
		Insert func(Object) `rest:"POST /insert/{ID}"
			Insert a Object.`

		//Query values can be specified by a struct when ommitting a
		//format parameter in the query and just specifying either
		// '?%v' or '&%v', the struct fields are then serialised as
		//query parameters.
		Query func(q Query) (string, int, error) `rest:"GET /query?String&Count String,Count"`

		Duplicate func(param string) error `rest:"POST /duplicate/{param=%[1]v}?param=%[1]v"`

		ReverseOrder func(a, b string) (string, string) `rest:"POST /reverse/{b=%[2]v}/{a=%[1]v} a,b"`
	}
}

// Create a bunch of basic implementations for the API.
func (i *Specification) Implement() {
	i.Basic.Print = func() { fmt.Println("Hello World") }
	i.Basic.Echo = func(message string) string { return message }
	i.Basic.Concat = func(a, b string) string { return a + b }
	i.Basic.Error = func() error { return errors.New("An error occured") }

	i.Auth.Secret = func(p Password) (string, error) { return "You guessed the password!", nil }
	i.Auth.NestedSecret = func(np NestedPassword) (string, error) { return "You guessed the password!", nil }
	i.Auth.WithKey.Print = func() error { fmt.Println("Hello Keyholder"); return nil }
	i.Auth.WithToken.Print = i.Auth.WithKey.Print
	i.Auth.WithAuthenticator.Print = i.Auth.WithKey.Print
	i.Auth.WithAuthentication.Print = i.Auth.WithKey.Print

	i.Advanced.Insert = func(o Object) { fmt.Println("Inserted", o.ID, o.Value) }
	i.Advanced.Query = func(q Query) (string, int, error) { return q.String, q.Count, nil }
	i.Advanced.Duplicate = func(param string) error { return nil }
	i.Advanced.ReverseOrder = func(a, b string) (string, string) { return b, a }

	i.Context.Old = func(ctx context.Context) error { return ctx.Err() }
}

// Test that all of the basic implementations return the expected results.
func (i *Specification) Test(t *testing.T) {
	//Basic
	i.Basic.Print()

	if i.Basic.Echo("HelloServer") != "HelloServer" {
		t.Error("Echo failed")
	}
	if i.Basic.Concat("This string", " is concatenated") != "This string is concatenated" {
		t.Error("Concat failed")
	}
	if i.Basic.Error() == nil {
		t.Error("Error failed")
	}
	//Auth
	if _, err := i.Auth.Secret(Password{Context: context.Background(), Val: "1234"}); err == nil {
		t.Error("Secret failed")
	}
	if _, err := i.Auth.Secret(Password{Context: context.Background(), Val: "password"}); err != nil {
		t.Error("Secret failed")
	}
	//Nested Auth
	if _, err := i.Auth.NestedSecret(NestedPassword{Password{Context: context.Background(), Val: "1234"}}); err == nil {
		t.Error("Secret failed")
	}
	if _, err := i.Auth.NestedSecret(NestedPassword{Password{Context: context.Background(), Val: "password"}}); err != nil {
		t.Error("Secret failed")
	}
	//Key
	if err := i.Auth.WithKey.Print(); err != nil {
		t.Error("Key failed")
	}
	i.Auth.WithKey.Token = "" //clear the token in order to check that auth will then fail.
	if err := i.Auth.WithKey.Print(); err == nil {
		t.Error("Key failed")
	}
	//Token
	if err := i.Auth.WithToken.Print(); err != nil {
		t.Error("Token failed")
	}
	i.Auth.WithToken.Token = "" //clear the token in order to check that auth will then fail.
	if err := i.Auth.WithToken.Print(); err == nil {
		t.Error("Token failed")
	}
	//Authenticator
	if err := i.Auth.WithAuthenticator.Print(); err != nil {
		t.Error("Authenticator failed")
	}
	i.Auth.WithAuthenticator.Password.Val = "" //clear the token in order to check that auth will then fail.
	if err := i.Auth.WithAuthenticator.Print(); err == nil {
		t.Error("Authenticator failed")
	}
	//Authentication
	if err := i.Auth.WithAuthentication.Print(); err != nil {
		t.Error("Authentication failed")
	}
	DisableAuthentication = true
	if err := i.Auth.WithAuthentication.Print(); err == nil {
		t.Error("Authentication failed")
	}
	//Test empty key.
	i.Auth.WithKey.Token = ""
	API.Auth.WithKey.Token = ""
	if err := i.Auth.WithKey.Print(); err == nil {
		t.Error("Key failed")
	}
	//Context
	if err := i.Context.Old(context.Background()); err != nil {
		t.Error("Context failed")
	}
	i.Advanced.Insert(Object{ID: "22", Value: "Object Value"})
	if err := i.Advanced.Duplicate("hello"); err != nil {
		t.Error("Duplicate failed")
	}
	a, b := i.Advanced.ReverseOrder("a", "b")
	if a != "b" || b != "a" {
		t.Error("ReverseOrder failed")
	}
	//var q = Query{"Hello", 22}
	//should.Equal(q.String, q.Count)(i.Advanced.Query(q))
}

var DisableAuthentication = false

func TestMain(t *testing.T) {
	//Display the swagger?
	/*doc, err := json.MarshalIndent(rest.SwaggerFor(&API), "", "\t")
	if err != nil {
		log.Println(err)
		return
	}
	fmt.Println(string(doc))*/

	//Setup the authentication keys.
	API.Auth.WithToken.Token = "1234"
	API.Auth.WithAuthenticator.Password.Val = "password"
	API.Auth.WithKey.Token = "4321"
	API.Auth.WithAuthentication.Authentication = func(ctx context.Context, _ api.Tags) (api.Authenticator, error) {
		if DisableAuthentication {
			return &Password{
				Context: ctx,
				Val:     "wrong",
			}, nil
		}
		return &Password{
			Context: ctx,
			Val:     "password",
		}, nil
	}

	if string(API.Auth.WithKey.Token) != "4321" {
		t.Fatal("Key not set")
	}

	API.Implement()
	//Host the API on http://localhost:$PORT
	go rest.ListenAndServe(":5679", &API)
	time.Sleep(10 * time.Millisecond)

	var local Specification

	local.Auth.WithToken.Token = "1234"
	local.Auth.WithAuthenticator.Password.Val = "password"
	local.Auth.WithKey.Token = "4321"
	local.Auth.WithAuthentication.Authentication = func(ctx context.Context, _ api.Tags) (api.Authenticator, error) {
		if ctx == nil {
			return &Password{}, nil
		}

		if DisableAuthentication {
			return &Password{
				Val: "wrong",
			}, nil
		}

		return &Password{
			Val: "password",
		}, nil
	}

	//Nil out the functions so that they can't be
	//called locally. Prevents the Importer from
	//cheating the test by calling the local implementations.
	local.Basic.Print = nil
	local.Basic.Echo = nil
	local.Basic.Concat = nil
	local.Basic.Error = nil
	local.Auth.Secret = nil
	local.Auth.WithKey.Print = nil
	local.Auth.WithToken.Print = nil

	//Set the API host to the same value that api.Export is listening on.
	if err := rest.Set(&local, "http://localhost:5679"); err != nil {
		t.Fatal(err)
	}

	local.Test(t)

}

func TestDuplicatePaths(t *testing.T) {
	var Spec struct {
		api.Specification

		A func() `rest:"GET /same-path"`
		B func() `rest:"GET /same-path"`
	}

	if _, err := rest.Handler(&Spec); err == nil {
		t.Fatal("expected an error")
	}

	var FixedSpec struct {
		api.Specification

		A func() `rest:"GET /same-path"`
		B func() `rest:"GET /different-path"`
	}

	if _, err := rest.Handler(&FixedSpec); err != nil {
		t.Fatal("expected an error")
	}
}

func TestDestructuring(t *testing.T) {
	type Sub struct {
		Z string
	}

	type Query struct {
		A string
		B string

		Sub Sub
	}

	var Test struct {
		api.Specification

		Destructure1 func(Query)      `rest:"GET /destructure1?%v"`
		Destructure2 func(Query, Sub) `rest:"GET /destructure2?%[1]v"`
	}
	Test.Destructure1 = func(q Query) {
		if q.A != "Hello" {
			t.Fatal("expected A to be Hello")
		}
		if q.B != "World" {
			t.Fatal("expected B to be World")
		}
		if q.Sub.Z != "Wow" {
			t.Fatal("expected Sub.Z to be Wow")
		}
	}
	Test.Destructure2 = func(q Query, s Sub) {
		if q.Sub.Z != "Wow" {
			t.Fatal("expected Sub.Z to be Wow")
		}
	}

	handler, err := rest.Handler(&Test)
	if err != nil {
		t.Fatal(err)
	}

	handler.ServeHTTP(httptest.NewRecorder(), httptest.NewRequest("GET", "/destructure1?A=Hello&B=World&Sub.Z=Wow", nil))
	handler.ServeHTTP(httptest.NewRecorder(), httptest.NewRequest("GET", "/destructure2?Z=Wow", nil))
}
