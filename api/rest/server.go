package rest

import (
	"context"
	"encoding"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/gorilla/mux"

	"pkg.iqhive.com/iq/api"
	http_api "pkg.iqhive.com/iq/api/internal/http"
	"pkg.iqhive.com/iq/api/rest/internal/rtags"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"

	"pkg.iqhive.com/iq/please"
)

// FIXME replace with dependency injection.
var ReportError = func(ctx context.Context, err error) {
	log.Print(err)
}

// ListenAndServe listens on the TCP network address addr and serves the rest
// specification.
//
// Errors returned by API endpoints will be masked as "internal server error"
// unless they implement the `StatusHTTP() int` method. In which case, the
// status will be set from the result of this method call and the error text
// will be returned.
func ListenAndServe(addr string, implementations ...api.WithSpecification) error {

	handler, err := Handler(implementations...)
	if err != nil {
		return oops.Wrap(err)
	}

	return http.ListenAndServe(addr, handler)
}

// Handler for more fine grained control.
func Handler(implementations ...api.WithSpecification) (http.Handler, error) {
	return HandlerWithFallback(nil, implementations...)
}

// HandlerWithFallback will serve the fallback handler if no matching endpoints
// are found.
func HandlerWithFallback(fallback http.Handler, implementations ...api.WithSpecification) (http.Handler, error) {
	var router = mux.NewRouter()

	for _, rest := range implementations {
		spec, err := SpecificationOf(rest)
		if err != nil {
			return nil, oops.Wrap(err)
		}

		if len(spec.duplicates) > 0 {
			return nil, errors.Join(spec.duplicates...)
		}

		if err := attach(router, spec); err != nil {
			return nil, oops.Wrap(err)
		}
	}

	router.NotFoundHandler = fallback

	return router, nil
}

func FieldByIndex(value reflect.Value, index []int) reflect.Value {
	if len(index) == 1 {
		return value.Field(index[0])
	}
	for i, x := range index {
		if i > 0 {
			if value.Kind() == reflect.Pointer && value.Type().Elem().Kind() == reflect.Struct {
				if value.IsNil() {
					value.Set(reflect.New(value.Type().Elem()))
				}
				value = value.Elem()
			}
		}
		value = value.Field(x)
	}
	return value
}

func attach(router *mux.Router, spec Specification) error {

	for path, resource := range spec.Resources {
		for method, operation := range resource.Operations {

			op := operation
			fn := op.Function

			path := rtags.CleanupPattern(path)

			var mimetype = string(fn.Tags.Get("mime"))

			var resultRules = rtags.ResultRulesOf(string(fn.Tags.Get("rest")))

			var responseNeedsMapping = len(resultRules) > 0
			var argumentsNeedsMapping = len(rtags.ArgumentRulesOf(string(fn.Tags.Get("rest")))) > 0

			if method == "GET" {
				router.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}).Methods("OPTIONS")
			}

			router.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
				vars := mux.Vars(r)
				ctx := r.Context()

				if rid := r.Header.Get("X-Request-ID"); rid != "" {
					ctx = xray.Set(ctx, r.Header.Get("X-Request-ID"))
				}

				handle := func(rw http.ResponseWriter, err error) {
					var status int = http.StatusInternalServerError

					if spec.ErrorSpec.Func != nil {
						err = spec.ErrorSpec.Func(ctx, err)

						switch v := err.(type) {
						case http_api.Error:
							status = v.StatusHTTP()
						}
						if status == 0 {
							status = http.StatusInternalServerError
						}

						if reflect.TypeOf(err) == spec.ErrorSpec.Type {
							w.Header().Set("Content-Type", "application/json")
							w.WriteHeader(status)
							err := json.NewEncoder(w).Encode(err)
							if err == nil {
								return
							}
							ReportError(ctx, err)
						}
					}

					var message = err.Error()

					switch v := err.(type) {
					case http_api.Error:
						status = v.StatusHTTP()
						if status == 0 {
							status = http.StatusInternalServerError
						}
					case oops.ErrorWithAgent:
						up, in, dn := v.Agent()
						if up != nil && in == nil && dn == nil {
							status = http.StatusBadRequest
						} else {
							message = "internal server error"
						}
					default:
						if errors.Is(err, oops.NotImplemented()) {
							status = http.StatusNotImplemented
							message = "not implemented"
						}
					}
					ReportError(r.Context(), err)
					http.Error(rw, message, status)
				}

				var closeBody bool = true
				defer func() {
					if closeBody {
						r.Body.Close()
					}
				}()

				//TODO decode body.
				results, err := fn.FuncValue.Call(ctx, func(keys []api.Key) error {

					//check keys
					for _, key := range keys {
						name := key.Name

						if replace := key.Tags.Get("rest").Value(); replace != "" {
							name = replace
						}

						tags := key.Tags.Get("rest")

						if debug {
							fmt.Println("KeyCheck Server", name, r.Header.Get(name), key)
						}

						var value string

						if value == "" && tags.Has("bearer") {
							if auth := r.Header.Get("Authorization"); strings.HasPrefix(auth, "Bearer ") {
								value = strings.TrimPrefix(auth, "Bearer ")
							}
						}

						if value == "" && tags.Has("basic") {
							username, password, ok := r.BasicAuth()
							if !ok {
								w.Header().Set("WWW-Authenticate", `Basic realm="restricted", charset="UTF-8"`)
							}
							if ok {
								value = username + ":" + password
							}
						}
						if value == "" && tags.Has("header") {
							value = r.Header.Get(name)
						}
						if value == "" && tags.Has("cookie") {
							cookie, err := r.Cookie(name)
							if err == nil {
								value = cookie.Value
							}
						}
						if value == "" && tags.Has("query") {
							value = r.URL.Query().Get(name)
						}

						switch {
						case tags.Has("basic"), tags.Has("header"), tags.Has("bearer"), tags.Has("cookie"), tags.Has("query"):
						default:
							return oops.Fixme("make sure %v has a rest tag configured for authentication", name)
						}

						*key.Value = value
					}

					return nil

				}, func(args []reflect.Value) error {

					var mapping map[string]json.RawMessage
					if argumentsNeedsMapping {
						mapping = make(map[string]json.RawMessage)

						if err := json.NewDecoder(r.Body).Decode(&mapping); err != nil {
							return please.Provide("a valid request body", err)
						}

						for i, param := range op.Parameters {
							if param.Location == ParameterInBody {
								raw := mapping[param.Name]
								if raw == nil {
									continue
								}
								if err := json.Unmarshal(raw, toPtr(&args[i])); err != nil {
									return please.Provide("a valid %v", err, param.Name)
								}
							}
						}
					}

					//Scan in the path/query arguments.
					for _, param := range op.Parameters {
						if param.Location == ParameterInVoid {
							continue
						}

						i := param.Index[0]

						var ref, deref reflect.Value

						if argumentIsDirect := len(param.Index) == 1; argumentIsDirect {
							ref = args[i]

							if fn.Type.In(i).Kind() != reflect.Ptr {
								ref = args[i].Addr()
								deref = args[i]
							} else {
								deref = args[i].Elem()
							}
						} else {
							//nested
							if fn.Type.In(i).Kind() == reflect.Ptr {
								deref = FieldByIndex(args[i].Elem(), param.Index[1:])
							} else {
								deref = FieldByIndex(args[i], param.Index[1:])
							}
							ref = deref.Addr()
						}

						if param.Location == ParameterInBody && !argumentsNeedsMapping {
							switch dst := ref.Interface().(type) {
							case *io.Reader:
								*dst = r.Body
							case *io.ReadCloser:
								*dst = r.Body
								closeBody = false
							default:
								if err := json.NewDecoder(r.Body).Decode(dst); err != nil {
									return please.Provide("a %v encoded %v", err, "json", args[i].Type().String())
								}
							}
						}

						var items = 1
						if deref.Kind() == reflect.Slice {
							if param.Location&ParameterInQuery != 0 {
								items = len(r.URL.Query()[param.Name+"[]"])
							}
							deref.Set(reflect.MakeSlice(deref.Type(), items, items))
						}
						var idx int
						for val := ""; idx < items; idx++ {
							deref := deref
							ref := ref
							if items > 1 {
								ref = deref.Index(idx).Addr()
								deref = deref.Index(idx)
							}
							if param.Location&ParameterInPath != 0 {
								val = vars[param.Name]
							}
							if param.Location&ParameterInQuery != 0 {
								if items > 1 {
									vals := r.URL.Query()[param.Name+"[]"]
									if idx < len(vals) {
										val = vals[idx]
									}
								} else {
									if v := r.URL.Query().Get(param.Name); v != "" {
										val = v
									}
								}
							}

							if !(param.Location == ParameterInBody) {
								if val == "" {
								} else {
									var success bool
									var failure []error
									if deref.Kind() == reflect.String {
										deref.SetString(val)
										success = true
									} else if text, ok := ref.Interface().(encoding.TextUnmarshaler); ok {
										if err := text.UnmarshalText([]byte(val)); err != nil {
											failure = append(failure, err)
										} else {
											success = true
										}
									} else if decoder, ok := ref.Interface().(json.Unmarshaler); ok {
										if _, err := strconv.ParseFloat(val, 64); err == nil || val == "true" || val == "false" {
											if err := decoder.UnmarshalJSON([]byte(val)); err == nil {
												success = true
												goto decoded
											} else {
												failure = append(failure, err)
											}
										}
										if err := decoder.UnmarshalJSON([]byte(strconv.Quote(val))); err != nil {
											failure = append(failure, err)
										} else {
											success = true
										}
									} else {
										_, err := fmt.Sscanf(val, "%v", ref.Interface())
										if err != nil && err != io.EOF {
											failure = append(failure, err)
										} else {
											success = true
										}
									}
									if !success {
										return please.Provide("a valid %v", errors.Join(failure...), ref.Type().String())
									}
								}
							}

						decoded:
							if ref.IsValid() && ref.CanAddr() {
								if reader, ok := ref.Interface().(http_api.HeaderReader); ok {
									reader.ReadHeadersHTTP(r.Header)
								}
							}
						}
					}

					return nil
				})

				if err != nil {
					handle(w, err)
					return
				}

				var code int = http.StatusOK
				// Custom HTTP Headers Support
				// TODO cache whether or not we need to do this loop?
				header := w.Header()
				for _, val := range results {
					if writer, ok := val.Interface().(http_api.HeaderWriter); ok {
						writer.WriteHeadersHTTP(header)
					}
					if status, ok := val.Interface().(interface{ StatusHTTP() int }); ok {
						code = status.StatusHTTP()
					}
				}
				if code != http.StatusOK {
					w.WriteHeader(code)
				}

				var mapping map[string]interface{}
				if responseNeedsMapping {
					mapping = make(map[string]interface{})

					for i, rule := range resultRules {
						mapping[rule] = results[i].Interface()
					}

					b, err := json.Marshal(mapping)
					if err != nil {
						handle(w, oops.Wrap(err))
					}

					w.Header().Set("Content-Type", "application/json")
					w.Header().Set("Content-Length", strconv.Itoa(len(b)))
					w.Write(b)

					return
				}

				// Endpoints can define a mime tag to overide the default JSON marshaling behaviour.
				// This is useful for serving files.
				if mimetype != "" {
					if len(results) != 1 {
						handle(w, oops.Fixme("%v: the 'mime' tag is not supported for multiple return values",
							strings.Join(append(fn.Path, fn.Name), ".")))
						return
					}

					w.Header().Set("Content-Type", mimetype)

					switch v := results[0].Interface().(type) {
					case io.WriterTo:
						if _, err := v.WriteTo(w); err != nil {
							handle(w, oops.Wrap(err))
						}
						return
					case io.ReadCloser:
						if _, err := io.Copy(w, v); err != nil {
							handle(w, oops.New(err))
						}
						v.Close()
						return
					case *io.LimitedReader:
						w.Header().Set("Content-Length", strconv.Itoa(int(v.N)))
						if _, err := io.Copy(w, v); err != nil {
							handle(w, oops.New(err))
						}
						return
					case io.Reader:
						if _, err := io.Copy(w, v); err != nil {
							handle(w, oops.New(err))
						}
						return
					case []byte:
						w.Header().Set("Content-Length", strconv.Itoa(len(v)))
						if _, err := w.Write(v); err != nil {
							handle(w, oops.New(err))
						}
						return
					}
				}

				if len(results) == 1 {

					//It may be useful to be able to override the default json
					//marshalling behaviour of this package.
					if marshaler, ok := results[0].Interface().(marshaler); ok {
						b, err := marshaler.MarshalREST()
						if err != nil {
							handle(w, oops.Wrap(err))
						}

						if _, err := w.Write(b); err != nil {
							handle(w, oops.Wrap(err))
						}
						return
					}

					b, err := json.Marshal(results[0].Interface())
					if err != nil {
						handle(w, oops.Wrap(err))
					}

					w.Header().Set("Content-Type", "application/json")
					w.Header().Set("Content-Length", strconv.Itoa(len(b)))
					w.Write(b)

					return
				}

				var converted = make([]interface{}, 0, len(results))

				for _, v := range results {
					converted = append(converted, v.Interface())
				}

				b, err := json.Marshal(converted)
				if err != nil {
					handle(w, oops.Wrap(err))
				}

				w.Header().Set("Content-Type", "application/json")
				w.Header().Set("Content-Length", strconv.Itoa(len(b)))
				w.Write(b)
			}).Methods(string(method))
		}
	}

	return nil
}
