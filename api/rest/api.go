/*
Package rest provides a REST api driver.

	var API struct {
		api.Specification

		Echo func(message string) string `rest:"GET /echo?message=%v"`
	}

This REST API can be imported into the program with 'Set'.

	rest.Set(&API, "https://example.com"),
	API.Echo("Hello World")

When Echo is called, it calls the function over HTTPS/REST to
'https://example.com/echo' and returns the result (if there
is an error, the function will panic but only because an
error hasn't been defined as a possible result).

Think of this package as a Linker over the network. You define
headers in Go and then Link this API into your program so you
can call the functions exposed by the API.

You can also host an API defined this way, instead of importing it:

	API.Echo = func(message string) string { return message }
	rest.ListenAndServe(":"+os.Getenv("PORT"), &API)

This starts a local HTTP/REST server and listens on PORT
for requests to /echo and responds to these requests with the
defined Echo function. Arguments and Results are automatically
converted by the REST package.

# Tags

Each API function can have a rest tag that formats
function arguments (%v) to query parameters.
Each tag must follow the space-seperated pattern:

	GET /path/to/endpoint/{object=%v}?query=%v (argument,mapping,rules) result,mapping,rules
	[METHOD] [PATH] (ARGUMENT_RULES) RESULT_RULES

It begins with a METHOD, then with a PATH format string
that descibes how the function arguments are mapped onto
the HTTP path & query. This follows standard fmt rules.

The path can contain path expansion parameters {name=%v} or
ordinary format parameters %v (similar to the fmt package).
Think of the arguments of the function as the parameters that
get passed to a printf call. Imagine it working like this:

	http.Get(fmt.Sprintf("/path/with/%v?query=%v", value, query))

If a path or query expansion parameter omits a format parameter,
the value will be considered to nested within a struct argument
and the name of the parameter will be used to look for the first
matching field in subsequent body structures. Either by field
name or by rest tag.

	POST /path/to/endpoint/{ID}
	{
		ID: "1234",
		Value: "something"
	}

ARGUMENT_RULES are optional, they are a comma separated list
of names to give the remaining arguments in the JSON body
of the request. By default, arguments are posted as an
array, however if there are ARGUMENT_RULES, the arguments
will be mapped into json fields of the name, matching the
argument's position.

	foo func(id int, value string) `rest:"POST /foo (id,value)"`
	foo(22, "Hello World") => {"id": 22, "value":"Hello World"}

RESULT_RULES are much like ARGUMENT_RULES, except they operate
on the results of the function instead of the arguments. They
map named json fields to the result values.

	getLatLong func() (float64, float64) `rest:"GET /latlong latitude,longitude"`
	{"latitude": 12.2, "longitude": 15.0} => lat, lon := getLatLong()

# Authentication

Authenticators can contain Keys that describe where and how to load each
key from the request. For example:

	MyKey string `api:"key" rest: "X-API-Key,header"`

Keys can have their own tag. Use the rest tag to adjust the
name this key is identified as via HTTP and you add flags to indicate
where it is found within a request. Supported tags are:

	omitempty 	 // key value is optional, is not required for authentication.
	random(uuid) // key value is expected to be universally unique and randomly generated.

	// in order of precedence (first match wins)

	basic // key value is taken from HTTP Basic Authentication (in format username:password), name is ignored.
	header // key value is taken from HTTP header with given name.
	cookie // key value is taken from HTTP cookie with given name.
	query // key value is taken from query parameter.

# Response Headers

In order to read and write HTTP headers in a request, an Authenticator should
be used. However to read and write response headers, result values can implement
the http.HeaderWriter and http.HeaderReader interfaces:

	type HeaderWriter interface {
		WriteHeadersHTTP(http.Header)
	}

	type HeaderReader interface {
		ReadHeadersHTTP(http.Header)
	}

If multiple result values implement these interfaces, they will be called in the order
they are returned. Here's an example:

	type ProfilePicture struct {
		io.ReadCloser
	}

	func (ProfilePicture) WriteHeadersHTTP(header http.Header) {
		header.Set("Content-Type", "image/png")
	}

	type API struct {
		api.Specification

		GetProfilePicture func() (ProfilePicture, error)
	}
*/
package rest

import (
	"encoding"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/internal/http"
	"pkg.iqhive.com/iq/api/rest/internal/rtags"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"
)

const debug = false

// Marshaler can be used to override the default JSON encoding of return values.
// This allows a custom format to be returned by a function.
type marshaler interface {
	MarshalREST() ([]byte, error)
}

// Resource describes a REST resource.
type Resource struct {
	Name string

	// Operations that can be performed on
	// this resource, keyed by HTTP method.
	Operations map[http.Method]Operation
}

// Operation describes a REST operation.
type Operation struct {
	api.Function

	// Parameters that can be passed to this operation.
	Parameters []Parameter

	Body reflect.Type

	// Possible responses returned by the operation,
	// keyed by HTTP status code.
	Responses map[int]reflect.Type

	argumentsNeedsMapping bool
}

type ParameterLocation int

const (
	ParameterInVoid ParameterLocation = -1
	ParameterInBody ParameterLocation = 0
	ParameterInPath ParameterLocation = 1 << iota
	ParameterInQuery
)

// Parameter description of an argument passed
// to a REST operation.
type Parameter struct {
	Name string
	Type reflect.Type

	Tags api.Tags

	// Locations where the parameter
	// can be found in the request.
	Location ParameterLocation

	// index is the indicies of the
	// parameter indexing into the
	// function argument.
	Index []int
}

// Specification describes a rest API specification.
type Specification struct {
	api.Specification

	Resources map[string]Resource `api:"-"`

	duplicates []error
}

func SpecificationOf(rest api.WithSpecification) (Specification, error) {
	core, err := api.SpecificationOf(rest)
	if err != nil {
		return Specification{}, oops.Wrap(err)
	}

	var spec Specification
	if err := spec.SetSpecification(core); err != nil {
		return Specification{}, oops.Wrap(err)
	}

	return spec, nil
}

func (spec *Specification) SetSpecification(to api.Specification) error {
	spec.Specification = to

	return oops.Wrap(spec.load(to))
}

func (spec *Specification) load(from api.Specification) error {

	for _, fn := range from.Functions {
		if err := spec.loadOperation(fn); err != nil {
			return oops.Wrap(err)
		}
	}
	for _, section := range from.Sections {
		if err := spec.load(section); err != nil {
			return oops.Wrap(err)
		}
	}

	return nil
}

func (spec *Specification) makeResponses(fn api.Function) (map[int]reflect.Type, error) {
	var responses = make(map[int]reflect.Type)

	rules := rtags.ResultRulesOf(string(fn.Tags.Get("rest")))

	if len(rules) == 0 && fn.Type.NumOut() == 1 {
		responses[200] = fn.Type.Out(0)
		return responses, nil
	}

	var p = newParser(fn)

	if len(rules) != fn.Type.NumOut() {
		return nil, oops.Fixme("%s result rules must match the number of return values", p.debug())
	}

	var fields []reflect.StructField
	for i := 0; i < fn.Type.NumOut(); i++ {
		fields = append(fields, reflect.StructField{
			Name: strings.Title(rules[i]),
			Tag:  reflect.StructTag(`json:"` + rules[i] + `"`),
			Type: fn.Type.Out(i),
		})
	}

	responses[200] = reflect.StructOf(fields)

	return responses, nil
}

func (spec *Specification) loadOperation(fn api.Function) error {
	tag := string(fn.Tags.Get("rest"))
	if tag == "-" {
		return nil //skip
	}

	if tag == "" {
		return oops.Fixme("add a 'rest' tag to %s", fn.Name)
	}

	var method, path, query string
	splits := strings.Split(tag, " ")
	if len(splits) < 2 {
		return oops.Fixme("make sure the 'rest' tag for %s is in the form '[METHOD] [PATH] (ARGUMENT_RULES) RESULT_RULES'", fn.Name)
	}
	method = splits[0]
	if method == "" {
		return oops.Fixme("provide a method in the 'rest' tag for %s, ie 'GET %s'", fn.Name, tag)
	}

	var params = newParser(fn)
	var args []reflect.Type

	for i := 0; i < fn.Type.NumIn(); i++ {
		arg := fn.Type.In(i)
		args = append(args, arg)
	}

	splits = strings.SplitN(splits[1], "?", 2)

	path = splits[0]

	if err := params.parsePath(path, args); err != nil {
		return oops.Wrap(err)
	}

	path = strings.ReplaceAll(path, "=%v", "")

	if prefix := fn.From.Tags.Get("rest"); prefix != "" {
		path = string(prefix) + path
	}

	if len(splits) > 1 {
		query = "?" + splits[1]

		if err := params.parseQuery(query, args); err != nil {
			return oops.Wrap(err)
		}
	}

	if err := params.parseBody(rtags.ArgumentRulesOf(tag)); err != nil {
		return oops.Wrap(err)
	}

	responses, err := spec.makeResponses(fn)
	if err != nil {
		return oops.Wrap(err)
	}

	resource := spec.Resources[path]
	if resource.Operations == nil {
		resource.Operations = make(map[http.Method]Operation)
	}

	// If two names collide, this is probably a mistake and we want to return an error.
	if existing, ok := resource.Operations[http.Method(method)]; ok {
		spec.duplicates = append(spec.duplicates, oops.Fixme("by deduplicating the duplicate endpoint '%s %s' (%s and %s)",
			method, path, strings.Join(append(existing.Path, existing.Name), "."), strings.Join(append(fn.Path, fn.Name), ".")))
	}

	var argumentsNeedsMapping = false
	var count int
	for _, param := range params.list {
		if param.Location == ParameterInBody {
			count++
			if count > 1 {
				argumentsNeedsMapping = true
			}
		}
	}
	var rules = rtags.ArgumentRulesOf(string(fn.Tags.Get("rest")))
	if len(rules) > 0 {
		argumentsNeedsMapping = true
	}

	var body reflect.Type
	if argumentsNeedsMapping {
		if len(rules) != count {
			xray.Print(params.list)
			fmt.Println(fn.Name, fn.Tags.Get("rest"), len(rules), count)
			return oops.Fixme("%v argument rules must match the number of arguments in the body", params.debug())
		}
		var fields []reflect.StructField
		var rule int
		for i, param := range params.list {
			if param.Location == ParameterInBody {
				fields = append(fields, reflect.StructField{
					Name: strings.Title(rules[rule]),
					Tag:  reflect.StructTag(`json:"` + rules[rule] + `"`),
					Type: fn.Type.In(i),
				})
				rule++
			}
		}
		body = reflect.StructOf(fields)
	}

	resource.Operations[http.Method(method)] = Operation{
		Function:   fn,
		Parameters: params.list,
		Responses:  responses,
		Body:       body,

		argumentsNeedsMapping: argumentsNeedsMapping,
	}
	if spec.Resources == nil {
		spec.Resources = make(map[string]Resource)
	}
	spec.Resources[path] = resource

	return nil
}

type parser struct {
	pos int

	list []Parameter

	fn api.Function
}

func newParser(fn api.Function) *parser {
	return &parser{
		list: make([]Parameter, fn.Type.NumIn()),
		fn:   fn,
	}
}

func (p *parser) debug() string {
	return strings.Join(append(p.fn.Path, p.fn.Name), ".")
}

func (p *parser) parseBody(rules []string) error {
	if len(rules) == 0 {
		for i, param := range p.list {
			if param.Location == ParameterInBody {
				p.list[i].Type = p.fn.Type.In(i)
				p.list[i].Index = []int{i}
			}
		}
		return nil
	}

	var rule int
	for i, param := range p.list {
		if param.Location == ParameterInBody {
			if rule >= len(rules) {
				return oops.Fixme("not enough argument rules for %s", p.debug())
			}

			p.list[i].Name = rules[rule]
			p.list[i].Index = []int{i}
			p.list[i].Type = p.fn.Type.In(i)
			rule++
		}
	}

	return nil
}

func (p *parser) parseParam(param string, args []reflect.Type, location ParameterLocation) error {
	if strings.Contains(param, "=") || strings.HasPrefix(param, "%") {

		name, format, ok := strings.Cut(param, "=")
		if !ok {
			format = name
		}
		if format[0] != '%' {
			return nil //FIXME need to do something to support constants?
		}
		if format[len(format)-1] != 'v' {
			return oops.Fixme("%s format parameter must end with v", p.debug())
		}
		if format[1] == '[' {
			//extract the numerial between the brackets.
			if !strings.Contains(format[2:len(format)-1], "]") {
				return oops.Fixme("%s format parameter with numeral must have closing bracket", p.debug())
			}

			numeral := strings.SplitN(format[2:len(format)-1], "]", 2)[0]
			i, err := strconv.Atoi(numeral)
			if err != nil {
				return oops.Fixme("%s format parameter with numeral must be a number", p.debug())
			}
			p.pos = i
		} else {
			p.pos++
		}

		if p.pos-1 >= len(args) {
			return oops.Fixme("%s format parameter %d is out of range (if you are referencing struct fields, omit the =%%v)", p.debug(), p.pos)
		}

		existing := &p.list[p.pos-1]

		if existing.Name != "" && existing.Name != name {
			return oops.Fixme("%s duplicate parameters must share the same name", p.debug())
		}
		if existing.Name == "" {
			existing.Name = name
		}
		existing.Type = args[p.pos-1]
		existing.Location |= location
		existing.Index = []int{p.pos - 1}
		return nil

	} else {
		result, err := p.parseStructParam(param, args)
		if err != nil {
			return oops.Wrap(err)
		}
		result.Location |= location
		p.list = append(p.list, result)
		return nil
	}
}

func (p *parser) parseQuery(query string, args []reflect.Type) error {
	if query == "" {
		return nil
	}

	if query[0] != '?' {
		return oops.Fixme("%s query must start with ?", p.debug())
	}

	params := strings.Split(query[1:], "&")
	for _, param := range params {

		// It's possible to destructure a Go struct into
		// a collection of possible query arguments. This
		// is represented with a '{}', ie. GET /path/to/endpoint?DoSomethingRequest{}
		// The name of this should map to the type in the function arguments.
		destructure := func(i int) error {
			if i >= len(args) {
				return oops.Fixme("%s destructured parameter %d is out of range", p.debug(), i)
			}
			arg := args[i]
			for arg.Kind() == reflect.Ptr {
				arg = arg.Elem()
			}
			if arg.Kind() != reflect.Struct {
				return oops.Fixme("%s only structs can be destructured, '%v' is not a struct", p.debug(), arg)
			}
			var walk func(arg reflect.Type, index []int, parent string)
			walk = func(arg reflect.Type, index []int, parent string) {
				for i := 0; i < arg.NumField(); i++ {
					field := arg.Field(i)

					if !field.IsExported() {
						continue
					}

					name := field.Tag.Get("rest")
					if name == "" {
						name, _, _ = strings.Cut(field.Tag.Get("json"), ",")
					}
					if name == "" {
						name = field.Name
					}

					if parent != "" {
						name = parent + "." + name
					}

					isTextUnmarshaler := field.Type.Implements(reflect.TypeOf([0]encoding.TextUnmarshaler{}).Elem()) ||
						reflect.PointerTo(field.Type).Implements(reflect.TypeOf([0]encoding.TextUnmarshaler{}).Elem())
					for field.Type.Kind() == reflect.Ptr {
						field.Type = field.Type.Elem()
					}
					if field.Type.Kind() != reflect.Struct || isTextUnmarshaler {
						p.list = append(p.list, Parameter{
							Name:     name,
							Type:     field.Type,
							Tags:     api.Tags(field.Tag),
							Index:    append(index, field.Index...),
							Location: ParameterInQuery,
						})
						continue
					}

					if field.Anonymous {
						walk(field.Type, append(index, field.Index...), parent)
						continue
					}
					walk(field.Type, append(index, field.Index...), name)
				}
			}
			walk(arg, []int{i}, "")
			return nil
		}

		if strings.HasPrefix(param, "%") {
			if p.pos >= len(p.list) {
				return oops.Fixme("%s query parameter %d is out of range", p.debug(), p.pos)
			}

			p.list[p.pos].Location = ParameterInVoid

			// walk through and destructure each field as a
			// query parameter.
			if err := destructure(p.pos); err != nil {
				return oops.Wrap(err)
			}
			p.pos++

		} else if strings.HasSuffix(param, "{}") {
			for i, arg := range args {
				if arg.Name() == strings.TrimSuffix(param, "{}") {
					p.list[i].Location = ParameterInVoid

					// walk through and destructure each field as a
					// query parameter.
					if err := destructure(i); err != nil {
						return oops.Wrap(err)
					}
					break
				}
			}
		} else {
			if err := p.parseParam(param, args, ParameterInQuery); err != nil {
				return oops.Wrap(err)
			}
		}
	}

	return nil
}

// parseStructParam parses a parameter that is located within a struct.
func (p *parser) parseStructParam(param string, args []reflect.Type) (Parameter, error) {
	path := strings.Split(param, ".")

	for i, arg := range args {
		for arg.Kind() == reflect.Ptr {
			arg = arg.Elem()
		}
		if arg.Kind() != reflect.Struct {
			continue
		}

		found := true // by exception
		index := []int{}
		for _, name := range path {
			field, ok := arg.FieldByName(name)
			if len(field.Index) > 1 {
				panic("my assumption was wrong")
			}
			if !ok {
				found = false
			} else {
				index = append(index, field.Index...)
				arg = field.Type
			}
		}

		if found {
			return Parameter{
				Name:  param,
				Type:  arg,
				Index: append([]int{i}, index...),
			}, nil
		}

		// check if there are any matching struct tags.
		for i := 0; i < arg.NumField(); i++ {
			field := arg.Field(i)
			if name := field.Tag.Get("rest"); name == param {
				return Parameter{
					Name:  name,
					Type:  field.Type,
					Tags:  api.Tags(field.Tag),
					Index: append([]int{i}, field.Index...),
				}, nil
			}
		}

	}

	return Parameter{},
		oops.Fixme(
			"%s parameter %s needs either a matching field "+
				"name or a matching 'rest' struct tag.",
			param, p.debug())
}

func (p *parser) parsePath(path string, args []reflect.Type) error {
	if path == "" {
		return nil
	}
	if path[0] != '/' {
		return oops.Fixme("%s path must start with /", p.debug())
	}

	// each parameter in the path is contained within a pair of braces.
	if strings.Contains(path, "{") {
		var params []string
		for _, candidate := range strings.Split(path[1:], "{")[1:] {

			splits := strings.SplitN(candidate, "}", 2)
			if len(splits) != 2 {
				return oops.Fixme("%s path parameter must be in the form {param}, with a closing brace", p.debug())
			}
			params = append(params, splits[0])
		}

		for _, param := range params {
			if err := p.parseParam(param, args, ParameterInPath); err != nil {
				return oops.Wrap(err)
			}
		}
	}

	return nil
}
