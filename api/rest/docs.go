package rest

import (
	"bytes"
	"encoding/json"
	"reflect"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/internal/http"
	"pkg.iqhive.com/iq/api/rest/internal/rtags"
	"pkg.iqhive.com/iq/oops"
)

// Documentation for an API.
type Documentation struct {
	Heading string

	Comments []Comment
	Sections []Documentation
}

// Comment within the documentation with
// a sample request and response.
type Comment struct {
	Text string

	HTTP http.Method
	Path string

	Body, Resp []byte
}

// DocumentationOf returns combined documentation for the given
// implementations.
func DocumentationOf(implementation ...api.WithDocumentation) (Documentation, error) {
	docs, err := api.DocumentationOf(implementation...)
	if err != nil {
		return Documentation{}, oops.New(err)
	}
	return convertDocs(docs), nil
}

func Sample(fn api.Function, args, rets []reflect.Value) (url string, req, resp []byte, err error) {
	var spec Specification
	if err := spec.loadOperation(fn); err != nil {
		return "", nil, nil, oops.New(err)
	}

	// there will only be one.
	for path, resource := range spec.Resources {
		for method, operation := range resource.Operations {
			var body bytes.Buffer
			var path = operation.clientWrite(path, args, &body, true)
			var resp bytes.Buffer

			if method == "GET" {
				body.Reset()
			}

			enc := json.NewEncoder(&resp)
			enc.SetIndent("", "\t")

			var rules = rtags.ResultRulesOf(string(fn.Tags.Get("rest")))
			if rules != nil {
				var mapping = make(map[string]json.RawMessage)
				for i, result := range rets {
					msg, _ := json.MarshalIndent(result.Interface(), "", "\t")
					mapping[rules[i]] = json.RawMessage(msg)
				}
				enc.Encode(mapping)
			} else if len(rets) == 1 {
				enc.Encode(rets[0].Interface())
			}

			return string(method) + " " + path, body.Bytes(), resp.Bytes(), nil
		}
	}
	return
}

func convertDocs(docs *api.Documentation) Documentation {
	var converted = Documentation{
		Heading:  docs.Heading,
		Comments: make([]Comment, len(docs.Comments)),
		Sections: make([]Documentation, len(docs.Sections)),
	}
	for i, comment := range docs.Comments {
		if comment.Call == nil {
			converted.Comments[i] = Comment{
				Text: comment.Text,
			}
			continue
		}
		fn := *comment.Call

		var spec Specification
		if err := spec.loadOperation(fn); err != nil {
			panic(err)
		}

		// there will only be one.
		for path, resource := range spec.Resources {
			for method, operation := range resource.Operations {
				var body bytes.Buffer
				var path = operation.clientWrite(path, comment.Args, &body, true)
				var resp bytes.Buffer

				if method == "GET" {
					body.Reset()
				}

				enc := json.NewEncoder(&body)
				enc.SetIndent("", "\t")

				var rules = rtags.ResultRulesOf(string(fn.Tags.Get("rest")))
				if rules != nil {
					var mapping = make(map[string]json.RawMessage)
					for i, result := range comment.Results {
						msg, _ := json.MarshalIndent(result.Interface(), "", "\t")
						mapping[rules[i]] = json.RawMessage(msg)
					}
					enc.Encode(mapping)
				} else if len(comment.Results) == 1 {
					enc.Encode(comment.Results[0].Interface())
				}

				converted.Comments[i] = Comment{
					Text: comment.Text,
					HTTP: method,
					Path: path,
					Body: body.Bytes(),
					Resp: resp.Bytes(),
				}
			}
		}

	}
	for i, section := range docs.Sections {
		converted.Sections[i] = convertDocs(section)
	}
	return converted
}
