/*
Package api allows type-safe APIs to be defined with Go structs.

	var API struct {
		api.Specification

		DoSomething func(request string) (response string, err error) `
			does something!`
	}

# Documentation

API functions can be documented using subsequent lines of the Go field
tag for that function. The identation defined by the first line is
removed from all lines of the documenation, format can loosely
be considered to be markdown.

Individual structures can be documented via implementing:

	Doc() string

Alternatively, the documentation can be provided via embedding an
api.Model (or other embedded type) in the structure and giving it
a documentation string.

	type Request struct {
		api.Model `
		 	that does something.`

		Parameter string `
			is a parameter.`
	}

Documentation strings should be treated as if the field name will
preceed the string. Drivers will be able to export the entire API docs.

# Authentication

There are three different kinds of ways to handle authentication.
Keys, Authenticator Types and Authentication functions.

Keys are the easiest to use, they are suitable when creating a
client API that requires a static API-Key. Add a string field
tagged with api:"key" and any driver specific tags. This key
will be used to authenticate API calls. Write this once before
initializing the API. The server will treat this as the value
that needs to be provided to use the API.

	MyKey string `api:"key"`

Authenticators are the next option. They work similarly to
keys except using a struct instead of a string. The struct
can contain Keys and the type must have an Authenticate() error
method that the server will call in order to check that the
authentication is valid. This has the same limitations as
Keys and can only be written to once before initialization.

	Auth MyAuthentication

Authentication functions are the most flexible, it is a
function that can return an Authenticator and therefore
can be used to implement dynamic credentials that need to
be refreshed over time. Embed it inside of your API in
order to use it.

	api.Authentication

# Sections

An API definition struct can contain nested sections, created
with sub structures that provide semantic categorisation for
the API and enable a clearer API structure.

	var API struct {
		Customer struct {
			Create(name string) error
		}
	}

The section inherits the authentication of the parent structure.
*/
package api

import (
	"context"
	"crypto/subtle"
	"errors"
	"fmt"
	"path"
	"reflect"
	"runtime/debug"
	"strings"
	"sync/atomic"
	"testing"

	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/please"
)

// Model is an intentionally empty structure, used to attach documentation
// to an object being passed through an API. Embed this as one of the first fields
// in your structures and the doc tag will be used by documentation generators for
// the type itself.
type Model struct{}

/*
Authenticator types enable flexible authentication for
String fields tagged with api:"key" are auto-populated by
the transport according to tags defined by the driver.

For example:

	type MyAuthentication struct {
		MyKey string `api:"key" rest:"x-api-key,header"`
	}

	func (auth MyAuthentication) Authenticate() error {
		if auth.MyKey.Equals("the_correct_key") {
			return nil
		}
		return access.Denied()
	}
*/
type Authenticator interface {
	context.Context

	/*
		Authenticate is called after the key fields have been auto-populated.
		Errors will be returned to the client and will prevent any API calls from executing.

		DoSomething func(MyAuthenticator, request string) (response string, err error)
	*/
	Authenticate() error
}

// Authorizer is an optional extension to an Authenticator that can be used to
// authorize API calls. If an Authorizer is returned by an Authentication function,
// the server will call Authorize before calling the API function.
type Authorizer interface {
	Authenticator

	// Authorize is called after successful authentication and argument validation
	// but before the API function is called. If this returns an error, the API
	// function will not be called and the error will be returned to the client.
	Authorize(args []reflect.Value) error
}

/*
Authentication can be used to specify advanced authentication.
It can be embedded inside of an API just like a Specification.

The Authenticator returned by this function will be passed to
the server and the server will call the object's Authenticate
method.

As this is a function type, it can be used to construct an
Authenticator dynamically, or from a context. As a special
case, if the context passed to this function is `nil` then
an empty typed Authenticator should be returned, this will
be used by the server and for generating documentation.
Because of this special case, it is best to return a
consistent typed Authenticator from your function in all
branches. In a future release of this package, this type
may be generic in order to enforce this behaviour.

A minimally correct implementation would be:

	API.Authentication = func(ctx context.Context) (Authenticator, error) {
		return MyAuthentication{
			MyKey: "the_correct_key",
		}, nil
	}
*/
type Authentication func(ctx context.Context, tags Tags) (Authenticator, error)

type testingAuthenticator struct {
	context.Context
}

func (*testingAuthenticator) Authenticate() error { return nil }

func (auth Authentication) Authenticator(ctx context.Context, tags Tags) (Authenticator, error) {
	if auth == nil {
		if test.Load() {
			return &testingAuthenticator{ctx}, nil
		}
		return &testingAuthenticator{ctx}, nil
	}

	return auth(ctx, tags)
}

/*
Key is a token stored in an Authenticator or API Specification
that is required to access API function(s).

Clients must set the Key in order to be authenticated.
Servers must set the Key so clients' keys can be verified.

	var API struct {
		api.Specification // Makes outer API struct a "specification"

		MyKey string `api:"key" rest:"x-api-key,header"`

		DoSomething func(request string) (response string, err error)
	}
*/
type Key struct {

	//Name of the key (usually the struct field name).
	Name string

	//Tags of this key, these define where it is located for a particular
	//driver and other optional attributes.
	Tags Tags

	//Value is a pointer to the value of the key.
	Value *string
}

func keysOf(recursive bool, rvalue reflect.Value) ([]Key, error) {
	rtype := rvalue.Type()

	var keys []Key

	for i := 0; i < rtype.NumField(); i++ {
		ftype := rtype.Field(i)
		fvalue := rvalue.Field(i)

		if !ftype.IsExported() {
			continue
		}

		var tags = Tags(ftype.Tag)

		spec := reflect.TypeOf([0]WithSpecification{}).Elem()

		if reflect.PtrTo(ftype.Type).Implements(spec) || ftype.Type.Implements(spec) {
			continue
		}

		if tag := tags.Get("api"); tag != "" {
			if tag.Value() != "key" {
				return nil, fmt.Errorf(`found api:"%v" tagged field "%v" unknown api directive "%[1]v" (only "key" is supported)`, tag.Value(), ftype.Name)
			}

			if ftype.Type.Kind() != reflect.String {
				return nil, fmt.Errorf(`found api:"key" tagged field "%v" with unsupported type: "%v" (only strings are supported)`, ftype.Name, ftype.Type)
			}

			var value *string

			if fvalue.CanSet() {
				value = fvalue.Addr().Convert(reflect.TypeOf([0]*string{}).Elem()).Interface().(*string)
			} else {
				value = new(string)
				*value = fvalue.String()
			}

			keys = append(keys, Key{
				Name:  ftype.Name,
				Tags:  tags,
				Value: value,
			})
		}

		if recursive && ftype.Type.Kind() == reflect.Struct {
			nested, err := keysOf(recursive, fvalue)
			if err != nil {
				return nil, err
			}

			keys = append(keys, nested...)
		}
	}

	return keys, nil
}

func keysOfInterface(auth interface{}) ([]Key, error) {
	if auth == nil {
		return nil, nil //Nil interface contains no keys.
	}

	rtype := reflect.TypeOf(auth)
	rvalue := reflect.ValueOf(auth)

	if rtype.Kind() == reflect.Ptr {
		rtype = reflect.TypeOf(auth).Elem()
		rvalue = reflect.ValueOf(auth).Elem()
	}

	if rtype.Kind() != reflect.Struct {
		return nil, nil //Only structs can contain keys.
	}

	return keysOf(true, rvalue)
}

// KeysOf extracts all keys recursively from an Authenticator.
// If you want to extract the keys from an API specification structure
// use spec, _ := SpecificationOf(api); spec.Keys
func KeysOf(auth Authenticator) ([]Key, error) {
	return keysOfInterface(auth)
}

// Stub allows an api to be stubbed, implemented so
// that their operations have no effect and return a nil error.
//
//	stub = func(...) (..., error) {
//		return ..., nil
//	}
//
// This can be useful for testing/tests. Stub functions can be
// overwritten with more complex, stubbing/dummy logic by
// implementing the functions yourself.
func Stub(api WithSpecification) {
	spec, _ := SpecificationOf(api)
	stub(spec)
}

func stub(spec Specification) {
	for i := range spec.Functions {
		fn := &spec.Functions[i]
		fn.FuncValue.Set(func(usr context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error) {

			results = make([]reflect.Value, fn.Type.NumOut())

			//Zero out the result values.
			for i := 0; i < fn.Type.NumOut(); i++ {

				out := fn.Type.Out(i)

				if out.Kind() == reflect.Ptr {
					results[i] = reflect.New(out.Elem())
				} else {
					results[i] = reflect.Zero(out)
				}
			}

			return results, nil
		})
	}

	for _, section := range spec.Sections {
		stub(section)
	}
}

// Specification of an API. It defines either the entire API
// or a sub-section of one. It implements "specification".
type Specification struct {
	Name, Version string

	// Package name where the specification was defined.
	Package string

	//Host of the API is a driver specific string that should
	//be configured before calling functions that use this
	//specification.
	Host string

	//Tags of the API, used for configuring the name,
	//documentation and transport-specific settings.
	Tags Tags

	Authentication Authentication

	//Authentication needed to call the
	//functions of this API.
	Authenticators []Authenticator

	Keys []Key

	//Functions is a list of functions this API provides.
	Functions []Function

	//ErrorType describes the type of error that is returned
	//by this API.
	ErrorSpec struct {
		Type reflect.Type
		Func func(context.Context, error) error
	}
	Errors []Error

	//Sections allows an API to be split into different
	//sections, each with their own configuration.
	Sections []Specification

	loaded bool
}

func (spec Specification) getSpecification() Specification {
	return spec
}

func (spec *Specification) ptrToSpecification() *Specification {
	return spec
}

// SpecificationOf returns the API specification
// structure for a given object.
func SpecificationOf(api WithSpecification) (Specification, error) {
	if spec, ok := api.(*Specification); ok {

		//This is required so that the Value functions
		//observe any changes made to the function.
		for i := range spec.Functions {
			fn := &spec.Functions[i]

			fn.FuncValue.Call = fn.exporter
			fn.FuncValue.Set = fn.importer
		}

		if spec.ErrorSpec.Func == nil {
			spec.ErrorSpec.Func = func(ctx context.Context, err error) error {
				return err
			}
		}

		return *spec, nil
	}

	var spec *Specification
	if ptr, ok := api.(interface {
		ptrToSpecification() *Specification
	}); ok {
		spec = ptr.ptrToSpecification()
	} else {
		value := api.getSpecification()
		spec = &value

		copy := reflect.New(reflect.ValueOf(api).Type()).Elem()
		copy.Set(reflect.ValueOf(api))
		api = copy.Addr().Interface().(WithSpecification)
	}
	if spec.loaded {
		return *spec, nil
	}

	named := reflect.TypeOf(api)
	if named.Kind() == reflect.Ptr {
		named = named.Elem()
	}
	spec.Name = path.Base(named.PkgPath())
	if spec.Name == "." {
		spec.Name = ""
	}

	spec.load(api, nil, spec.Authentication, nil, nil)

	if spec.ErrorSpec.Func == nil {
		spec.ErrorSpec.Func = func(ctx context.Context, err error) error {
			return err
		}
	}

	return *spec, nil
}

func (api *Specification) countFunctions(rtype reflect.Type) int {
	var count int

	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)

		if !field.IsExported() {
			continue
		}

		if field.Type.Kind() == reflect.Func {
			count++
		}

		if field.Type.Kind() == reflect.Struct {
			count += api.countFunctions(field.Type)
		}
	}

	return count
}

// load takes an api definition struct pointer and builds the API definition.
// it returns a slice of any children APIs (inside their own sub structures).
func (api *Specification) load(spec interface{}, stack []string, auth Authentication, auths []Authenticator, keys []Key) (defs []Specification) {
	rtype := reflect.TypeOf(spec)
	rvalue := reflect.ValueOf(spec)
	if rtype.Kind() == reflect.Pointer {
		rtype = rtype.Elem()
		rvalue = rvalue.Elem()
	}

	api.Package = path.Base(rtype.PkgPath())
	api.Authentication = auth
	api.Authenticators = auths
	api.Keys = keys

	if errs, ok := spec.(WithErrors); ok {
		api.Errors = errs.errors()
	}

	k, _ := keysOf(false, rvalue)
	api.Keys = append(api.Keys, k...)

	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		value := rvalue.Field(i)

		if !field.IsExported() {
			continue
		}

		api.loadInterface(field, value)
		api.loadError(field, value)
		api.loadAuth(field, value)
	}

	//preallocate, so that appends do not screw up
	//the memory addresses of each function.
	api.Functions = make([]Function, 0, api.countFunctions(rtype))

	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		value := rvalue.Field(i)

		if !field.IsExported() {
			continue
		}

		api.loadFunction(stack, field, value)
	}

	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		value := rvalue.Field(i)

		if !field.IsExported() {
			continue
		}

		if field.Type == reflect.TypeOf(Specification{}) {
			spec := rvalue.Field(i).Interface().(Specification)
			api.Host = spec.Host
			if name, ok := field.Tag.Lookup("api"); ok {
				api.Name, api.Version, _ = strings.Cut(name, ",")
			}
			continue
		}

		//A sub-section of the API.
		//That can have different properties to this definition.
		if field.Type.Kind() == reflect.Struct {
			var section Specification

			subpath := stack
			if field.Name != "Specification" {
				subpath = append(stack, field.Name)
			} else {
				if name, ok := field.Tag.Lookup("api"); ok {
					api.Name = name
				}
			}

			section.load(value.Addr().Interface(), subpath, api.Authentication, api.Authenticators, api.Keys)
			section.Tags = Tags(field.Tag) + " " + section.Tags
			section.Errors = append(api.Errors, section.Errors...)
			api.Sections = append(api.Sections, section)
		}
	}

	api.loaded = true

	if api.ErrorSpec.Func == nil {
		api.ErrorSpec.Func = func(ctx context.Context, err error) error {
			return err
		}
	}

	return
}

// loadInterface attempts to load the embedded Interface that enables the API
// to be exported or imported. Tags can be defined on this field to
// configure how the importer/exporter behaves.
func (api *Specification) loadInterface(field reflect.StructField, rvalue reflect.Value) {
	type Tagger interface {
		Tag() string
	}

	spec := reflect.TypeOf([0]WithSpecification{}).Elem()

	if reflect.PtrTo(field.Type).Implements(spec) || field.Type.Implements(spec) {
		api.Tags += Tags(field.Tag)
	}

	if field.Type.Implements(reflect.TypeOf([0]Tagger{}).Elem()) {
		api.Tags += Tags(fmt.Sprintf(`grpc:"%v"`, field.Tag))
	}
}

// loadError tries to load the defined error type from the field.
// the error type is returned as a Go error from functions inside this API.
func (api *Specification) loadError(field reflect.StructField, rvalue reflect.Value) {
	if field.Type.Implements(reflect.TypeOf([0]error{}).Elem()) {
		api.ErrorSpec.Type = field.Type
	}
	if field.Type.Kind() == reflect.Func {
		if field.Type.NumIn() == 1 && field.Type.NumOut() == 1 {
			if field.Type.In(0) == reflect.TypeOf([0]error{}).Elem() {
				if field.Type.Out(0).Implements(reflect.TypeOf([0]error{}).Elem()) {
					api.ErrorSpec.Type = field.Type.Out(0)
					api.ErrorSpec.Func = func(ctx context.Context, err error) error {
						return rvalue.Call([]reflect.Value{reflect.ValueOf(err)})[0].Interface().(error)
					}
				}
			}
		}
		if field.Type.NumIn() == 2 && field.Type.NumOut() == 1 {
			if field.Type.In(1) == reflect.TypeOf([0]error{}).Elem() && field.Type.In(0) == reflect.TypeOf([0]context.Context{}).Elem() {
				if field.Type.Out(0).Implements(reflect.TypeOf([0]error{}).Elem()) {
					api.ErrorSpec.Type = field.Type.Out(0)
					api.ErrorSpec.Func = func(ctx context.Context, err error) error {
						return rvalue.Call([]reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(err)})[0].Interface().(error)
					}
				}
			}
		}
	}
}

// loadFunction tries to load a function of the API.
// The function can have an optional authenticator as the first argument and
// an optional error as the last result value. These values are managed by
// this package when called.
func (api *Specification) loadFunction(path []string, field reflect.StructField, rvalue reflect.Value) {
	if field.Type.Kind() == reflect.Func {

		if field.Type == reflect.TypeOf([0]Authentication{}).Elem() {
			return
		}

		tags := Tags(field.Tag)

		fn := Function{
			Name: field.Name,
			Tags: tags,
		}
		fn.Access.Authentication = &api.Authentication
		fn.Access.Authenticators = api.Authenticators
		fn.Access.Keys = api.Keys
		fn.Errors = api.Errors
		fn.Path = path
		fn.From = api

		api.Functions = append(api.Functions, fn)
		api.Functions[len(api.Functions)-1].set(rvalue)
	}
}

// load tries to load the defined authenticator for the API.
// This authenticator is responsible for authenticating every function call
// inside this Definition. It can be overidden by per-function authenticators.
func (api *Specification) loadAuth(field reflect.StructField, rvalue reflect.Value) {

	//Authentication function.
	if field.Type.Kind() == reflect.Func && field.Type == reflect.TypeOf([0]Authentication{}).Elem() {
		api.Authentication = rvalue.Interface().(Authentication)
	}

	//We still want zero value types to act as authenticators when they have
	//an (*T) Authenticate method. So we want to handle T and *T types.
	if field.Type.Implements(reflect.TypeOf([0]Authenticator{}).Elem()) {
		//default case
	} else if reflect.PtrTo(field.Type).Implements(reflect.TypeOf([0]Authenticator{}).Elem()) {
		field.Type = reflect.PtrTo(field.Type)
		if rvalue.IsValid() {
			rvalue = rvalue.Addr()
		}
	} else {
		return
	}

	api.Authenticators = append(api.Authenticators,
		rvalue.Interface().(Authenticator),
	)

}

// Function is an individual function provided by an API usually
// exposed on a particular endpoint.
type Function struct {

	//Name of the function. Normally the name of the struct field.
	Name string

	// From is the original specification that this function was
	// instantiated from.
	From *Specification

	// If this function is nested within multiple structs,
	// this is a period-separated list of the names of the
	// structs that contain this function.
	Path []string

	//Tags is a list of struct tags attached to this function. Use
	//these to configure the behaviour of the driver.
	Tags Tags

	//Type of the function, excludes any Authenticators or
	//errors, as they are handled by this package.
	Type reflect.Type

	//Access related fields for authentication.
	Access struct {
		Authentication *Authentication

		//Authenticaters that are required to access this function.
		Authenticators []Authenticator

		//Keys that are required to access this function.
		Keys []Key
	}

	Errors []Error

	//FuncValue is a restricted interface to the value of the function.
	//Used by drivers to import and/or export the function.
	FuncValue struct {

		//Call calls the function using the given user and a function that will retrieve the keys and arguments
		//that will be passed to the function. Call returns the results of the call to this function.
		Call func(ctx context.Context, keys func([]Key) error, args func([]reflect.Value) error) (results []reflect.Value, err error)

		//Set sets the function using the given implementation. The implementation will receive the calling user,
		//keys and arguments and should return the results.
		Set func(fn func(ctx context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error))
	}

	//The real type and value.
	//This is hidden from the driver so that the api package
	//can automatically handle standard features such as
	//the Authenticator, context.Context and error types.
	rtype reflect.Type
	value reflect.Value

	//helper flags.
	firstArgumentIsAnAuthenticator, returnsAnError bool
	firstArgumentIsContext                         bool
}

func (dn Function) Copy() Function {
	value := reflect.New(dn.rtype).Elem()
	value.Set(dn.value)
	dn.value = value
	dn.FuncValue.Set = dn.importer
	dn.FuncValue.Call = dn.exporter
	return dn
}

func (dn Function) Call(ctx context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error) {
	return dn.FuncValue.Call(ctx, func(dst []Key) error {
		copy(dst, keys)
		return nil
	}, func(dst []reflect.Value) error {
		copy(dst, args)
		return nil
	})
}

func (dn Function) Set(fn func(ctx context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error)) {
	dn.FuncValue.Set(fn)
}

// FunctionOf returns the Function of the given
// func argument. This is useful for manually
// constructing a Specification.
func FunctionOf(fn interface{}) (Function, error) {
	rvalue := reflect.ValueOf(fn)
	rtype := reflect.TypeOf(fn)

	if rtype.Kind() == reflect.Ptr {
		rtype = rtype.Elem()
		rvalue = rvalue.Elem()
	}

	if rtype.Kind() == reflect.Func {
		fn := Function{}
		fn.set(rvalue)

		return fn, nil
	}
	return Function{}, errors.New(rtype.String() + " is not a function")
}

func (fn *Function) set(rvalue reflect.Value) {
	rtype := rvalue.Type()

	var rtAuthenticator = reflect.TypeOf([0]Authenticator{}).Elem()
	var rtUser = reflect.TypeOf([0]context.Context{}).Elem()

	var hasAuthenticator, hasUser, hasError bool

	//We want to hide some of the function arguments,
	//so that implementations don't need to worry about
	//Authenticators and/or errors. We take care of them.
	var in, out []reflect.Type

	for i := 0; i < rtype.NumIn(); i++ {
		in = append(in, rtype.In(i))
	}
	for i := 0; i < rtype.NumOut(); i++ {
		out = append(out, rtype.Out(i))
	}

	if rtype.NumIn() > 0 {
		firstArgType := rtype.In(0)

		if firstArgType.Implements(rtUser) {
			hasUser = true
		}

		if firstArgType.Implements(rtAuthenticator) {
			hasAuthenticator = true
			fn.Access.Authenticators = append(fn.Access.Authenticators, reflect.Zero(rtype.In(0)).Interface().(Authenticator))
			in = in[1:]
		} else if reflect.PtrTo(firstArgType).Implements(rtAuthenticator) {
			hasAuthenticator = true
			fn.Access.Authenticators = append(fn.Access.Authenticators, reflect.New(rtype.In(0)).Interface().(Authenticator))
			in = in[1:]
		} else if hasUser {
			in = in[1:]
		}

	}

	//Filter out error.
	if len(out) > 0 && out[len(out)-1].Implements(reflect.TypeOf([0]error{}).Elem()) {
		out = out[: len(out)-1 : len(out)-1] //capacity is important here, because we append later.
		hasError = true
	}

	fn.Type = reflect.FuncOf(in, out, rtype.IsVariadic())
	fn.rtype = rtype
	fn.value = rvalue
	fn.firstArgumentIsContext = hasUser
	fn.firstArgumentIsAnAuthenticator = hasAuthenticator
	fn.returnsAnError = hasError

	fn.FuncValue.Set = fn.importer

	//if the function is nil, we will fill it with a function
	//that returns a Not Implemented error. This avoids nil
	//pointer panics when calling a function that hasn't
	//been implemented yet.
	if rvalue.IsNil() && rvalue.CanSet() {
		fn.FuncValue.Set(func(usr context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error) {
			return nil, oops.NotImplemented()
		})
	}
	fn.FuncValue.Call = fn.exporter
}

// Export exports the function given FunctionExporter.
// This should be called by functions who implement an api.Exporter.
func (fn *Function) exporter(ctx context.Context, getKeys func([]Key) error, getArgs func([]reflect.Value) error) (results []reflect.Value, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("panic: %v\n%v", r, string(debug.Stack()))
		}
	}()

	//Preallocate the arguments slice.
	//TODO use a pool to avoid allocations.
	var args = make([]reflect.Value, fn.rtype.NumIn())
	for i := 0; i < fn.rtype.NumIn(); i++ {
		if fn.rtype.In(i).Kind() != reflect.Ptr {
			args[i] = reflect.New(fn.rtype.In(i)).Elem()
		} else {
			args[i] = reflect.New(fn.rtype.In(i).Elem())
		}
	}

	if getKeys != nil {
		var checkers []Key
		var manual []Key

		var auths []reflect.Value

		var authenticators = fn.Access.Authenticators

		//Either load the keys from the caller-provided Authenticator argument
		//or else we load the keys from the API Specification. As they would
		//have been configured when the API was exported.
		if fn.firstArgumentIsAnAuthenticator {
			T := reflect.TypeOf(fn.Access.Authenticators[len(fn.Access.Authenticators)-1])

			var auth reflect.Value

			if T.Kind() == reflect.Ptr {
				auth = reflect.New(T.Elem())
			} else {
				auth = reflect.New(T).Elem()
			}

			auths = append(auths, auth)

			keys, err := KeysOf(auth.Interface().(Authenticator))
			if err != nil {
				return nil, err
			}

			checkers = append(checkers, keys...)

			//We trim the function's authenticators because we don't
			//want to process this authenticator twice.
			//(it is stored in two places because it also needs to be
			// accessible so that the authentication can be documented).
			authenticators = authenticators[:len(authenticators)-1]
		}

		//or else we load the keys from the API Specification. As they would
		//have been configured when the API was imported.
		for _, authenticator := range authenticators {
			T := reflect.TypeOf(authenticator)

			var auth reflect.Value

			if T.Kind() == reflect.Ptr {
				auth = reflect.New(T.Elem())
			} else {
				auth = reflect.New(T).Elem()
			}

			auths = append(auths, auth)

			keys, err := KeysOf(auth.Interface().(Authenticator))
			if err != nil {
				return nil, err
			}

			checkers = append(checkers, keys...)
		}

		//Handle Authentication by parent.
		if auth := *fn.Access.Authentication; auth != nil {
			a, err := auth.Authenticator(ctx, fn.Tags)
			if err != nil {
				return nil, err
			}

			ctx = a

			auths = append(auths, reflect.ValueOf(a))

			keys, err := KeysOf(a)
			if err != nil {
				return nil, err
			}

			checkers = append(checkers, keys...)
		}

		for _, k := range fn.Access.Keys {
			k.Value = new(string)
			manual = append(manual, k)
		}

		if err := getKeys(append(checkers, manual...)); err != nil {
			return nil, err
		}

		if fn.firstArgumentIsAnAuthenticator {
			args[0] = auths[0]

			//The authenticator being provided to the function needs to be mutable
			//and may have been created as a pointer, we will dereference it here
			//if the function being called, expects the authenticator to be passed
			//by value.
			if fn.rtype.In(0).Kind() != reflect.Ptr && args[0].Type().Kind() == reflect.Ptr {
				args[0] = args[0].Elem()
			}

			// If the authenticator has an embedded context, we inject it.
			field := args[0].FieldByName("Context")
			if field.IsValid() && field.CanSet() && field.Type() == reflect.TypeOf([0]context.Context{}).Elem() {
				args[0].FieldByName("Context").Set(reflect.ValueOf(ctx))
			}
		}

		//These two loops do the authentication check, this is done
		//before the arguments are checked.
		for _, auth := range auths {
			if err := auth.Interface().(Authenticator).Authenticate(); err != nil {
				return nil, err
			}
		}
		for i, checker := range manual {
			if checker.Value == nil {
				return nil, please.Authenticate(checker.Name + " key")
			}
			if fn.Access.Keys[i].Value == nil || *fn.Access.Keys[i].Value == "" {
				return nil, oops.NotImplemented()
			}
			if subtle.ConstantTimeCompare([]byte(*checker.Value), []byte(*fn.Access.Keys[i].Value)) != 1 {
				return nil, oops.AccessDenied()
			}
		}
	} else {
		if fn.firstArgumentIsAnAuthenticator {
			//The authenticator being provided to the function needs to be mutable
			//and may have been created as a pointer, we will dereference it here
			//if the function being called, expects the authenticator to be passed
			//by value.
			if fn.rtype.In(0).Kind() != reflect.Ptr && args[0].Type().Kind() == reflect.Ptr {
				args[0] = args[0].Elem()
			}

			// If the authenticator has an embedded context, we inject it.
			field := args[0].FieldByName("Context")
			if field.IsValid() && field.CanSet() && field.Type() == reflect.TypeOf([0]context.Context{}).Elem() {
				args[0].FieldByName("Context").Set(reflect.ValueOf(ctx))
			}
		}
	}

	if fn.firstArgumentIsAnAuthenticator {

		if err := getArgs(args[1:]); err != nil {
			return nil, err
		}
		if auth, ok := ctx.(Authorizer); ok && getKeys != nil {
			if err := auth.Authorize(args[1:]); err != nil {
				return nil, err
			}
		}
		first := args[0]
		if first.CanAddr() {
			first = first.Addr()
		}
		if auth, ok := first.Interface().(Authorizer); ok && getKeys != nil {
			if err := auth.Authorize(args[1:]); err != nil {
				return nil, err
			}
		}
	} else {

		//Load the context/user
		if fn.firstArgumentIsContext {
			args[0] = reflect.ValueOf(ctx)

			if err := getArgs(args[1:]); err != nil {
				return nil, err
			}
			if auth, ok := ctx.(Authorizer); ok && getKeys != nil {
				if err := auth.Authorize(args[1:]); err != nil {
					return nil, err
				}
			}

		} else {
			if err := getArgs(args); err != nil {
				return nil, err
			}

			if auth, ok := ctx.(Authorizer); ok && getKeys != nil {
				if err := auth.Authorize(args); err != nil {
					return nil, err
				}
			}
		}
	}

	//Authentication was successful if we got to this point
	//so we are allowed to actually call the function.
	if fn.Type.IsVariadic() {
		slice := args[len(args)-1]
		args = args[:len(args)-1]
		for i := 0; i < slice.Len(); i++ {
			args = append(args, slice.Index(i))
		}
	}
	results = fn.value.Call(args)

	if len(results) == 0 {
		return results, nil
	}

	if fn.returnsAnError {
		if i := results[len(results)-1].Interface(); i != nil {
			return results[:len(results)-1], i.(error)
		}
		return results[:len(results)-1], nil
	}

	return results, nil
}

var nilErrorReflectValue = reflect.Zero(reflect.TypeOf((*error)(nil)).Elem())

// Import imports the function from the API Specification using the given FunctionImporter.
// This should be called by packages who implement an api.Importer.
func (fn *Function) importer(importer func(ctx context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error)) {

	made := reflect.MakeFunc(fn.rtype, func(args []reflect.Value) (results []reflect.Value) {

		var keys []Key

		var authenticators = fn.Access.Authenticators

		var user = context.Background()

		if fn.firstArgumentIsContext {
			user = args[0].Interface().(context.Context)
		}

		//Either load the keys from the caller-provided Authenticator argument
		if fn.firstArgumentIsAnAuthenticator {
			keys, _ = keysOfInterface(args[0].Interface())

			args = args[1:]

			//We trim the function's authenticators because we don't
			//want to process this authenticator twice.
			//(it is stored in two places because it also needs to be
			// accessible so that the authentication can be documented).
			authenticators = authenticators[:len(authenticators)-1]

		} else if fn.firstArgumentIsContext {
			args = args[1:]
		}

		//or else we load the keys from the API Specification. As they would
		//have been configured when the API was imported.
		for _, authenticator := range authenticators {
			k, _ := KeysOf(authenticator)
			keys = append(keys, k...)
		}

		handle := func(err error) (results []reflect.Value) {
			//handle any errors that occur when trying to call
			//the API function. If the function has any error results
			//we stuff the error into it. If it hasn't got one,
			//then we panic. MAYBE we should retry instead of panic.
			if fn.returnsAnError {
				results = make([]reflect.Value, fn.rtype.NumOut())
				for i := 0; i < fn.rtype.NumOut(); i++ {
					results[i] = reflect.Zero(fn.rtype.Out(i))
				}
				results[len(results)-1] = reflect.ValueOf(err)
				return results
			}
			panic(err)
		}

		if fn.Access.Authentication != nil {
			authenticator, err := fn.Access.Authentication.Authenticator(user, fn.Tags)
			if err != nil {
				return handle(err)
			}
			k, _ := KeysOf(authenticator)
			keys = append(keys, k...)
		}

		keys = append(keys, fn.Access.Keys...)

		results, err := importer(user, keys, args)
		if err != nil {
			return handle(err)
		}

		if fn.returnsAnError { //return nil error.
			return append(results, nilErrorReflectValue)
		}

		return results
	})

	fn.value.Set(made)

	fn.FuncValue.Call = fn.exporter
}

func fill(spec Specification) {
	for i := range spec.Functions {
		i := i

		old := spec.Functions[i].Copy().FuncValue

		fn := &spec.Functions[i]
		fn.FuncValue.Set(func(ctx context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error) {
			if test.Load() || ctx.Value(contextKeyDocs{}) != nil {
				return old.Call(ctx, func(dst []Key) error {
					copy(dst, keys)
					return nil
				}, func(dst []reflect.Value) error {
					copy(dst, args)
					return nil
				})
			}
			return nil, oops.Fixme("by providing an implementation for the " + spec.Name + " API")
		})
	}
	for _, section := range spec.Sections {
		fill(section)
	}
}

var test atomic.Bool

// Entrypoint registers the given API specification structure as the
// official singleton API entrypoint for that API. Typically this
// will be the top-level 'API' variable in the package that defined
// the specification. An implementation constructor for the API
// should be provided, it will be enabled when [Test] is called.
// Otherwise, the API endpoints will return an error advising the
// caller to implement the API.
func Entrypoint[Impl WithSpecification, Core any](constructor func(Core) Impl, integration Core) Impl {
	var entrypoint = constructor(integration)
	spec, _ := SpecificationOf(entrypoint)
	fill(spec)
	return entrypoint
}

// Test sets the package into testing mode and attaches the testing implementation
// to all known entrypoints. This should only be called in test packages.
// As a special case, passing nil will disable testing mode. This function
// returns a context that should be used where possible when calling APIs.
func Test(t *testing.T, implementations ...WithDocumentation) context.Context {
	test.Store(t != nil)

	ctx := context.Background()

	var cursor *Documentation
	var docs = new(Documentation) // resuable
	docs.Context = ctx
	docs.test = t
	docs.cursor = &cursor

	cursor = docs

	for _, impl := range implementations {
		if err := impl.Documentation(docs); err != nil {
			t.Fatal(err)
		}
		*docs = Documentation{
			Context: ctx,
			test:    t, // reset the documentation
		}
	}

	return ctx
}

var testingSingletons = make(map[reflect.Type]any)

// Testing returns a singleton pointer to a value of
// type T, every call to Testing with the sane type
// will return the same pointer. This is useful for
// representing shared resources between multiple
// APIs.
func Testing[T any]() *T {
	rtype := reflect.TypeOf([0]T{}).Elem()
	if t, ok := testingSingletons[rtype]; ok {
		return t.(*T)
	}
	var t = new(T)
	testingSingletons[rtype] = t
	return t
}

type Context struct {
	context.Context
}

func NewContext(ctx context.Context) Context {
	return Context{ctx}
}

func (ctx Context) Authenticate() error {
	return nil
}

type WithSpecification interface {
	getSpecification() Specification
}

func (spec Specification) configure(base reflect.Type, configuration reflect.Value) error {
	for _, fn := range spec.Functions {
		method := configuration.MethodByName(fn.Name)
		if !method.IsValid() {
			continue
		}
		if method.Type() != fn.rtype {
			return fmt.Errorf("%s does not implement %s (wrong type for method %s)\n\thave %s\n\twant %s",
				configuration.Type(), base, fn.Name, method.Type(), fn.rtype)
		}
		fn.value.Set(method)
	}
	for _, section := range spec.Sections {
		if err := section.configure(base, configuration); err != nil {
			return err
		}
	}
	return nil
}

type Implements[T WithSpecification] struct{}

func (Implements[T]) gateway()       {}
func (Implements[T]) integration() T { var zero T; return zero }
func (Implements[T]) implements() reflect.Type {
	var zero T
	return reflect.TypeOf(zero)
}

type implements interface {
	implements() reflect.Type
}

type Integration[T WithSpecification] interface {
	integration() T
}

// New fills an [Implementation] with the matching methods found in the configuration
// and returns it.
func New[Implementation WithSpecification](integration Integration[Implementation]) Implementation {
	var impl Implementation
	var spec Specification

	spec.Name = path.Base(reflect.TypeOf(impl).PkgPath())
	if spec.Name == "." {
		spec.Name = ""
	}

	spec.load(&impl, nil, spec.Authentication, nil, nil)

	if err := spec.configure(reflect.TypeOf(impl), reflect.ValueOf(integration)); err != nil {
		panic(err)
	}

	return impl
}
