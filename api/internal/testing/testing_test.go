package testing_test

import (
	"context"
	"log"
	"testing"

	"pkg.iqhive.com/iq/api/internal/testing/api/echo"
	"pkg.iqhive.com/iq/factor"
)

func TestComplexDependencies(t *testing.T) {
	ctx := context.Background()

	Echo := new(echo.Implementation)
	Echo.Logs = factor.New(log.Default())

	result, err := Echo.String(ctx, "hello world")
	if err != nil {
		t.Error(err)
	}
	if result != "hello world" {
		t.Error("unexpected result", result)
	}

	//rest.ListenAndServe(":"+os.Getenv("PORT"), )
}
