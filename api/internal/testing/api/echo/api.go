package echo

import (
	"context"
	"log"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/factor"
)

type API struct {
	api.Specification

	String func(context.Context, string) (string, error)
}

type Implementation struct {
	api.Implements[API]

	Logs factor.Out[*log.Logger]
}

func (echo Implementation) Factor() API {
	return API{
		String: echo.String,
	}
}

func (echo Implementation) String(ctx context.Context, s string) (string, error) {
	Logs := echo.Logs.Get()
	Logs.Println(s)
	return s, nil
}
