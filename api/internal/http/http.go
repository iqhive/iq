// Package http provides an extendable shell API based on http.
package http

import (
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/google/uuid"
	"pkg.iqhive.com/iq/api"
)

type Method string

type Header = http.Header

type HeaderWriter interface {
	WriteHeadersHTTP(http.Header)
}

type HeaderReader interface {
	ReadHeadersHTTP(http.Header)
}

// Error that can be returned to HTTP clients.
type Error interface {
	error

	//StatusHTTP should return the HTTP status code
	//relating to this error.
	StatusHTTP() int
}

// AttachQueryKeys appends any authentication keys that are configured
// to be passed as a query parameter.
func AttachQueryKeys(pkg string, keys []api.Key, endpoint string) string {
	var containsQuery = strings.Contains(endpoint, "?")

	for _, key := range keys {
		name := key.Name

		if key.Tags.Get("http").Has("query") || key.Tags.Get(pkg).Has("query") {

			if !containsQuery {
				endpoint += "?"
			} else {
				endpoint += "&"
			}

			endpoint += fmt.Sprintf("%v=%v", name, url.QueryEscape(*key.Value))
		}
	}

	return endpoint
}

// AttachKeys attaches the given keys to the request.
func AttachKeys(pkg string, keys []api.Key, req *http.Request) error {
	if pkg != "http" {
		AttachKeys("http", keys, req)
	}

	//Attach any required Authentication keys to the request.
	for _, key := range keys {
		name := key.Name

		if replace := key.Tags.Get(pkg).Value(); replace != "" {
			name = replace
		}

		tags := key.Tags.Get(pkg)

		if tags.Has("random(uuid)") {
			s := uuid.New().String()
			key.Value = &s
		}

		switch {
		case tags.Has("bearer"):
			req.Header.Set("Authorization", "Bearer "+*key.Value)
		case tags.Has("basic"):
			//fmt.Println("KEY DUMP ", key.String())
			req.Header.Set("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(*key.Value)))
		case tags.Has("header"):
			req.Header.Set(name, *key.Value)
		case tags.Has("cookie"):
			req.AddCookie(&http.Cookie{
				Name:  name,
				Value: *key.Value,
			})
		case tags.Has("query"):
		default:
			if pkg != "http" && !key.Tags.Get("api").Has("omitempty") {
				return errors.New("missing key configuration")
			}
		}
	}

	return nil
}

type responseError struct {
	Internal error

	Code    int
	Subject string
	Message string
}

func (e *responseError) StatusHTTP() int {
	if e.Code == 0 {
		e.Code = 500
	}
	return e.Code
}

func (e *responseError) Error() string {
	return e.Message
}

func (e *responseError) Unwrap() error {
	return e.Internal
}

// ResponseError converts a http.Response into an error.
func ResponseError(resp *http.Response, body []byte) error {
	var subject string

	switch resp.StatusCode {
	case http.StatusOK, http.StatusCreated, http.StatusAccepted:
		//ok

	//400s
	case http.StatusBadRequest:
		subject = "request"
	case http.StatusUnauthorized, http.StatusForbidden, http.StatusProxyAuthRequired,
		http.StatusNetworkAuthenticationRequired:
		subject = "access denied"
	case http.StatusPaymentRequired:
		subject = "wallet"
	case http.StatusNotFound, http.StatusGone:
		subject = "missing"
	case http.StatusMethodNotAllowed:
		subject = "method"
	case http.StatusNotAcceptable:
		subject = "user-agent"
	case http.StatusRequestTimeout, http.StatusGatewayTimeout:
		subject = "timeout"
	case http.StatusConflict:
		subject = "conflict"
	case http.StatusLengthRequired:
		return &responseError{
			Code:    resp.StatusCode,
			Subject: "content-length",
			Message: "please provide a Content-Length header",
		}
	case http.StatusPreconditionFailed, http.StatusPreconditionRequired:
		subject = "precondition"
	case http.StatusRequestEntityTooLarge:
		return &responseError{
			Code:    resp.StatusCode,
			Subject: "content-length",
			Message: "please provide a smaller payload",
		}
	case http.StatusRequestURITooLong:
		return &responseError{
			Code:    resp.StatusCode,
			Subject: "uri",
			Message: "please provide a smaller uri",
		}
	case http.StatusUnsupportedMediaType:
		subject = "mediatype"
	case http.StatusRequestedRangeNotSatisfiable:
		subject = "range"
	case http.StatusExpectationFailed:
		subject = "expectation"
	case http.StatusTeapot:
		subject = "teapot"
	case http.StatusMisdirectedRequest:
		subject = "misdirection"
	case http.StatusUnprocessableEntity:
		subject = "unprocessable"
	case http.StatusLocked:
		subject = "lock"
	case http.StatusFailedDependency:
		subject = "dependency"
	case http.StatusTooEarly:
		subject = "early"
	case http.StatusUpgradeRequired:
		subject = "upgrade"
	case http.StatusTooManyRequests:
		return &responseError{
			Code:    resp.StatusCode,
			Subject: "ratelimit",
			Message: "please slow down",
		}
	case http.StatusRequestHeaderFieldsTooLarge:
		return &responseError{
			Code:    resp.StatusCode,
			Subject: "header",
			Message: "please reduce the size of your header fields",
		}
	case http.StatusUnavailableForLegalReasons:
		subject = "legal"

		//500s
	case http.StatusInternalServerError:
		subject = ""

	case http.StatusNotImplemented:
		subject = "todo"

	case http.StatusBadGateway:
		subject = "gateway"

	case http.StatusServiceUnavailable:
		subject = "unavailable"

	case http.StatusHTTPVersionNotSupported, http.StatusVariantAlsoNegotiates:
		subject = "http"
	case http.StatusInsufficientStorage:
		subject = "storage"
	case http.StatusLoopDetected:
		subject = "infinite loop"
	case http.StatusNotExtended:
		subject = "request"

	default:
		subject = "unexpected"
	}

	if subject != "" {
		message := strings.TrimSpace(string(body))
		return &responseError{
			Internal: errors.New(message),
			Code:     resp.StatusCode,
			Subject:  subject,
			Message:  message,
		}
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		b, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.New("unexpected status (failed read): " + resp.Status)

		}
		return errors.New("unexpected status : " + resp.Status + " " + string(b))
	}

	return nil
}
