package api

import (
	"context"
	"fmt"
	"reflect"
	"strings"
	"testing"

	"pkg.iqhive.com/iq/oops"
)

// Documentation encapsulates the documentation for an API,
// this could consist of tutorials, guides, use-cases or
// examples. The documentation is a tree, with each node
// having it's own heading and a sequence of comments
// which can contain recorded API calls that can be
// displayed in the documentation.
//
// Implements context.Context and this context should be
// passed to any API calls made within the documentation.
type Documentation struct {
	context.Context

	Heading string

	Comments []Comment
	Sections []*Documentation

	cursor **Documentation
	test   *testing.T // used for the Test method
}

type contextKeyDocs struct{}

func record(spec Specification, docs *Documentation) {
	for i, old := range spec.Functions {
		old := old.Copy()
		fn := &spec.Functions[i]
		fn.Set(func(ctx context.Context, keys []Key, args []reflect.Value) (results []reflect.Value, err error) {
			ctx = context.WithValue(ctx, contextKeyDocs{}, true)

			results, err = old.Call(ctx, keys, args)
			if err != nil {
				return
			}
			ref := *docs.cursor
			if len(ref.Comments) == 0 {
				ref.Comments = append(ref.Comments, Comment{})
			}
			recording := &ref.Comments[len(ref.Comments)-1]
			recording.Call = fn
			recording.Args = args
			recording.Results = results
			ref.Comments = append(ref.Comments, Comment{})
			return
		})
	}
	for _, section := range spec.Sections {
		record(section, docs)
	}
}

// DocumentationOf returns combined documentation for the given
// API implementations. It should be called before any APIs are
// implemented.
func DocumentationOf(implementations ...WithDocumentation) (*Documentation, error) {
	var cursor *Documentation

	var docs = new(Documentation)
	docs.Context = context.Background()
	docs.cursor = &cursor

	cursor = docs

	for _, impl := range implementations {
		spec, err := SpecificationOf(impl)
		if err != nil {
			return nil, oops.New(err)
		}
		spec.Authentication = Authentication(func(ctx context.Context, tags Tags) (Authenticator, error) {
			return &testingAuthenticator{ctx}, nil
		})

		record(spec, docs)

		if err := impl.Documentation(docs); err != nil {
			return nil, oops.New(err)
		}
	}

	return docs, nil
}

// Add a new section to the documentation, with the specified heading and
// function. The function will be called to add the documentation for that
// section and will also be used as a test.
func (docs *Documentation) Add(heading string, fn func(*Documentation) error) {
	section := &Documentation{
		Context: docs.Context,
		Heading: heading,
		test:    docs.test,
		cursor:  docs.cursor,
	}
	docs.Sections = append(docs.Sections, section)

	backup := *docs.cursor
	*docs.cursor = section
	if err := fn(section); err != nil && docs.test != nil {
		docs.test.Fatal(err)
	}
	*docs.cursor = backup
}

// Test records a test function to be run when the API is
// being tested, if an error is returned, the test will
// fail.
func (docs *Documentation) Test(fn func() error) {
	if docs.test == nil {
		return
	}
	if err := fn(); err != nil {
		docs.test.Fatal(err)
	}
}

// Note attaches information to the current documentation.
func (docs *Documentation) Note(format string, args ...any) {
	if strings.Contains(format, "\n") {
		format = Tags(format).Doc()
	}

	if len(docs.Comments) > 0 {
		comment := &docs.Comments[len(docs.Comments)-1]
		if comment.Text == "" {
			comment.Text = fmt.Sprintf(format, args...)
			return
		}
	}

	docs.Comments = append(docs.Comments, Comment{
		Text: fmt.Sprintf(format, args...),
	})
}

// WithDocumentation is an API that is documented, the Documentation
// method will be called to extract the documentation for the API.
// This documentation must be testable, as it will be used to test
// the API.
type WithDocumentation interface {
	WithSpecification

	Documentation(*Documentation) error
}

// Comment about an API call.
type Comment struct {
	Text string

	Call *Function

	Args, Results []reflect.Value
}
