package api_test

import (
	"context"
	"errors"
	"fmt"
	"log"
	"reflect"
	"testing"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/factor"
	"pkg.iqhive.com/iq/oops"
)

type TestImplementation struct {
	api.Specification `api:"Test"`

	DoSomething func(context.Context) error

	EditCustomer func(customer string) error
}

func (API TestImplementation) Documentation(docs *api.Documentation) error {
	docs.Note("this API does something!")
	return API.DoSomething(docs)
}

func NewImplementation(core TestIntegration) *TestImplementation {
	return &TestImplementation{
		DoSomething: func(ctx context.Context) error {
			if core.action != nil {
				core.action()
			}
			fmt.Println("did something")
			return nil
		},
		EditCustomer: func(customer string) error {
			fmt.Println("edited customer", customer)
			return nil
		},
	}
}

type TestIntegration struct {
	action func()
}

type Password struct {
	context.Context

	Key string `api:"key"`
}

func (p Password) Authenticate() error {
	return nil
}

type Stringy string

type StringyPassword struct {
	context.Context

	Key Stringy `api:"key"`
}

func (p StringyPassword) Authenticate() error {
	return nil
}

func TestKeys(t *testing.T) {
	//Test value.
	keys, err := api.KeysOf(Password{context.Background(), "Hello World"})
	if err != nil {
		t.Error(err)
	}
	if *keys[0].Value != "Hello World" {
		t.Error("value not set")
	}
	//Test value passed by pointer.
	keys, err = api.KeysOf(&Password{context.Background(), "Hello World"})
	if err != nil {
		t.Error(err)
	}
	if *keys[0].Value != "Hello World" {
		t.Error("value not set")
	}
	//Test string
	var stringy = &StringyPassword{context.Background(), "Hello World"}
	keys, err = api.KeysOf(stringy)
	if err != nil {
		t.Error(err)
	}
	if string(*keys[0].Value) != string(stringy.Key) {
		t.Error("value not set")
	}
	*keys[0].Value = "Goodbye World"
	if string(stringy.Key) != "Goodbye World" {
		t.Error("value not set")
	}
}

func TestNotImplemented(t *testing.T) {
	var API struct {
		api.Specification

		DoSomething func(ctx context.Context) error
	}

	api.Test(t)

	//simulate ListenAndServe or Set
	_, err := api.SpecificationOf(&API)
	if err != nil {
		t.Error(err)
	}

	if err := API.DoSomething(context.Background()); !errors.Is(err, oops.NotImplemented()) {
		t.Error(err)
	}
}

func TestEntrypoint(t *testing.T) {

	var API = api.Entrypoint(NewImplementation, TestIntegration{})

	ctx := api.Test(nil)

	if err := API.DoSomething(ctx); err == nil {
		t.Fatal("entrypoints should return errors by default.")
	}

	ctx = api.Test(t)

	if err := API.DoSomething(ctx); err != nil {
		t.Fatal(err)
	}
}

func TestDocumentation(t *testing.T) {
	var API = api.Entrypoint(NewImplementation, TestIntegration{})
	api.Test(t, API)

	docs, err := api.DocumentationOf(API)
	if err != nil {
		t.Error(err)
	}

	if docs.Comments[0].Call == nil {
		t.Fatal("documentation should have recorded call")
	}
}

func TestSpecificationModification(t *testing.T) {
	var API = api.Entrypoint(NewImplementation, TestIntegration{})
	if API.Name != "Test" {
		t.Error("Name should be 'Test'")
	}
	API.Name = "Renamed"

	spec, err := api.SpecificationOf(API)
	if err != nil {
		t.Error(err)
	}
	if spec.Name != "Renamed" {
		t.Error("Name should be 'Renamed'")
	}
}

func TestWrapping(t *testing.T) {
	ctx := context.Background()

	var original, wrapper bool

	var API = api.Entrypoint(NewImplementation, TestIntegration{
		action: func() {
			original = true
		},
	})

	old := API.Functions[0].Copy()
	API.Functions[0].Set(func(ctx context.Context, keys []api.Key, args []reflect.Value) ([]reflect.Value, error) {
		wrapper = true
		return old.Call(ctx, keys, args)
	})

	if err := API.DoSomething(ctx); err != nil {
		t.Error(err)
	}

	if !wrapper {
		t.Fatal("wrapper was not called")
	}
	if !original {
		t.Fatal("original was not called")
	}
}

type TestAuthenticator struct {
	context.Context
}

func (TestAuthenticator) Authenticate() error {
	return nil
}

func (TestAuthenticator) Authorize(args []reflect.Value) error {
	if args[0].String() == "TestCustomer" {
		return nil
	}
	return oops.AccessDenied()
}

func TestAuthorisation(t *testing.T) {
	var API = api.Entrypoint(NewImplementation, TestIntegration{})

	API.Authentication = func(ctx context.Context, tags api.Tags) (api.Authenticator, error) {
		return TestAuthenticator{}, nil
	}

	api.Test(t)

	if err := API.EditCustomer("TestCustomer"); err != nil {
		t.Error(err)
	}
	if err := API.EditCustomer("SomeOtherCustomer"); err == nil {
		t.Error(err)
	}
}

type Integration struct {
	api.Implements[DoSomethingAPI]

	Logs factor.Out[*LogsAPI]
}

func (API Integration) DoSomething(ctx context.Context) error {
	Logs := API.Logs.Get()
	return Logs.Log(ctx, "something happened")
}

type LogsAPI struct {
	api.Specification

	Log func(context.Context, string) error
}

type LogsImplementation struct {
	api.Implements[LogsAPI]
}

func (API LogsImplementation) Log(ctx context.Context, message string) error {
	log.Print(message)
	return nil
}

func TestDependencyInjection(t *testing.T) {
	var logs = api.New[LogsAPI](LogsImplementation{})
	var impl = api.New[DoSomethingAPI](Integration{
		Logs: factor.New(&logs),
	})

	_, err := api.SpecificationOf(impl)
	if err != nil {
		t.Fatal(err)
	}

	if err := impl.DoSomething(context.Background()); err != nil {
		t.Fatal(err)
	}
}

type DoSomethingAPI struct {
	api.Specification

	DoSomething func(context.Context) error
}

type DoSomethingElseAPI struct {
	api.Specification

	DoSomethingElse func(context.Context) error
}

type Something string

type DoesSomething struct {
	api.Implements[DoSomethingAPI]

	Something Something

	DoesSomethingElse factor.Out[*DoSomethingElseAPI]
}

func (something DoesSomething) DoSomething(ctx context.Context) error {
	somethingElse := something.DoesSomethingElse.Get()
	return somethingElse.DoSomethingElse(ctx)
}

type DoesSomethingElse struct {
	api.Implements[DoSomethingElseAPI]

	Pointer factor.Out[*string]
}

func (API DoesSomethingElse) DoSomethingElse(ctx context.Context) error {
	fmt.Println(API.Pointer.Get())
	return nil
}

func TestDependencies(t *testing.T) {
	var d DoesSomethingElse
	var something DoesSomething

	somethingElse := api.New[DoSomethingElseAPI](DoesSomethingElse{
		Pointer: factor.New(new(string)),
	})
	something.DoesSomethingElse = factor.New(&somethingElse)

	if err := d.DoSomethingElse(context.Background()); err != nil {
		t.Error(err)
	}
	if err := something.DoSomething(context.Background()); err != nil {
		t.Error(err)
	}
}
