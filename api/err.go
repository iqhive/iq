package api

import (
	"errors"
	"reflect"
	"runtime"
	"strconv"
	"strings"

	"pkg.iqhive.com/iq/xray"
)

type Errors[ErrorTypes any] struct{}

func (err Errors[ErrorTypes]) errorTypes() ErrorTypes {
	var zero ErrorTypes
	var rvalue = reflect.ValueOf(&zero).Elem()
	for i := 0; i < rvalue.NumField(); i++ {
		field := rvalue.Field(i).Addr().Interface().(*Error)
		field.Name = rvalue.Type().Field(i).Name
		field.Tags = Tags(rvalue.Type().Field(i).Tag)
	}
	return zero
}

func (err Errors[ErrorTypes]) errors() []Error {
	var zero ErrorTypes
	var rvalue = reflect.ValueOf(&zero).Elem()
	var errors []Error
	for i := 0; i < rvalue.NumField(); i++ {
		field := rvalue.Field(i).Addr().Interface().(*Error)
		field.Name = rvalue.Type().Field(i).Name
		field.Tags = Tags(rvalue.Type().Field(i).Tag)
		errors = append(errors, *field)
	}
	return errors
}

type withErrors[ErrorTypes any] interface {
	errorTypes() ErrorTypes
	errors() []Error
}

type WithErrors interface {
	errors() []Error
}

func ErrorsFor[T any, Specification withErrors[T]](spec Specification) T {
	return spec.errorTypes()
}

type Error struct {
	Name string
	Tags Tags
}

type trace = xray.Trace

type errorWrapped struct {
	trace
	wrapped error
	details Error
}

func (e errorWrapped) StatusHTTP() int {
	tag := e.details.Tags.Get("http")
	if tag == "" {
		return 500
	}
	status, err := strconv.Atoi(string(tag))
	if err != nil {
		return 500
	}
	return status
}

func (e errorWrapped) Error() string {
	message := e.details.Tags.Doc()
	if message == "" {
		message = e.details.Name
	}
	return strings.Replace(message, "\n", " ", -1)
}

func (e errorWrapped) Unwrap() error {
	return e.wrapped
}

func (e Error) New(err error) error {
	return errorWrapped{
		trace:   xray.NewTrace(runtime.Caller(1)),
		wrapped: err,
		details: e,
	}
}

func ErrorOf(err error) Error {
	var e errorWrapped
	errors.As(err, &e)
	return e.details
}
