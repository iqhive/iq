package api_test

import (
	"context"
	"errors"
	"testing"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/please"
)

type WithErrors struct {
	api.Specification
	api.Errors[struct {
		Internal api.Error `http:"500"
			oops, something went wrong`
		Validation api.Error `http:"400"
			please provide valid parameters`
	}]
	DoSomething func(ctx context.Context, code int) error
}

var Error = api.ErrorsFor(WithErrors{})

func TestErrors(t *testing.T) {
	ctx := context.Background()
	var impl = WithErrors{
		DoSomething: func(ctx context.Context, code int) error {
			switch code {
			case 0:
				return Error.Validation.New(please.Provide("a valid code"))
			case 1:
				return Error.Internal.New(errors.New("failure"))
			default:
				return nil
			}
		},
	}
	err := impl.DoSomething(ctx, 0)
	switch api.ErrorOf(err) {
	case Error.Internal:
		t.Fatal("expected validation error")
	case Error.Validation:
	default:
		t.Fatal("expected validation error")
	}
	err = impl.DoSomething(ctx, 1)
	switch api.ErrorOf(err) {
	case Error.Internal:

	case Error.Validation:
		t.Fatal("expected internal error")
	default:
		t.Fatal("expected internal error")
	}
}

func TestReflectionErrors(t *testing.T) {
	spec, err := api.SpecificationOf(&WithErrors{})
	if err != nil {
		t.Fatal(err)
	}
	if len(spec.Errors) != 2 {
		t.Fatal("expected 2 errors")
	}
}
