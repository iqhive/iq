package api

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
)

// ArgumentScanner can scan arguments via a formatting pattern.
// Either %v, %[n]v or FieldName
type ArgumentScanner struct {
	args []reflect.Value
	n    int
}

func NewArgumentScanner(args []reflect.Value) ArgumentScanner {
	return ArgumentScanner{args, 0}
}

func (scanner *ArgumentScanner) Scan(format string) (reflect.Value, error) {
	switch {
	case format == "":
		return reflect.Value{}, errors.New("api.ArgumentScanner: empty format")
	case format == "%v":
	case strings.HasPrefix(format, "%[") && strings.HasSuffix(format, "]v"):
		var n int
		if _, err := fmt.Sscanf(format, "%%[%d]v", &n); err != nil {
			return reflect.Value{}, errors.New("api.ArgumentScanner: invalid format")
		}
		if n < 1 {
			return reflect.Value{}, errors.New("api.ArgumentScanner: invalid format")
		}
		if scanner.n+n > len(scanner.args) {
			return reflect.Value{}, errors.New("api.ArgumentScanner: invalid format")
		}
		return scanner.args[scanner.n+n-1], nil
	default:
	scanning:
		for _, arg := range scanner.args {
			for arg.Kind() == reflect.Pointer {
				if arg.IsNil() {
					continue scanning
				}
				arg = arg.Elem()
			}
			if arg.Kind() == reflect.Struct {
				rtype := arg.Type()
				for j := 0; j < rtype.NumField(); j++ {
					if rtype.Field(j).Name == format {
						return arg.Field(j), nil
					}
				}
			}
		}
		return reflect.Value{}, errors.New("api.ArgumentScanner: no argument named " + format)
	}
	if scanner.n < 0 || scanner.n >= len(scanner.args) {
		return reflect.Value{}, errors.New("api.ArgumentScanner: invalid argument index")
	}
	scanner.n++
	return scanner.args[scanner.n-1], nil
}
