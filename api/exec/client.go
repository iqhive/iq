package exec

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"reflect"
	"strconv"
	"strings"
	"syscall"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"
)

// Set sets the host command of the given exec api.
func Set(rest api.WithSpecification, cmd string) error {
	spec, err := api.SpecificationOf(rest)
	if err != nil {
		return oops.Wrap(err)
	}
	if err := set(spec, cmd); err != nil {
		return oops.Wrap(err)
	}
	return nil
}

type listArguments []string

func (execArgs *listArguments) add(val reflect.Value) error {
	switch val.Kind() {
	case reflect.Struct:
		rtype := val.Type()
		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)
			if !field.IsExported() {
				continue
			}
			if field.Anonymous && field.Type.Kind() == reflect.Struct {
				if err := execArgs.add(val.Field(i)); err != nil {
					return oops.Wrap(err)
				}
				continue
			}
			exec := field.Tag.Get("exec")
			if exec == "-" {
				continue
			}
			if api.Tags(field.Tag).Get("exec").Has("omitempty") && val.Field(i).IsZero() {
				continue
			}

			exec, _, _ = strings.Cut(exec, ",")

			parts := strings.Split(exec, " ")
			for _, part := range parts {
				if strings.HasPrefix(part, "%") || strings.Contains(part, "%") {
					*execArgs = append(*execArgs, fmt.Sprintf(part, val.Field(i).Interface()))
				} else {
					*execArgs = append(*execArgs, part)
				}
			}
		}
	default:
		*execArgs = append(*execArgs, fmt.Sprint(val.Interface()))
	}
	return nil
}

func set(spec api.Specification, cmd string) error {
	for _, fn := range spec.Functions {
		fn := fn
		tag := string(fn.Tags.Get("exec"))

		var isJSON bool = false
		if strings.HasSuffix(tag, " | json") {
			tag = strings.TrimSuffix(tag, " | json")
			isJSON = true
		}

		fn.Set(func(ctx context.Context, keys []api.Key, args []reflect.Value) (results []reflect.Value, err error) {
			scanner := api.NewArgumentScanner(args)

			var execArgs listArguments
			for _, component := range strings.Split(string(tag), " ") {
				if strings.HasPrefix(component, "%") || strings.HasPrefix(component, "{") {
					component = strings.Trim(component, "{}")
					val, err := scanner.Scan(component)
					if err != nil {
						return nil, oops.Wrap(err)
					}
					if err := execArgs.add(val); err != nil {
						return nil, oops.Wrap(err)
					}
				} else {
					execArgs = append(execArgs, component)
				}
			}

			var stdout bytes.Buffer
			var stderr bytes.Buffer

			xray.Scan(ctx, cmd, execArgs)

			stdoutRead, stdoutWrite, err := os.Pipe()
			if err != nil {
				return nil, oops.Wrap(err)
			}
			stderrRead, stderrWrite, err := os.Pipe()
			if err != nil {
				return nil, oops.Wrap(err)
			}

			cmd := exec.CommandContext(ctx, cmd, execArgs...)
			cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
			cmd.Cancel = func() error {
				if err := stdoutWrite.Close(); err != nil {
					return oops.Wrap(err)
				}
				if err := stderrWrite.Close(); err != nil {
					return oops.Wrap(err)
				}
				if err := syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL); err != nil {
					return cmd.Process.Kill()
				}
				return nil
			}

			if fn.Type.NumOut() > 0 {
				cmd.Stdout = stdoutWrite
			} else {
				cmd.Stdout = os.Stdout
			}

			cmd.Stderr = stderrWrite

			go io.Copy(&stdout, stdoutRead)
			go io.Copy(&stderr, stderrRead)

			if err := cmd.Run(); err != nil {
				if text := stderr.String(); strings.TrimSpace(text) != "" {
					return nil, errors.New(text)
				}
				return nil, oops.New(err)
			}

			if fn.Type.NumOut() > 0 {
				if isJSON {
					var result = reflect.New(fn.Type.Out(0)).Interface()
					if err := json.NewDecoder(&stdout).Decode(result); err != nil {
						return nil, oops.Wrap(err)
					}
					return []reflect.Value{reflect.ValueOf(result).Elem()}, nil
				} else {

					if fn.Type.NumOut() == 1 {
						var value = reflect.New(fn.Type.Out(0)).Elem()
						switch fn.Type.Out(0).Kind() {
						case reflect.String:
							result := stdout.String()
							result = strings.TrimSuffix(result, "\n")
							return []reflect.Value{reflect.ValueOf(result)}, nil
						case reflect.Slice:
							var lines = bufio.NewReader(&stdout)
							var result = reflect.MakeSlice(fn.Type.Out(0), 0, 0)
							for {
								line, err := lines.ReadString('\n')
								if err != nil {
									if err == io.EOF {
										break
									}
									return nil, oops.Wrap(err)
								}
								line = strings.TrimSuffix(line, "\n")
								elem := reflect.New(fn.Type.Out(0).Elem()).Elem()
								elem.SetString(line)
								result = reflect.Append(result, elem)
							}
							return []reflect.Value{result}, nil
						case reflect.Int32:
							result := stdout.String()
							result = strings.TrimSuffix(result, "\n")
							i, err := strconv.Atoi(result)
							if err != nil {
								return nil, oops.Wrap(err)
							}
							value.SetInt(int64(i))
							return []reflect.Value{value}, nil
						}
					}

					return nil, fmt.Errorf("exec: return type %v: not implemented", fn.Type.Out(0))
				}
			}

			return
		})

	}
	for _, section := range spec.Sections {
		if err := set(section, cmd); err != nil {
			return oops.Wrap(err)
		}
	}
	return nil
}
