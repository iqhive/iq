// Package nano is a time package for storing and working with nano times.
package nano

import (
	"fmt"
	"time"

	"pkg.iqhive.com/iq/api"
)

const (
	//Second is a single nano second
	Second Time = 1
)

// Time is time value measured as the number of nanoseconds since the Unix epoch.
type Time int64

func (t Time) String() string {
	return time.Unix(0, int64(t)).String()
}

func (t Time) MarshalText() (string, error) {
	return fmt.Sprintf("%v", int64(t)), nil
}

// TimeSince returns the time since the specified nano time.
func TimeSince(t Time) Time {
	return Now() - t
}

func (t *Time) UnmarshalText(s string) error {
	var i int64
	_, err := fmt.Sscanf(s, "%v", &i)
	if err != nil {
		return err
	}
	*t = Time(i)
	return nil
}

// Now returns time.Now as a nano time.
func Now() Time {
	return Time(time.Now().UnixNano())
}

func (t Time) Add(d time.Duration) Time {
	return t + Time(d)
}

// Range of time represented as two unix nanosecond timestamps (inclusive).
type Range struct {
	api.Model `
		of time represented as two unix nanosecond timestamps (inclusive).`

	From Time `
		is the start of the range.`
	Upto Time `
		is the end of the range.`
}
