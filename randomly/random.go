// Package randomly provides methods of generating mocked values for testing and documentation.
package randomly

import (
	"context"
	"math"
	"math/rand"
	"reflect"
	"strings"
	"unsafe"
)

type Seed uint64

type randKey struct{}

// Generated returns a new 'mocked' instance of the type T.
// If the value is a struct, field names are used to
// determine how to mock the value, based on registered
// mock functions. Otherwise the values are completely
// randomized based on their type. The seed for this is
// derived from the context's [journal.ID]. Change this
// in order to generate different values. Randomisation
// is deterministic given the same sequence of calls
// to New and the same [journey.Journal].
func Generated[T any](ctx context.Context) T {
	var value T

	source, ok := ctx.Value(randKey{}).(rand.Source64)
	if !ok {
		source = rand.NewSource(0).(rand.Source64)
	}

	mock(source, "", &value)
	return value
}

// Seeded returns a seeded context.
func Seeded(ctx context.Context, seed int64) context.Context {
	return context.WithValue(ctx, randKey{}, rand.NewSource(seed))
}

func mock(source rand.Source64, name string, value any) {
	if mocker, ok := value.(interface {
		Mock()
	}); ok {
		mocker.Mock()
		return
	}

	v := reflect.ValueOf(value).Elem()

	if mockers, ok := mocks[name]; ok {
		if mocker, ok := mockers[v.Kind()]; ok {
			v.Set(mocker(Seed(source.Uint64())).Convert(v.Type()))
			return
		}
	}

	random := rand.New(source)

	switch v.Kind() {
	case reflect.Bool:
		v.SetBool(source.Uint64()%2 == 0)

	case reflect.Int8:
		v.SetInt(int64(random.Int31n(1<<8)) - int64(1<<7))
	case reflect.Int16:
		v.SetInt(int64(random.Int31n(1<<16)) - int64(1<<15))
	case reflect.Int32:
		v.SetInt(int64(random.Int63n(1<<32)) - int64(1<<31))
	case reflect.Int:
		if v.Type().Bits() == 32 {
			v.SetInt(int64(random.Int63n(1<<32)) - int64(1<<31))
			return
		}
		fallthrough
	case reflect.Int64:
		var u = random.Uint64()
		v.SetInt(*(*int64)(unsafe.Pointer(&u)))

	case reflect.Uint8:
		v.SetUint(uint64(random.Int31n(1 << 8)))
	case reflect.Uint16:
		v.SetUint(uint64(random.Int31n(1 << 16)))
	case reflect.Uint32:
		v.SetUint(uint64(random.Int63n(1 << 32)))
	case reflect.Uint:
		if v.Type().Bits() == 32 {
			v.SetUint(uint64(random.Int63n(1 << 32)))
		} else {
			v.SetUint(source.Uint64())
		}
	case reflect.Uint64:
		v.SetUint(source.Uint64())

	case reflect.Float64:
		v.SetFloat(math.Float64frombits(source.Uint64()))
	case reflect.Float32:
		v.SetFloat(float64(math.Float32frombits(uint32(source.Uint64() % (1 << 32)))))
	case reflect.String:
		v.SetString(Strings[source.Uint64()%uint64(len(Strings))])
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			field := v.Type().Field(i)
			if !field.IsExported() {
				continue
			}
			name := strings.ToLower(field.Name)
			if tag, ok := field.Tag.Lookup("mock"); ok {
				name = tag
			}
			mock(source, name, v.Field(i).Addr().Interface())
		}
	}
}

type Tags []string

var mocks = make(map[string]map[reflect.Kind]func(Seed) reflect.Value)

// Register a deterministic mocking function that returns different
// values depending on the seed. If names are provided, they are
// used to determine how to mock the value. Based on lower-cased
// field name or the field's mock tag.
func Register[T any](tags Tags, mock func(Seed) T) {
	var zero T
	for _, tag := range tags {
		if mocks[tag] == nil {
			mocks[tag] = make(map[reflect.Kind]func(Seed) reflect.Value)
		}
		mocks[tag][reflect.TypeOf(zero).Kind()] = func(seed Seed) reflect.Value {
			return reflect.ValueOf(mock(seed))
		}
	}
}
