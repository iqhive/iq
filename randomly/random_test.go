package randomly_test

import (
	"context"
	"testing"

	"pkg.iqhive.com/iq/randomly"
	"pkg.iqhive.com/iq/xray"
)

func TestMock(t *testing.T) {
	type Value struct {
		S string
		I int64
	}

	ctx := randomly.Seeded(context.Background(), 0)

	xray.Print(randomly.Generated[Value](ctx))
	xray.Print(randomly.Generated[Value](ctx))
	xray.Print(randomly.Generated[Value](ctx))
}
