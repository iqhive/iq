package bitflag_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/bitflag"
)

type DeliveryPreferences struct {
	DeliverToEmail bool
	DeliverToPhone bool
	DeliverToHouse bool
}

func TestBitFlags(t *testing.T) {
	var flags = bitflag.New64(DeliveryPreferences{
		DeliverToEmail: true,
		DeliverToPhone: false,
		DeliverToHouse: true,
	})

	fmt.Printf("%b\n", flags)

	var preferences = flags.Get()
	fmt.Printf("%+v\n", preferences)

	fmt.Println(flags)
}
