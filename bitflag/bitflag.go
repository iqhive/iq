/*
Package bitflag provides marshalable and documentable bitflags.
Model your bitflags as a struct of boolean values and use one
of the bit containers for them.

	var flags = bitflag.New64(DeliveryPreferences{
		DeliverToEmail: true,
		DeliverToPhone: false,
		DeliverToHouse: true,
	})

	fmt.Println(flags.Get())
*/
package bitflag

import (
	"encoding/json"
	"reflect"
	"strings"

	"pkg.iqhive.com/iq/oops"
)

// Bits64 can store up to 64 flags.
type Bits64[Values comparable] uint64

// New64 returns a new bitflag from the given boolean
// structure.
func New64[Values comparable](values Values) Bits64[Values] {
	rvalue := reflect.ValueOf(values)
	rtype := rvalue.Type()

	if rtype.Kind() != reflect.Struct {
		return 0
	}

	if rtype.NumField() > 64 {
		panic("too many fields")
	}

	var flags Bits64[Values]
	for i := 0; i < rtype.NumField(); i++ {
		if !rtype.Field(i).IsExported() {
			continue
		}
		if !rvalue.Field(i).IsZero() {
			flags |= 1 << i
		}
	}
	return flags
}

// TypeJSON returns the JSON marshaled structure of the
// bitflag.
func (flags Bits64[Values]) TypeJSON() reflect.Type {
	var values Values
	return reflect.TypeOf(values)
}

// String implements [fmt.Stringer].
func (flags Bits64[Values]) String() string {
	var values Values
	rtype := reflect.ValueOf(values).Type()

	if rtype.NumField() > 64 {
		panic("too many fields")
	}

	var names []string
	for i := 0; i < rtype.NumField(); i++ {
		if !rtype.Field(i).IsExported() {
			continue
		}
		if flags&(1<<i) != 0 {
			names = append(names, rtype.Field(i).Name)
		}
	}

	return strings.Join(names, "|")
}

// Get returns the boolean structure of the bitflag.
func (flags Bits64[Values]) Get() Values {
	var values Values

	rvalue := reflect.ValueOf(&values).Elem()
	rtype := rvalue.Type()

	if rtype.NumField() > 64 {
		panic("too many fields")
	}

	for i := 0; i < rtype.NumField(); i++ {
		if !rtype.Field(i).IsExported() {
			continue
		}
		if flags&(1<<i) != 0 {
			rvalue.Field(i).SetBool(true)
		}
	}

	return values
}

// MarshalJSON implements [json.Marshaler].
func (flags Bits64[Values]) MarshalJSON() ([]byte, error) {
	return json.Marshal(flags.Get())
}

// UnmarshalJSON implements [json.Unmarshaler].
func (flags *Bits64[Values]) UnmarshalJSON(b []byte) error {
	var values Values
	if err := json.Unmarshal(b, &values); err != nil {
		return oops.New(err)
	}
	*flags = New64(values)
	return nil
}
