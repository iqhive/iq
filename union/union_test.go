package union_test

import (
	"bufio"
	"encoding/json"
	"io"
	"os"
	"testing"

	"pkg.iqhive.com/iq/union"
)

type Triplet = union.Any[Triplets]

type Triplets struct {
	One   union.Add[Triplets, One]
	Two   union.Add[Triplets, Two]
	Three union.Add[Triplets, Three]
}

func NewTriplet() Triplets {
	return union.New[Triplets]()
}

type One struct{}

func (One) String() string { return "ONE" }

type Two struct{}

func (Two) String() string { return "TWO" }

type Three struct{}

func (Three) String() string { return "THREE" }

func TestUnion(t *testing.T) {
	var triplet Triplet
	triplet.Switch(Triplets{
		union.Case(triplet, func(one One) {}),
		union.Case(triplet, func(two Two) { t.Fatal() }),
		union.Case(triplet, func(three Three) { t.Fatal() }),
	})
	triplet = NewTriplet().One(One{})
	triplet.Switch(Triplets{
		union.Case(triplet, func(one One) {}),
		union.Case(triplet, func(two Two) { t.Fatal() }),
		union.Case(triplet, func(three Three) { t.Fatal() }),
	})
	triplet = NewTriplet().Two(Two{})
	triplet.Switch(Triplets{
		union.Case(triplet, func(one One) { t.Fatal() }),
		union.Case(triplet, func(two Two) {}),
		union.Case(triplet, func(three Three) { t.Fatal() }),
	})
	triplet = NewTriplet().Three(Three{})
	triplet.Switch(Triplets{
		union.Case(triplet, func(one One) { t.Fatal() }),
		union.Case(triplet, func(two Two) { t.Fatal() }),
		union.Case(triplet, func(three Three) {}),
	})

	b, err := json.Marshal(triplet)
	if err != nil {
		t.Fatal(err)
	}
	triplet = Triplet{}
	if err := json.Unmarshal(b, &triplet); err != nil {
		t.Fatal(err)
	}

	triplet.Switch(Triplets{
		union.Case(triplet, func(one One) { t.Fatal() }),
		union.Case(triplet, func(two Two) { t.Fatal() }),
		union.Case(triplet, func(three Three) {}),
	})
}

type readerValue[T io.Reader] struct{ union.Field[Reader, T] }
type Reader union.Interface[io.Reader, struct {
	Bufio readerValue[*bufio.Reader]
	File  readerValue[*os.File]
}]

var Readers = new(Reader).Values()

func TestInterface(t *testing.T) {
	var b = new(bufio.Reader)

	var reader = Readers.Bufio.New(b)

	switch union.Tag(reader) {
	case Readers.Bufio.Value:
	case Readers.File.Value:
		t.Fatal("should be a bufio")
	}

	reader = Readers.File.New(os.Stdin)

	switch union.Tag(reader) {
	case Readers.Bufio.Value:
		t.Fatal("should be a file")
	case Readers.File.Value:
	}

	reader = Reader{}
}

type ABC union.Interface[any, struct {
	A union.Field[ABC, string] `json:"a"`
	B union.Field[ABC, int]    `json:"data?type=b"`
	C union.Field[ABC, bool]
	D union.Field[ABC, Vec3] `json:"|?type=vec3"`
}]

var ABCs = new(ABC).Values()

type Vec3 struct {
	X, Y, Z float64
}

func TestJSON(t *testing.T) {
	var result ABC

	a, err := json.Marshal(ABCs.A.New("hello world"))
	if err != nil {
		t.Fatal(err)
	}
	if string(a) != `{"a":"hello world"}` {
		t.Fatal(string(a))
	}
	if err := json.Unmarshal(a, &result); err != nil {
		t.Fatal(err)
	}
	if result.String() != "hello world" {
		t.Fatal(result.String())
	}

	b, err := json.Marshal(ABCs.B.New(22))
	if err != nil {
		t.Fatal(err)
	}
	if string(b) != `{"data":22,"type":"b"}` {
		t.Fatal(string(b))
	}
	if err := json.Unmarshal(b, &result); err != nil {
		t.Fatal(err)
	}
	if result.String() != "22" {
		t.Fatal(result.String())
	}

	c, err := json.Marshal(ABCs.C.New(true))
	if err != nil {
		t.Fatal(err)
	}
	if string(c) != `{"C":true}` {
		t.Fatal(string(c))
	}
	if err := json.Unmarshal(c, &result); err != nil {
		t.Fatal(err)
	}
	if result.String() != "true" {
		t.Fatal(result.String())
	}

	d, err := json.Marshal(ABCs.D.New(Vec3{1, 2, 3}))
	if err != nil {
		t.Fatal(err)
	}
	if string(d) != `{"X":1,"Y":2,"Z":3,"type":"vec3"}` {
		t.Fatal(string(d))
	}
	if err := json.Unmarshal(d, &result); err != nil {
		t.Fatal(err)
	}
	if result.String() != "{1 2 3}" {
		t.Fatal(result.String())
	}
}
