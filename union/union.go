/*
Package union provides sum types / discriminated unions / variant types.

	type CanBeStringOrInt union.Interface[any, stringOrInt]

	type stringOrInt struct {
		String union.Field[CanBeStringOrInt, any, stringOrInt, string]
		Int    union.Field[CanBeStringOrInt, any, stringOrInt, int]
	}

	var StringOrInt = union.New[stringOrInt]()

	var result CanBeStringOrInt
	result = StringOrInt.String.New("hello world")
	StringOrInt.String.Get(result) // "hello world"

	result = StringOrInt.Int.New(22)
	StringOrInt.Int.Get(result) // 22
*/
package union

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	"pkg.iqhive.com/iq/api"
)

// Any is a sum type / discriminated union / variant type
// the type parameter should refer to a named Fields struct
// with sum.Add fields that add to the sum of types.
type Interface[Implements any, Values any] struct {
	Methods[Implements, Values]
}

type isInterface interface {
	Interface() any
	int() int
}

func Tag[T isInterface](val T) tag[T] {
	return tag[T]{
		int: val.int(),
	}
}

type Field[T isInterface, Add any] struct {
	Value tag[T]
}

func (a Field[T, Add]) Type() reflect.Type {
	return reflect.TypeOf([0]Add{}).Elem()
}

func (a *Field[T, Add]) init2(i int) {
	*a = Field[T, Add]{Value: tag[T]{int: i}}
}

func (tag Field[T, Add]) New(val Add) T {
	var zero T
	any(&zero).(interface{ set(int, any) }).set(tag.Value.int, val)
	return zero
}

func (tag Field[T, Add]) Get(val T) Add {
	if val.int() != tag.Value.int || tag.Value.int == 0 {
		var zero Add
		return zero
	}
	return any(val.Interface()).(Add)
}

type Methods[Implements any, Values any] struct {
	any Implements
	tag int // needed to distinguish fields of the same type and enable an O(1) Switch.
}

func (i Methods[Implements, Values]) Get() Implements {
	return i.any
}

func (i Methods[Implements, Values]) Interface() any {
	return i.any
}

func (i Methods[Implements, Values]) int() int {
	return i.tag
}

func (i *Methods[Implements, Values]) set(tag int, val any) {
	i.tag = tag
	i.any = val.(Implements)
}

func (union *Methods[Implements, Values]) Values() Values {
	return Any[Values]{}.sum()
}

// MarshalJSON implements json.Marshaler.
func (i Methods[Implements, Values]) MarshalJSON() ([]byte, error) {
	if i.int() == 0 {
		return []byte("null"), nil
	}
	var fields Values
	var field = reflect.TypeOf(fields).Field(i.int() - 1)

	tag, ok := field.Tag.Lookup("json")
	if !ok {
		return json.Marshal(map[string]any{
			field.Name: i.any,
		})
	}
	value, merge, ok := strings.Cut(tag, "?")
	if !ok {
		return json.Marshal(map[string]any{
			value: i.any,
		})
	}
	key, val, ok := strings.Cut(merge, "=")
	if !ok {
		val = field.Name
	}
	rtype := reflect.TypeOf(i.any)
	if value == "|" && rtype.Kind() == reflect.Struct {
		merged := reflect.New(reflect.StructOf([]reflect.StructField{
			{
				Name:      "UnionValue",
				Anonymous: true,
				Type:      rtype,
			},
			{
				Name: "UnionType",
				Tag:  reflect.StructTag(`json:"` + key + `"`),
				Type: reflect.TypeOf(val),
			},
		})).Elem()
		merged.Field(0).Set(reflect.ValueOf(i.any))
		merged.Field(1).Set(reflect.ValueOf(val))
		return json.Marshal(merged.Interface())
	}
	return json.Marshal(map[string]any{
		key:   val,
		value: i.any,
	})
}

func (union *Methods[Implements, Values]) String() string {
	return fmt.Sprintf("%v", union.any)
}

func (union *Methods[Implements, Values]) UnmarshalJSON(b []byte) error {
	var decoded = make(map[string]json.RawMessage)
	if err := json.Unmarshal(b, &decoded); err != nil {
		return err
	}

	alloc := func(field reflect.StructField) reflect.Value {
		return reflect.New(reflect.Zero(field.Type).Interface().(interface{ Type() reflect.Type }).Type())
	}

	var values Values
	rtype := reflect.TypeOf(values)
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}
		name, ok := field.Tag.Lookup("json")
		if !ok {
			name = field.Name
		}
		name, query, ok := strings.Cut(name, "?")
		if !ok {
			b, ok := decoded[name]
			if ok {
				var value = alloc(field)
				if err := json.Unmarshal(b, value.Interface()); err != nil {
					return err
				}
				union.any = value.Elem().Interface().(Implements)
				union.tag = i + 1
				return nil
			}
			continue
		}
		key, val, ok := strings.Cut(query, "=")
		if !ok {
			val = field.Name
		}
		if kind, ok := decoded[key]; !ok || !bytes.Equal(kind, []byte(`"`+val+`"`)) {
			continue
		}
		var value = alloc(field)
		if name != "|" {
			b = []byte(decoded[name])
		}
		if err := json.Unmarshal(b, value.Interface()); err != nil {
			return err
		}
		union.any = value.Elem().Interface().(Implements)
		union.tag = i + 1
		return nil
	}
	return nil
}

type tag[T any] struct {
	ptr *[0]T
	int
}

func New[Sum any]() Sum {
	return Any[Sum]{}.sum()
}

// Type describes an emumerated type.
// Consisting of a collection of named values.
type Type struct {
	Tags api.Tags

	Values []Entry
}

// Value describes a named field within an
// union type.
type Entry struct {
	Tags api.Tags

	Name string
	Type reflect.Type
}

// TypeOf returns a reflected view of the enum.
func TypeOf(v reflect.Type) (Type, bool) {
	for v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	type enumerable interface {
		values() []Entry
	}
	if v.Implements(reflect.TypeOf((*enumerable)(nil)).Elem()) {
		union := reflect.New(v).Elem().Interface().(enumerable)

		return Type{
			Tags:   api.Tags(`json:"string"`),
			Values: union.values(),
		}, true
	}

	return Type{}, false
}

func (union Any[Sum]) values() []Entry {
	var sum Sum
	rtype := reflect.TypeOf(sum)
	fields := make([]Entry, 0, rtype.NumField())
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}

		meta, ok := reflect.Zero(field.Type).Interface().(interface {
			Type() reflect.Type
		})
		if ok {
			fields = append(fields, Entry{
				Tags: api.Tags(field.Tag),
				Name: field.Name,
				Type: meta.Type(),
			})
		}
	}
	return fields
}

// Sum returns a set of fields that can be used to set the value of
// a sum type, you may wish to initialise this as a global variable
// to cache the result.
func (Any[Sum]) sum() Sum {
	var sum Sum
	rvalue := reflect.ValueOf(&sum).Elem()
	for i := 0; i < rvalue.NumField(); i++ {
		switch field := rvalue.Field(i).Addr().Interface().(type) {
		case interface{ init(int) }:
			field.init(i)
		case interface{ init2(int) }:
			field.init2(i + 1)
		}
	}
	return sum
}

// Any is a sum type / discriminated union / variant type
// the type parameter should refer to a named Fields struct
// with sum.Add fields that add to the sum of types.
type Any[Sum any] struct {
	any
	tag int // needed to distinguish fields of the same type and enable an O(1) Switch.
}

// String implements fmt.Stringer.
func (sum Any[Sum]) String() string {
	return fmt.Sprint(sum.any)
}

// MarshalJSON implements json.Marshaler.
func (sum Any[Sum]) MarshalJSON() ([]byte, error) {
	var fields Sum
	var container struct { // TODO configurable container? struct tags?
		Type string      `json:"type"`
		Data interface{} `json:"data"`
	}
	container.Type = reflect.TypeOf(fields).Field(sum.tag).Name
	container.Data = sum.any
	return json.Marshal(container)
}

// MarshalJSON implements json.Unmarshaler.
func (sum *Any[Sum]) UnmarshalJSON(data []byte) error {
	var container struct { // TODO configurable container? struct tags?
		Type string          `json:"type"`
		Data json.RawMessage `json:"data"`
	}
	if err := json.Unmarshal(data, &container); err != nil {
		return err
	}
	if container.Data == nil {
		*sum = Any[Sum]{}
		return nil
	}

	var fields Sum
	rtype := reflect.TypeOf(fields)
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}
		if field.Name == container.Type {
			var meta = reflect.Zero(field.Type).Interface().(interface {
				Type() reflect.Type
			})
			var zero = reflect.New(meta.Type())
			if err := json.Unmarshal(container.Data, zero.Interface()); err != nil {
				return err
			}
			sum.any = zero.Elem().Interface()
			sum.tag = i
			return nil
		}
	}
	return fmt.Errorf("unknown type %s", container.Type)
}

// Add is used to add fixed types to the Fields of a Type.
type Add[Sum any, T any] func(T) Any[Sum]

func (field *Add[Sum, T]) init(index int) {
	*field = func(val T) Any[Sum] {
		return Any[Sum]{
			any: val,
			tag: index,
		}
	}
}

func (field Add[Sum, T]) Type() reflect.Type {
	return reflect.TypeOf([0]T{}).Elem()
}

func (field Add[Sum, T]) switchable(u Any[Sum]) {
	v, ok := u.any.(T)
	if field != nil && ok {
		field(v)
	}
}

// Switch can be used to read the value of a sum type, the cases
// can should be added with Case calls. If the cases are named, then
// this is a non-exhaustive switch. If the cases are not named, then
// the switch is exhaustive. You can force a type switch to be non
// exhaustive by adding a _ struct{} field to the Fields of the sum
// type.
func (val Any[Sum]) Switch(cases Sum) {
	switcher, ok := reflect.ValueOf(cases).Field(val.tag).Interface().(interface {
		switchable(Any[Sum])
	})
	if ok {
		switcher.switchable(val)
	}
}

// Case can be passed to a switch to run a function when the associated
// type in the sum type is matched. The val passed to this function
// should refer to the value being switched on.
func Case[Sum any, T any](val Any[Sum], fn func(T)) Add[Sum, T] {
	return func(v T) Any[Sum] {
		fn(v)
		return val
	}
}
