package keep_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/keep"
)

func TestKeep(t *testing.T) {
	var name keep.Private = "Alice"
	fmt.Println(name)

	var pass = keep.Secure("1234")
	fmt.Println(pass)

	_ = pass.IsKeptSecureBecause("we are ignoring the result")
}
