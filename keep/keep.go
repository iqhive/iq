// Package keep encourages the protection of personal information and secrets.
package keep

// Private is value that contains personal information and needs to be kept
// private.
type Private string

// String returns a masked string.
func (p Private) String() string {
	return "<private>"
}

// Secured is a value that contains a secret, perhaps an API key or private key
// it needs additional caution whenever it is used. It cannot be encoded or
// decoded.
type Secured struct {
	string
}

// Secure the given string.
func Secure(s string) Secured {
	return Secured{s}
}

// String returns a string of asterisks.
func (s Secured) String() string {
	return "<secured>"
}

// IsKeptSecureBecause returns the raw string if the reason is not empty.
// The reason must explain why and how the string is kept secure.
func (s Secured) IsKeptSecureBecause(reason string) string {
	if len(reason) > 0 {
		return s.string
	}
	return ""
}
