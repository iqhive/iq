package factor_test

import (
	"testing"

	"pkg.iqhive.com/iq/factor"
)

func TestFactor(t *testing.T) {
	var fn factor.Out[func() error]
	if fn.Get()() == nil {
		t.Error("expected error")
	}

	fn = factor.New(func() error {
		return nil
	})
	if fn.Get()() != nil {
		t.Error("expected no error")
	}

	var api factor.Out[struct {
		DoSomething func() error
	}]
	if api.Get().DoSomething() == nil {
		t.Error("expected error")
	}

	var ptr factor.Out[*int]
	if ptr.Get() == nil {
		t.Error("expected non-nil pointer")
	}

	type recursive struct {
		Next *recursive
	}
	var rec factor.Out[*recursive]
	if rec.Get() == nil {
		t.Error("expected non-nil recursive pointer")
	}

	type partial struct {
		Implemented    func() error
		NotImplemented func() error
	}
	var impl partial
	impl.Implemented = func() error {
		return nil
	}

	var p = factor.New(impl)
	if p.Get().Implemented() != nil {
		t.Error("expected no error")
	}
	if p.Get().NotImplemented() == nil {
		t.Error("expected error")
	}
}
