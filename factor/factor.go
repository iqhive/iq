// Package factor provides safe, panic-free dependency injection.
package factor

import (
	"fmt"
	"reflect"
)

// Out that can be injected into an implementation.
type Out[T any] struct {
	value T
	valid bool
}

func valid(value reflect.Value) bool {
	switch value.Kind() {
	case reflect.Pointer:
		if value.IsNil() {
			return false
		}
		return valid(value.Elem())
	case reflect.Struct:
		for i := 0; i < value.NumField(); i++ {
			if !valid(value.Field(i)) {
				return false
			}
		}
	case reflect.Map, reflect.Func, reflect.Interface, reflect.Chan, reflect.UnsafePointer:
		return !value.IsNil()
	}
	return true
}

func merge(done map[reflect.Type]bool, value reflect.Value, intoAddr reflect.Value) {
	switch value.Kind() {
	case reflect.Pointer:
		intoAddr.Set(alloc(done, value.Type()))
		if value.IsNil() {
			return
		}
		merge(done, value.Elem(), intoAddr.Elem())
	case reflect.Struct:
		for i := 0; i < value.NumField(); i++ {
			if intoAddr.Field(i).CanSet() {
				merge(done, value.Field(i), intoAddr.Field(i))
			}
		}
		return
	case reflect.Map, reflect.Func, reflect.Interface, reflect.Chan, reflect.UnsafePointer:
		if value.IsNil() {
			intoAddr.Set(alloc(done, value.Type()))
			return
		}
	}
	intoAddr.Set(value)
}

func alloc(done map[reflect.Type]bool, value reflect.Type) reflect.Value {
	if done[value] {
		return reflect.Zero(value)
	}
	done[value] = true

	switch value.Kind() {
	case reflect.Pointer:
		ptr := reflect.New(value.Elem())
		ptr.Elem().Set(alloc(done, value.Elem()))
		return ptr
	case reflect.Struct:
		structure := reflect.New(value).Elem()
		for i := 0; i < structure.NumField(); i++ {
			if structure.Field(i).CanSet() {
				structure.Field(i).Set(alloc(done, structure.Field(i).Type()))
			}
		}
		return structure
	case reflect.Map:
		return reflect.MakeMap(value)
	case reflect.Chan:
		return reflect.MakeChan(value, 0)
	case reflect.Interface:
		if fallback, ok := fallbacks[value]; ok {
			return fallback
		}
	case reflect.Func:
		length := value.NumOut()
		results := make([]reflect.Value, length)
		for i := 0; i < value.NumOut(); i++ {
			if i == length-1 && value.Out(i) == reflect.TypeOf((*error)(nil)).Elem() {
				results[i] = reflect.ValueOf(fmt.Errorf("nil function call"))
				continue
			}
			results[i] = alloc(done, value.Out(i))
		}
		return reflect.MakeFunc(value, func([]reflect.Value) []reflect.Value {
			return results
		})
	}
	return reflect.Zero(value)
}

// New returns a new dependency, set to the
// given pointer (which may be nil).
func New[T any](dependency T) Out[T] {
	if valid(reflect.ValueOf(dependency)) {
		return Out[T]{value: dependency, valid: true}
	}
	var rtype = reflect.TypeOf([0]T{}).Elem()
	var value = reflect.ValueOf(dependency)
	if rtype.Kind() == reflect.Interface && !value.IsNil() {
		return Out[T]{value: dependency, valid: true}
	}
	if rtype.Kind() == reflect.Interface && value.IsNil() {
		return Out[T]{valid: false}
	}
	var done = make(map[reflect.Type]bool)
	var dst = reflect.New(reflect.TypeOf(dependency)).Elem()
	merge(done, value, dst)
	return Out[T]{value: dst.Interface().(T), valid: true}
}

// Get returns a value that can be safely used without
// producing any nil pointer dereferences. If T contains
// recursive pointers or T is an interface and it hasn't
// been provided with New or Fallback, then the value may
// still contain nils.
func (dep Out[T]) Get() T {
	if dep.valid {
		return dep.value // fast path can be inlined.
	}
	var zero [0]T
	var done = make(map[reflect.Type]bool)
	return alloc(done, reflect.TypeOf(zero).Elem()).Interface().(T)
}

// Lookup is like [Out.Get] except it returns a boolean
// indicating whether the underlying value is available
// or not.
func (dep Out[T]) Lookup() (T, bool) {
	if dep.valid {
		return dep.value, true // fast path can be inlined.
	}
	var zero [0]T
	var done = make(map[reflect.Type]bool)
	return alloc(done, reflect.TypeOf(zero).Elem()).Interface().(T), false
}

var fallbacks = make(map[reflect.Type]reflect.Value)

// Fallback registers a fallback implementation for the
// given interface type T. It should do nothing except
// return errors or zero values.
func Fallback[T any](fallback T) {
	var zero [0]T
	rtype := reflect.TypeOf(zero).Elem()
	switch rtype.Kind() {
	case reflect.Interface:
		fallbacks[rtype] = reflect.ValueOf(fallback)
	}
}

type Dependencies[T any] Out[T]

type Underlying[T any] interface {
	~struct {
		value T
		valid bool
	}
}
