# IQ
An A-Z toolkit for writing type-safe distributed services in Go.

Module documentation is not publicly hosted, but you can view it 
by running `godoc -http=:8080` inside of a module that IQ has been
required in.

## Communication
```c
api //specification.
mq //message queues.
```
## Configuration
```c
service //mapping and discovery.
config //configuration.
```
## Storage
```c
vfs //blob/files
db //databases.
```
## Serialisation
```c
box //format.
yr //dynamic serialisation.
```
## Syncronisation
```c
netsync //ratelimiting & consensus.
onetime //idempotency.
```
## Performance 
```c
funpack //batching / singleflight
keep //caching
```
## Logging & Metrics
```c
llog //leveled logging with context.
xp //metrics and stats.
```
## Error Handling
```c
please //error tracing & tagging.
retry //retry logic.
```
## Code Generation
```c
jit //runtime code generation toolkit.
gex //General expressions (regexp).
```
## Reflection
```c
qtags //struct tag parsing.
where //query structure fields.
```
## Testing
```c
unit //unit testing assertions and helpers.
etc //mocking structures and dummy values.
```

## Debugging
```c
trace //tracing & traceable context and errors.
zz //"lazy" debugging package.
```

## Company packages
```c
hive //IQ Hive specific directory/package
iq //reserved
```