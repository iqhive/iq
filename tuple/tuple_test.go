package tuple_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/tuple"
)

func TestTuple(t *testing.T) {
	var pair = tuple.NewPair("a", 22)

	a, b := pair.Get()
	fmt.Println(a, b)

	txt, _ := pair.MarshalJSON()
	fmt.Println(string(txt))
}
