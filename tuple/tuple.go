// Package tuple provides short ordered sequences of values.
package tuple

import "encoding/json"

// Pair holds two values.
type Pair[A, B any] struct {
	a A
	b B
}

// NewPair returns a new [Pair] from the given values.
func NewPair[A, B any](a A, b B) Pair[A, B] {
	return Pair[A, B]{a, b}
}

// Get returns the values in the [Pair].
func (p Pair[A, B]) Get() (A, B) {
	return p.a, p.b
}

// Split returns the values in the [Pair].
func (p Pair[A, B]) Split() (A, B) {
	return p.a, p.b
}

func (p Pair[A, B]) MarshalJSON() ([]byte, error) {
	return json.Marshal([2]interface{}{p.a, p.b})
}

func (p *Pair[A, B]) UnmarshalJSON(data []byte) error {
	var v [2]json.RawMessage
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	if err := json.Unmarshal(v[0], &p.a); err != nil {
		return err
	}
	if err := json.Unmarshal(v[1], &p.b); err != nil {
		return err
	}
	return nil
}

// Trio holds three values.
type Trio[A, B, C any] struct {
	a A
	b B
	c C
}

// NewTrio returns a new [Trio] from the given values.
func NewTrio[A, B, C any](a A, b B, c C) Trio[A, B, C] {
	return Trio[A, B, C]{a, b, c}
}

// Split returns the values in the [Trio].
func (t Trio[A, B, C]) Split() (A, B, C) {
	return t.a, t.b, t.c
}

// Get returns the values in the [Trio].
func (t Trio[A, B, C]) Get() (A, B, C) {
	return t.a, t.b, t.c
}

func (t Trio[A, B, C]) MarshalJSON() ([]byte, error) {
	return json.Marshal([3]interface{}{t.a, t.b, t.c})
}

func (t *Trio[A, B, C]) UnmarshalJSON(data []byte) error {
	var v [3]json.RawMessage
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	if err := json.Unmarshal(v[0], &t.a); err != nil {
		return err
	}
	if err := json.Unmarshal(v[1], &t.b); err != nil {
		return err
	}
	if err := json.Unmarshal(v[2], &t.c); err != nil {
		return err
	}
	return nil
}

// Quad holds four values.
type Quad[A, B, C, D any] struct {
	a A
	b B
	c C
	d D
}

// NewQuad returns a new [Quad] from the given values.
func NewQuad[A, B, C, D any](a A, b B, c C, d D) Quad[A, B, C, D] {
	return Quad[A, B, C, D]{a, b, c, d}
}

// Split returns the values in the [Quad].
func (q Quad[A, B, C, D]) Split() (A, B, C, D) {
	return q.a, q.b, q.c, q.d
}

// Get returns the values in the [Quad].
func (q Quad[A, B, C, D]) Get() (A, B, C, D) {
	return q.a, q.b, q.c, q.d
}

func (q Quad[A, B, C, D]) MarshalJSON() ([]byte, error) {
	return json.Marshal([4]interface{}{q.a, q.b, q.c, q.d})
}

func (q *Quad[A, B, C, D]) UnmarshalJSON(data []byte) error {
	var v [4]json.RawMessage
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}
	if err := json.Unmarshal(v[0], &q.a); err != nil {
		return err
	}
	if err := json.Unmarshal(v[1], &q.b); err != nil {
		return err
	}
	if err := json.Unmarshal(v[2], &q.c); err != nil {
		return err
	}
	if err := json.Unmarshal(v[3], &q.d); err != nil {
		return err
	}
	return nil
}
