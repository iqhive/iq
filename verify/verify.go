// Package verify provides functions for assertions and verifying behavior.
package verify

import (
	"errors"
	"fmt"

	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/xray"
)

// Valid verifies that the given struct satisfies its own
// embedded assertions.
func Valid(value is.Assertable) error {
	_, err := is.Assert(value, value)
	return err
}

// Value verifies that the value satisfies the assertion.
func Value[Assertion is.Assertable](value any) error {
	var zero Assertion
	_, err := is.Assert(value, zero)
	return err
}

// Pair verifies that the values satisfy the assertions.
func Pair[A, B is.Assertable](a, b any) error {
	var zeroA A
	var zeroB B
	_, errA := is.Assert(a, zeroA)
	_, errB := is.Assert(b, zeroB)
	return errors.Join(errA, errB)
}

// Trio verifies that the values satisfy the assertions.
func Trio[A, B, C is.Assertable](a, b, c any) error {
	var zeroA A
	var zeroB B
	var zeroC C
	_, errA := is.Assert(a, zeroA)
	_, errB := is.Assert(b, zeroB)
	_, errC := is.Assert(c, zeroC)
	return errors.Join(errA, errB, errC)
}

// Quad verifies that the values satisfy the assertions.
func Quad[A, B, C, D is.Assertable](a, b, c, d any) error {
	var zeroA A
	var zeroB B
	var zeroC C
	var zeroD D
	_, errA := is.Assert(a, zeroA)
	_, errB := is.Assert(b, zeroB)
	_, errC := is.Assert(c, zeroC)
	_, errD := is.Assert(d, zeroD)
	return errors.Join(errA, errB, errC, errD)
}

// That checks if the value satisfies the assertion.
// It is more natural to read than using the validator
// directly. It also returns an error if any values
// are error typed with a non-nil value.
func That(values ...any) func(...is.Assertable) error {
	return func(assertions ...is.Assertable) error {
		for i, value := range values {
			var assertion is.Assertable
			if i < len(assertions) {
				assertion = assertions[i]
			}
			if assertion == nil {
				if err, ok := value.(error); ok && err != nil {
					return err
				}
			}
		}
		for i, value := range values {
			var assertion is.Assertable
			if i < len(assertions) {
				assertion = assertions[i]
			}
			if assertion == nil {
				continue
			}
			if _, err := is.Assert(value, assertion); err != nil {
				return err
			}
		}
		for i := len(values); i < len(assertions); i++ {
			if assertions[i] != nil {
				if _, err := is.Assert(nil, assertions[i]); err != nil {
					return err
				}
			}
		}
		return nil
	}
}

// Panic returns an error if the given function does not
// panic.
func Panic(fn func()) (err error) {
	defer func() {
		if r := recover(); r == nil {
			err = fmt.Errorf("expected function to panic")
		}
	}()
	fn()
	return nil
}

// Error returns an error if the given function does not
// return an error.
func Error(results ...any) error {
	for _, result := range results {
		if err, ok := result.(error); ok && err != nil {
			return nil
		}
	}
	return fmt.Errorf("expected function to return an error")
}

type literal string

func Note(note literal, fn func() error) error {
	err := fn()
	if err != nil {
		return fmt.Errorf("%s:\n\n%v", note, xray.Sprint(err))
	}
	return nil
}
