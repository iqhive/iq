package verify_test

import (
	"errors"
	"testing"

	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/verify"
)

func TestVerify(t *testing.T) {
	if err := verify.That(22)(is.Value("22")); err != nil {
		t.Fatal(err)
	}
	if err := verify.That(23)(is.Value("22")); err == nil {
		t.Fatal("expected error")
	}

	var x *int
	if err := verify.Panic(func() { _ = *x }); err != nil {
		t.Fatal(err)
	}

	DoSomething := func() error {
		return errors.New("error")
	}
	if err := verify.Value[is.Error](DoSomething()); err != nil {
		t.Fatal(err)
	}

	MultipleReturns := func() (int, error) {
		return 1, nil
	}
	if err := verify.That(MultipleReturns())(is.Value("1")); err != nil {
		t.Fatal(err)
	}

	var Structure struct {
		X int
		Y int
	}
	Structure.X = 2
	Structure.Y = 3

	type isValidStructure = is.AllOf[struct {
		X is.Field[is.Value] `2`
		Y is.Field[is.Value] `3`
	}]
	if err := verify.Value[isValidStructure](Structure); err != nil {
		t.Fatal(err)
	}

	var structure struct {
		X, Y int

		is.AllOf[struct {
			X is.Field[is.Value] `2`
			Y is.Field[is.Value] `3`
		}]
	}
	structure.X = 2
	structure.Y = 3
	if err := verify.Valid(structure); err != nil {
		t.Fatal(err)
	}
}
