package human_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/human"
)

func TestHuman(t *testing.T) {
	var language = human.NewLanguage().English
	fmt.Println(language)

	var question human.Question = "Hello World"
	if err := question.Validate(); err == nil {
		t.Fatal("question should not be ok")
	}
	question = "How are you today?"
	if err := question.Validate(); err != nil {
		t.Fatal(err)
	}

	var readable human.Readable = "1234 ā"
	fmt.Println(readable)
}
