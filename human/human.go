// Package human contains types that are meant to be understood by a person or language model.
package human

import (
	"fmt"

	"pkg.iqhive.com/iq/enum"
	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/maybe"
)

// Readable is meant to be understood by a person or language model, it shouldn't have any kind
// of opaque value or extractable structure. ie. a description.
type Readable maybe.String[is.Printable]

func (r Readable) Validate() error {
	return maybe.String[is.Printable](r).Validate()
}

// Sprintf returns a Readable that is the result of fmt.Sprintf
func Sprintf(format string, args ...any) Readable {
	return Readable(fmt.Sprintf(format, args...))
}

// Language is a two-letter code that represents a language (ISO 639-1)
type Language = enum.Int[Languages]

// Languages in ISO 639-1
type Languages struct {
	Abkhazian        Language `text:"ab"`
	Afar             Language `text:"aa"`
	Afrikaans        Language `text:"af"`
	Akan             Language `text:"ak"`
	Albanian         Language `text:"sq"`
	Amharic          Language `text:"am"`
	Arabic           Language `text:"ar"`
	Aragonese        Language `text:"an"`
	Armenian         Language `text:"hy"`
	Assamese         Language `text:"as"`
	Avaric           Language `text:"av"`
	Avestan          Language `text:"ae"`
	Aymara           Language `text:"ay"`
	Azerbaijani      Language `text:"az"`
	Bambara          Language `text:"bm"`
	Bashkir          Language `text:"ba"`
	Basque           Language `text:"eu"`
	Belarusian       Language `text:"be"`
	Bengali          Language `text:"bn"`
	Bislama          Language `text:"bi"`
	Bosnian          Language `text:"bs"`
	Breton           Language `text:"br"`
	Bulgarian        Language `text:"bg"`
	Burmese          Language `text:"my"`
	Catalan          Language `text:"ca"`
	Chamorro         Language `text:"ch"`
	Chechen          Language `text:"ce"`
	Chichewa         Language `text:"ny"`
	Chinese          Language `text:"zh"`
	ChurchSlavonic   Language `text:"cu"`
	Chuvash          Language `text:"cv"`
	Cornish          Language `text:"kw"`
	Corsican         Language `text:"co"`
	Cree             Language `text:"cr"`
	Croatian         Language `text:"hr"`
	Czech            Language `text:"cs"`
	Danish           Language `text:"da"`
	Divehi           Language `text:"dv"`
	Dutch            Language `text:"nl"`
	Dzongkha         Language `text:"dz"`
	English          Language `text:"en"`
	Esperanto        Language `text:"eo"`
	Estonian         Language `text:"et"`
	Ewe              Language `text:"ee"`
	Faroese          Language `text:"fo"`
	Fijian           Language `text:"fj"`
	Finnish          Language `text:"fi"`
	French           Language `text:"fr"`
	WesternFrisian   Language `text:"fy"`
	Fulah            Language `text:"ff"`
	Gaelic           Language `text:"gd"`
	Galician         Language `text:"gl"`
	Ganda            Language `text:"lg"`
	Georgian         Language `text:"ka"`
	German           Language `text:"de"`
	Greek            Language `text:"el"`
	Kalaallisut      Language `text:"kl"`
	Guarani          Language `text:"gn"`
	Gujarati         Language `text:"gu"`
	Haitian          Language `text:"ht"`
	Hausa            Language `text:"ha"`
	Hebrew           Language `text:"he"`
	Herero           Language `text:"hz"`
	Hindi            Language `text:"hi"`
	HiriMotu         Language `text:"ho"`
	Hungarian        Language `text:"hu"`
	Icelandic        Language `text:"is"`
	Ido              Language `text:"io"`
	Igbo             Language `text:"ig"`
	Indonesian       Language `text:"id"`
	Interlingua      Language `text:"ia"`
	Interlingue      Language `text:"ie"`
	Inuktitut        Language `text:"iu"`
	Inupiaq          Language `text:"ik"`
	Irish            Language `text:"ga"`
	Italian          Language `text:"it"`
	Japanese         Language `text:"ja"`
	Javanese         Language `text:"jv"`
	Kannada          Language `text:"kn"`
	Kanuri           Language `text:"kr"`
	Kashmiri         Language `text:"ks"`
	Kazakh           Language `text:"kk"`
	CentralKhmer     Language `text:"km"`
	Kikuyu           Language `text:"ki"`
	Kinyarwanda      Language `text:"rw"`
	Kirghiz          Language `text:"ky"`
	Komi             Language `text:"kv"`
	Kongo            Language `text:"kg"`
	Korean           Language `text:"ko"`
	Kuanyama         Language `text:"kj"`
	Kurdish          Language `text:"ku"`
	Lao              Language `text:"lo"`
	Latin            Language `text:"la"`
	Latvian          Language `text:"lv"`
	Limburgan        Language `text:"li"`
	Lingala          Language `text:"ln"`
	Lithuanian       Language `text:"lt"`
	LubaKatanga      Language `text:"lu"`
	Luxembourgish    Language `text:"lb"`
	Macedonian       Language `text:"mk"`
	Malagasy         Language `text:"mg"`
	Malay            Language `text:"ms"`
	Malayalam        Language `text:"ml"`
	Maltese          Language `text:"mt"`
	Manx             Language `text:"gv"`
	Maori            Language `text:"mi"`
	Marathi          Language `text:"mr"`
	Marshallese      Language `text:"mh"`
	Mongolian        Language `text:"mn"`
	Nauru            Language `text:"na"`
	Navajo           Language `text:"nv"`
	NorthNdebele     Language `text:"nd"`
	SouthNdebele     Language `text:"nr"`
	Ndonga           Language `text:"ng"`
	Nepali           Language `text:"ne"`
	Norwegian        Language `text:"no"`
	NorwegianBokmål  Language `text:"nb"`
	NorwegianNynorsk Language `text:"nn"`
	SichuanYi        Language `text:"ii"`
	Occitan          Language `text:"oc"`
	Ojibwa           Language `text:"oj"`
	Oriya            Language `text:"or"`
	Oromo            Language `text:"om"`
	Ossetian         Language `text:"os"`
	Pali             Language `text:"pi"`
	Pashto           Language `text:"ps"`
	Persian          Language `text:"fa"`
	Polish           Language `text:"pl"`
	Portuguese       Language `text:"pt"`
	Punjabi          Language `text:"pa"`
	Quechua          Language `text:"qu"`
	Romanian         Language `text:"ro"`
	Romansh          Language `text:"rm"`
	Rundi            Language `text:"rn"`
	Russian          Language `text:"ru"`
	NorthernSami     Language `text:"se"`
	Samoan           Language `text:"sm"`
	Sango            Language `text:"sg"`
	Sanskrit         Language `text:"sa"`
	Sardinian        Language `text:"sc"`
	Serbian          Language `text:"sr"`
	Shona            Language `text:"sn"`
	Sindhi           Language `text:"sd"`
	Sinhala          Language `text:"si"`
	Slovak           Language `text:"sk"`
	Slovenian        Language `text:"sl"`
	Somali           Language `text:"so"`
	SouthernSotho    Language `text:"st"`
	Spanish          Language `text:"es"`
	Sundanese        Language `text:"su"`
	Swahili          Language `text:"sw"`
	Swati            Language `text:"ss"`
	Swedish          Language `text:"sv"`
	Tagalog          Language `text:"tl"`
	Tahitian         Language `text:"ty"`
	Tajik            Language `text:"tg"`
	Tamil            Language `text:"ta"`
	Tatar            Language `text:"tt"`
	Telugu           Language `text:"te"`
	Thai             Language `text:"th"`
	Tibetan          Language `text:"bo"`
	Tigrinya         Language `text:"ti"`
	Tonga            Language `text:"to"`
	Tsonga           Language `text:"ts"`
	Tswana           Language `text:"tn"`
	Turkish          Language `text:"tr"`
	Turkmen          Language `text:"tk"`
	Twi              Language `text:"tw"`
	Uighur           Language `text:"ug"`
	Ukrainian        Language `text:"uk"`
	Urdu             Language `text:"ur"`
	Uzbek            Language `text:"uz"`
	Venda            Language `text:"ve"`
	Vietnamese       Language `text:"vi"`
	Volapük          Language `text:"vo"`
	Walloon          Language `text:"wa"`
	Welsh            Language `text:"cy"`
	Wolof            Language `text:"wo"`
	Xhosa            Language `text:"xh"`
	Yiddish          Language `text:"yi"`
	Yoruba           Language `text:"yo"`
	Zhuang           Language `text:"za"`
	Zulu             Language `text:"zu"`
}

var newLanguages = enum.New[Languages]()

// NewLanguage constructor.
func NewLanguage() Languages {
	return newLanguages
}

type isQuestion = is.AllOf[struct {
	is.Printable
	is.EndingWith `?`
}]

// Question is a readable string that ends with a question mark.
type Question maybe.String[isQuestion]

func (q Question) Validate() error {
	return maybe.String[isQuestion](q).Validate()
}
