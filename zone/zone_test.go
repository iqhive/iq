package zone_test

import (
	"context"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/zone"
)

func TestZone(t *testing.T) {
	var ctx = zone.Context(context.Background())

	n := zone.New[int](ctx)
	fmt.Println(n.Get(ctx))
	n.Set(ctx, 22)
	fmt.Println(n.Get(ctx))
}

// BenchmarkZone whilst a micro-benchmark, shows that we can improve standard Go allocation performance by 50%.
func BenchmarkZone(b *testing.B) {
	var ctx = zone.Context(context.Background())
	var n zone.Pointer[int]
	for i := 0; i < b.N; i++ {
		for i := 0; i < 1000; i++ {
			n = zone.New[int](ctx)
			n.Set(ctx, 22)
		}
		zone.Free(ctx)
	}
}

func BenchmarkGo(b *testing.B) {
	var n *int
	for i := 0; i < b.N; i++ {
		for i := 0; i < 1000; i++ {
			n = new(int)
			*n = 22
		}
	}
}
