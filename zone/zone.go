// Package zone provides a efficient bump allocator for small context-bound allocations.
package zone

import (
	"context"
	"math"
	"reflect"
	"sync/atomic"
	"unsafe"
)

/*
	Zone provides a efficient bump allocator that is useful for
	allocating small objects that have a short context-bound
	lifetime. For example, allocations made within a request handler.
*/

const pageSize = 65536

type zoneKey struct{}

var check atomic.Uint64

// Context returns a context with a zone allocator that can be used to allocate
// values that are only valid for the lifetime of the context.
func Context(ctx context.Context) context.Context {
	return &zone{
		check: check.Add(1),
		limit: math.MaxUint16,
		pages: []*[pageSize]byte{new([pageSize]byte)},
	}
}

// Pointer to value within a zone.
type Pointer[T any] struct {
	check uint64
	page  uint16
	addr  uint16
}

func (ptr Pointer[T]) unsafe(ctx context.Context) *T {
	var zone = ctx.Value(zoneKey{}).(*zone)
	return (*T)(unsafe.Pointer(&zone.pages[ptr.page][ptr.addr]))
}

func (ptr Pointer[T]) Get(ctx context.Context) T {
	zone := get(ctx)
	if ptr.check != zone.check {
		panic("zone: pointer used outside of context")
	}
	return *(*T)(unsafe.Pointer(&zone.pages[ptr.page][ptr.addr]))
}

func (ptr Pointer[T]) Set(ctx context.Context, value T) {
	zone := get(ctx)
	if ptr.check != zone.check {
		panic("zone: pointer used outside of context")
	}
	*(*T)(unsafe.Pointer(&zone.pages[ptr.page][ptr.addr])) = value
}

type zone struct {
	context.Context

	pages []*[pageSize]byte

	check uint64
	count uint16 // bump counter within the page
	limit uint16 // page limit
}

func get(ctx context.Context) *zone {
	if z, ok := ctx.(*zone); ok {
		return z
	}
	if z, ok := ctx.Value(zoneKey{}).(*zone); ok {
		return z
	}
	panic("zone: context is missing an allocator, did you call zone.Context?")
}

func (z *zone) Value(key any) any {
	if key == (zoneKey{}) {
		return z
	}
	return z.Context.Value(key)
}

func Free(ctx context.Context) {
	zone := get(ctx)
	zone.count = 0
	zone.check = check.Add(1)
}

func New[T any](ctx context.Context) Pointer[T] {
	zone := get(ctx)

	var zero T

	if unsafe.Sizeof(zero) > pageSize {
		panic("zone: object too large to allocate")
	}
	if uintptr(zone.count)+unsafe.Sizeof(zero) > pageSize {
		zone.pages = append(zone.pages, new([pageSize]byte))
		zone.count = 0
	}

	var ptr Pointer[T]
	ptr.check = zone.check
	ptr.page = uint16(len(zone.pages) - 1)
	ptr.addr = zone.count
	zone.count += uint16(unsafe.Sizeof(zero))

	// quick sanity check, we can't put Go pointers into the zone-allocated memory.
	p := ptr.unsafe(ctx)
	rvalue := reflect.ValueOf(p).Elem()
	switch rvalue.Kind() {
	case reflect.Pointer, reflect.String, reflect.Slice, reflect.Map, reflect.Chan, reflect.Func, reflect.Interface, reflect.UnsafePointer:
		panic("zone: cannot allocate pointer, string, map, channel, function, interface, or unsafe.Pointer")
	case reflect.Struct:
		for i := 0; i < rvalue.NumField(); i++ {
			switch rvalue.Kind() {
			case reflect.Pointer, reflect.String, reflect.Slice, reflect.Map, reflect.Chan, reflect.Func, reflect.Interface, reflect.UnsafePointer:
				panic("zone: cannot allocate pointer, string, map, channel, function, interface, or unsafe.Pointer")
			}
		}
	}

	return ptr
}
