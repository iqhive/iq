package enum_test

import (
	"testing"

	"pkg.iqhive.com/iq/enum"
)

type Animal enum.Of[Animals]

type Animals struct {
	Dog     Animal
	Cat     Animal
	Chicken Animal
	Cow     Animal
	Horse   Animal `json:"horse"`
}

func NewAnimal() Animals {
	return enum.New[Animals]()
}

func TestSwitch(t *testing.T) {
	var animal = NewAnimal().Dog

	enum.Switch(animal, func(animals Animals) Animals {
		return Animals{
			enum.Case(animals.Dog, func() {}),
			enum.Case(animals.Cat, func() { t.Fatal() }),
			enum.Case(animals.Chicken, func() { t.Fatal() }),
			enum.Case(animals.Cow, func() { t.Fatal() }),
			enum.Case(animals.Horse, func() { t.Fatal() }),
		}
	})

	animal = NewAnimal().Cat
	enum.Switch(animal, func(animals Animals) Animals {
		return Animals{
			enum.Case(animals.Dog, func() { t.Fatal() }),
			enum.Case(animals.Cat, func() {}),
			enum.Case(animals.Chicken, func() { t.Fatal() }),
			enum.Case(animals.Cow, func() { t.Fatal() }),
			enum.Case(animals.Horse, func() { t.Fatal() }),
		}
	})
}

func TestEnum(t *testing.T) {
	if NewAnimal().Dog == NewAnimal().Cat {
		t.Error("Dog and Cat should be different")
	}

	if NewAnimal().Dog.String() != "Dog" {
		t.Error("Dog should be Dog")
	}

	if NewAnimal().Cat.String() != "Cat" {
		t.Error("Cat should be Cat")
	}

	var a Animal
	if err := a.UnmarshalText([]byte("Dog")); err != nil {
		t.Error(err)
	}

	if a != NewAnimal().Dog {
		t.Error("Dog should be Dog")
	}

	if err := a.UnmarshalText([]byte("Cat")); err != nil {
		t.Error(err)
	}

	if a != NewAnimal().Cat {
		t.Error("Cat should be Cat")
	}

	if err := a.UnmarshalText([]byte("Car")); err == nil {
		t.Error("Car should be invalid")
	}

	if b, _ := NewAnimal().Dog.MarshalJSON(); string(b) != `"Dog"` {
		t.Error("Dog should be Dog: ", string(b))
	}

	if b, _ := NewAnimal().Cat.MarshalJSON(); string(b) != `"Cat"` {
		t.Error("Dog should be Dog: ", string(b))
	}

	if a != NewAnimal().Cat {
		t.Error("Cat should be Cat")
	}

	if err := a.UnmarshalJSON([]byte(`"Car"`)); err == nil {
		t.Error("Car should be invalid")
	}
	if err := a.UnmarshalJSON([]byte(`"horse"`)); err != nil {
		t.Error("horse should be valid")
	}
}
