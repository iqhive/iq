/*
Package enum provides enumerated types with support for text serialisation.

	type Animal enum.Int[Animals]

	type Animals struct {
		Dog     Animal
		Cat     Animal
		Chicken Animal
		Cow     Animal
		Horse   Animal `json:"horse"`
	}

	func NewAnimal() Animals {
		return enum.New[Animals]()
	}

	var animal Animal = NewAnimal().Dog
*/
package enum

import (
	"fmt"
	"reflect"
	"strconv"

	"pkg.iqhive.com/iq/api"
)

// Type describes an emumerated type.
// Consisting of a collection of named values.
type Type struct {
	Tags api.Tags

	Values []Value
}

// Value describes a named value within an
// enumerated type.
type Value struct {
	Tags api.Tags

	Name string
}

// TypeOf returns a reflected view of the enum.
func TypeOf(v reflect.Type) (Type, bool) {
	for v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	type enumerable interface {
		values() []Value
	}
	if v.Implements(reflect.TypeOf((*enumerable)(nil)).Elem()) {
		enum := reflect.New(v).Elem().Interface().(enumerable)

		return Type{
			Tags:   api.Tags(`json:"string"`),
			Values: enum.values(),
		}, true
	}

	return Type{}, false
}

type isEnum[T any] interface {
	sum() T
}

func Values[Values any, Enum isEnum[Values]](enum Enum) Values {
	return enum.sum()
}

// Int is a special kind of sum type, where each field is untyped. The
// underlying value is stored as an int.
type Int[Sum comparable] struct {
	internalInt[Sum]
}

// Of is a enumuration of V values, where V is a struct
// with fields of the same type as the enum.
type Of[V comparable] struct {
	internalInt[V]
}

type internalInt[Sum comparable] struct {
	_ [0]*Sum
	int
}

// New returns a new instance of the sum type.
func New[T comparable]() T {
	return Int[T]{}.sum()
}

// String implements fmt.Stringer.
func (enum internalInt[Sum]) String() string {
	var sum Sum
	var field = reflect.TypeOf(sum).Field(enum.int)

	var text = field.Name
	if tag, ok := field.Tag.Lookup("text"); ok {
		text = tag
	}

	return text
}

// MarshalText implements encoding.TextMarshaler.
func (enum internalInt[Sum]) MarshalText() ([]byte, error) {
	return []byte(enum.String()), nil
}

// UmarshalText implements encoding.TextUnmarshaler.
func (enum *internalInt[Sum]) UnmarshalText(text []byte) error {
	var sum Sum

	rtype := reflect.TypeOf(sum)
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}

		if string(text) == field.Name {
			enum.int = i
			return nil
		}
		if tag, ok := field.Tag.Lookup("text"); ok && string(text) == tag {
			enum.int = i
			return nil
		}
	}

	return fmt.Errorf("invalid %T value: %s", sum, text)
}

// MarshalJSON implements json.Marshaler.
func (enum internalInt[Sum]) MarshalJSON() ([]byte, error) {
	var sum Sum
	var field = reflect.TypeOf(sum).Field(enum.int)

	var text = field.Name
	if tag, ok := field.Tag.Lookup("json"); ok {
		if tag == "-" {
			return []byte(`""`), nil
		}
		text = tag
	} else {
		if tag, ok := field.Tag.Lookup("text"); ok {
			text = tag
		}
	}

	return []byte(fmt.Sprintf(`"%s"`, text)), nil
}

// MarshalJSON implements json.Unmarshaler.
func (enum *internalInt[Sum]) UnmarshalJSON(data []byte) error {
	text, err := strconv.Unquote(string(data))
	if err != nil {
		return err
	}

	var sum Sum

	rtype := reflect.TypeOf(sum)
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}

		if string(text) == field.Name {
			enum.int = i
			return nil
		}
		if tag, ok := field.Tag.Lookup("json"); ok && string(text) == tag || tag == "-" && string(text) == "" {
			enum.int = i
			return nil
		}
		if tag, ok := field.Tag.Lookup("text"); ok && string(text) == tag {
			enum.int = i
			return nil
		}
	}

	return fmt.Errorf("invalid %T value: %s", sum, text)
}

func (enum internalInt[Sum]) values() []Value {
	var sum Sum
	rtype := reflect.TypeOf(sum)
	values := make([]Value, rtype.NumField())
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}

		if reflect.PtrTo(field.Type).Implements(reflect.TypeOf([0]interface {
			setInt(int, Sum)
		}{}).Elem()) {
			values[i] = Value{
				Tags: api.Tags(field.Tag),
				Name: field.Name,
			}
		}
	}
	return values
}

func (enum *internalInt[Sum]) setInt(value int, _ Sum) {
	enum.int = value
}

func (enum internalInt[Sum]) getInt() int {
	return enum.int
}

// Sum returns a set of fields that can be used to set the value of
// an enumerated sum type, you may wish to initialise this as a global
// variable to cache the result.
func (enum internalInt[Sum]) sum() Sum {
	var sum Sum

	rvalue := reflect.ValueOf(&sum).Elem()
	for i := 0; i < rvalue.NumField(); i++ {
		field := rvalue.Field(i)
		if !field.CanInterface() {
			continue
		}

		setter, ok := field.Addr().Interface().(interface {
			setInt(int, Sum)
		})
		if ok {
			setter.setInt(i, sum)
		}
	}

	return sum
}

// Depreciated: Switch can be used to create an O(N) exaustive switch on this value
// by providing unnamed cases. To perform a non-exhaustive switch, use
// the builtin switch statement instead, which will likely be O(1).
func (enum internalInt[Sum]) Switch(fields any, cases Sum) {
	var zero Sum
	if fields == zero || fields != cases { //TODO disable this check with build tag?
		panic("invalid switch, fields and cases do not match")
	}
}

// Switch can be used to create an O(N) exaustive switch on this value
// by providing unnamed cases. To perform a non-exhaustive switch, use
// the builtin switch statement instead, which will likely be O(1).
func Switch[T comparable](value underlying, cases func(T) T) {
	var fields = new(T)
	reflect.ValueOf(fields).Elem().Field(value.getInt()).Addr().Interface().(interface {
		setInt(int, T)
	}).setInt(1, *fields)
	cases(*fields)
}

type underlying interface {
	getInt() int
}

// Case evaulates that that the enumerated value is equal to the
// given case, if it is, the provided function is executed.
func Case[T underlying](val T, fn func()) T {
	if val.getInt() != 0 {
		fn()
	}
	return val
}
