/*
Package maybe provides a container for assertable primitive types and optional values.

For example, if you have a struct with a field that must be a string between 1 and 32 characters
it can be defined like this:

	type Request struct {
		Field maybe.String[is.AllOf[struct {
			is.AtLeast `1`
			is.AtMost  `32`
		}]
	}

A validation method is available on maybe types that can be used to validate the value:

	var req Request
	req.Field = "hello"
	if err := req.Field.Validate(); err != nil {
		// there will be no error
	}
	req.Field = ""
	if err := req.Field.Validate(); err == nil {
		// error
	}
*/
package maybe

import (
	"encoding/json"

	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/please"
)

// Valid is like [valid.When] but works with struct values
// that embed their own validation constraints.
type Valid[T is.Assertable] struct {
	value T
	valid is.Assertable
	error error
}

// New creates a new [Valid] with the given value.
func New[T is.Assertable](value T) Valid[T] {
	var valid Valid[T]
	valid.Set(value)
	return valid
}

// Set a value, returning an error if it is invalid.
func (s *Valid[T]) Set(value T) error {
	s.value = value
	var assertion T
	s.valid, s.error = is.Assert(value, assertion)
	return please.Fix(s.error)
}

// Get the value and a boolean indicating if it is valid.
func (s *Valid[T]) Get() (T, bool) {
	if s.valid == nil {
		var assertion T
		s.valid, s.error = is.Assert(s.value, assertion)
		if s.error != nil {
			return s.value, false
		}
	}
	return s.value, s.valid != nil
}

// Err returns the validation error, if any.
func (s *Valid[T]) Err() error {
	return s.error
}

// MarshalJSON implements the json.Marshaler interface.
func (s *Valid[T]) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.value)
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (s *Valid[T]) UnmarshalJSON(data []byte) error {
	var value T
	if err := json.Unmarshal(data, &value); err != nil {
		return please.Fix(err)
	}
	s.Set(value)
	return nil
}

type Value interface {
	Validate() error
}

func As[V Value](value V) (V, bool) {
	if err := value.Validate(); err == nil {
		return value, true
	}
	return value, false
}

// String can be aliased in order to represent types
// with validation.
type String[T is.Assertable] string

// Validate returns an error if the string does not pass
// the assertions defined in the type parameter.
func (s String[T]) Validate() error {
	var assertion T
	_, err := is.Assert(string(s), assertion)
	return err
}

func (s String[T]) Validation() is.Assertable {
	var assertion T
	validation, _ := is.Assert(string(s), assertion)
	return validation
}

// Int can be aliased in order to represent values
// with validation.
type Int[T is.Assertable] int

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Int[T]) Validate() error {
	var assertion T
	_, err := is.Assert(int(s), assertion)
	return err
}

// Int8 can be aliased in order to represent values
// with validation.
type Int8[T is.Assertable] int8

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Int8[T]) Validate() error {
	var assertion T
	_, err := is.Assert(int8(s), assertion)
	return err
}

// Int16 can be aliased in order to represent values
// with validation.
type Int16[T is.Assertable] int16

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Int16[T]) Validate() error {
	var assertion T
	_, err := is.Assert(int16(s), assertion)
	return err
}

// Int32 can be aliased in order to represent values
// with validation.
type Int32[T is.Assertable] int32

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Int32[T]) Validate() error {
	var assertion T
	_, err := is.Assert(int32(s), assertion)
	return err
}

// Int64 can be aliased in order to represent values
// with validation.
type Int64[T is.Assertable] int64

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Int64[T]) Validate() error {
	var assertion T
	_, err := is.Assert(int64(s), assertion)
	return err
}

// Uint can be aliased in order to represent values
// with validation.
type Uint[T is.Assertable] uint

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Uint[T]) Validate() error {
	var assertion T
	_, err := is.Assert(uint(s), assertion)
	return err
}

// Uint8 can be aliased in order to represent values
// with validation.
type Uint8[T is.Assertable] uint8

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Uint8[T]) Validate() error {
	var assertion T
	_, err := is.Assert(uint8(s), assertion)
	return err
}

// Uint16 can be aliased in order to represent values
// with validation.
type Uint16[T is.Assertable] uint16

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Uint16[T]) Validate() error {
	var assertion T
	_, err := is.Assert(uint16(s), assertion)
	return err
}

// Uint32 can be aliased in order to represent values
// with validation.
type Uint32[T is.Assertable] uint32

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Uint32[T]) Validate() error {
	var assertion T
	_, err := is.Assert(uint32(s), assertion)
	return err
}

// Uint64 can be aliased in order to represent values
// with validation.
type Uint64[T is.Assertable] uint64

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Uint64[T]) Validate() error {
	var assertion T
	_, err := is.Assert(uint64(s), assertion)
	return err
}

// Uintptr can be aliased in order to represent values
// with validation.
type Uintptr[T is.Assertable] uintptr

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Uintptr[T]) Validate() error {
	var assertion T
	_, err := is.Assert(uintptr(s), assertion)
	return err
}

// Float32 can be aliased in order to represent values
// with validation.
type Float32[T is.Assertable] float32

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Float32[T]) Validate() error {
	var assertion T
	_, err := is.Assert(float32(s), assertion)
	return err
}

// Float64 can be aliased in order to represent values
// with validation.
type Float64[T is.Assertable] float64

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Float64[T]) Validate() error {
	var assertion T
	_, err := is.Assert(float64(s), assertion)
	return err
}

// Complex64 can be aliased in order to represent values
// with validation.
type Complex64[T is.Assertable] complex64

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Complex64[T]) Validate() error {
	var assertion T
	_, err := is.Assert(complex64(s), assertion)
	return err
}

// Complex128 can be aliased in order to represent values
// with validation.
type Complex128[T is.Assertable] complex128

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Complex128[T]) Validate() error {
	var assertion T
	_, err := is.Assert(complex128(s), assertion)
	return err
}

// Byte can be aliased in order to represent values
// with validation.
type Byte[T is.Assertable] byte

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Byte[T]) Validate() error {
	var assertion T
	_, err := is.Assert(byte(s), assertion)
	return err
}

// Rune can be aliased in order to represent values
// with validation.
type Rune[T is.Assertable] rune

// Validate returns an error if the value does not pass
// the assertions defined in the type parameter.
func (s Rune[T]) Validate() error {
	var assertion T
	_, err := is.Assert(rune(s), assertion)
	return err
}
