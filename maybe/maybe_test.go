package maybe_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/maybe"
)

type AdultAge = maybe.Uint[is.AllOf[struct {
	is.AtLeast `18`
}]]

func TestMaybe(t *testing.T) {
	var age AdultAge = 17

	if err := age.Validate(); err == nil {
		t.Fatal("should not be valid")
	}

	age = 18

	if err := age.Validate(); err != nil {
		t.Fatal(err)
	}

	var enum maybe.String[is.OneOf[struct {
		A is.Value `hello`
		B is.Value `world`
	}]]
	enum = "hello"
	enum = "world"
	if err := enum.Validate(); err != nil {
		t.Fatal(err)
	}

	enum = "foo"
	if err := enum.Validate(); err == nil {
		t.Fatal("should not be valid")
	}

	// Style 1
	type Request struct {
		ExternalID maybe.String[is.ASCII]
		Age        AdultAge
	}
	var req = Request{
		ExternalID: "hello wōrld",
		Age:        18,
	}
	fmt.Println(req.ExternalID)

	// Style 2
	type Request2 struct {
		is.AllOf[struct {
			ExternalID is.Field[is.ASCII]
		}]

		ExternalID string
	}
	var req2 = maybe.New(Request2{
		ExternalID: "hello wōrld",
	})
	fmt.Println(req2.Get())
}
