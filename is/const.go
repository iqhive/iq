package is

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
)

type basic interface {
	~string | ~int | ~int8 | ~int16 | ~int32 | ~int64 | ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr | ~float32 | ~float64 | ~complex64 | ~complex128 | ~bool
}

type value interface {
	value()
}

// Const is a special type that denotes that the value of the type is constant
// ie.
//
//	var PI is.Const[float64, struct {
//		is.Value `3.14`
//	}]
type Const[T basic, V value] [0]T

// Get returns the value of the constant.
func (c Const[T, V]) Get() T {
	var zero = new(T)
	var val = reflect.ValueOf(zero).Elem()
	var s = c.String()
	switch val.Kind() {
	case reflect.String:
		val.SetString(s)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		i, err := strconv.ParseInt(s, 10, 64)
		if err != nil {
			panic(err)
		}
		val.SetInt(i)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		u, err := strconv.ParseUint(s, 10, 64)
		if err != nil {
			panic(err)
		}
		val.SetUint(u)
	case reflect.Float32, reflect.Float64:
		f, err := strconv.ParseFloat(s, 64)
		if err != nil {
			panic(err)
		}
		val.SetFloat(f)
	case reflect.Bool:
		b, err := strconv.ParseBool(s)
		if err != nil {
			panic(err)
		}
		val.SetBool(b)
	case reflect.Complex64, reflect.Complex128:
		c, err := strconv.ParseComplex(s, 128)
		if err != nil {
			panic(err)
		}
		val.SetComplex(c)
	}
	return *zero
}

// MarshalJSON implements the json.Marshaler interface.
func (c Const[T, V]) MarshalJSON() ([]byte, error) {
	return json.Marshal(c.Get())
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (c Const[T, V]) UnmarshalJSON(data []byte) error {
	assert, err := json.Marshal(c.Get())
	if err != nil {
		return err
	}
	if string(assert) != string(data) {
		return fmt.Errorf("%v should be %v", string(data), string(assert))
	}
	return nil
}

// String implements the fmt.Stringer interface.
func (c Const[T, V]) String() string {
	var zero V
	return string(reflect.TypeOf(zero).Field(0).Tag)
}
