package is_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/is"
)

func TestConst(t *testing.T) {
	var PI is.Const[float64, struct {
		is.Value `3.14`
	}]
	var TAU is.Const[float64, struct {
		is.Value `6.28`
	}]
	fmt.Println(PI, TAU)
}
