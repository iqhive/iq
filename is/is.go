// Package is provides validators for validating primitive values. This package can be used to constrain
// the values of fields in structs. For example, the following validator could be used to ensure that
// the value of an age field is at least 18:
//
//	var IsAdult is.AllOf[struct {
//		is.AtLeast[18]
//	}]
//
//	var age uint
//	if err := is.Assert(age, IsAdult); err != nil {
//		return err
//	}
//
// Validators can be configured with the use of struct tags.
//
// See the maybe package for an example of how to automatically add validation to your types and
// structures.
package is

import (
	"errors"
	"fmt"
	"net"
	"net/url"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"github.com/google/uuid"
	"pkg.iqhive.com/iq/oops"
)

// Assert returns an error if the value cannot be asserted.
func Assert(value any, assertion Assertable) (Assertable, error) {
	return assertion.assert(value)
}

type assertable interface {
	~string
	assert(any) (Assertable, error)
}

type Assertable interface {
	assert(any) (Assertable, error)
}

type identity string

// AllOf returns an error if any of the struct-field validators return an error.
type AllOf[T any] identity

func (rule AllOf[T]) assert(value any) (Assertable, error) {
	rtype := reflect.TypeOf([0]T{}).Elem()
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}
		rvalue := reflect.ValueOf(field.Name + "`" + string(field.Tag))
		if rvalue.Type().ConvertibleTo(field.Type) {
			if _, err := rvalue.Convert(field.Type).Interface().(Assertable).assert(value); err != nil {
				return nil, oops.New(err)
			}
		}
	}
	return rule, nil
}

// OneOf returns an error if exactly one of the struct-field validators returns nil.
type OneOf[T any] identity

func (rule OneOf[T]) assert(value any) (Assertable, error) {
	rtype := reflect.TypeOf([0]T{}).Elem()
	var matches int
	var match Assertable
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}
		rvalue := reflect.ValueOf(field.Name + "`" + string(field.Tag))
		if rvalue.Type().ConvertibleTo(field.Type) {
			if assertable, err := rvalue.Convert(field.Type).Interface().(Assertable).assert(value); err == nil {
				matches++
				match = assertable
			}
		}
	}
	if matches == 1 {
		return match, nil
	}
	var fields []string
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}
		fields = append(fields, field.Name)
	}
	return nil, fmt.Errorf("value '%v' is not one of '%v'", value, strings.Join(fields, ", "))
}

// AnyOf returns an error if all of the struct-field validators return an error.
type AnyOf[T any] identity

func (rule AnyOf[T]) assert(value any) (Assertable, error) {
	rtype := reflect.TypeOf([0]T{}).Elem()
	for i := 0; i < rtype.NumField(); i++ {
		field := rtype.Field(i)
		if !field.IsExported() {
			continue
		}
		rvalue := reflect.ValueOf(field.Name + "`" + string(field.Tag))
		if rvalue.Type().ConvertibleTo(field.Type) {
			if assertable, err := rvalue.Convert(field.Type).Interface().(Assertable).assert(value); err == nil {
				return assertable, nil
			}
		}
	}
	return nil, fmt.Errorf("value '%v' is not any of '%v'", value, rule)
}

// Value matches an exact value.
type Value identity

func (rule Value) value() {}

func (rule Value) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}
	if string(tag) == fmt.Sprintf("%v", value) {
		return rule, nil
	}
	return nil, fmt.Errorf("value '%v' is not '%v'", value, rule)
}

/*
AtLeast return true if value >= rule, or len(value) >= rule.
*/
type AtLeast identity

func (rule AtLeast) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		atleast, err := strconv.ParseUint(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atleast, err)
		}
		if rvalue.Uint() >= uint64(atleast) {
			return rule, nil
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		atleast, err := strconv.ParseInt(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atleast, err)
		}
		if rvalue.Int() >= atleast {
			return rule, nil
		}
	case reflect.String, reflect.Array, reflect.Map, reflect.Slice:
		atleast, err := strconv.Atoi(string(tag))
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atleast, err)
		}
		if rvalue.Len() >= atleast {
			return rule, nil
		}
	case reflect.Float32, reflect.Float64:
		atleast, err := strconv.ParseFloat(string(tag), 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atleast, err)
		}
		if rvalue.Float() >= atleast {
			return rule, nil
		}
	}
	return nil, errors.New("must be at least " + string(tag))
}

/*
AtMost return true if value <= rule, or len(value) <= rule.
*/
type AtMost identity

func (rule AtMost) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		atmost, err := strconv.ParseUint(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atmost, err)
		}
		if rvalue.Uint() <= uint64(atmost) {
			return rule, nil
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		atmost, err := strconv.ParseInt(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atmost, err)
		}
		if rvalue.Int() <= atmost {
			return rule, nil
		}
	case reflect.String, reflect.Array, reflect.Map, reflect.Slice:
		atmost, err := strconv.Atoi(string(tag))
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atmost, err)
		}
		if rvalue.Len() <= atmost {
			return rule, nil
		}
	case reflect.Float32, reflect.Float64:
		atmost, err := strconv.ParseFloat(string(tag), 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", atmost, err)
		}
		if rvalue.Float() <= atmost {
			return rule, nil
		}
	}
	return nil, errors.New("must be at most " + string(tag))
}

/*
MoreThan return true if value > rule, or len(value) > rule.
*/
type MoreThan identity

func (rule MoreThan) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		morethan, err := strconv.ParseUint(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", morethan, err)
		}
		if rvalue.Uint() > uint64(morethan) {
			return rule, nil
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		morethan, err := strconv.ParseInt(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", morethan, err)
		}
		if rvalue.Int() > morethan {
			return rule, nil
		}
	case reflect.String, reflect.Array, reflect.Map, reflect.Slice:
		morethan, err := strconv.Atoi(string(tag))
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", morethan, err)
		}
		if rvalue.Len() > morethan {
			return rule, nil
		}
	case reflect.Float32, reflect.Float64:
		morethan, err := strconv.ParseFloat(string(tag), 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", morethan, err)
		}
		if rvalue.Float() > morethan {
			return rule, nil
		}
	}
	return nil, errors.New("must be more than " + string(tag))
}

/*
LessThan return true if value < rule, or len(value) < rule.
*/
type LessThan identity

func (rule LessThan) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		lessthan, err := strconv.ParseUint(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", lessthan, err)
		}
		if rvalue.Uint() < uint64(lessthan) {
			return rule, nil
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		lessthan, err := strconv.ParseInt(string(tag), 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", lessthan, err)
		}
		if rvalue.Int() < lessthan {
			return rule, nil
		}
	case reflect.String, reflect.Array, reflect.Map, reflect.Slice:
		lessthan, err := strconv.Atoi(string(tag))
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", lessthan, err)
		}
		if rvalue.Len() < lessthan {
			return rule, nil
		}
	case reflect.Float32, reflect.Float64:
		lessthan, err := strconv.ParseFloat(string(tag), 64)
		if err != nil {
			return nil, fmt.Errorf("invalid rule '%v': %w", lessthan, err)
		}
		if rvalue.Float() < lessthan {
			return rule, nil
		}
	}
	return nil, errors.New("must be less than " + string(tag))
}

// UUID is a type for validating UUIDs.
type UUID identity

func (rule UUID) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		_, err := uuid.Parse(rvalue.String())
		if err == nil {
			return rule, nil
		}
	}
	return nil, errors.New("must be a valid UUID")
}

// Alpha validates that a string contains only unicode letters (a-zA-Z).
type Alpha identity

func (rule Alpha) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		var s = rvalue.String()
		for _, r := range s {
			if !unicode.IsLetter(r) {
				return nil, errors.New("must contain only letters")
			}
		}
		return rule, nil
	}
	return nil, errors.New("must contain only letters")
}

// AlphaNumeric validates that a string contains only letters and numbers (a-zA-Z0-9).
type AlphaNumeric identity

func (rule AlphaNumeric) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		var s = rvalue.String()
		for _, r := range s {
			if !unicode.IsLetter(r) && !unicode.IsNumber(r) {
				return nil, errors.New("must contain only letters and numbers (a-zA-Z0-9)")
			}
		}
		return rule, nil
	}
	return nil, errors.New("must contain only letters and numbers (a-zA-Z0-9)")
}

// ASCII validates that a string contains only ASCII characters.
type ASCII identity

func (rule ASCII) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		var s = rvalue.String()
		for _, r := range s {
			if r > 127 {
				return nil, errors.New("must contain only ASCII characters")
			}
		}
		return rule, nil
	}
	return nil, errors.New("must contain only ASCII characters")
}

// Boolean validates that a string is a valid boolean.
type Boolean identity

func (rule Boolean) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		_, err := strconv.ParseBool(rvalue.String())
		if err == nil {
			return rule, nil
		}
	}
	return nil, errors.New("must be a valid boolean")
}

// Containing validates that a string contains a substring.
type Containing identity

func (rule Containing) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		if strings.Contains(rvalue.String(), string(tag)) {
			return rule, nil
		}
	}
	return nil, errors.New("must contain " + string(tag))
}

// StartingWith validates that a string starts with a substring.
type StartingWith identity

func (rule StartingWith) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		if strings.HasPrefix(rvalue.String(), string(tag)) {
			return rule, nil
		}
	}
	return nil, errors.New("must start with " + string(tag))
}

// EndingWith validates that a string ends with a substring.
type EndingWith identity

func (rule EndingWith) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		if strings.HasSuffix(rvalue.String(), string(tag)) {
			return rule, nil
		}
	}
	return nil, errors.New("must end with " + string(tag))
}

// Not validates that the nested validation fails.
type Not[T assertable] identity

func (rule Not[T]) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	_, err := T(rule).assert(value)
	if err != nil {
		return rule, nil
	}
	return nil, errors.New("must not be " + string(tag))
}

// Lowercase validates that a string is lowercase.
type Lowercase identity

func (rule Lowercase) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		if strings.ToLower(rvalue.String()) == rvalue.String() {
			return rule, nil
		}
	}
	return nil, errors.New("must be lowercase")
}

// Uppercase validates that a string is uppercase.
type Uppercase identity

func (rule Uppercase) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		if strings.ToUpper(rvalue.String()) == rvalue.String() {
			return rule, nil
		}
	}
	return nil, errors.New("must be uppercase")
}

// Numeric validates that a string contains only numeric characters.
type Numeric identity

func (rule Numeric) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		s := rvalue.String()
		for _, r := range s {
			if !unicode.IsNumber(r) || r == '.' {
				return nil, errors.New("must be numeric")
			}
		}
	}
	return rule, nil
}

// Printable validates that a string contains only printable characters.
type Printable identity

func (rule Printable) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		s := rvalue.String()
		for _, r := range s {
			if !unicode.IsPrint(r) {
				return nil, errors.New("must be printable")
			}
		}
	}
	return rule, nil
}

// URL validates that a string is a valid URL.
type URL identity

func (rule URL) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		_, err := url.Parse(rvalue.String())
		if err == nil {
			return rule, nil
		}
	}
	return nil, errors.New("must be a valid URL")
}

// IP validates that a string is a valid IP address.
type IP identity

func (rule IP) assert(value any) (Assertable, error) {
	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		if net.ParseIP(rvalue.String()) != nil {
			return rule, nil
		}
	}
	return nil, errors.New("must be a valid IP address")
}

// MatchingRegularExpression validates that a string matches a regular expression.
type MatchingRegularExpression identity

func (rule MatchingRegularExpression) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.String:
		program, err := regexp.Compile(string(tag))
		if err != nil {
			return nil, err
		}
		if program.MatchString(rvalue.String()) {
			return rule, nil
		}
	}
	return nil, errors.New("must match regular expression " + string(tag))
}

// Error validates that a value is a not-nil error.
type Error identity

func (rule Error) assert(value any) (Assertable, error) {
	_, tag, ok := strings.Cut(string(rule), "`")
	if !ok {
		tag = string(rule)
	}

	if err, ok := value.(error); ok && err != nil && strings.Contains(err.Error(), string(tag)) {
		return rule, nil
	}
	return nil, errors.New("must be an error")
}

type Field[T assertable] identity

func (rule Field[T]) assert(value any) (Assertable, error) {
	field, tag, _ := strings.Cut(string(rule), "`")

	rvalue := reflect.ValueOf(value)
	switch rvalue.Kind() {
	case reflect.Struct:
		field := rvalue.FieldByName(string(field))
		if field.IsValid() {
			return T(rule).assert(field.Interface())
		}
	}
	return nil, errors.New("must have field " + string(tag))
}
