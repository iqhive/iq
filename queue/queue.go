/*
Package queue provides a representation for message queuing, event handling and
callback flows. Where there is a fan-out messaging pattern, ie. Pub/Sub semantics.

Variables of type [queue.Topic] are references to a queue, T-typed messages can be
sent to the queue.Topic using the [Topic.Send] method and/or subscriptions can be
attached by calling [Topic.Handle] with a [TopicHandler].

These queue topics can be used to model event streams, let's say you want to model
an event that is fired whenever somebody changes their name,

	type NameChangeEvent struct {
	    Customer string
	    OldName string
	    NewName string
	}

	var NameChanges queue.Topic[NameChangeEvent]

At the location of your updateName function you would send an event:

	err := NameChanges.Send(ctx, NameChangeEvent{ ... })

Another system may want to listen to these events, so that they can
update their customer records. This system can subscribe to the name
changes queue that was passed (as an integration value) to their
NewImplementation function.

	NameChanges.Handle(ctx, func(ctx context.Context, event NameChangeEvent) error {
	   // update our copy of the customer
	   return nil
	})
*/
package queue

import (
	"context"
	"errors"
	"sync/atomic"
)

var ctxBackground = context.Background()

// Handler for values of type T on a [Chan].
type Handler[Message any] func(context.Context, Message) error

// Topic is similar to a Go channel, except that each values sent to it is
// broadcast to every registered [Handler]. Each Handler will have it's own
// queue and messages will be processed in an undefined order across multiple
// goroutines. All operations on a Topic are goroutine safe.
type Topic[Message any] struct {
	// if true, handlers are attached to 'next' and sends are only delivered
	// to this topic's handler. Only ever set on creation, immutable.
	pipe bool

	// if read is true, the value of context and handler
	// are permitted to be read.
	read atomic.Bool
	next atomic.Pointer[Topic[Message]] // next in the linked list.

	// unless context == context.Background(), the first handler in a linked list of
	// Chans will be nil, this is because we need to be able to delete the chan when
	// the context is cancelled and we cannot remove the head of the list as it used
	// as the entry point.
	handler Handler[Message]
	context context.Context
}

// Pipe can be used to control the delivery mechanism for a [Topic], all sends for
// the returned topic will *only* hit the provided delivery handler, new handlers
// attached on the returned topic will only be called when a message is sent to
// the second returned 'handlers' topic.
//
// For example (using Go channels as the delivery mechanism):
//
//	var ch = make(chan string, 10)
//	var jobs, handlers = queue.Pipe(func(ctx context.Context, value string) error {
//		ch <- value
//		return nil
//	})
//	go func() {
//		for val := range ch {
//			handlers.Send(val)
//		}
//	}()
func Pipe[Message any](delivery Handler[Message]) (topic, handlers *Topic[Message]) {
	handlers = new(Topic[Message])
	topic = &Topic[Message]{
		pipe:    true,
		handler: delivery,
	}
	topic.next.Store(handlers)
	return
}

// Chan returns a channel-backed Topic, useful for testing asyncronous delivery.
// A goroutine will be spawned that will manage delivery until the context is
// cancelled. Delivery failures (handlers that return an error) will be retried.
func Chan[Message any](ctx context.Context, buffer int) *Topic[Message] {
	var ch = make(chan Message, buffer)
	topic, handlers := Pipe(func(ctx context.Context, msg Message) error {
		ch <- msg
		return nil
	})
	go func() {
		var index int
		var slice []Message
		var retry = make(chan struct{}, 1)
		for {
			select {
			case <-ctx.Done():
				return
			case msg := <-ch:
				if err := handlers.Send(ctx, msg); err != nil {
					slice = append(slice, msg)
					retry <- struct{}{}
				}
			case <-retry:
				if len(slice) > 0 {
					index %= len(slice)
					if err := handlers.Send(ctx, slice[index]); err != nil {
						index++
						retry <- struct{}{}
					}
				}
			}
		}
	}()
	return topic
}

// Handle attaches the given handler for the lifetime of the context,
// any messages delivered to the channel after this function returns
// will trigger the given handler to be called to process this message.
// The handler must return a nil error in order to acknowledge the message.
func (head *Topic[Message]) Handle(ctx context.Context, handler Handler[Message]) {
	if head == nil {
		return
	}
	if head.pipe {
		head.next.Load().Handle(ctx, handler)
		return
	}

	// if we can attach this zero channel to a node in the list
	// then we have permission to write to the node before it.
	var zero = new(Topic[Message])

	// if the context is the background context, it will never be cancelled
	// and we can use the head of the list to store the handler, optimises
	// for the fast path where only a single global handler is registered.
	if ctx == ctxBackground {

		// if compare and swap returns true, then we can safely mutate the head
		// of the list.
		if head.next.CompareAndSwap(nil, zero) {
			head.context = ctx
			head.handler = handler
			head.read.Store(true)
			return
		}
	}

	// otherwise we need to find the end of the list and attach the new
	// handler to the end.
	node := head
	for {
		next := node.next.Load()
		if next == nil {
			if node.next.CompareAndSwap(nil, zero) {
				node.context = ctx
				node.handler = handler
				node.read.Store(true)
				return
			}
			continue
		} else {
			// delete/cleanup from the list, only safe to do if the resulting next is not nil.

			if read := next.read.Load(); read && next.context.Err() != nil {
				if nextnext := next.next.Load(); nextnext != nil {
					if node.next.CompareAndSwap(next, nextnext) {
						next = nextnext
					}
				}
			}
		}
		node = next
	}
}

// Send will broadcast the given message to all registered handlers.
func (head *Topic[Message]) Send(ctx context.Context, message Message) error {
	if head == nil {
		return errors.New("Topic.Send called on a nil queue.Topic")
	}
	if head.pipe {
		return head.handler(ctx, message)
	}
	node := head
	for {
		if node == nil {
			return nil
		}
		next := node.next.Load()
		if read := node.read.Load(); read {
			if err := node.handler(ctx, message); err != nil {
				return err
			}
			if next != nil {
				// delete/cleanup from the list, only safe to do if the resulting next is not nil.
				if read = next.read.Load(); read && next.context.Err() != nil {
					if nextnext := next.next.Load(); nextnext != nil {
						if node.next.CompareAndSwap(next, nextnext) {
							next = nextnext
						}
					}
				}
			}
		}
		node = next
	}
}
