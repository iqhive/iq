package xray

import (
	"context"
	"reflect"
)

// verb is intentionally not exported, so that values need to be written
// to be readable by hand.
type verb string

type literal string

/*
Log can be used to record a conditional log relating to the scanner's subject.
For example, consider that you want to record the status whenever a customer
attempts to update their username.

	func UpdateCustomerUsername(ctx context.Context, customer CustomerID, username string) error {
		xray.Log(ctx, "update", customer, "updated", "their username to %v?", username)
	}

Now in the handler/transport layer of your application, you can look for logs on customer IDs
and route them to your customer support system.

	func onError(ctx context.Context, err error) {
		for _, log := range xray.For[CustomerID](ctx) {
			// send log to customer support system.
		}
	}

The subject should be a named type that relates to what is being logged. The verbs should be represent
what is happening, present and past tense respectively. The question should be a question about how the
subject is being affected by the verb. The entire log line should be readable. Context can be added to
the question string as bracket-seperated formatting arguments "(%v)". These may or may not be inserted
into the formatting string, so the question should be written to be readable in the case that the
brackets are removed.
*/
func Log[T any](ctx context.Context, fail verb, subject T, past verb, question literal, args ...any) {
	var operations map[reflect.Type][]operation
	if w, ok := ctx.(*scanner); ok {
		operations = w.operations
	} else {
		operations, ok = ctx.Value(keyOperations{}).(map[reflect.Type][]operation)
		if !ok {
			return
		}
	}
	if operations == nil {
		return
	}
	key := reflect.TypeOf([0]T{}).Elem()
	operations[key] = append(operations[key], operation{
		subject: subject,
		failure: fail,
		success: past,
		message: string(question),
		context: args,
	})
}

// Operation records information about how to communicate
// success or failure.
type Operation[T any] struct {
	Subject T      // subject of the operation.
	Failure verb   // verb used when prefixing failure.
	Success verb   // verb used to prefix success.
	Message string // description of how the event affects the subject.
	Context []any  // arguments to the verb.
}

type operation struct {
	subject any
	failure verb
	success verb
	message string
	context []any
}

func toOperations[T any](operations map[reflect.Type][]operation) []Operation[T] {
	Print(operations)
	ops := operations[reflect.TypeOf([0]T{}).Elem()]
	out := make([]Operation[T], len(ops))
	for i, op := range ops {
		out[i] = Operation[T]{
			Subject: op.subject.(T),
			Failure: op.failure,
			Success: op.success,
			Message: op.message,
			Context: op.context,
		}
	}
	return out
}

// For returns all events for the given subject.
func For[T any](ctx context.Context) []Operation[T] {
	var operations map[reflect.Type][]operation
	if w, ok := ctx.(*scanner); ok {
		operations = w.operations
	} else {
		operations, ok = ctx.Value(keyOperations{}).(map[reflect.Type][]operation)
		if !ok {
			return nil
		}
	}
	return toOperations[T](operations)
}
