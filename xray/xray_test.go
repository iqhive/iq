package xray_test

import (
	"context"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/api/rest"
	"pkg.iqhive.com/iq/xray"
)

type Key string

func TestXray(t *testing.T) {
	ctx := xray.New(context.WithValue(context.Background(), Key("key1"), "val1"))
	ctx = context.WithValue(ctx, Key("key2"), "val2")

	fmt.Println(xray.Get(ctx))

	if !xray.Scanning(ctx) {
		t.Fatal("expected tracing")
	}
}

func TestRest(t *testing.T) {
	type Spec struct {
		api.Specification

		GetIndex func(context.Context, string) `rest:"GET /"`
	}

	var spec Spec
	if err := rest.Set(&spec, "http://www.example.com"); err != nil {
		t.Fatal(err)
	}

	ctx := context.WithValue(xray.New(context.Background()), Key("key1"), "val1")

	spec.GetIndex(ctx, "val2")

	entries := xray.Debug(ctx)
	xray.Print(entries)
}
