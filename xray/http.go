package xray

import "net/http"

type responseWriter struct {
	http.ResponseWriter
	status *int
}

// NewResponseWriter returns a new ResponseWriter that writes the
// status code to the given pointer.
func NewResponseWriter(w http.ResponseWriter, status *int) http.ResponseWriter {
	if status == nil {
		return w
	}
	return &responseWriter{w, status}
}

func (w *responseWriter) WriteHeader(code int) {
	*w.status = code
	w.ResponseWriter.WriteHeader(code)
}
