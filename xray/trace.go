package xray

import (
	"errors"
	"runtime"
	"strconv"
	"strings"
)

// Error is an error that can be traced to a location in source code.
type Error interface {
	error

	//Source is the location in the code that raised
	//this particular error. This enables tracing of
	//errors as they travel through the system. The
	//exact semantics are equivalant to
	//runtime.Caller(s)
	Source() (function string, file string, line int, ok bool)
}

// Trace can be embedded inside of a custom error type in
// order to support tracing. It is immutable and can be
// created with New. If you do embed this in your
// type, you should alias it within your package as
// lowercase trace. In order to avoid exporting the Field.
//
//	type trace = traceable.Trace
//
//	type TracedError struct {
//		trace
//		error
//	}
//
//	func NewTracedError(err error) *TracedError {
//		var t TracedError
//		t.error = err
//		t.trace = traceable.New(runtime.Caller(1))
//		return &t
//	}
type Trace struct {
	pc   uintptr
	file string
	line int
	ok   bool
}

func (e *Trace) StackTrace() []uintptr {
	return []uintptr{e.pc}
}

// New creates a new Trace, use the values returned by runtime.Caller
func NewTrace(pc uintptr, file string, line int, ok bool) Trace {
	return Trace{pc, file, line, ok}
}

// Source implements the Error interface.
func (t Trace) Source() (fn string, file string, line int, ok bool) {
	return runtime.FuncForPC(t.pc).Name(), t.file, t.line, t.ok
}

// Sprint returns a traceback of the given error, formatted
// like a stack trace.
func Sprint(err error) string {
	var trace []error
	for e := err; e != nil; e = errors.Unwrap(e) {
		trace = append(trace, e)
	}

	var traceback strings.Builder
	traceback.WriteString("Error: ")
	traceback.WriteString(err.Error())
	traceback.WriteRune('\n')
	traceback.WriteRune('\n')
	for i := len(trace) - 1; i >= 0; i-- {
		e := trace[i]

		switch e := e.(type) {
		case Error:
			function, file, line, ok := e.Source()
			if ok {
				traceback.WriteString(function)
				traceback.WriteRune('(')
				traceback.WriteRune(')')
				traceback.WriteRune('\n')
				traceback.WriteRune('\t')
				traceback.WriteString(file)
				traceback.WriteRune(':')
				traceback.WriteString(strconv.Itoa(line))
				traceback.WriteRune('\n')
			}
		}
	}
	return traceback.String()
}
