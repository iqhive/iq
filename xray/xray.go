// Package xray provides debugging tools and tracing functionality.
package xray

import (
	"context"
	"encoding/json"
	"os"
	"reflect"
	"runtime"
	"time"

	"github.com/google/uuid"
)

// Print prints the given values to standard out.
// The output has no guaranteed format and should
// only be used for debugging purposes.
func Print(v ...interface{}) {
	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "\t")
	enc.SetEscapeHTML(false)
	for _, val := range v {
		enc.Encode(val)
	}
}

// Sheet that was scanned.
type Sheet struct {
	Time time.Time // Time that the log was recorded
	Text string    // Text format.
	Args []any     // Arguments to the text format.

	Func string // Function name
	File string // File name
	Line int    // Line number
}

type keyID struct{}
type keyLogs struct{}
type keyOperations struct{}
type keyScanner struct{}

type scanner struct {
	parent context.Context

	id         string
	sheets     []Sheet
	operations map[reflect.Type][]operation
}

func (w *scanner) Deadline() (time.Time, bool) {
	return w.parent.Deadline()
}

func (w *scanner) Done() <-chan struct{} {
	return w.parent.Done()
}

func (w *scanner) Err() error {
	return w.parent.Err()
}

func (w *scanner) Value(key interface{}) interface{} {
	switch key.(type) {
	case keyID:
		return w.id
	case keyLogs:
		return &w.sheets
	case keyOperations:
		return w.operations
	case keyScanner:
		return w
	}
	return w.parent.Value(key)
}

// New returns a xray context with a unique identifier.
func New(ctx context.Context) context.Context {
	id := Get(ctx)
	if id == "" {
		id = uuid.New().String()
	}
	if w, ok := ctx.(*scanner); ok {
		copy := *w
		copy.id = id
		copy.parent = ctx
		return &copy
	}
	if s, ok := ctx.Value(keyScanner{}).(*scanner); ok {
		copy := *s
		copy.id = id
		copy.parent = ctx
		return &copy
	}
	return &scanner{
		id:         id,
		parent:     ctx,
		operations: make(map[reflect.Type][]operation),
	}
}

type disabled struct {
	context.Context
}

func (w *disabled) Value(key interface{}) interface{} {
	switch key.(type) {
	case keyID:
		return nil
	case keyLogs:
		return nil
	case keyOperations:
		return nil
	case keyScanner:
		return nil
	}
	return w.Context.Value(key)
}

// Off returns a context with any parent xray disabled.
func Off(ctx context.Context) context.Context {
	return &disabled{ctx}
}

// Get returns the string identifier for the xray.
func Get(ctx context.Context) string {
	if w, ok := ctx.(*scanner); ok {
		return w.id
	}
	id, ok := ctx.Value(keyID{}).(string)
	if !ok {
		return ""
	}
	return id
}

// Set returns a context with the xray's identifier
// set to the given string value. If the given string
// is empty, a new xray is created.
func Set(ctx context.Context, id string) context.Context {
	if id == "" {
		return New(ctx)
	}
	if w, ok := ctx.(*scanner); ok {
		w.id = id
		return ctx
	}
	if s, ok := ctx.Value(keyScanner{}).(*scanner); ok {
		s.id = id
		return ctx
	}
	return &scanner{
		id:     id,
		parent: ctx,
	}
}

// Scanning returns true if the given context contains
// an xray.
func Scanning(ctx context.Context) bool {
	if _, ok := ctx.(*scanner); ok {
		return true
	}
	_, ok := ctx.Value(keyLogs{}).(*[]Sheet)
	return ok
}

// Debug returns the xray sheets attached to the given
// context.
func Debug(ctx context.Context) []Sheet {
	if w, ok := ctx.(*scanner); ok {
		if w.sheets == nil {
			return nil
		}
		return w.sheets
	}
	logs, ok := ctx.Value(keyLogs{}).(*[]Sheet)
	if !ok {
		return nil
	}
	if logs == nil {
		return nil
	}
	return *logs
}

// Scan scans the given format and arguments and produces
// an xray sheet that can be later inspected for debugging.
func Scan(ctx context.Context, format string, args ...any) {
	var logs *[]Sheet
	if w, ok := ctx.(*scanner); ok {
		logs = &w.sheets
	} else {
		logs, ok = ctx.Value(keyLogs{}).(*[]Sheet)
		if !ok {
			return
		}
	}
	if logs != nil {
		fn := ""
		pc, file, line, ok := runtime.Caller(1)
		if ok {
			fn = runtime.FuncForPC(pc).Name()
		}
		*logs = append(*logs, Sheet{
			Time: time.Now(),
			Text: format,
			Args: args,

			Func: fn,
			File: file,
			Line: line,
		})
	}
}
