package limit_test

import (
	"testing"

	"pkg.iqhive.com/iq/limit"
)

type Request struct {
	Name limit.Bytes[string, [32]limit.Len]
}

func TestLimit(t *testing.T) {
	var req Request

	if err := req.Name.Set("hello"); err != nil {
		t.Fatal(err)
	}
	if err := req.Name.Set("dkfjsfdsmfs;dfmsd;fmsd;fmsdfmsd;fsdmf;dskmfsd;fms;fkmsd;fmds;kfmds;fmsd;fdsfdsf"); err == nil {
		t.Fatal("should have errored")
	}
}
