/*
Package limit provides a way to create slices, strings and maps with a maximum size

For example, if you have a struct with a field that must be a string between 1 and 32 characters
it can be defined like this:

	type Request struct {
		Name limit.Bytes[string, [32]limit.Max]
	}

Both this package and the maybe package are very similar. The difference is that the maybe package
is for primitive types and the limit package is for slices, strings and maps.
*/
package limit

import (
	"encoding"
	"encoding/json"
	"fmt"
)

type Len struct{}

type length interface {
	[1]Len | [2]Len | [3]Len | [4]Len | [5]Len | [6]Len | [7]Len | [8]Len | [9]Len | [10]Len |
		[11]Len | [12]Len | [13]Len | [14]Len | [15]Len | [16]Len | [17]Len | [18]Len | [19]Len | [20]Len |
		[21]Len | [22]Len | [23]Len | [24]Len | [25]Len | [26]Len | [27]Len | [28]Len | [29]Len | [30]Len |
		[31]Len | [32]Len | [33]Len | [34]Len | [35]Len | [36]Len | [37]Len | [38]Len | [39]Len | [40]Len |
		[41]Len | [42]Len | [43]Len | [44]Len | [45]Len | [46]Len | [47]Len | [48]Len | [49]Len | [50]Len |
		[51]Len | [52]Len | [53]Len | [54]Len | [55]Len | [56]Len | [57]Len | [58]Len | [59]Len | [60]Len |
		[61]Len | [62]Len | [63]Len | [64]Len | [65]Len | [66]Len | [67]Len | [68]Len | [69]Len | [70]Len |
		[71]Len | [72]Len | [73]Len | [74]Len | [75]Len | [76]Len | [77]Len | [78]Len | [79]Len | [80]Len |

		[100]Len |

		// common powers of two
		[128]Len | [256]Len | [512]Len | [1024]Len | [2048]Len | [4096]Len | [8192]Len | [16384]Len | [32768]Len | [65536]Len
}

// Bytes limits the container length to the given maximum length.
type Bytes[T ~string | ~[]byte, Length length] struct {
	bytes[T, Length]
}

type bytes[T ~string | ~[]byte, Length length] struct {
	value T
}

// Get returns the value of the container.
func (s bytes[T, Length]) Get() T {
	return s.value
}

// Set sets the value of the container. If the value is too long, an error is returned.
func (s *bytes[T, Length]) Set(value T) error {
	var limit Length
	if len(value) > len(limit) {
		return fmt.Errorf("cannot fit value of length %d into string with limit %d", len(value), len(limit))
	}
	s.value = value
	return nil
}

// String returns the value of the container as a string.
func (s bytes[T, Length]) String() string {
	return string(s.value)
}

// MarshalText implements the encoding.TextMarshaler interface.
func (s bytes[T, Length]) MarshalText() ([]byte, error) {
	return []byte(s.value), nil
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (s *bytes[T, Length]) UnmarshalText(text []byte) error {
	var limit Length
	if len(text) > len(limit) {
		return fmt.Errorf("cannot fit value of length %d into string with limit %d", len(text), len(limit))
	}
	s.value = T(text)
	return nil
}

// Slice limits the length of a slice to the given maximum length.
type Slice[T any, Length length] struct {
	exportSliceMethods[T, Length]
}

type exportSliceMethods[T any, Length length] struct {
	value []T
}

// Index returns a pointer to the value at the given index.
func (slice *exportSliceMethods[T, Length]) Index(i int) *T {
	return &slice.value[i]
}

// Len returns the length of the slice.
func (slice exportSliceMethods[T, Length]) Len() int {
	return len(slice.value)
}

// Get returns the value of the container.
func (slice exportSliceMethods[T, Length]) Get() []T {
	return slice.value
}

// Set sets the value of the container. If the value is too long, an error is returned.
func (slice *exportSliceMethods[T, Length]) Set(value []T) error {
	var limit Length
	if len(value) > len(limit) {
		return fmt.Errorf("cannot fit value of length %d into slice with limit %d", len(value), len(limit))
	}
	slice.value = value
	return nil
}

// MarshalJSON implements the json.Marshaler interface.
func (slice exportSliceMethods[T, Length]) MarshalJSON() ([]byte, error) {
	return json.Marshal(slice.value)
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (slice *exportSliceMethods[T, Length]) UnmarshalJSON(text []byte) error {
	if err := json.Unmarshal(text, &slice.value); err != nil {
		return err
	}
	var limit Length
	if len(slice.value) > len(limit) {
		return fmt.Errorf("cannot fit value of length %d into slice with limit %d", len(slice.value), len(limit))
	}
	return nil
}

// Map limits the length of a map to the given maximum length.
type Map[K comparable, V any, Length length] struct {
	exportMapMethods[K, V, Length]
}

type exportMapMethods[K comparable, V any, Length length] struct {
	value map[K]V
}

// Get returns the value of the map at the given key.
func (m exportMapMethods[K, V, Length]) Get(key K) V {
	return m.value[key]
}

// Set sets the value of the map at the given key. If the map is already full, an error is returned.
func (m *exportMapMethods[K, V, Length]) Set(key K, value V) error {
	if m.value == nil {
		m.value = make(map[K]V)
	}
	var limit Length
	if len(m.value) == len(limit) {
		return fmt.Errorf("cannot set value in map with limit %d", len(limit))
	}
	m.value[key] = value
	return nil
}

// Len returns the length of the map.
func (m exportMapMethods[K, V, Length]) Len() int {
	return len(m.value)
}

// Range calls the given function for each key/value pair in the map.
func (m exportMapMethods[K, V, Length]) Range(fn func(key K, value V)) {
	for key, value := range m.value {
		fn(key, value)
	}
}

// MarshalJSON implements the json.Marshaler interface.
func (m exportMapMethods[K, V, Length]) MarshalJSON() ([]byte, error) {
	return json.Marshal(m.value)
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (m *exportMapMethods[K, V, Length]) UnmarshalJSON(text []byte) error {
	if err := json.Unmarshal(text, &m.value); err != nil {
		return err
	}
	var limit Length
	if len(m.value) > len(limit) {
		return fmt.Errorf("cannot fit value of length %d into map with limit %d", len(m.value), len(limit))
	}
	return nil
}

type isText interface {
	encoding.TextMarshaler
}

// Text limits the length of the string returned from the
// constrained type's String() method. Will be marshalled
// as text.
type Text[T isText, Length length] struct {
	text[T, Length]
}

type text[T isText, Length length] struct {
	value T
}

// Get returns the value of the container.
func (s *text[T, Length]) Get() T {
	return s.value
}

// Set sets the value of the container. If the value is too long, an error is returned
// and the value is not set.
func (s *text[T, Length]) Set(value T) error {
	var limit Length
	b, err := text[T, Length]{value: value}.MarshalText()
	if err != nil {
		return err
	}
	if len(b) > len(limit) {
		return fmt.Errorf("cannot fit value of encoded-length %d into text with limit %d", len(b), len(limit))
	}
	s.value = value
	return nil
}

// String returns the value of the container as a string.
func (s text[T, Length]) String() string {
	b, err := s.MarshalText()
	if err != nil {
		return fmt.Sprintf("[ERROR %s]", err)
	}
	return string(b)
}

// MarshalText implements the encoding.TextMarshaler interface.
func (s text[T, Length]) MarshalText() ([]byte, error) {
	return s.value.MarshalText()
}

// UnmarshalText implements the encoding.TextUnmarshaler interface.
func (s *text[T, Length]) UnmarshalText(text []byte) error {
	var limit Length
	if len(text) > len(limit) {
		return fmt.Errorf("cannot fit value of length %d into string with limit %d", len(text), len(limit))
	}
	decoder, ok := any(&s.value).(encoding.TextUnmarshaler)
	if !ok {
		return fmt.Errorf("cannot unmarshal text into %T", &s.value)
	}
	return decoder.UnmarshalText(text)
}
