package please_test

import (
	"errors"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/please"
	"pkg.iqhive.com/iq/xray"
)

func TestErrorHandling(t *testing.T) {
	err := please.Provide("a valid argument")
	fmt.Println(err)
	fmt.Println(xray.Sprint(err))

	err = please.Provide("a valid thing", errors.New("invalid thing"))
	fmt.Println(err)
	fmt.Println(xray.Sprint(err))
}
