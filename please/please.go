// Package please provides helpful errors to return to the user/client/caller of your function
package please

import (
	"errors"
	"fmt"
	"runtime"

	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"
)

type literal string

type trace = xray.Trace

type errorWithTrace struct {
	trace
	error

	customer, developer, thirdparty *string

	status int
}

func (e *errorWithTrace) StatusHTTP() int {
	if e.status == 0 {
		return 500
	}
	return e.status
}

func (e *errorWithTrace) Unwrap() error {
	return e.error
}

func (e *errorWithTrace) Agent() (customer, developer, thirdparty *string) {
	return e.customer, e.developer, e.thirdparty
}

var _ oops.ErrorWithAgent = (*errorWithTrace)(nil)

// Fix wraps the given error, if that error is a client/caller/user
// error, the solution will be preserved. Useful to enable
// helper functions to handle validation for you.
func Fix(err error) error {
	if err == nil {
		return nil
	}

	var w = errorWithTrace{
		trace: xray.NewTrace(runtime.Caller(1)),
		error: err,
	}

	if actionable, ok := err.(oops.ErrorWithAgent); ok {
		w.customer, w.developer, w.thirdparty = actionable.Agent()
	}

	if hasStatus, ok := err.(interface {
		StatusHTTP() int
	}); ok {
		status := hasStatus.StatusHTTP()
		if status >= 400 && status < 500 {
			w.status = status
		}
	}

	return &w
}

// pleaseError is a user-visible error.
type pleaseError struct {
	trace

	toFix  error
	please string
}

func (e *pleaseError) Error() string {
	return fmt.Sprintf("please %v", e.please)
}

func (e *pleaseError) Unwrap() error {
	return e.toFix
}

func (e *pleaseError) StatusHTTP() int {
	return 400
}

var unspecified string

func (e *pleaseError) Agent() (customer, developer, thirdparty *string) {
	return &unspecified, nil, nil
}

// Authenticate indicates that the returned error is caused by
// missing authentication and the the missing key(s) should be
// detailed. Semantically acts like please.Provide.
func Authenticate(key string) error {
	return &pleaseError{
		trace:  xray.NewTrace(runtime.Caller(1)),
		toFix:  errors.New("missing authentication"),
		please: "please provide " + key + " (so that you can be authenticated)",
	}
}

// Provide indicates that the returned error is caused by
// a malformed or invalid input, argument or request to this
// function. You must include a description of what needs to
// be provided to this function in order for it to succeed.
//
// The solution is formatted and should contain a string
// that describes what needs to be provided in order to
// avoid this error.
//
// ie. "a valid UTF8 string"
// ie. "a name parameter"
//
// As a special case, the first 'args' argument may be an error.
// this value WILL BE REMOVED from the formatting and used
// to record the internal error that prompted what needs to be
// provided to this function.
//
// The subject of the returned error is 'input'.
func Provide(description literal, args ...interface{}) oops.ErrorWithAgent {
	var err error
	if len(args) > 0 {
		if e, ok := args[0].(error); ok {
			err = e
			args = args[1:]
		}
	}

	if err == nil {
		err = errors.New("invalid input")
	}

	return &pleaseError{
		trace:  xray.NewTrace(runtime.Caller(1)),
		toFix:  err,
		please: fmt.Sprintf("provide "+string(description), args...),
	}
}
