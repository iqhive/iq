// Package cash provides representations for monetary values.
package cash

// Money amount in the base denomination of the currency.
// Either a credit or debit amount. ie. cents.
type Money int64

type literal uint64

// Dollars returns the specified amount in dollars,
// where 1 dollar is 100 cents.
func Dollars(amount literal) Money {
	return Money(amount * 100)
}

// Currency is a 3-letter ISO 4217 currency code.
type Currency string

// Debit amount in the base denomination of the currency.
type Debit int64

// Credit amount in the base denomination of the currency.
type Credit int64

// Balance amount, can be positive or negative.
type Balance int64
