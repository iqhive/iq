package data

import (
	"context"

	"pkg.iqhive.com/iq/oops"
)

// Log of values.
type Log[V comparable] interface {
	// Append a new value to the log.
	Append(context.Context, V) error
	// Length returns the length of the log.
	Length(context.Context) (int, error)
	// Offset returns a reference to the Nth
	// entry in the log.
	Offset(int) Ref[V]
	// Stream returns a stream of the log's
	// values within the given range.
	Stream(context.Context, *Range) Stream[V]
	// Shrink the log by the given number of
	// values.
	Shrink(context.Context, int) error
}

// NewLog returns an in-memory list of values.
func NewLog[T comparable](values ...T) Log[T] {
	ctx := context.Background()
	var log = New[int, T]()
	for i, value := range values {
		log.Insert(ctx, i, value)
	}
	return log
}

// Append a new value to the list, an index will be automatically
// created using [Map.Create].
func (data Source[K, V]) Append(ctx context.Context, val V) error {
	index, err := data.Create(ctx)
	if err != nil {
		return err
	}
	return data.Insert(ctx, index, val)
}

// Length returns the number of values in the map.
func (data Source[K, V]) Length(ctx context.Context) (int, error) {
	var count Counter[int64]
	if err := data.Report(ctx, nil, func(key *K, val *V) Stats { return Stats{count.Add()} }); err != nil {
		return 0, err
	}
	return int(count.value), nil
}

// Result returns the Nth value in the map unless the map was filtered
// with an sort order, the index is chosen with an unspecified order.
func (data Source[K, V]) Offset(n int) Ref[V] {
	return refSourceOffset[K, V]{
		Source: data,
		Offset: n,
	}
}

// Values returns a result iterator over the values in the map, unless the map was filtered
// with an sort order, values are returned in an unspecified order.
func (data Source[K, V]) Stream(ctx context.Context, slice *Range) Stream[V] {
	var query QueryBuilder[K, V]
	if slice != nil {
		query = func(key *K, val *V) Query {
			return Query{
				slice,
			}
		}
	}
	results := data.Search(ctx, query)
	var stream = make(Stream[V])
	go func() {
		for result := range results {
			_, value, err := result.Get()
			if err != nil {
				stream.Close(ctx, err)
				return
			}
			stream.Send(ctx, value)
		}
		stream.Close(ctx, nil)
	}()
	return stream
}

// Shrink is a destructive operation that deletes the given number of
// values from the end of the map. To delete all values, pass the
// maps length here. unless the map was filtered
// with an sort order, the discarded values may be chosen with an
// unspecified order.
func (data Source[K, V]) Shrink(ctx context.Context, length int) error {
	query := func(key *K, val *V) Query {
		return Query{
			Range{
				From: -length,
				Upto: -1,
			},
		}
	}
	_, err := data.Update(ctx, query, func(val *V) Patch { return Patch{Delete} })
	if err != nil {
		return oops.New(err)
	}
	return nil
}
