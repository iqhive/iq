package data_test

import (
	"context"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/data"
)

func TestLog(t *testing.T) {
	ctx := context.Background()

	var log = data.NewLog[int](0, 1, 2)
	if err := log.Append(ctx, 3); err != nil {
		t.Fatal(err)
	}
	for val := range log.Stream(ctx, nil) {
		fmt.Println(val.Get())
	}

}
