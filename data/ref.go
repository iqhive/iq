package data

import (
	"context"
	"errors"

	"pkg.iqhive.com/iq/oops"
)

// Ref refers to a single value inside a data source.
type Ref[V any] interface {
	// Put the value. If the value already exists, it will be
	// overwritten. This operation is idempotent.
	Put(context.Context, V) error
	// Mod modifies the value using the given patch. If the value does
	// not exist, it will be created.
	Mod(context.Context, PatchBuilder[V]) error
	// Get the value. If the value does not exist, the second return
	// value will be false.
	Get(context.Context) (V, bool, error)
	// Del the value (deleting it). If the value does not exist,
	// the operation is idempotent.
	Del(context.Context) error
}

// Select returns a reference to the value at the given index.
func (m Source[K, V]) Select(key K) Ref[V] {
	return refSourceKey[K, V]{
		Source: m,
		Key:    key,
	}
}

type refSourceKey[K comparable, V any] struct {
	Source Source[K, V]
	Key    K
}

func (data refSourceKey[K, V]) Put(ctx context.Context, value V) error {
	return data.Source.Insert(ctx, data.Key, value)
}

func (data refSourceKey[K, V]) Mod(ctx context.Context, patch PatchBuilder[V]) error {
	return data.Source.Mutate(ctx, data.Key, patch)
}

func (data refSourceKey[K, V]) Get(ctx context.Context) (V, bool, error) {
	return data.Source.Lookup(ctx, data.Key)
}

func (data refSourceKey[K, V]) Del(ctx context.Context) error {
	return data.Source.Delete(ctx, data.Key)
}

type refSourceOffset[K comparable, V any] struct {
	Source Source[K, V]
	Offset int
}

func (data refSourceOffset[K, V]) Put(ctx context.Context, val V) error {
	query := func(key *K, val *V) Query {
		return Query{
			Range{
				From: data.Offset,
				Upto: data.Offset,
			},
		}
	}
	ok, err := data.Source.Update(ctx, query, func(v *V) Patch { return Patch{Alter(v, val)} })
	if err != nil {
		return oops.New(err)
	}
	if !ok {
		return oops.New(errors.New("out of bounds"))
	}
	return nil
}

func (data refSourceOffset[K, V]) Mod(ctx context.Context, patch PatchBuilder[V]) error {
	query := func(key *K, val *V) Query {
		return Query{
			Range{
				From: data.Offset,
				Upto: data.Offset,
			},
		}
	}
	ok, err := data.Source.Update(ctx, query, patch)
	if err != nil {
		return oops.New(err)
	}
	if !ok {
		return oops.New(errors.New("out of bounds"))
	}
	return nil
}

func (data refSourceOffset[K, V]) Get(ctx context.Context) (V, bool, error) {
	results := data.Source.Search(ctx, func(key *K, val *V) Query {
		return Query{
			Range{
				From: data.Offset,
				Upto: data.Offset,
			},
		}
	})
	result, ok := <-results
	if !ok {
		var val V
		return val, false, oops.New(errors.New("out of bounds"))
	}
	_, val, err := result.Get()
	if err != nil {
		return val, false, oops.New(err)
	}
	return val, true, nil
}

func (data refSourceOffset[K, V]) Del(ctx context.Context) error {
	query := func(key *K, val *V) Query {
		return Query{
			Range{
				From: data.Offset,
				Upto: data.Offset,
			},
		}
	}
	ok, err := data.Source.Update(ctx, query, func(value *V) Patch { return Patch{Delete} })
	if err != nil {
		return oops.New(err)
	}
	if !ok {
		return oops.New(errors.New("out of bounds"))
	}
	return nil
}
