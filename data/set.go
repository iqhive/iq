package data

import "context"

// Set only records the presence of values.
type Set[V comparable] interface {
	// Add the value to the set, the operation is idempotent.
	Add(context.Context, V) error
	// Has returns true if the value is in the set.
	Has(context.Context, V) (bool, error)
	// Del the value from the set, the operation is idempotent.
	Del(context.Context, V) error
	// Len returns the number of values in the set.
	Len(context.Context) (int, error)
	// Idx returns a stream of each element in the
	// set within the given range.
	Idx(context.Context, *Range) Stream[V]
}

// Add the specified index to the data source, such that any Lookup operations
// will return true. The value will be initialized to an empty value.
func (data Source[K, V]) Add(ctx context.Context, key K) error {
	var val V
	return data.Insert(ctx, key, val)
}

// Has returns true if the index exists in the data source.
func (data Source[K, V]) Has(ctx context.Context, key K) (bool, error) {
	_, ok, err := data.Lookup(ctx, key)
	return ok, err
}

// Len returns the number of values in the data source.
func (data Source[K, V]) Len(ctx context.Context) (int, error) {
	return data.Length(ctx)
}

// Idx returns a stream of the indicies in the data source within the given range.
func (data Source[K, V]) Idx(ctx context.Context, slice *Range) Stream[K] {
	var query QueryBuilder[K, V]
	if slice != nil {
		query = func(key *K, val *V) Query { return Query{slice} }
	}
	results := data.Search(ctx, query)
	var stream = make(Stream[K])
	go func() {
		for result := range results {
			index, _, err := result.Get()
			if err != nil {
				stream.Close(ctx, err)
				return
			}
			stream.Send(ctx, index)
		}
		stream.Close(ctx, nil)
	}()
	return stream
}
