/*
Package data provides a set of interfaces for working with external data-sources.

Several data collections are supported, [Source], [Ref], [Set], [Map] and [Log].

If you need full support for search queries, atomic operations and conditional
writes. Use a [Source] directly. Otherwise, if you just need a key-value store
with Go map semantics, use a [Map]. If you prefer a bag of values, use a [Set].
If you can do with a list of values, use a [Log]. A single value can be
represented with a [Ref].

A [Source] implements all of these collection types and can be opened from a
[Base] or created in-memory with [New].

Index and value types may not contain pointers (unless those pointers implement [Type])
and the only permitted interface values are [Source], [Ref], [Set], [Map] and [Log] types.

# Designing a schema

Domain objects should be split into comparable index and value pairs.

	// Domain type.
	type Customer struct {
		api.Model `
			domain model.`

		ID int64
		Age uint
		Name string
	}

	// Internal Database Index Type.
	type dbID int64

	// Internal Database Value Type.
	type dbCustomer struct {
		data.Model `
			database model.`

		Name string
		Age uint
	}

	// Database representation.
	var customers data.Map[dbID, dbCustomer]

Insert a record into the table, overwriting it if the ID already exists.

	err := customers.Insert(ctx, id, &dbCustomer{Name: "Bob", Age: 23})

Lookup a record from the table.

	customer, exists, err := customers.Lookup(ctx, id)

Delete the record from the table.

	err := customers.Delete(ctx, id)

# Searches

It is possible to search for records in the table and iterate through
the results. For example, to search for customers with the age "22":

	query := func(index *dbID, value *dbCustomer) data.Query {
		return data.Query{
			data.Where(&value.Age).Equals(22),
		}
	}
	for result := range customers.Search(ctx, query) {
		id, customer, err := result()
		if err != nil {
			return err
		}
		fmt.Println(customer.Name)
	}

# Queries

Similarly to SQL, queries can be constructed using a series of operations:

	data.Where(&field).Equals(x)   // field == x
	data.Where(&field).AtLeast(x)  // field >= x
	data.Where(&field).AtMost(x)   // field <= x
	data.Where(&field).MoreThan(x) // field > x
	data.Where(&field).LessThan(x) // field < x

	data.Match(&field).Contains(x)  // strings.Contains(field, x)
	data.Match(&field).HasPrefix(x) // strings.HasPrefix(field, x)
	data.Match(&field).HasSuffix(x) // strings.HasSuffix(field, x)

	data.Order(&field).Increasing() // ORDER BY field ASC
	data.Order(&field).Decreasing() // ORDER BY field DESC

	data.Range{From:0, Upto:10} // data[from:upto+1]

	data.Empty(&field) // reflect.ValueOf(field).IsZero()
	data.Avoid(q)      // !q

	// switch
	data.Cases(
		data.Where(&field).Equals(x),
		data.Where(&field).Equals(y),
		...
	)

# Conditional Writes

Each write operation supports conditionals, so that they can only be completed
when the provided checks match the existing record (which may be a zero value).

	ifBob := func(index *dbUUID, value *dbCustomer) data.Query {
		return data.Query{
			data.Where(&index).Equals(1),
			data.Where(&value.Name).Equals("Bob",),
		}
	}
	ok, err := customers.Update(ctx, ifBob, func(value dbCustomer) data.Patch {
		return data.Patch{
			data.Alter(&value.Name, "Alice"),
		}
	}))

In the example above, the Customer with ID 1 will only have their name
updated to "Alice" if their name was "Bob". If the customer's names is
not Bob, ok will be false.

# Reporting (Not Implemented / Design)

Statistics can be reported on a map in the database with [db.Counter].

	var (
		count data.Counter[uint]
		total data.Counter[uint]
	)

	allBobs := func(index *dbUUID, value *dbCustomer) data.Query {
		return data.Query{
			data.Where(&value.Name).Equals("Bob"),
		}
	}
	err := customers.Report(ctx, allBobs, func(index dbUUID, value dbCustomer) data.Stats {
		return data.Stats{
			count.Add(),
			total.Sum(&value.Age),
		}
	})

# Joins (Not Implemented / Design)

	var join data.Join[string, customer, string, order]

	cutomerID, _ := join.Index()
		_, order := join.Value()
	var links = data.Links(customerID, &order.CustomerID)


	var query = func() data.Query {
		customer, order := join.Value()
		return data.Query{
			data.Where(&customer.Name).Equals("Alice"),
		}
	}

	// Intersection method is used to control Inner vs Outer join.
	for result := range join.Inner(links, customers, orders).Search(query) {
		index, value, err := result.Get()
		if err != nil {
			return err
		}
		customer, order := value.Split()
		fmt.Println(order)
	}

# Atomic Operations

In order to avoid partial updates and race-conditions, multi-write operations
can be semantically grouped together as an atomic operation so that they can be
managed atomically.

	tx := customers.Atomic(func(ctx context.Context) error {
		//...
	})
	if err := tx(ctx); err != nil {
		// was rolled back
	}
*/
package data

// FIXME currently the following Where clause is terrible for implementations to manage:
// 	query := func(index *string, value *string) data.Query {
//		return data.Query{
//			data.Where(&index).Equals("123"),
//		}
// 	}
//
// it produces a [Field] with an empty 'Name' with 'Index' set to true and drivers need
// to figure out the name.

import (
	"context"
	"reflect"
	"strings"

	"pkg.iqhive.com/iq/oops"
)

// Model can be embedded inside a database value, to document its purpose as
// a database structure.
type Model struct{}

// Exists is short for checking that value != zero
// where zero is the zero value for the Value's type.
// By convention, zero values are considered to
// correspond to a database record that does not exist.
func Exists[Value comparable](value Value) bool {
	var zero Value
	return value != zero
}

// QueryComponent can be placed inside of a [Query].
type QueryComponent interface {
	eval() bool
}

// Query describes a filter on a given database table, has AND semantics
// for each clause.
type Query []QueryComponent

func (q Query) eval() bool {
	for _, clause := range q {
		if !clause.eval() {
			return false
		}
	}
	return true
}

// Matches returns true if the given index and value match the where
// clauses in the query.
func (q Query) Matches() bool {
	for _, clause := range q {
		if !clause.eval() {
			return false
		}
	}
	return true
}

// Range returns the range clause in the query and if it exists.
func (q Query) Range() (r Range, ok bool) {
	for _, clause := range q {
		if rangeClause, ok := clause.(Range); ok {
			return rangeClause, true
		}
	}
	return Range{}, false
}

// Sorter used for in-memory implementations.
type Sorter interface {
	Sort()
	Less() bool
}

// Sorters returns the sorters in the query.
func (q Query) Sorters() []Sorter {
	var sorters []Sorter
	for _, clause := range q {
		if sorter, ok := clause.(Sorter); ok {
			sorters = append(sorters, sorter)
		}
	}
	return sorters
}

// QueryBuilder defines a type-safe query.
type QueryBuilder[K comparable, V any] func(index *K, value *V) Query

// Build returns the constructed query.
func (q QueryBuilder[K, V]) Build() Query {
	if q == nil {
		return Query{}
	}
	index := sentinal[K](indexSentinals, true)
	value := sentinal[V](valueSentinals, false)
	return q(index, value)
}

// PatchComponent can be placed inside of a [Patch].
type PatchComponent interface {
	write()
}

// Patch describes a set of changes to be applied to one or more database records.
type Patch []PatchComponent

// Apply the patch.
func (p Patch) Apply() {
	for _, component := range p {
		component.write()
	}
}

// PatchBuilder defines a type-safe patch.
type PatchBuilder[V any] func(value *V) Patch

// Build returns the constructed query.
func (p PatchBuilder[V]) Build() Patch {
	return p(sentinal[V](valueSentinals, false))
}

// Operation is a retryable (and deterministic) function that performs
// a sequence of consistent reads and writes.
type Operation func(context.Context) error

type ordered interface {
	~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~int | ~int8 | ~int16 | ~int32 | ~int64 | ~float32 | ~float64 | ~string
}

// Comparison operations, for use in queries.
type Comparison[T ordered] struct {
	Equals   func(T) Equals
	AtMost   func(T) AtMost
	AtLeast  func(T) AtLeast
	MoreThan func(T) MoreThan
	LessThan func(T) LessThan
}

// Index returns a comparison that checks for equality
// on the index, the index must be provided.
func Index[T comparable](index *T, x T) Equals {
	return equals[T]{field: index, value: x}
}

// Where selects a database field/column for making a comparison on.
func Where[T ordered](value *T) Comparison[T] {
	return Comparison[T]{
		Equals:   func(x T) Equals { return equals[T]{field: value, value: x} },
		AtMost:   func(x T) AtMost { return atMost[T]{field: value, value: x} },
		AtLeast:  func(x T) AtLeast { return atLeast[T]{field: value, value: x} },
		MoreThan: func(x T) MoreThan { return moreThan[T]{field: value, value: x} },
		LessThan: func(x T) LessThan { return lessThan[T]{field: value, value: x} },
	}
}

// Pattern matching operations, for use in queries.
type Pattern[T ordered] struct {
	Contains  func(T) Contains
	HasPrefix func(T) HasPrefix
	HasSuffix func(T) HasSuffix
}

// Match selects a database field/column for perfoming a pattern match on.
func Match[T ~string](value *T) Pattern[T] {
	return Pattern[T]{
		Contains:  func(x T) Contains { return contains[T]{field: value, value: x} },
		HasPrefix: func(x T) HasPrefix { return hasPrefix[T]{field: value, value: x} },
		HasSuffix: func(x T) HasSuffix { return hasSuffix[T]{field: value, value: x} },
	}
}

// Empty matches zero values.
func Empty[T comparable](value *T) Equals {
	var zero T
	return equals[T]{field: value, value: zero}
}

// Avoid matches data that does not match the given expression.
func Avoid(expr QueryComponent) QueryComponent {
	return Not{expr}
}

// Ordering selection.
type Ordering struct {
	Increasing func() Increasing
	Decreasing func() Decreasing
}

// Order selects a database field/column for ordering the results by.
func Order[T ordered](value *T) Ordering {
	return Ordering{
		Increasing: func() Increasing { return &increasing[T]{field: value} },
		Decreasing: func() Decreasing { return &decreasing[T]{field: value} },
	}
}

// Result of a search.
type result[K comparable, V any] struct {
	index K
	value V
	error error
}

// Get result values.
func (r result[Index, Value]) Get() (Index, Value, error) {
	return r.index, r.value, r.error
}

// Results is a channel of results from a search.
type Results[K comparable, V any] chan result[K, V]

// Close the results, sending the given error if it is non-nil.
func (results Results[K, V]) Close(ctx context.Context, err error) {
	if err != nil {
		select {
		case results <- result[K, V]{error: err}:
		case <-ctx.Done():
			select {
			case results <- result[K, V]{error: ctx.Err()}:
			default:
			}
		}
	}
	close(results)
}

// Send a result to the given channel.
func (results Results[Index, Value]) Send(ctx context.Context, index Index, value Value) {
	sending := result[Index, Value]{index: index, value: value}
	select {
	case results <- sending:
	case <-ctx.Done():
		select {
		case results <- result[Index, Value]{error: ctx.Err()}:
		default:
		}
	}
}

type single[T any] struct {
	value T
	error error
}

// Get result values.
func (r single[T]) Get() (T, error) {
	return r.value, r.error
}

type Stream[T any] chan single[T]

// Close the results, sending the given error if it is non-nil.
func (results Stream[Value]) Close(ctx context.Context, err error) {
	if err != nil {
		select {
		case results <- single[Value]{error: err}:
		case <-ctx.Done():
			select {
			case results <- single[Value]{error: ctx.Err()}:
			default:
			}
		}
	}
	close(results)
}

// Send a result to the given channel.
func (results Stream[Value]) Send(ctx context.Context, value Value) {
	sending := single[Value]{value: value}
	select {
	case results <- sending:
	case <-ctx.Done():
		select {
		case results <- single[Value]{error: ctx.Err()}:
		default:
		}
	}
}

// Alter returns a [PatchComponent] that sets the given field to the given value.
func Alter[T any](field *T, value T) Alteration {
	return alteration[T]{field: field, value: value}
}

type where[T any] struct {
	field *T
	value T
}

type Equals interface {
	QueryComponent

	Equals() Field
}

type equals[T comparable] where[T]

func (e equals[T]) eval() bool { return *e.field == e.value }
func (e equals[T]) Equals() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type Not struct {
	QueryComponent
}

func (n Not) eval() bool { return !n.QueryComponent.eval() }

type AtLeast interface {
	QueryComponent

	AtLeast() Field
}

type atLeast[T ordered] where[T]

func (e atLeast[T]) eval() bool { return *e.field >= e.value }
func (e atLeast[T]) AtLeast() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type AtMost interface {
	QueryComponent

	AtMost() Field
}

type atMost[T ordered] where[T]

func (e atMost[T]) eval() bool { return *e.field <= e.value }
func (e atMost[T]) AtMost() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type MoreThan interface {
	QueryComponent

	MoreThan() Field
}

type moreThan[T ordered] where[T]

func (e moreThan[T]) eval() bool { return *e.field > e.value }
func (e moreThan[T]) MoreThan() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type LessThan interface {
	QueryComponent

	LessThan() Field
}

type lessThan[T ordered] where[T]

func (e lessThan[T]) eval() bool { return *e.field < e.value }
func (e lessThan[T]) LessThan() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type Contains interface {
	QueryComponent

	Contains() Field
}

type contains[T ~string] where[T]

func (e contains[T]) eval() bool { return strings.Contains(string(*e.field), string(e.value)) }
func (e contains[T]) Contains() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type HasPrefix interface {
	QueryComponent

	HasPrefix() Field
}

type hasPrefix[T ~string] where[T]

func (e hasPrefix[T]) eval() bool {
	return strings.HasPrefix(string(*e.field), string(e.value))
}
func (e hasPrefix[T]) HasPrefix() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type HasSuffix interface {
	QueryComponent

	HasSuffix() Field
}

type hasSuffix[T ~string] where[T]

func (e hasSuffix[T]) eval() bool { return strings.HasSuffix(string(*e.field), string(e.value)) }
func (e hasSuffix[T]) HasSuffix() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type Decreasing interface {
	QueryComponent

	Decreasing() Field
}

type decreasing[T ordered] where[T]

func (e *decreasing[T]) Sort()      { e.value = *e.field }
func (e *decreasing[T]) Less() bool { return *e.field < e.value }
func (e *decreasing[T]) eval() bool { return true }
func (e *decreasing[T]) Decreasing() Field {
	return mirror[e.field]
}

type Increasing interface {
	QueryComponent

	Increasing() Field
}

type increasing[T ordered] where[T]

func (e *increasing[T]) Sort()      { e.value = *e.field }
func (e *increasing[T]) Less() bool { return *e.field > e.value }
func (e *increasing[T]) eval() bool { return true }
func (e *increasing[T]) Increasing() Field {
	return mirror[e.field]
}

type Alteration interface {
	PatchComponent

	Alteration() Field
}

type alteration[T any] struct {
	field *T
	value T
}

func (e alteration[T]) write() { *e.field = e.value }
func (e alteration[T]) Alteration() Field {
	f := mirror[e.field]
	f.Value = reflect.ValueOf(&e.value).Elem()
	return f
}

type dangerous struct{}

func (dangerous) write() {}

// Delete is a data patch component that deletes the record.
var Delete dangerous

// Range is an inclusive range (or slice) on the data
// to be operated on. Index starts at 0. Range{0,0}
// means the first element, Range{0,1} means the first
// two elements, etc.
type Range struct {
	From int
	Upto int
}

func (r Range) eval() bool { return true }

// Cases is a list of QueryComponents that are evaluated in order,
// if any one case is true, the entire expression is true.
type Cases []QueryComponent

func (c Cases) eval() bool {
	for _, c := range c {
		if c.eval() {
			return true
		}
	}
	return false
}

func (m Source[K, V]) Report(context.Context, QueryBuilder[K, V], StatsBuilder[K, V]) error {
	return oops.NotImplemented()
}

type StatsBuilder[K comparable, V any] func(*K, *V) Stats

type Stats []Stat

type Stat interface {
	measure()
}

type Countable interface {
	~int8 | ~int16 | ~int32 | ~int64 | ~uint8 | ~uint16 | ~uint32 | ~uint64 | ~uintptr | ~float32 | ~float64
}

type Counter[T Countable] struct {
	value T
}

func (c Counter[T]) Add() Stat {
	return nil
}

func (c Counter[T]) Sum(field T) Stat {
	return nil
}

func (c Counter[T]) Avg(field T) Stat {
	return nil
}
