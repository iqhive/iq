package firestore_test

import (
	"context"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/data/firestore"
)

type Customer struct {
	FirstName string
	LastName  string
	CreatedAt int64
}

var customers = firestore.Map[string, Customer]("tellonz-preprod customers")

func TestFirestore(t *testing.T) {
	ctx := context.Background()

	query := func(uuid *string, customer *Customer) data.Query {
		return data.Query{
			data.Where(&customer.FirstName).Equals("Quentin"),
			data.Range{From: 0, Upto: 100},
		}
	}
	for result := range customers.Search(ctx, query) {
		uuid, customer, err := result.Get()
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(uuid, customer)
	}
}

func TestCompound(t *testing.T) {
	ctx := context.Background()

	type Address struct {
		City string
	}
	type AddressIndex struct {
		CustomerUUID string
		AddressUUID  string
	}

	var addresses = firestore.Map[AddressIndex, Address]("tellonz-preprod customers/{CustomerUUID}/addresses/{AddressUUID}")

	query := func(index *AddressIndex, address *Address) data.Query {
		return data.Query{
			data.Where(&index.CustomerUUID).Equals("00003829-f9e5-4ea2-a8a5-89f0eb66cf25"),
			data.Range{From: 0, Upto: 100},
		}
	}
	for result := range addresses.Search(ctx, query) {
		uuid, address, err := result.Get()
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(uuid, address)
	}
}
