package firestore

import (
	"context"
	"path"
	"strings"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"
)

// TODO define the value semantics for Go structures, potentially use a map layer.

// key for context values
type key string

const (
	keyBulkWriter  key = "BulkWriter"
	keyTransaction key = "Transaction"
)

type locationWalker[Index, Value comparable] struct {
	ctx context.Context

	client *firestore.Client

	index Index
	value Value

	built data.Query
	check data.Query

	location data.Location[Index]

	results data.Results[Index, Value]
}

func (q *locationWalker[Index, Value]) Lookup(uri string) error {
	ref := q.client.Collection(path.Dir(uri)).Doc(path.Base(uri))

	doc, err := ref.Get(q.ctx)
	if err != nil {
		return oops.New(err)
	}

	_, path, _ := strings.Cut(doc.Ref.Path, "documents")
	q.index, err = q.location.Scan(path)
	if err != nil {
		return oops.New(err)
	}

	if err := doc.DataTo(&q.value); err != nil {
		return oops.New(err)
	}

	if q.check.Matches() {
		q.results.Send(q.ctx, q.index, q.value)
	}

	return nil
}

func (q *locationWalker[Index, Value]) Search(uri string, more bool, each func(string) error) error {
	uri = strings.TrimSuffix(uri, "/")

	if more {
		iter := q.client.Collection(uri).Documents(q.ctx)
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return err
			}
			if err := each(doc.Ref.ID); err != nil {
				return err
			}
		}
		return nil
	}

	var search = q.client.Collection(uri).Query
	var filter = true

	// Most of the DB query operations have 1:1 firestore equivalents.
	for _, component := range q.built {
		switch v := component.(type) {
		case data.Equals:
			field := v.Equals()
			search = search.Where(field.Name, "==", field.Value.Interface())
		case data.AtLeast:
			field := v.AtLeast()
			search = search.Where(field.Name, ">=", field.Value.Interface())
		case data.AtMost:
			field := v.AtMost()
			search = search.Where(field.Name, "<=", field.Value.Interface())
		case data.MoreThan:
			field := v.MoreThan()
			search = search.Where(field.Name, ">", field.Value.Interface())
		case data.LessThan:
			field := v.LessThan()
			search = search.Where(field.Name, "<", field.Value.Interface())

		case data.Contains, data.HasPrefix, data.HasSuffix, data.Cases, data.Not:
			filter = false // no support in Firestore (https://groups.google.com/g/firebase-talk/c/AygJJEF8zcM?pli=1

		case data.Increasing:
			field := v.Increasing()
			search = search.OrderBy(field.Name, firestore.Asc)
		case data.Decreasing:
			field := v.Decreasing()
			search = search.OrderBy(field.Name, firestore.Desc)

		case data.Range:
			search = search.Offset(int(v.From))
			search = search.Limit(int(v.Upto - v.From))
		}
	}

	var iter *firestore.DocumentIterator

	if tx, ok := q.ctx.Value(keyTransaction).(*firestore.Transaction); ok {
		iter = tx.Documents(q.client.Collection(uri))
		if filter {
			iter = tx.Documents(search)
		}
	} else {
		iter = q.client.Collection(uri).Documents(q.ctx)
		if filter {
			iter = search.Documents(q.ctx)
		}
	}

	for {
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return oops.New(err)
		}

		_, path, _ := strings.Cut(doc.Ref.Path, "documents")
		q.index, err = q.location.Scan(path)
		if err != nil {
			return oops.New(err)
		}

		if err := doc.DataTo(&q.value); err != nil {
			return oops.New(err)
		}

		if q.check.Matches() {
			q.results.Send(q.ctx, q.index, q.value)
		}
	}

	return nil
}

func search[Index, Value comparable](ctx context.Context, location data.Location[Index], project string, builder data.QueryBuilder[Index, Value], results data.Results[Index, Value]) {
	client, err := firestore.NewClient(ctx, project)
	if err != nil {
		results.Close(ctx, err)
		return
	}
	defer client.Close()

	walker := &locationWalker[Index, Value]{
		ctx:      ctx,
		client:   client,
		built:    builder.Build(),
		location: location,
		results:  results,
	}
	walker.check = builder(&walker.index, &walker.value)

	results.Close(ctx, location.Walk(walker.built, walker))
}

// Set a database to be backed by Firestore.
// Connection string is the GCP project name followed by a space and then the [db.MapLocation].
func Map[Index, Value comparable](connection string) data.Source[Index, Value] {
	var m data.Source[Index, Value]

	project, location, ok := strings.Cut(connection, " ")
	if !ok {
		panic("invalid location")
	}
	collection := data.Location[Index](location)

	m.Mutate = func(ctx context.Context, index Index, p data.PatchBuilder[Value]) error {
		client, err := firestore.NewClient(ctx, project)
		if err != nil {
			return oops.New(err)
		}
		defer client.Close()

		uri := collection.Path(index)
		ref := client.Collection(path.Dir(uri)).Doc(path.Base(uri))
		if ref == nil {
			xray.Scan(ctx, uri)
		}

		patch := p.Build()

		var updates []firestore.Update
		for _, component := range patch {
			switch component := component.(type) {
			case data.Alteration:
				field := component.Alteration()

				name := field.Name
				if tag, ok := field.Tags.Lookup("firestore"); ok {
					name, _, _ = strings.Cut(tag, ",")
				}

				updates = append(updates, firestore.Update{
					Path:  name,
					Value: field.Value.Interface(),
				})
			}
		}

		tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction)
		if ok {
			err = tx.Update(ref, updates)
		} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
			_, err = writer.Update(ref, updates)
		} else {
			_, err = ref.Update(ctx, updates)
		}

		if err != nil {
			if status.Code(err) != codes.NotFound {
				return oops.New(err)
			}

			// the document does not exist yet, so we need to atomically create it with
			// the requested patch applied to the zero value.
			createPatched := func(ctx context.Context, t *firestore.Transaction) error {
				var value Value

				doc, err := t.Get(ref)
				switch err {
				case nil:
					if err := doc.DataTo(&value); err != nil {
						return oops.New(err)
					}
				default:
					if status.Code(err) != codes.NotFound {
						return oops.New(err)
					}
				}

				p(&value).Apply()

				return oops.New(t.Set(ref, value))
			}

			if tx == nil {
				err := client.RunTransaction(ctx, createPatched)
				if err != nil {
					return oops.New(err)
				}
			} else {
				createPatched(ctx, tx)
			}
		}

		return nil
	}
	m.Update = func(ctx context.Context, q data.QueryBuilder[Index, Value], p data.PatchBuilder[Value]) (bool, error) {
		var found bool
		for result := range m.Search(ctx, q) {
			index, _, err := result.Get()
			if err != nil {
				return false, err
			}

			if err := m.Mutate(ctx, index, p); err != nil {
				return false, err
			}

			found = true
		}
		return found, nil
	}
	m.Search = func(ctx context.Context, query data.QueryBuilder[Index, Value]) data.Results[Index, Value] {
		var results = make(data.Results[Index, Value])
		go search(ctx, collection, project, query, results)
		return results
	}
	m.Insert = func(ctx context.Context, index Index, value Value) error {
		client, err := firestore.NewClient(ctx, project)
		if err != nil {
			return oops.New(err)
		}
		defer client.Close()

		uri := collection.Path(index)
		ref := client.Collection(path.Dir(uri)).Doc(path.Base(uri))
		if ref == nil {
			xray.Scan(ctx, uri)
		}

		if data.Exists(value) {
			if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
				err = tx.Set(ref, value)
			} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
				_, err = writer.Set(ref, value)
			} else {
				_, err = ref.Set(ctx, value)
			}
			if err != nil {
				return oops.New(err)
			}
		} else {
			if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
				err = tx.Delete(ref)
			} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
				_, err = writer.Delete(ref)
			} else {
				_, err = ref.Delete(ctx)
			}
			if err != nil {
				return oops.New(err)
			}
		}

		return nil
	}
	m.Create = func(ctx context.Context) (Index, error) {
		var index Index

		client, err := firestore.NewClient(ctx, project)
		if err != nil {
			return index, oops.New(err)
		}
		defer client.Close()

		uri := collection.Path(index)
		dir := client.Collection(path.Dir(uri))

		ref := dir.NewDoc()
		if ref == nil {
			xray.Scan(ctx, uri)
		}

		return collection.Scan(ref.Path)
	}
	m.Atomic = func(f data.Operation) data.Operation {
		return func(ctx context.Context) error {
			client, err := firestore.NewClient(ctx, project)
			if err != nil {
				return oops.New(err)
			}
			defer client.Close()

			return client.RunTransaction(ctx, func(ctx context.Context, t *firestore.Transaction) error {
				return f(context.WithValue(ctx, keyTransaction, t))
			})
		}
	}
	m.Lookup = func(ctx context.Context, index Index) (Value, bool, error) {
		var value Value

		client, err := firestore.NewClient(ctx, project)
		if err != nil {
			return value, false, oops.New(err)
		}
		defer client.Close()

		uri := collection.Path(index)
		ref := client.Collection(path.Dir(uri)).Doc(path.Base(uri))
		if ref == nil {
			xray.Scan(ctx, uri)
		}

		var doc *firestore.DocumentSnapshot

		if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
			doc, err = tx.Get(ref)
		} else {
			doc, err = ref.Get(ctx)
		}
		if err != nil {
			if status.Code(err) == codes.NotFound {
				return value, false, nil
			}
			return value, false, oops.New(err)
		}

		if err := doc.DataTo(&value); err != nil {
			return value, false, oops.New(err)
		}

		return value, true, nil
	}
	m.Delete = func(ctx context.Context, index Index) error {
		client, err := firestore.NewClient(ctx, project)
		if err != nil {
			return oops.New(err)
		}
		defer client.Close()

		uri := collection.Path(index)
		ref := client.Collection(path.Dir(uri)).Doc(path.Base(uri))
		if ref == nil {
			xray.Scan(ctx, uri)
		}

		if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
			err = tx.Delete(ref)
		} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
			_, err = writer.Delete(ref)
		} else {
			_, err = ref.Delete(ctx)
		}
		if err != nil {
			return oops.New(err)
		}

		return nil
	}
	m.Buffer = func(f data.Operation) data.Operation {
		return func(ctx context.Context) error {
			client, err := firestore.NewClient(ctx, project)
			if err != nil {
				return oops.New(err)
			}
			defer client.Close()

			writer := client.BulkWriter(ctx)
			defer writer.Flush()

			return f(context.WithValue(ctx, keyBulkWriter, writer))
		}
	}

	return m
}
