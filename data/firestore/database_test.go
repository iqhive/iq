package firestore_test

import (
	"context"
	"os"
	"testing"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/data/firestore"
	"pkg.iqhive.com/iq/xray"
)

func TestDatabase(t *testing.T) {
	Project := os.Getenv("FIRESTORE")
	if Project == "" {
		t.Skip()
	}
	db, err := firestore.New(context.Background(), Project)
	if err != nil {
		t.Fatal(xray.Sprint(err))
	}
	if err := data.Test(context.Background(), db); err != nil {
		t.Fatal(xray.Sprint(err))
	}
}
