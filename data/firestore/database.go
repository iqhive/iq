package firestore

import (
	"context"
	"fmt"
	"path"
	"reflect"
	"strings"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"
)

type Database struct {
	project string
	client  *firestore.Client
}

func New(ctx context.Context, project string) (Database, error) {
	client, err := firestore.NewClient(ctx, project)
	if err != nil {
		return Database{}, oops.New(err)
	}
	return Database{
		project: project,
		client:  client,
	}, nil
}

// Close the database connection.
func (db Database) Close() error {
	if db.client == nil {
		return nil
	}
	return db.client.Close()
}

func newMapping(pointer data.Pointer) map[string]any {
	var mapping = make(map[string]any)
	for _, field := range pointer.Fields {
		name := field.Name
		if tag, ok := field.Tags.Lookup("firestore"); ok {
			name, _, _ = strings.Cut(tag, ",")
		}
		value := reflect.ValueOf(field.Value.Interface())
		switch value.Kind() {
		case reflect.Uint, reflect.Uintptr, reflect.Uint64:
			mapping[name] = fmt.Sprint(value.Uint())
		default:
			mapping[name] = field.Value.Interface()
		}
	}
	return mapping
}

func setMapping(pointer data.Pointer, fields map[string]any) error {
	for _, field := range pointer.Fields {
		name := field.Name
		if tag, ok := field.Tags.Lookup("firestore"); ok {
			name, _, _ = strings.Cut(tag, ",")
		}
		value, ok := fields[name]
		if !ok {
			continue
		}
		rvalue := reflect.ValueOf(value)
		if rvalue.Type().AssignableTo(field.Value.Type()) {
			field.Value.Set(rvalue)
		}
	}
	return nil
}

func (db Database) Mutate(ctx context.Context, pointer data.Pointer, patch data.Patch) (err error) {
	uri := pointer.Path()
	if strings.Count(uri, "/")%2 == 0 {
		uri = path.Dir(uri) + "/collection/" + path.Base(uri)
	}
	ref := db.client.Collection(path.Dir(uri)).Doc(path.Base(uri))
	if ref == nil {
		xray.Scan(ctx, uri)
	}
	var updates []firestore.Update
	for _, component := range patch {
		switch component := component.(type) {
		case data.Alteration:
			field := component.Alteration()

			name := field.Name
			if tag, ok := field.Tags.Lookup("firestore"); ok {
				name, _, _ = strings.Cut(tag, ",")
			}

			updates = append(updates, firestore.Update{
				Path:  name,
				Value: field.Value.Interface(),
			})
		}
	}
	tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction)
	if ok {
		err = tx.Update(ref, updates)
	} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
		_, err = writer.Update(ref, updates)
	} else {
		_, err = ref.Update(ctx, updates)
	}
	if err != nil {
		if status.Code(err) != codes.NotFound {
			return oops.New(err)
		}
		// the document does not exist yet, so we need to atomically create it with
		// the requested patch applied to the zero value.
		createPatched := func(ctx context.Context, t *firestore.Transaction) error {
			mapping := newMapping(pointer)
			_, err := t.Get(ref)
			switch err {
			case nil:
				return oops.New(t.Update(ref, updates))
			default:
				if status.Code(err) != codes.NotFound {
					return oops.New(err)
				}
			}
			return oops.New(t.Set(ref, mapping))
		}
		if tx == nil {
			err := db.client.RunTransaction(ctx, createPatched)
			fmt.Println("TX ", err)
			if err != nil {
				return oops.New(err)
			}
		} else {
			createPatched(ctx, tx)
		}
	}

	return nil
}

func (db Database) Update(ctx context.Context, pointer data.Pointer, query data.Query, patch data.Patch) (bool, error) {
	var found bool
	if err := db.Search(ctx, pointer, query, func() error {
		found = true
		return oops.New(db.Mutate(ctx, pointer, patch))
	}); err != nil {
		return false, oops.New(err)
	}
	return found, nil
}

func (db Database) Search(ctx context.Context, pointer data.Pointer, query data.Query, next func() error) error {
	return oops.New(pointer.Walk(query, &walker{
		ctx:     ctx,
		client:  db.client,
		query:   query,
		pointer: pointer,
		next:    next,
	}))
}

func (db Database) Insert(ctx context.Context, pointer data.Pointer) (err error) {
	uri := pointer.Path()
	if strings.Count(uri, "/")%2 == 0 {
		uri = path.Dir(uri) + "/collection/" + path.Base(uri)
	}
	fmt.Println(uri)
	ref := db.client.Collection(path.Dir(uri)).Doc(path.Base(uri))
	if ref == nil {
		xray.Scan(ctx, uri)
	}
	value := newMapping(pointer)
	if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
		err = tx.Set(ref, value)
	} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
		_, err = writer.Set(ref, value)
	} else {
		_, err = ref.Set(ctx, value)
	}
	if err != nil {
		return oops.New(err)
	}
	return nil
}

func (db Database) Create(ctx context.Context, pointer data.Pointer) error {
	uri := pointer.Path()
	if strings.Count(uri, "/")%2 == 0 {
		uri = path.Dir(uri) + "/collection/" + path.Base(uri)
	}
	dir := db.client.Collection(path.Dir(uri))
	ref := dir.NewDoc()
	if ref == nil {
		xray.Scan(ctx, uri)
	}
	return oops.New(pointer.Scan(ref.Path))
}

func (db Database) Atomic(pointer data.Pointer, fn data.Operation) data.Operation {
	return func(ctx context.Context) error {
		return db.client.RunTransaction(ctx, func(ctx context.Context, t *firestore.Transaction) error {
			return fn(context.WithValue(ctx, keyTransaction, t))
		})
	}
}

func (db Database) Lookup(ctx context.Context, pointer data.Pointer) (ok bool, err error) {
	uri := pointer.Path()
	if strings.Count(uri, "/")%2 == 0 {
		uri = path.Dir(uri) + "/collection/" + path.Base(uri)
	}
	ref := db.client.Collection(path.Dir(uri)).Doc(path.Base(uri))
	if ref == nil {
		xray.Scan(ctx, uri)
	}
	var doc *firestore.DocumentSnapshot
	if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
		doc, err = tx.Get(ref)
	} else {
		doc, err = ref.Get(ctx)
	}
	if err != nil {
		if status.Code(err) == codes.NotFound {
			return false, nil
		}
		return false, oops.New(err)
	}
	if err := setMapping(pointer, doc.Data()); err != nil {
		return false, oops.New(err)
	}
	return true, nil
}

func (db Database) Delete(ctx context.Context, pointer data.Pointer) (err error) {
	uri := pointer.Path()
	if strings.Count(uri, "/")%2 == 0 {
		uri = path.Dir(uri) + "/collection/" + path.Base(uri)
	}
	ref := db.client.Collection(path.Dir(uri)).Doc(path.Base(uri))
	if ref == nil {
		xray.Scan(ctx, uri)
	}
	if tx, ok := ctx.Value(keyTransaction).(*firestore.Transaction); ok {
		err = tx.Delete(ref)
	} else if writer, ok := ctx.Value(keyBulkWriter).(*firestore.BulkWriter); ok {
		_, err = writer.Delete(ref)
	} else {
		_, err = ref.Delete(ctx)
	}
	if err != nil {
		return oops.New(err)
	}
	return nil
}

func (db Database) Buffer(pointer data.Pointer, fn data.Operation) data.Operation {
	return func(ctx context.Context) error {
		writer := db.client.BulkWriter(ctx)
		defer writer.Flush()
		return fn(context.WithValue(ctx, keyBulkWriter, writer))
	}
}

type walker struct {
	ctx     context.Context
	client  *firestore.Client
	query   data.Query
	pointer data.Pointer
	next    func() error
}

func (result *walker) Lookup(uri string) error {
	ref := result.client.Collection(path.Dir(uri)).Doc(path.Base(uri))
	doc, err := ref.Get(result.ctx)
	if err != nil {
		return oops.New(err)
	}
	_, path, _ := strings.Cut(doc.Ref.Path, "documents")
	if err := result.pointer.Scan(path); err != nil {
		return oops.New(err)
	}
	var mapping = newMapping(result.pointer)
	if err := doc.DataTo(&mapping); err != nil {
		return oops.New(err)
	}
	if result.query.Matches() {
		if err := result.next(); err != nil {
			return oops.New(err)
		}
	}
	return nil
}

func (result *walker) Search(uri string, more bool, each func(string) error) error {
	uri = strings.TrimSuffix(uri, "/")
	if strings.Count(uri, "/")%2 != 0 {
		uri = uri + "/collection"
	}
	if more {
		iter := result.client.Collection(uri).Documents(result.ctx)
		for {
			doc, err := iter.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				return err
			}
			if err := each(doc.Ref.ID); err != nil {
				return err
			}
		}
		return nil
	}
	fmt.Println("Searching inside ", uri)
	var search = result.client.Collection(uri).Query
	var filter = true
	// Most of the DB query operations have 1:1 firestore equivalents.
	for _, component := range result.query {
		switch v := component.(type) {
		case data.Equals:
			field := v.Equals()
			search = search.Where(field.Name, "==", field.Value.Interface())
		case data.AtLeast:
			field := v.AtLeast()
			search = search.Where(field.Name, ">=", field.Value.Interface())
		case data.AtMost:
			field := v.AtMost()
			search = search.Where(field.Name, "<=", field.Value.Interface())
		case data.MoreThan:
			field := v.MoreThan()
			search = search.Where(field.Name, ">", field.Value.Interface())
		case data.LessThan:
			field := v.LessThan()
			search = search.Where(field.Name, "<", field.Value.Interface())

		case data.Contains, data.HasPrefix, data.HasSuffix, data.Cases, data.Not:
			filter = false // no support in Firestore (https://groups.google.com/g/firebase-talk/c/AygJJEF8zcM?pli=1

		case data.Increasing:
			field := v.Increasing()
			search = search.OrderBy(field.Name, firestore.Asc)
		case data.Decreasing:
			field := v.Decreasing()
			search = search.OrderBy(field.Name, firestore.Desc)

		case data.Range:
			search = search.Offset(int(v.From))
			search = search.Limit(int(v.Upto - v.From))
		}
	}
	var iter *firestore.DocumentIterator
	if tx, ok := result.ctx.Value(keyTransaction).(*firestore.Transaction); ok {
		iter = tx.Documents(result.client.Collection(uri))
		if filter {
			iter = tx.Documents(search)
		}
	} else {
		iter = result.client.Collection(uri).Documents(result.ctx)
		fmt.Println("iterator on documents", uri)
		if filter {
			fmt.Println("iterator filtered")
			iter = search.Documents(result.ctx)
		}
	}
	for {
		doc, err := iter.Next()
		fmt.Println(doc, err)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return oops.New(err)
		}
		_, path, _ := strings.Cut(doc.Ref.Path, "documents")
		if err := result.pointer.Scan(path); err != nil {
			return oops.New(err)
		}
		if err := setMapping(result.pointer, doc.Data()); err != nil {
			return oops.New(err)
		}
		if err := result.next(); err != nil {
			return oops.New(err)
		}
	}
	return nil
}
