package data

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"sync"

	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/please"
)

// Source corresponds to a table or collection in the database. It should be used as the
// value type of fields within a Schema structure. The K type can be thought of as the
// primary key(s) in a SQL database, or the key in a key-value store.
type Source[K comparable, V any] struct {

	// Mutate use the given patch builder to apply a patch to the map entry uniquely
	// identified by the given index, the operation is idempotent. Only the fields
	// specified in the patch will be modified.  The K value may not be empty.
	Mutate func(context.Context, K, PatchBuilder[V]) error

	// Update applies the given patch to a series of map entries (not guaranteed to be atomic),
	// if an error occurs, the update operation may have been partially applied. It's best
	// to structure the query, so that updated entries are excluded, this enables the update to
	// be incrementally applied. This operation is idempotent.
	Update func(context.Context, QueryBuilder[K, V], PatchBuilder[V]) (bool, error)

	// Search return a [Results] channel that will be populated with the results of the query.
	// If an error occurs, it will be passed to the results channel and can be inspected by
	// calling Get on the result. The channel will be closed, either when the context is Done,
	// after an error is delivered on the channel, or when the results are exhausted.
	Search func(context.Context, QueryBuilder[K, V]) Results[K, V]

	// Insert an entry into the Map, overwriting any existing value at the given index. This
	// operation is idempotent. It has HTTP PUT semantics. The K value may not be empty.
	Insert func(context.Context, K, V) error

	// Create returns an database-generated index, where available. If a client-generated index
	// is required, override this function with a custom implementation.
	Create func(context.Context) (K, error)

	// Atomic requests that database operations performed by this function on this Map are made
	// atomically. If the same database and driver is being used for multiple Maps, then any
	// operations made within the function may be atomic across both Maps. Also known as a
	// Transaction. Nested Buffer calls will behave as if they called this function instead of
	// Buffer.
	Atomic func(Operation) Operation

	// Lookup a value in the map, uniquely identified by the given index. If the value does not
	// exist, the second return value will be false.  The K value may not be empty.
	Lookup func(context.Context, K) (V, bool, error)

	// Delete an entry from the Map, the operation is idempotent. It has HTTP DELETE semantics.
	// The K value may not be empty.
	Delete func(context.Context, K) error

	// Buffer requests that any Mutate, Search, Insert and Delete calls made to the map made
	// within this function's context should be buffered, threaded and batched where possible.
	// Any write operations made within this function should be considered to be performed in
	// parallel in an undefined order. Read operations made within this function are not
	// garuanteed to observe any previous write calls made within the function. Search results
	// may arrive in any order (so they should be selected upon within a goroutine). Update,
	// Create, Atomic and Lookup operations are not permitted to be called within this function.
	Buffer func(Operation) Operation
}

func fieldsOf(op any) []Field {
	var fields []Field
	switch op := op.(type) {
	case Equals:
		fields = append(fields, op.Equals())
	case AtLeast:
		fields = append(fields, op.AtLeast())
	case AtMost:
		fields = append(fields, op.AtMost())
	case MoreThan:
		fields = append(fields, op.MoreThan())
	case LessThan:
		fields = append(fields, op.LessThan())
	case Contains:
		fields = append(fields, op.Contains())
	case HasPrefix:
		fields = append(fields, op.HasPrefix())
	case HasSuffix:
		fields = append(fields, op.HasSuffix())
	case Not:
		fields = append(fields, fieldsOf(op)...)
	case Alteration:
		fields = append(fields, op.Alteration())
	}
	return fields
}

// Filter returns a filtered view of the map, that only includes records that match
// the given query. Writes made to the filtered map will only be applied to the
// underlying map if the result can be proven to match the filter. Values will only
// be deleted from the underlying map if they match the filter.
func (data Source[K, V]) Filter(filtered QueryBuilder[K, V]) Source[K, V] {
	filter := filtered.Build()
	return Source[K, V]{
		Mutate: func(ctx context.Context, index K, patch PatchBuilder[V]) error {
			var filledFields = fieldsOf(patch.Build())
			var filterFields = fieldsOf(filter)
			var filledMap = make(map[string]Field)
			for _, field := range filledFields {
				filledMap[field.Name] = field
			}
			for i, field := range filterFields {
				if field.Index {
					filterFields[i] = filledFields[len(filledFields)-1]
					filledFields = filledFields[:len(filledFields)-1]
				}
			}
			for _, field := range filterFields {
				if _, ok := filledMap[field.Name]; !ok {
					return oops.New(fmt.Errorf("cannot assert filter, requires field %s to be specified in the data.Patch", field.Name))
				}
			}
			var value V
			patch(&value).Apply()
			filter := filtered(&index, &value)
			if !filter.Matches() {
				return oops.New(fmt.Errorf("cannot mutate data.Map, the resulting value does not match the filter"))
			}
			query := func(i *K, v *V) Query {
				return append(filtered(i, v), Index(i, index))
			}
			ok, err := data.Update(ctx, query, patch)
			if err != nil {
				return oops.New(err)
			}
			if !ok {
				return oops.NotFound()
			}
			return nil
		},
		Update: func(ctx context.Context, query QueryBuilder[K, V], patch PatchBuilder[V]) (bool, error) {
			var filledFields = fieldsOf(patch.Build())
			var filterFields = fieldsOf(filter)
			var filledMap = make(map[string]Field)
			for _, field := range filledFields {
				filledMap[field.Name] = field
			}

			var index K
			var value V
			patch(&value).Apply()
			filterStripIndex := filtered(&index, &value)

			for i, field := range filterFields {
				if field.Index {
					filterFields[i] = filledFields[len(filledFields)-1]
					filledFields = filledFields[:len(filledFields)-1]
					filterStripIndex[i] = filterStripIndex[len(filterStripIndex)-1]
					filterStripIndex = filterStripIndex[:len(filterStripIndex)-1]
				}
			}
			for _, field := range filterFields {
				if _, ok := filledMap[field.Name]; !ok {
					return false, oops.New(fmt.Errorf("cannot assert filter, requires field %s to be specified in the data.Patch", field.Name))
				}
			}
			if !filterStripIndex.Matches() {
				return false, oops.New(fmt.Errorf("cannot update data.Map, updated values won't match the filter"))
			}
			filtered := func(i *K, v *V) Query {
				return append(filtered(i, v), query(i, v)...)
			}
			return data.Update(ctx, filtered, patch)
		},
		Search: func(ctx context.Context, query QueryBuilder[K, V]) Results[K, V] {
			return data.Search(ctx, func(i *K, v *V) Query {
				return append(filtered(i, v), query(i, v)...)
			})
		},
		Insert: func(ctx context.Context, index K, value V) error {
			filter := filtered(&index, &value)
			if filter.Matches() {
				return data.Insert(ctx, index, value)
			}
			return please.Provide("a value that matches the data.Map's filter") // TODO detail the filter?
		},
		Create: func(ctx context.Context) (K, error) {
			return data.Create(ctx)
		},
		Atomic: data.Atomic,
		Lookup: func(ctx context.Context, index K) (V, bool, error) {
			var value V
			results := data.Search(ctx, func(i *K, v *V) Query {
				return append(filtered(i, v), Index(i, index), Range{0, 0})
			})
			result, ok := <-results
			if !ok {
				return value, false, nil
			}
			_, value, err := result.Get()
			if err != nil {
				return value, false, oops.New(err)
			}
			return value, true, nil
		},
		Delete: func(ctx context.Context, index K) error {
			query := func(i *K, v *V) Query {
				return append(filtered(i, v), Index(i, index))
			}
			ok, err := data.Update(ctx, query, func(value *V) Patch {
				return Patch{Delete}
			})
			if err != nil {
				return oops.New(err)
			}
			if !ok {
				return oops.NotFound()
			}
			return nil
		},
		Buffer: data.Buffer,
	}
}

type atomic struct{}

type dbResult[K comparable, V any] struct {
	Index K
	Value V
}

// New returns an empty in-memory data source for the given Index and Value types.
func New[K comparable, V any]() Source[K, V] {
	var m Source[K, V]

	var table = make(map[K]V)
	var mutex sync.RWMutex

	m.Create = func(ctx context.Context) (K, error) {
		var key K
		var rvalue = reflect.ValueOf(&key).Elem()
		switch rvalue.Kind() {
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			rvalue.SetInt(int64(len(table)))
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			rvalue.SetUint(uint64(len(table)))
		default:
			return key, oops.NotImplemented()
		}
		return key, nil
	}

	m.Mutate = func(ctx context.Context, key K, write PatchBuilder[V]) error {
		if ctx.Value(atomic{}) == nil {
			mutex.Lock()
			defer mutex.Unlock()
		}

		value := table[key]
		write(&value).Apply()
		table[key] = value
		return nil
	}
	m.Update = func(ctx context.Context, query QueryBuilder[K, V], patch PatchBuilder[V]) (bool, error) {
		if ctx.Value(atomic{}) == nil {
			mutex.Lock()
			defer mutex.Unlock()
		}

		var index K
		var value V
		var reader = query(&index, &value)
		var writer = patch(&value)

		var count int
		slice, limit := reader.Range()

		for index, value = range table {
			if reader.Matches() {
				if limit && count < slice.From || limit && count > slice.Upto {
					count++
					continue
				}
				count++

				writer.Apply()
				table[index] = value
			}
		}
		return count > 0, nil
	}
	m.Search = func(ctx context.Context, query QueryBuilder[K, V]) Results[K, V] {
		var channel = make(Results[K, V])
		go func() {

			var index K
			var value V
			var q Query
			if query != nil {
				q = query(&index, &value)
			}

			slice, limit := q.Range()
			var count int

			sorters := q.Sorters()

			switch {
			case sorters == nil: // results can be streamed in an undefined order.
				var results []dbResult[K, V]

				if ctx.Value(atomic{}) == nil {
					mutex.RLock()
				}
				for index, value = range table {
					if q.Matches() {
						if limit && count < slice.From || limit && count > slice.Upto {
							count++
							continue
						}
						count++
						results = append(results, dbResult[K, V]{index, value})
					}
				}
				if ctx.Value(atomic{}) == nil {
					mutex.RUnlock()
				}
				for _, result := range results {
					index, value = result.Index, result.Value
					channel.Send(ctx, index, value)
				}
			case sorters != nil: // results need to be loaded and then returned in sorted order.

				var results []dbResult[K, V]

				if ctx.Value(atomic{}) == nil {
					mutex.RLock()
				}
				for index, value = range table {
					if q.Matches() {
						results = append(results, dbResult[K, V]{index, value})
					}
				}
				if ctx.Value(atomic{}) == nil {
					mutex.RUnlock()
				}

				sort.Slice(results, func(i, j int) bool {
					for _, sorter := range sorters {
						index = results[i].Index
						value = results[i].Value
						sorter.Sort()
						index = results[j].Index
						value = results[j].Value
						if !sorter.Less() {
							return false
						}
					}
					return true
				})

				for _, result := range results {
					if limit && count < slice.From || limit && count > slice.Upto {
						count++
						continue
					}
					count++

					index, value := result.Index, result.Value
					channel.Send(ctx, index, value)
				}
			}
			channel.Close(ctx, nil)
		}()
		return channel
	}
	m.Insert = func(ctx context.Context, key K, val V) error {
		if ctx.Value(atomic{}) == nil {
			mutex.Lock()
			defer mutex.Unlock()
		}
		table[key] = val
		return nil
	}
	m.Atomic = func(f Operation) Operation {
		return func(ctx context.Context) error {
			if ctx.Value(atomic{}) == nil {
				mutex.Lock()
				defer mutex.Unlock()
			}

			ctx = context.WithValue(ctx, atomic{}, struct{}{})

			return f(ctx)
		}
	}
	m.Lookup = func(ctx context.Context, key K) (V, bool, error) {
		if ctx.Value(atomic{}) == nil {
			mutex.RLock()
			defer mutex.RUnlock()
		}
		val, ok := table[key]
		return val, ok, nil
	}
	m.Delete = func(ctx context.Context, key K) error {
		if ctx.Value(atomic{}) == nil {
			mutex.Lock()
			defer mutex.Unlock()
		}
		delete(table, key)
		return nil
	}
	m.Buffer = func(f Operation) Operation {
		return f
	}
	return m
}
