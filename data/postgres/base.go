package postgres

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/xray"
)

type Database struct {
	connection string
	pgPool     *pgPool

	mutex    sync.RWMutex // protects the schema assertion process, so that only one goroutine attempts it at a time.
	asserted map[string]bool
}

// Open returns a new postgres database connection that can be used
// to create data maps. When a [data.Source] using this [data.Base]
// tables will be created the first time the [data.Source] is used.
// If any missing fields are detected, they will be added to the table.
// This driver will not delete fields, nor change their types.
func New(ctx context.Context, connection string) (*Database, error) {
	db := Database{
		connection: connection,
		pgPool:     new(pgPool),
	}
	pool, err := pgxpool.Connect(ctx, connection)
	if err != nil {
		return &db, oops.New(err)
	}
	db.pgPool.ptr = pool
	return &db, nil
}

// only need to do this once for the Map.
func (c *Database) assertSchema(ctx context.Context, database pgConnection, record data.Pointer) error {
	table := record.TableName()

	if func() bool {
		c.mutex.RLock()
		defer c.mutex.RUnlock()
		return c.asserted[table]
	}() {
		return nil
	}
	c.mutex.Lock()
	defer c.mutex.Unlock()

	xray.Scan(ctx, selectColumns)

	rows, err := database.Query(ctx, selectColumns, table)
	if err != nil {
		return oops.New(err)
	}

	// figure out which columns are missing.
	var existing = make(map[string]pgColumn)

	var empty bool = true
	for rows.Next() {
		empty = false
		var name string
		var kind pgType
		var pkey bool
		if err := rows.Scan(&name, &kind, &pkey); err != nil {
			return oops.New(err)
		}
		existing[strconv.Quote(name)] = pgColumn{Type: kind, PrimaryKey: pkey}
	}

	var missing bool
	for _, column := range record.Fields {
		name := pgColumnNameFor(column)
		if current, ok := existing[name]; !ok || (column.Index && !current.PrimaryKey) {
			missing = true
		}
	}

	// split into two phases here, because there is no declarative way to create
	// or alter a table in SQL.
	if empty {
		query := "CREATE TABLE IF NOT EXISTS " + table + "();"

		xray.Scan(ctx, query)

		_, err := database.Exec(ctx, query)
		if err != nil {
			return oops.New(err)
		}
	}

	// add missing columns to the table.
	if missing {

		var query strings.Builder
		query.WriteString("BEGIN;")

		var keys = make([]string, 0) // all primary keys
		var adds []string

		for _, column := range record.Fields {
			name := pgColumnNameFor(column)

			if column.Index {
				keys = append(keys, pgColumnNameFor(column))
			}
			if _, ok := existing[name]; ok {
				continue
			}
			adds = append(adds, fmt.Sprintf("ADD COLUMN %s %s", name, pgTypeFor(column)))
		}
		if len(adds) > 0 {
			query.WriteString("ALTER TABLE " + table + "\n")
			query.WriteString(strings.Join(adds, ",\n"))
			query.WriteString(";\n")
		}

		var needToDropPrimaryKeys = false
		for _, key := range keys {
			if info, ok := existing[key]; !ok || !info.PrimaryKey {
				needToDropPrimaryKeys = true
			}
		}
		var needToAddPrimaryKeys = false
		for _, column := range record.Fields {
			name := pgColumnNameFor(column)
			if column.Index {
				if _, ok := existing[name]; !ok {
					needToAddPrimaryKeys = true
				}
			}
		}
		if needToDropPrimaryKeys {
			query.WriteString("ALTER TABLE " + table + "\n")
			query.WriteString("DROP CONSTRAINT IF EXISTS " + table + "_pkey;")
			query.WriteString(";\n")
		}
		if needToAddPrimaryKeys || needToDropPrimaryKeys {
			query.WriteString("ALTER TABLE " + table + "\n")
			fmt.Fprintf(&query, "ADD PRIMARY KEY (%s)", strings.Join(keys, ", "))
			query.WriteString(";\n")
		}
		query.WriteString("COMMIT;")

		xray.Scan(ctx, query.String())

		_, err := database.Exec(ctx, query.String())
		if err != nil {
			return oops.New(err)
		}
	}
	if c.asserted == nil {
		c.asserted = make(map[string]bool)
	}
	c.asserted[table] = true
	return nil
}

func (connection *Database) open(ctx context.Context, record data.Pointer) (conn pgConnection, done func(), err error) {
	if connection.pgPool == nil {
		return nil, nil, fmt.Errorf("nil postgres.Connection")
	}
	var pool *pgxpool.Pool
	func() {
		connection.pgPool.mut.RLock()
		defer connection.pgPool.mut.RUnlock()
		pool = connection.pgPool.ptr
	}()
	if pool == nil {
		if err := func() error {
			connection.pgPool.mut.Lock()
			defer connection.pgPool.mut.Unlock()
			pool, err = pgxpool.Connect(ctx, connection.connection)
			if err != nil {
				return oops.New(err)
			}
			connection.pgPool.ptr = pool
			return nil
		}(); err != nil {
			return nil, nil, err
		}
	}
	c, err := pool.Acquire(ctx)
	if err != nil {
		return nil, nil, oops.New(err)
	}
	if err := connection.assertSchema(ctx, c, record); err != nil {
		return c, nil, err
	}
	if tx := ctx.Value(pgContextKey{}); tx != nil {
		return tx.(pgConnection), func() {}, nil
	}
	return c, c.Release, nil
}

func (c *Database) Mutate(ctx context.Context, pointer data.Pointer, patch data.Patch) error {
	database, close, err := c.open(ctx, pointer)
	if err != nil {
		return oops.New(err)
	}
	defer close()

	var updateSettings = make([]string, 0, len(patch))
	var updateValues = make([]any, 0, len(patch))
	var table = make(map[string]int)

	for _, component := range patch {
		switch component := component.(type) {
		case data.Alteration:
			field := component.Alteration()

			table[field.Name] = len(updateValues)

			updateSettings = append(updateSettings, fmt.Sprintf("%s = $%d", pgColumnNameFor(field), len(updateSettings)+1))
			updateValues = append(updateValues, field.Value.Interface())
		default:
			return fmt.Errorf("unexpected patch component: %T", component)
		}
	}

	var conditions strings.Builder
	for i, col := range pointer.Fields {
		if col.Index {
			if i > 0 {
				conditions.WriteString(" AND ")
			}
			fmt.Fprintf(&conditions, "%s = $%d ", col.Name, len(updateValues)+1)
			updateValues = append(updateValues, col.Value.Interface())
		}
	}

	var query strings.Builder
	fmt.Fprintf(&query, "UPDATE %s SET %s WHERE %s;", pointer.TableName(), strings.Join(updateSettings, ", "), conditions.String())

	result, err := database.Exec(ctx, query.String(), updateValues...)
	if err != nil {
		xray.Scan(ctx, query.String())
		return oops.New(err)
	}

	if result.RowsAffected() == 0 {
		var values = make([]any, 0, len(pointer.Fields))
		var columns = make([]string, 0, len(pointer.Fields))
		var indicies = make([]string, 0, len(pointer.Fields))
		var placeholders = make([]string, 0, len(pointer.Fields))
		var settings = make([]string, 0, len(pointer.Fields))
		for i, col := range pointer.Fields {
			if col.Index {
				indicies = append(indicies, col.Name)
			}
			columns = append(columns, pgColumnNameFor(col))
			placeholders = append(placeholders, fmt.Sprintf("$%d", i+1))
			if index, ok := table[col.Name]; ok {
				values = append(values, updateValues[index])
			} else {
				values = append(values, col.Value.Interface())
			}
			settings = append(settings, fmt.Sprintf("%s = $%d", columns[i], i+1))
		}

		var upsert strings.Builder
		fmt.Fprintf(&upsert, "INSERT INTO %s (%s)", pointer.TableName(), strings.Join(columns, ", "))
		fmt.Fprintf(&upsert, "VALUES (%s)", strings.Join(placeholders, ", "))
		fmt.Fprintf(&upsert, "ON CONFLICT(%s) DO UPDATE SET %s;", strings.Join(indicies, ","), strings.Join(settings, ","))

		_, err := database.Exec(ctx, upsert.String(), values...)
		if err != nil {
			xray.Scan(ctx, upsert.String())
			return oops.New(err)
		}
	}

	return nil
}

func (db *Database) Update(ctx context.Context, pointer data.Pointer, query data.Query, patch data.Patch) (bool, error) {
	table := pointer.TableName()

	database, close, err := db.open(ctx, pointer)
	if err != nil {
		return false, oops.New(err)
	}
	defer close()

	var settings = make([]string, 0, len(patch))
	var values = make([]any, 0, len(patch))

	for _, component := range patch {
		switch component := component.(type) {
		case data.Alteration:
			field := component.Alteration()

			settings = append(settings, fmt.Sprintf("%s = $%d", pgColumnNameFor(field), len(settings)+1))
			values = append(values, field.Value.Interface())
		}
	}

	var offset string
	var orderBy strings.Builder
	var ordering bool
	orderBy.WriteString("ORDER BY ")

	// Hacky way to get the index in the case of
	// single-valued index.
	var indexName string
	for _, col := range pointer.Fields {
		if col.Index {
			indexName = pgColumnNameFor(col)
		}
	}

	var conditions strings.Builder
	for i, component := range query {
		switch v := component.(type) {
		case data.Increasing:
			field := v.Increasing()
			if ordering {
				orderBy.WriteString(", ")
			}
			orderBy.WriteString(fmt.Sprintf("%s ASC ", pgColumnNameFor(field)))
			ordering = true
		case data.Decreasing:
			field := v.Decreasing()
			if ordering {
				orderBy.WriteString(", ")
			}
			orderBy.WriteString(fmt.Sprintf("%s DESC ", pgColumnNameFor(field)))
			ordering = true

		case data.Range:
			offset = fmt.Sprintf("OFFSET %d LIMIT %d", v.From, v.Upto-v.From)

		default:
			if i > 0 {
				conditions.WriteString(" AND ")
			}
			where(indexName, &conditions, &values, component)
		}

	}

	var q strings.Builder
	fmt.Fprintf(&q, "UPDATE %s SET %s WHERE %s ", table, strings.Join(settings, ", "), conditions.String())

	if order := orderBy.String(); order != "ORDER BY " {
		q.WriteString(order)
	}
	q.WriteString(offset)
	q.WriteString(";")

	result, err := database.Exec(ctx, q.String(), values...)
	if err != nil {
		xray.Scan(ctx, q.String())
		return false, oops.New(err)
	}

	return result.RowsAffected() > 0, nil
}

func (db *Database) Search(ctx context.Context, pointer data.Pointer, query data.Query, next func() error) error {
	table := pointer.TableName()

	database, close, err := db.open(ctx, pointer)
	if err != nil {
		return oops.New(err)
	}
	defer close()

	var orderBy strings.Builder
	var ordering bool
	orderBy.WriteString("ORDER BY ")

	var offset string

	// Hacky way to get the index in the case of
	// single-valued index.
	var indexName string
	for _, col := range pointer.Fields {
		if col.Index {
			indexName = pgColumnNameFor(col)
		}
	}

	var conditions strings.Builder
	var values = make([]any, 0, 10)
	for i, component := range query {
		switch v := component.(type) {
		case data.Increasing:
			field := v.Increasing()
			if ordering {
				orderBy.WriteString(", ")
			}
			orderBy.WriteString(fmt.Sprintf("%s ASC ", pgColumnNameFor(field)))
			ordering = true
		case data.Decreasing:
			field := v.Decreasing()
			if ordering {
				orderBy.WriteString(", ")
			}
			orderBy.WriteString(fmt.Sprintf("%s DESC ", pgColumnNameFor(field)))
			ordering = true

		case data.Range:
			offset = fmt.Sprintf("OFFSET %d LIMIT %d", v.From, (v.Upto-v.From)+1)

		default:
			if i > 0 {
				conditions.WriteString(" AND ")
			}
			where(indexName, &conditions, &values, component)
		}
	}

	var columns = make([]string, 0, len(pointer.Fields))
	var pointers = make([]any, 0, len(pointer.Fields))
	for _, col := range pointer.Fields {
		columns = append(columns, pgColumnNameFor(col))
		pointers = append(pointers, col.Value.Addr().Interface())
	}

	var q strings.Builder
	var condition = conditions.String()
	if condition == "" {
		condition = "TRUE"
	}

	fmt.Fprintf(&q, "SELECT %s FROM %s WHERE %s ", strings.Join(columns, ","), table, condition)
	if order := orderBy.String(); order != "ORDER BY " {
		q.WriteString(order)
	}
	q.WriteString(offset)
	q.WriteString(";")

	rows, err := database.Query(ctx, q.String(), values...)
	if err != nil {
		xray.Scan(ctx, q.String())
		return oops.New(err)
	}
	defer rows.Close()

	for rows.Next() {
		if rows.Err() != nil {
			return oops.New(err)
		}
		if err := rows.Scan(pointers...); err != nil {
			return oops.New(err)
		}
		if err := next(); err != nil {
			return oops.New(err)
		}
	}
	return nil
}

func (db *Database) Insert(ctx context.Context, pointer data.Pointer) error {
	database, close, err := db.open(ctx, pointer)
	if err != nil {
		return oops.New(err)
	}
	defer close()

	var values = make([]any, 0, len(pointer.Fields))
	var columns = make([]string, 0, len(pointer.Fields))
	var indicies = make([]string, 0, len(pointer.Fields))
	var placeholders = make([]string, 0, len(pointer.Fields))
	var settings = make([]string, 0, len(pointer.Fields))
	for i, col := range pointer.Fields {
		if col.Index {
			indicies = append(indicies, col.Name)
		}
		columns = append(columns, pgColumnNameFor(col))
		values = append(values, col.Value.Interface())
		placeholders = append(placeholders, fmt.Sprintf("$%d", i+1))
		settings = append(settings, fmt.Sprintf("%s = $%d", columns[i], i+1))
	}

	var upsert strings.Builder
	fmt.Fprintf(&upsert, "INSERT INTO %s (%s)", pointer.TableName(), strings.Join(columns, ", "))
	fmt.Fprintf(&upsert, "VALUES (%s)", strings.Join(placeholders, ", "))
	fmt.Fprintf(&upsert, "ON CONFLICT(%s) DO UPDATE SET %s;", strings.Join(indicies, ","), strings.Join(settings, ","))

	_, err = database.Exec(ctx, upsert.String(), values...)
	if err != nil {
		xray.Scan(ctx, upsert.String())
		return oops.New(err)
	}

	return nil
}

func (db *Database) Create(context.Context, data.Pointer) error {
	return oops.NotImplemented()
}

func (db *Database) Atomic(pointer data.Pointer, fn data.Operation) data.Operation {
	return func(ctx context.Context) error {
		database, close, err := db.open(ctx, pointer)
		if err != nil {
			return oops.New(err)
		}
		defer close()

		switch database := database.(type) {
		case *pgx.Conn:
			tx, err := database.BeginTx(ctx, pgx.TxOptions{
				IsoLevel: pgx.RepeatableRead,
			})
			if err != nil {
				return oops.New(err)
			}
			err = fn(context.WithValue(ctx, pgContextKey{}, tx))
			if err != nil {
				return oops.New(tx.Rollback(ctx))
			}
			return oops.New(tx.Commit(ctx))
		case pgx.Tx:
			return fn(ctx) // already in a transaction.
		default:
			return errors.New("unknown database type: " + reflect.TypeOf(database).String())
		}
	}
}

func (db *Database) Lookup(ctx context.Context, pointer data.Pointer) (bool, error) {
	var ok bool

	database, close, err := db.open(ctx, pointer)
	if err != nil {
		return ok, oops.New(err)
	}
	defer close()

	var columns = make([]string, 0, len(pointer.Fields))
	var pointers = make([]any, 0, len(pointer.Fields))

	var conditions strings.Builder
	var values = make([]any, 0, 10)

	for _, col := range pointer.Fields {
		if !col.Index {
			columns = append(columns, pgColumnNameFor(col))
			pointers = append(pointers, col.Value.Addr().Interface())
		} else {
			if conditions.Len() > 0 {
				conditions.WriteString(" AND ")
			}
			fmt.Fprintf(&conditions, "%s = $%d", pgColumnNameFor(col), len(values)+1)
			values = append(values, col.Value.Interface())
		}
	}

	var query strings.Builder
	fmt.Fprintf(&query, "SELECT %s FROM %s WHERE %s;", strings.Join(columns, ","), pointer.TableName(), conditions.String())

	if err := database.QueryRow(ctx, query.String(), values...).Scan(pointers...); err != nil {
		if err == pgx.ErrNoRows {
			return ok, nil
		}
		xray.Scan(ctx, query.String())
		return ok, oops.New(err)
	}
	return true, nil
}

func (db *Database) Delete(ctx context.Context, pointer data.Pointer) error {
	database, close, err := db.open(ctx, pointer)
	if err != nil {
		return oops.New(err)
	}
	defer close()

	var conditions strings.Builder
	var updateValues = make([]any, 0, 10)

	for _, col := range pointer.Fields {
		if col.Index {
			if conditions.Len() > 0 {
				conditions.WriteString(" AND ")
			}
			fmt.Fprintf(&conditions, "%s = $%d", pgColumnNameFor(col), len(updateValues)+1)
			updateValues = append(updateValues, col.Value.Interface())
		}
	}

	var query strings.Builder
	fmt.Fprintf(&query, "DELETE FROM %s WHERE %s;", pointer.TableName(), conditions.String())

	if _, err := database.Exec(ctx, query.String(), updateValues...); err != nil {
		xray.Scan(ctx, query.String())
		return oops.New(err)
	}

	return nil
}

func (db *Database) Buffer(pointer data.Pointer, fn data.Operation) data.Operation {
	return func(ctx context.Context) error {
		database, close, err := db.open(ctx, pointer)
		if err != nil {
			return oops.New(err)
		}
		defer close()

		buffered := batchedConnection{
			batch: new(pgx.Batch),
		}

		err = fn(context.WithValue(ctx, pgContextKey{}, buffered))
		if err != nil {
			return oops.New(err)
		}

		switch database := database.(type) {
		case *pgx.Conn:
			return oops.New(buffered.run(ctx, database))
		case pgx.Tx:
			return fn(ctx)
		default:
			return errors.New("unknown database type: " + reflect.TypeOf(database).String())
		}
	}
}
