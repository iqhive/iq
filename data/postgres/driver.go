/*
Package postgres provides a postgres [data.Map] implementation.

The postgres driver will manage the schema for any `data.Map` in
use. Any missing columns will be added, the schema will be asserted
(and table/columns added if necessary) before the first Map operation.

The name of the primary key can be specified within the location
string. ie.

	type Customer struct {
		Name string
		Age uint
	}

	table := postgres.Map[string, Customer]("customers/{ID=%v}",
		"postgres://username:password@localhost:5432/database_name")

	type ProductIndex struct {
		ProductID string
		CustomerID string
	}

	type Product struct {
		Name string
	}

	table := postgres.Map[ProductIndex, Product]("products:customers/{CustomerID}/products/{ProductID}",
		"postgres://username:password@localhost:5432/database_name")

Column Types

	bool       -> boolean NOT NULL DEFAULT 0
	int        -> bigint NOT NULL DEFAULT 0
	int8       -> smallint CHECK (%v BETWEEN -128 and 127) NOT NULL DEFAULT 0
	int16      -> smallint NOT NULL DEFAULT 0
	int32      -> integer NOT NULL DEFAULT 0
	int64      -> bigint NOT NULL DEFAULT 0
	uint       -> numeric(20) CHECK (%v BETWEEN 0 and 18446744073709551615) NOT NULL DEFAULT 0
	uint8      -> smallint CHECK (%v BETWEEN 0 and 255) NOT NULL DEFAULT 0
	uint16     -> integer CHECK (%v BETWEEN 0 and 65535) NOT NULL DEFAULT 0
	uint32     -> bigint CHECK (%v BETWEEN 0 and 4294967295) NOT NULL DEFAULT 0
	uint64     -> numeric(20) CHECK (%v BETWEEN 0 and 18446744073709551615) NOT NULL DEFAULT 0
	float32    -> real NOT NULL DEFAULT 0
	float64    -> double precision NOT NULL DEFAULT 0
	complex64  -> real[2] NOT NULL DEFAULT '{0,0}'
	complex128 -> double precision[2] NOT NULL DEFAULT '{0,0}'
	string     -> text NOT NULL DEFAULT ''
	time.Time  -> timestamp with time zone NOT NULL DEFAULT 1970-01-01 00:00:00+00
	netip.Addr -> inet NOT NULL DEFAULT '0.0.0.0'
	uuid.UUID  -> uuid NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'
	*[]byte    -> bytea
	*T 	       -> jsonb
*/
package postgres

import (
	"context"
	"fmt"
	"io"
	"net/netip"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgconn"
	"github.com/jackc/pgproto3/v2"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/oops"
)

// pgConnection is implemented by [*pgx.Conn], [batchedConnection] and [pgx.Tx]
type pgConnection interface {
	QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row
	Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error)
	Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error)
}

// pgContextKey is used to store the active pgConnection in the context, in the
// case of a transaction or [batchedConnection].
type pgContextKey struct{}

// pgColumn is our representation of a postgres column.
type pgColumn struct {
	Type pgType

	PrimaryKey bool
}

// pgColumnNameFor returns the postgres column name for the given field.
// at the moment this is just a lowercase conversion, because SQL columns
// are typically normalized to lowercase and treated as case-insensitive.
// quoted, so that reserved words can be used as column names.
func pgColumnNameFor(field data.Field) string {
	if rename, ok := field.Tags.Lookup("postgres"); ok {
		return strconv.Quote(rename)
	}
	return strconv.Quote(strings.ToLower(field.Name))
}

// pgType name, used for ADD COLUMN.
type pgType string

// pgTypeFor converts a Go type to a postgres type.
func pgTypeFor(field data.Field) pgType {
	switch field.Type.Kind() {
	case reflect.Bool:
		return "boolean NOT NULL DEFAULT false"
	case reflect.Int, reflect.Int64:
		return "bigint NOT NULL DEFAULT 0"
	case reflect.Int8:
		return pgType(fmt.Sprintf("smallint NOT NULL DEFAULT 0 CHECK (%v BETWEEN -128 and 127)", pgColumnNameFor(field)))
	case reflect.Int16:
		return "smallint NOT NULL DEFAULT 0"
	case reflect.Int32:
		return "integer NOT NULL DEFAULT 0"
	case reflect.Uint, reflect.Uint64:
		return pgType(fmt.Sprintf("numeric(20) NOT NULL DEFAULT 0 CHECK (%v BETWEEN 0 and 18446744073709551615)", pgColumnNameFor(field)))
	case reflect.Uint8:
		return pgType(fmt.Sprintf("smallint NOT NULL DEFAULT 0 CHECK (%v BETWEEN 0 and 255)", pgColumnNameFor(field)))
	case reflect.Uint16:
		return pgType(fmt.Sprintf("integer NOT NULL DEFAULT 0 CHECK (%v BETWEEN 0 and 65535)", pgColumnNameFor(field)))
	case reflect.Uint32:
		return pgType(fmt.Sprintf("bigint NOT NULL DEFAULT 0 CHECK (%v BETWEEN 0 and 4294967295)", pgColumnNameFor(field)))
	case reflect.Float32:
		return "real NOT NULL DEFAULT 0"
	case reflect.Float64:
		return "double precision NOT NULL DEFAULT 0"
	case reflect.Complex64:
		return "real[2] NOT NULL DEFAULT '{0,0}'"
	case reflect.Complex128:
		return "double precision[2] NOT NULL DEFAULT '{0,0}'"
	case reflect.String:
		return "text NOT NULL DEFAULT ''"
	case reflect.Struct:
		switch field.Type {
		case reflect.TypeOf(time.Time{}):
			return "timestamp with time zone NOT NULL DEFAULT 1970-01-01 00:00:00"
		case reflect.TypeOf(netip.Addr{}):
			return "inet NOT NULL DEFAULT '0.0.0.0'"
		case reflect.TypeOf(uuid.UUID{}):
			return "uuid NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000'"
		}
		return "jsonb"
	case reflect.Slice:
		if field.Type.Elem().Kind() == reflect.Uint8 {
			return "bytea"
		}
		return "jsonb"
	case reflect.Map, reflect.Ptr, reflect.Func, reflect.Chan, reflect.Interface, reflect.Array, reflect.UnsafePointer:
		return "jsonb"
	}
	return "jsonb"
}

// where uses the given [data.QueryComponent] to write the equivalent WHERE clause
// conditions to the given writer. values are appended to the given slice, so that
// they can be safely passed as parameters to the query.
func where(index string, conditions io.Writer, values *[]any, component data.QueryComponent) {

	cname := func(field data.Field) string {
		if field.Name == "" && field.Index {
			return index
		}
		return pgColumnNameFor(field)
	}

	switch v := component.(type) {
	case data.Equals:
		field := v.Equals()
		fmt.Fprintf(conditions, "%s = $%d ", cname(field), len(*values)+1)
		*values = append(*values, field.Value.Interface())
	case data.AtLeast:
		field := v.AtLeast()
		fmt.Fprintf(conditions, "%s >= $%d ", cname(field), len(*values)+1)
		*values = append(*values, field.Value.Interface())
	case data.AtMost:
		field := v.AtMost()
		fmt.Fprintf(conditions, "%s <= $%d ", cname(field), len(*values)+1)
		*values = append(*values, field.Value.Interface())
	case data.MoreThan:
		field := v.MoreThan()
		fmt.Fprintf(conditions, "%s > $%d ", cname(field), len(*values)+1)
		*values = append(*values, field.Value.Interface())
	case data.LessThan:
		field := v.LessThan()
		fmt.Fprintf(conditions, "%s < $%d ", cname(field), len(*values)+1)
		*values = append(*values, field.Value.Interface())

	case data.Contains:
		field := v.Contains()
		fmt.Fprintf(conditions, "%s LIKE $%d ", cname(field), len(*values)+1)
		*values = append(*values, "%"+field.Value.String()+"%")

	case data.HasPrefix:
		field := v.HasPrefix()
		fmt.Fprintf(conditions, "%s LIKE $%d ", cname(field), len(*values)+1)
		*values = append(*values, field.Value.String()+"%")

	case data.HasSuffix:
		field := v.HasSuffix()
		fmt.Fprintf(conditions, "%s LIKE $%d ", cname(field), len(*values)+1)
		*values = append(*values, "%"+field.Value.String())

	case data.Not:
		fmt.Fprintf(conditions, "NOT (")
		where(index, conditions, values, v.QueryComponent)
		fmt.Fprintf(conditions, ")")

	case data.Cases:
		fmt.Fprintf(conditions, "(")
		for i, c := range v {
			if i > 0 {
				fmt.Fprintf(conditions, " OR ")
			}
			where(index, conditions, values, c)
		}
		fmt.Fprintf(conditions, ")")
	case data.Query:
		fmt.Fprintf(conditions, "(")
		for i, c := range v {
			if i > 0 {
				fmt.Fprintf(conditions, " AND ")
			}
			where(index, conditions, values, c)
		}
		fmt.Fprintf(conditions, ")")
	}
}

type pgPool struct {
	mut sync.RWMutex
	ptr *pgxpool.Pool
}

// Connection to a PostgreSQL database.
type Connection struct {
	string string
	pgPool *pgPool
}

// Open returns a new postgres database connection that can be used
// to create data maps.
func Open(ctx context.Context, connection string) (Connection, error) {
	conn := Connection{
		string: connection,
		pgPool: new(pgPool),
	}
	pool, err := pgxpool.Connect(ctx, connection)
	if err != nil {
		return conn, oops.New(err)
	}
	conn.pgPool.ptr = pool
	return conn, nil
}

// https://dba.stackexchange.com/questions/75015/get-column-names-and-data-types-of-a-query-table-or-view/75124#75124
// modified to include primary key information
const selectColumns = `SELECT a.attname, format_type(a.atttypid, a.atttypmod), COALESCE(i.indisprimary, false) AS type
FROM   pg_attribute a
LEFT JOIN   pg_index i ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
WHERE  a.attrelid = $1::regclass
AND    a.attnum > 0
AND    NOT a.attisdropped
ORDER  BY a.attnum;
`

// Map backed by a PostgreSQL database.
func Map[Index, Value comparable](location data.Location[Index], connection Connection) data.Source[Index, Value] {
	db := &Database{
		connection: connection.string,
		pgPool:     connection.pgPool,
	}
	return data.Open[Index, Value](db, location)
}

// waitingQuery implements [pgx.Rows] and is used to block the Search
// goroutine until the query is ready to be returned.
type waitingQuery struct {
	ctx context.Context

	in pgx.Rows
	ch chan pgx.Rows
}

var _ pgx.Rows = new(waitingQuery)

func (w *waitingQuery) Close() {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return
		}
	}
	w.in.Close()
}

func (w *waitingQuery) Err() error {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return w.ctx.Err()
		}
	}
	return w.in.Err()
}

func (w *waitingQuery) CommandTag() pgconn.CommandTag {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return nil
		}
	}
	return w.in.CommandTag()
}

func (w *waitingQuery) FieldDescriptions() []pgproto3.FieldDescription {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return nil
		}
	}
	return w.in.FieldDescriptions()
}

func (w *waitingQuery) Next() bool {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return false
		}
	}
	return w.in.Next()
}

func (w *waitingQuery) Scan(dest ...interface{}) error {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return w.ctx.Err()
		}
	}
	return w.in.Scan(dest...)
}

func (w *waitingQuery) Values() ([]interface{}, error) {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return nil, w.ctx.Err()
		}
	}
	return w.in.Values()
}

func (w *waitingQuery) RawValues() [][]byte {
	if w.in == nil {
		select {
		case w.in = <-w.ch:
		case <-w.ctx.Done():
			return nil
		}
	}
	return w.in.RawValues()
}

type batchedConnection struct {
	batch *pgx.Batch
	query []*waitingQuery
}

func (b *batchedConnection) run(ctx context.Context, conn *pgx.Conn) error {
	if b.batch.Len() == 0 {
		return nil
	}

	results := conn.SendBatch(ctx, b.batch)
	for _, q := range b.query {
		if q == nil { // we record a nil waitingQuery for Exec operations.
			if _, err := results.Exec(); err != nil {
				return oops.New(err)
			}
		} else {
			r, err := results.Query()
			if err != nil {
				return oops.New(err)
			}
			select {
			case q.ch <- r:
			case <-ctx.Done():
				return oops.New(ctx.Err())
			}
		}
	}
	return oops.New(results.Close())
}

func (b *batchedConnection) QueryRow(ctx context.Context, sql string, args ...interface{}) pgx.Row {
	panic("Lookup operations are not permitted within a Buffered data.Operation")
}

func (b *batchedConnection) Query(ctx context.Context, sql string, args ...interface{}) (pgx.Rows, error) {
	b.batch.Queue(sql, args...)
	ch := make(chan pgx.Rows)
	rows := &waitingQuery{ctx: ctx, ch: ch}
	b.query = append(b.query, rows)
	return rows, nil
}

func (b *batchedConnection) Exec(ctx context.Context, sql string, args ...interface{}) (pgconn.CommandTag, error) {
	b.batch.Queue(sql, args...)
	b.query = append(b.query, nil) // we record a nil waitingQuery for Exec operations.
	return nil, nil
}
