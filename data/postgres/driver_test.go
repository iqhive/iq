package postgres_test

import (
	"context"
	"os"
	"testing"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/data/postgres"
	"pkg.iqhive.com/iq/xray"
)

func TestPostgres(t *testing.T) {
	//postgres://postgres:testing@localhost:5432/testing
	ConnectionString := os.Getenv("POSTGRES")
	if ConnectionString == "" {
		t.Skip()
	}
	db, err := postgres.New(context.Background(), ConnectionString)
	if err != nil {
		t.Fatal(xray.Sprint(err))
	}
	if err := data.Test(context.Background(), db); err != nil {
		t.Fatal(xray.Sprint(err))
	}
}
