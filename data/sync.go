package data

// Sync returns a database that incrementally syncronizes all data
// in src and replicates it to dst. An atomic wal (write-ahead log)
// database needs to be provided, as it will be used to ensure data
// consistency between the two databases. By omitting destination
// databases, Sync can also be used to add atomics to any database
// that doesn't already support them natively. Sync can be reliably
// executed in a distributed context as long as all distributed
// versions share two [Base] arguments, destinations can be added over
// time and sync will bring them up to speed, once a destination is
// up to date, the source database can be replaced by a destination
// database and Sync will continue to work. Once a destination is
// up to date, reads may be routed to it. NOT IMPLEMENTED.
func Sync(wal, src Base, dst ...Base) Base {

	// For writes, as a cooperative node, the strategy is first to look at the WAL's
	// current revision, block size and ID of the leader. Next you can record your write to
	// the WAL along with an ID (that uniquely identifies you) and a vote for the next WAL
	// size. After that, perform a read on the WAL and check if the length of the WAL is
	// greater than the current batch size, if it is, then the log entry at that position
	// becomes the new leader. Not the leader? your write is done, you can return a nil error
	// to your caller. You are the leader? Ouch, you have some more work to do.

	// As a prospective leader, for each living destination, you need to check if the last leader
	// did their job and if they didn't acknowledge their writes, you will need to start from
	// where they left off. So For each write in your 'leader' block of the WAL (which has a
	// subsequent revision number) you will need to serialise each affected record in the
	// database if, and *only* if a record has the preceding block number. Once you have done
	// this, you can acknowledge the write for that database by writing the new revision and
	// block size to the WAL. If a database falls too far behind the source database, you can
	// mark it as dead and stop writing to it. If a destination database lacks WAL information
	// take the time to copy some records from an earlier revision from the source database to
	// the outdated destination.

	// For reads, always download the latest entries in the WAL and apply them in order to any
	// reads you perform. If you read a record with an epoch value greater than your current
	// copy of the WAL, you will need to download it again.

	// Transactions are distinguished in the WAL with a transaction ID, they should only be
	// applied when there is a commit for them.

	panic("not implemented")
}

type syncronisation struct {
	wal, src Base

	dst []Base
}

// Shard returns a database that shards data across multiple databases. Data is sharded
// based on the index value of the record. NOT IMPLEMENTED.
func Shard(wal Base, shards ...Base) Base {

	// Read https://aws.amazon.com/what-is/database-sharding/ for a good introduction on
	// strategies. Individual record lookups are easy, just look up the shard based on the
	// index value and then look up the record in the shard. For search queries with an
	// order, things are tricker and you will need to merge the results from each shard.

	// A merge strategy could be something as follows:
	// 1. Ask each shard to count the number of records they have that match the query.
	// 2. Ask each shard to return the first N records that match the query.
	// 3. Merge the results from each shard, if the number of records returned is less than
	//    the number of records requested, then you can stop asking shards for more records.
	// 4. If you need to do an offset, you will need to do a few roundtrips, figuring out
	//    which shards have the most records that match the query. Expensive.

	panic("not implemented")
}
