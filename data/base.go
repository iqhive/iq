package data

import (
	"context"
	"errors"
	"fmt"
	"path"
	"reflect"
	"runtime/debug"
	"strings"

	"pkg.iqhive.com/iq/oops"
)

// Pointer is used by drivers, and provides a reflected view of a
// database record including the index and value and the information
// about where that record lives in the database.
type Pointer struct {
	location string

	index any

	Fields []Field
}

// TableName returns the name of the table where this value
// will be stored.
func (r Pointer) TableName() string {
	name, _, ok := strings.Cut(string(r.location), ":")
	if !ok {
		name, _, _ := strings.Cut(string(r.location), "/")
		return string(name)
	}
	return name
}

func (r Pointer) Path() string {
	_, path, ok := strings.Cut(string(r.location), ":")
	if !ok {
		path = string(r.location)
	}
	var result []string
	for _, component := range strings.Split(path, "/") {
		if strings.HasPrefix(component, "{") && strings.HasSuffix(component, "}") {
			component = strings.Trim(component, "{}")
			if strings.HasSuffix(component, "=%v") {
				result = append(result, fmt.Sprint(reflect.ValueOf(r.index).Elem().Interface()))
				continue
			}
			result = append(result, fmt.Sprint(reflect.ValueOf(r.index).FieldByName(component)))
		} else {
			result = append(result, component)
		}
	}
	return strings.Join(result, "/")
}

func (r Pointer) Walk(query Query, walker LocationWalker) error {
	_, s, _ := strings.Cut(r.location, ":")
	if !strings.Contains(s, "{") {
		s += "/{}"
	}
	for _, component := range query {
		switch v := component.(type) {
		case Equals:
			field := v.Equals()
			if field.Index {
				s = strings.ReplaceAll(s, "{"+field.Name+"}", fmt.Sprint(field.Value.Interface()))
			}
		}
	}
	var walk func(path string) error
	walk = func(path string) error {
		root, remaining, ok := strings.Cut(path, "{")
		if !ok {
			return walker.Lookup(root)
		}
		_, remaining, ok = strings.Cut(remaining, "}")
		if !ok {
			return errors.New("invalid MapLocation: " + s)
		}
		if err := walker.Search(root, remaining != "", func(path string) error {
			return walk(root + path + remaining)
		}); err != nil {
			return err
		}
		return nil
	}
	return walk(s)
}

// Scan the given path and write the index into the pointer.
func (loc Pointer) Scan(p string) error {
	p = strings.TrimPrefix(p, "/")
	if reflect.TypeOf(loc.index).Elem().Kind() == reflect.String {
		reflect.ValueOf(loc.index).Elem().SetString(path.Base(p))
		return nil
	}
	if reflect.TypeOf(loc.index).Kind() == reflect.Struct {
		parts := strings.Split(p, "/")
		template := strings.Split(string(loc.location), "/")
		if len(parts) != len(template) {
			return oops.New(errors.New("path does not match template"))
		}
		for i, name := range template {
			if strings.HasPrefix(name, "{") && strings.HasSuffix(name, "}") {
				name = strings.Trim(name, "{}")
				field, ok := reflect.TypeOf(loc.index).Elem().FieldByName(name)
				if !ok {
					return oops.New(errors.New("invalid index type for MapLocation.Scan"))
				}
				switch field.Type.Kind() {
				case reflect.String:
					reflect.ValueOf(loc.index).Elem().FieldByIndex(field.Index).SetString(parts[i])
				default:
					return oops.New(errors.New("unsupported index type for MapLocation.Scan"))
				}

			}
		}
		return nil
	}
	return oops.New(errors.New("unsupported index type for MapLocation.Scan"))
}

// Base driver, implemented by drivers and used to open a [Source].
type Base interface {
	// Mutate should use apply the given patch to the table defined
	// by the given schema.
	Mutate(context.Context, Pointer, Patch) error
	// Update should apply the given patch to any records matching
	// the query to the table defined by the given schema and return
	// true if any records were changed.
	Update(context.Context, Pointer, Query, Patch) (bool, error)
	// Search should write index and value results for records
	// matching the query into the schema and then call the given
	// callback for any errors that occur.
	Search(context.Context, Pointer, Query, func() error) error
	// Insert should insert the given record into the table defined
	// by the given schema (index and value are in the schema fields).
	Insert(context.Context, Pointer) error
	// Create should fill any index fields in the schema with usable
	// values that can be used by a future Mutate call.
	Create(context.Context, Pointer) error
	// Atomic should execute the given operation in an atomic
	// transactional context.
	Atomic(Pointer, Operation) Operation
	// Lookup should return true if the given record exists in the
	// table defined by the given schema. It should write the record's
	// values into the schema (index values are in the schema).
	Lookup(context.Context, Pointer) (bool, error)
	// Delete should remove the given record from the table defined
	// by the given schema (index values are in the schema).
	Delete(context.Context, Pointer) error
	// Buffer should batch any operations that take place inside the
	// given function.
	Buffer(Pointer, Operation) Operation
}

// Open a [Map] or [Keys] located at [Path] using the given data [Base].
func Open[K comparable, V any](base Base, path Location[K]) Source[K, V] {
	if base == nil {
		return New[K, V]()
	}
	return Source[K, V]{
		Mutate: func(ctx context.Context, index K, patch PatchBuilder[V]) error {
			var value V
			patch(&value).Apply()
			var record = Pointer{
				location: string(path),
				index:    &index,
				Fields:   Reflection(&index, &value, Location[K](path)),
			}
			mutation := patch.Build()
			if len(mutation) == 0 {
				return nil
			}
			return base.Mutate(ctx, record, mutation)
		},
		Update: func(ctx context.Context, query QueryBuilder[K, V], patch PatchBuilder[V]) (bool, error) {
			var index K
			var value V
			var record = Pointer{
				location: string(path),
				index:    &index,
				Fields:   Reflection(&index, &value, Location[K](path)),
			}
			update := patch.Build()
			if len(update) == 0 {
				return false, nil
			}
			return base.Update(ctx, record, query.Build(), update)
		},
		Search: func(ctx context.Context, query QueryBuilder[K, V]) Results[K, V] {
			var results = make(Results[K, V])
			go func() {
				defer func() {
					if r := recover(); r != nil {
						results.Close(ctx, fmt.Errorf("panic: %v\n%v", r, string(debug.Stack())))
					}
				}()
				var index K
				var value V
				var record = Pointer{
					location: string(path),
					index:    &index,
					Fields:   Reflection(&index, &value, Location[K](path)),
				}
				var matcher Query
				if query != nil {
					matcher = query(&index, &value)
				}
				var filter = query.Build()
				err := base.Search(ctx, record, filter, func() error {
					if matcher.Matches() {
						results.Send(ctx, index, value)
					}
					return nil
				})
				results.Close(ctx, err)
			}()
			return results
		},
		Insert: func(ctx context.Context, index K, value V) error {
			return base.Insert(ctx, Pointer{
				location: string(path),
				index:    &index,
				Fields:   Reflection(&index, &value, Location[K](path)),
			})
		},
		Create: func(ctx context.Context) (index K, err error) {
			var value V
			err = base.Create(ctx, Pointer{
				location: string(path),
				index:    &index,
				Fields:   Reflection(&index, &value, Location[K](path)),
			})
			return
		},
		Atomic: func(o Operation) Operation {
			return base.Atomic(Pointer{
				location: string(path),
			}, o)
		},
		Lookup: func(ctx context.Context, index K) (value V, ok bool, err error) {
			ok, err = base.Lookup(ctx, Pointer{
				location: string(path),
				index:    &index,
				Fields:   Reflection(&index, &value, Location[K](path)),
			})
			return
		},
		Delete: func(ctx context.Context, index K) error {
			var value V
			return base.Delete(ctx, Pointer{
				location: string(path),
				index:    &index,
				Fields:   Reflection(&index, &value, Location[K](path)),
			})
		},
		Buffer: func(o Operation) Operation {
			return base.Buffer(Pointer{
				location: string(path),
			}, o)
		},
	}
}
