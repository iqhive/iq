package data_test

import (
	"context"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/data"
)

func TestSet(t *testing.T) {
	var ctx = context.Background()

	var set data.Set[int] = data.New[int, struct{}]()
	set.Add(ctx, 1)
	set.Add(ctx, 2)
	set.Add(ctx, 3)

	for val := range set.Idx(ctx, nil) {
		i, _ := val.Get()
		fmt.Println(i)
	}
}
