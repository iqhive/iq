package data

import (
	"encoding/json"
	"errors"
	"fmt"
	"path"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"pkg.iqhive.com/iq/enum"
)

// Type is a value that changes when it is stored as data inside of a
// database. It is a useful interface for generic container types or
// values with private fields which can be stored in a database. If
// the type implements [is.Assertable] then the assertions on the
// type may be used by the driver to enforce constraints on the data.
type Type interface {

	// DataType returns the reflect.Type that the MarshalData and
	// UnmarshalData methods will use to encode and decode the data.
	// The type must be comparable and should not contain pointers.
	DataType() reflect.Type

	// MarshalData encodes the data into a format that can be stored
	// in a database. The value must match the type returned by
	// DataType.
	MarshalData() (any, error)

	// UnmarshalData decodes the data from a format that can be stored
	// in a database. The value must match the type returned by
	// DataType.
	UnmarshalData(any) error
}

// Validator is an interface that can be implemented by a Type to indicate
// that it can validate itself. Values that implement this interface will
// typically be represented inside the database as a nullable value.
type Validator interface {
	Validate() error
}

// Field reflects a single field in a structure.
type Field struct {
	Name string
	Type reflect.Type
	Tags reflect.StructTag

	Index bool          // located in the index.
	Value reflect.Value // underlying reference to the value (is addressable).
}

// Structure represents a single logical row view composed of an index + value pair.
// Useful for tabular Map implementations.
type Structure []Field

func (row *Structure) add(rvalue reflect.Value, name string, index bool) {
	rtype := rvalue.Type()

	switch rtype.Kind() {
	case reflect.Struct:
		_, ok := enum.TypeOf(rtype)
		if !ok {
			if rtype.Implements(reflect.TypeOf([0]json.Marshaler{}).Elem()) {
				*row = append(*row, Field{
					Name:  name,
					Type:  rtype,
					Index: index,
					Value: rvalue,
				})
				return
			}
		}
		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)
			if !field.IsExported() {
				continue
			}
			if field.Anonymous {
				row.add(rvalue.Field(i), "", index)
			} else {
				if field.Type.Kind() == reflect.Array {
					for j := 0; j < rvalue.Field(i).Len(); j++ {
						row.add(rvalue.Field(i).Index(j), name+field.Name+strconv.Itoa(j+1), index)
					}
					continue
				}
				row.add(rvalue.Field(i), name+field.Name, index)
			}
		}
	default:
		*row = append(*row, Field{
			Name:  name,
			Type:  rtype,
			Index: index,
			Value: rvalue,
		})
	}
}

// Reflection is a driver function that returns the data structure for the given map index and value,
// given the specified map location. The structure returned is a flat tabular sequence of fields/columns.
func Reflection[K comparable, V any](index *K, value *V, location Location[K]) Structure {
	var row Structure

	loc := string(location)
	if strings.HasSuffix(loc, "=%v}") {
		loc = strings.TrimSuffix(loc, "=%v}")
		_, loc, _ = strings.Cut(loc, "{")
	} else {
		loc = ""
	}

	row.add(reflect.ValueOf(index).Elem(), loc, true)
	row.add(reflect.ValueOf(value).Elem(), "", false)

	return row
}

// maps used by the Compile function.
var (
	mutex          sync.RWMutex
	indexSentinals = make(map[reflect.Type]any) // sentinal values for each type
	valueSentinals = make(map[reflect.Type]any) // sentinal values for each type
	mirror         = make(map[any]Field)        // mirror of each sentinal's pointers to their respective paths.
)

/*
Consider the following example:

	type Customer struct {
		Name string
		Age  int

		Group struct {
			Tags string
		}
	}

The maps will be populated as follows:

	var sentinal = new(Customer)
	sentinals[reflect.TypeOf(sentinal)] = sentinal

	mirror[&sentinal] = ""
	mirror[&sentinal.Name] = "Name"
	mirror[&sentinal.Age] = "Age"
	mirror[&sentinal.Group] = "Group"
	mirror[&sentinal.Group.Tags] = "Group.Tags"
*/
func sentinal[T any](class map[reflect.Type]any, index bool) *T {

	newSentinal := func(arg any) any {
		mutex.Lock()
		defer mutex.Unlock()

		rvalue := reflect.ValueOf(arg)
		rtype := rvalue.Type()

		class[rtype] = arg

		var row Structure
		row.add(rvalue.Elem(), "", index)

		for _, field := range row {
			mirror[field.Value.Addr().Interface()] = field
		}
		return arg
	}

	// each type has a sentinal value that is used as the ptr to be
	// passed to fn, we can use this to figure out where each pointer
	// is pointing to within the argument.
	var arg *T
	mutex.RLock()
	ptr, ok := class[reflect.TypeOf(arg)]
	mutex.RUnlock()
	if !ok {
		ptr = newSentinal(reflect.New(reflect.TypeOf(arg).Elem()).Interface())
	}

	return ptr.(*T)
}

// Location has been designed to encourage drivers to adopt
// a standard string format for defining the location of
// a [Map]. A Location is defined as beginning with an optional
// label denoted with a colon, followed by a slash-separated
// path of components where each component is either a constant
// string or a curly-brace reference to a field within the Index
// type. Unless the index is a struct, a Location will always
// end with '=%v}' and the name of this parameter will be the
// name of the index column (ie. primary key).
//
// When multiple index parameters are present in the path, the
// parameters will treated as a composite primary key
// (multiple primary keys).
//
// Typically, the first segment of the location will reflect
// the name of the table where the data would be stored in a SQL
// database. The remaining segements, reflect where the data
// would be found within a file-system or a key-value-store.
//
// For example:
//
//		"customers/{ID=%v}"  (customers table)
//	 	"products:customers/{Customer}/products/{ID}" (products table)
type Location[Index comparable] string

type LocationWalker interface {
	Search(path string, more bool, each func(string) error) error
	Lookup(path string) error
}

/*
Walk applies a constant search space folding optimisation to walk through the given
[Location] using the given [LocationWalker].

A constant search folding optimisation is applied, we can fold any query components
of the form (that target the index parameter):

	db.Where(X).Equals

For example, if we have an index type like this:

	type Index struct {
		ID string
		Customer string
	}

Given a MapLocation of "customers/{customer}/orders/{id}" and an index of Index{ID:"123"}
Walk reduces the search space to "customers/" and "orders/123". Only the customers
collection will be passed to the feeder function for iteration purposes.
*/
func (loc Location[Index]) Walk(query Query, walker LocationWalker) error {
	s := string(loc)
	if !strings.Contains(s, "{") {
		s += "/{}"
	}

	for _, component := range query {
		switch v := component.(type) {
		case Equals:
			field := v.Equals()
			if field.Index {
				s = strings.ReplaceAll(s, "{"+field.Name+"}", fmt.Sprint(field.Value.Interface()))
			}
		}
	}

	var walk func(path string) error
	walk = func(path string) error {
		root, remaining, ok := strings.Cut(path, "{")
		if !ok {
			return walker.Lookup(root)
		}
		_, remaining, ok = strings.Cut(remaining, "}")
		if !ok {
			return errors.New("invalid MapLocation: " + s)
		}
		if err := walker.Search(root, remaining != "", func(path string) error {
			return walk(root + path + remaining)
		}); err != nil {
			return err
		}
		return nil
	}

	return walk(s)
}

// Name returns the base name of the Map location.
func (loc Location[Index]) Name() string {
	name, _, ok := strings.Cut(string(loc), ":")
	if !ok {
		name, _, _ := strings.Cut(string(loc), "/")
		return string(name)
	}
	return name
}

// Path returns the path of the Map location for
// a given index and value.
func (loc Location[Index]) Path(index Index) string {
	_, path, ok := strings.Cut(string(loc), ":")
	if !ok {
		path = string(loc)
	}

	var result []string
	for _, component := range strings.Split(path, "/") {
		if strings.HasPrefix(component, "{") && strings.HasSuffix(component, "}") {
			component = strings.Trim(component, "{}")
			if strings.HasSuffix(component, "=%v") {
				result = append(result, fmt.Sprint(index))
				continue
			}
			result = append(result, fmt.Sprint(reflect.ValueOf(index).FieldByName(component)))
		} else {
			result = append(result, component)
		}
	}

	return strings.Join(result, "/")
}

// Scan the given path and return the index value that corresponds to it.
func (loc Location[Index]) Scan(p string) (Index, error) {
	p = strings.TrimPrefix(p, "/")

	var index Index

	if reflect.TypeOf(index).Kind() == reflect.String {
		reflect.ValueOf(&index).Elem().SetString(path.Base(p))
		return index, nil
	}

	if reflect.TypeOf(index).Kind() == reflect.Struct {

		parts := strings.Split(p, "/")
		template := strings.Split(string(loc), "/")

		if len(parts) != len(template) {
			return index, errors.New("path does not match template")
		}

		for i, name := range template {
			if strings.HasPrefix(name, "{") && strings.HasSuffix(name, "}") {
				name = strings.Trim(name, "{}")
				field, ok := reflect.TypeOf(index).FieldByName(name)
				if !ok {
					return index, errors.New("invalid index type for MapLocation.Scan")
				}
				switch field.Type.Kind() {
				case reflect.String:
					reflect.ValueOf(&index).Elem().FieldByIndex(field.Index).SetString(parts[i])
				default:
					return index, errors.New("unsupported index type for MapLocation.Scan")
				}

			}
		}

		return index, nil
	}
	return index, errors.New("unsupported index type for MapLocation.Scan")
}
