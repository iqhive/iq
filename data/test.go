package data

import (
	"context"
	"errors"

	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/verify"
)

// Test a [Base] implementation, all tables will be prefixed with "test"
// the test can be safely retried later in case of an error.
func Test(ctx context.Context, base Base) error {
	return newTestSuite(base).test(ctx)
}

type testCustomerID string

type testCustomer struct {
	Name string
	Tag  string
	Age  uint

	//Description data.Text[[256]data.Len]

	//Tags data.List[string, [12]limit.Len]
	//Keys data.Keys[string, string, [12]limit.Len]
}

type dataTestSuite struct {
	Customers Source[testCustomerID, testCustomer]
}

func newTestSuite(db Base) dataTestSuite {
	return dataTestSuite{
		Customers: Open[testCustomerID, testCustomer](db, "test_customers:test/customers/{ID=%v}"),
	}
}

func (table dataTestSuite) test(ctx context.Context) error {
	if err := table.testMutate(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testUpdate(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testSearch(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testInsert(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testCreate(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testAtomic(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testLookup(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testDelete(ctx); err != nil {
		return oops.New(err)
	}
	if err := table.testBuffer(ctx); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testMutate(ctx context.Context) error {
	var ID testCustomerID = "1234"
	if err := table.Customers.Delete(ctx, ID); err != nil {
		return oops.New(err)
	}
	if err := verify.Note(`records that "don't exist" should still be mutable`, func() error {
		if err := table.Customers.Mutate(ctx, ID, func(customer *testCustomer) Patch {
			return Patch{
				Alter(&customer.Name, "John"),
			}
		}); err != nil {
			return oops.New(err)
		}
		john, ok, err := table.Customers.Lookup(ctx, ID)
		if err != nil {
			return oops.New(err)
		}
		return oops.New(errors.Join(
			verify.That(ok)(is.Value(`true`)),
			verify.That(john.Name)(is.Value(`John`)),
		))
	}); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testUpdate(ctx context.Context) error {
	if err := verify.Note(`all the records should be updated`, func() error {
		if err := table.Customers.Insert(ctx, "1234", testCustomer{
			Name: "John",
			Age:  18,
		}); err != nil {
			return oops.New(err)
		}
		if err := table.Customers.Insert(ctx, "4321", testCustomer{
			Name: "Alice",
			Age:  22,
		}); err != nil {
			return oops.New(err)
		}
		query := func(index *testCustomerID, value *testCustomer) Query {
			return Query{Cases{
				Index(index, "1234"),
				Index(index, "4321"),
			}}
		}
		b, err := table.Customers.Update(ctx, query, func(customer *testCustomer) Patch {
			return Patch{
				Alter(&customer.Tag, "Loyal"),
			}
		})
		if err != nil {
			return oops.New(err)
		}
		if err := verify.That(b)(is.Value(`true`)); err != nil {
			return oops.New(err)
		}
		for _, ID := range []testCustomerID{"1234", "4321"} {
			customer, ok, err := table.Customers.Lookup(ctx, ID)
			if err != nil {
				return oops.New(err)
			}
			if err := errors.Join(
				verify.That(ok)(is.Value(`true`)),
				verify.That(customer.Tag)(is.Value(`Loyal`)),
			); err != nil {
				return oops.New(err)
			}
		}
		return nil
	}); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testSearch(ctx context.Context) error {
	if err := verify.Note(`search by one of the value's fields`, func() error {
		if err := table.Customers.Insert(ctx, "1234", testCustomer{
			Name: "John",
			Age:  18,
		}); err != nil {
			return oops.New(err)
		}
		if err := table.Customers.Insert(ctx, "4321", testCustomer{
			Name: "Alice",
			Age:  22,
		}); err != nil {
			return oops.New(err)
		}
		query := func(index *testCustomerID, value *testCustomer) Query {
			return Query{
				Where(&value.Age).Equals(18),
			}
		}
		var count int
		for result := range table.Customers.Search(ctx, query) {
			id, customer, err := result.Get()
			if err != nil {
				return oops.New(err)
			}
			if count > 0 {
				return oops.New(errors.New("search should only return one result"))
			}
			if err := errors.Join(
				verify.That(id)(is.Value(`1234`)),
				verify.That(customer.Age)(is.Value(`18`)),
			); err != nil {
				return oops.New(err)
			}
			count++
		}
		return nil
	}); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testInsert(ctx context.Context) error {
	if err := verify.Note(`insert should be idempotent`, func() error {
		if err := table.Customers.Insert(ctx, "1234", testCustomer{
			Name: "John",
			Age:  18,
		}); err != nil {
			return oops.New(err)
		}
		if err := table.Customers.Insert(ctx, "1234", testCustomer{
			Name: "John Doe",
			Age:  18,
		}); err != nil {
			return oops.New(err)
		}
		john, ok, err := table.Customers.Lookup(ctx, "1234")
		if err != nil {
			return oops.New(err)
		}
		return errors.Join(
			verify.That(ok)(is.Value(`true`)),
			verify.That(john.Name)(is.Value(`John Doe`)),
		)
	}); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testCreate(ctx context.Context) error {
	return nil
}

func (table dataTestSuite) testAtomic(ctx context.Context) error {
	return nil
}

func (table dataTestSuite) testLookup(ctx context.Context) error {
	if err := verify.Note(`lookup should return false for non-existent records`, func() error {
		if err := table.Customers.Delete(ctx, "1234"); err != nil {
			return oops.New(err)
		}
		_, ok, err := table.Customers.Lookup(ctx, "1234")
		if err != nil {
			return oops.New(err)
		}
		if err := verify.That(ok)(is.Value(`false`)); err != nil {
			return oops.New(err)
		}
		return nil
	}); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testDelete(ctx context.Context) error {
	if err := verify.Note(`delete should be idempotent`, func() error {
		if err := table.Customers.Delete(ctx, "1234"); err != nil {
			return oops.New(err)
		}
		if err := table.Customers.Delete(ctx, "1234"); err != nil {
			return oops.New(err)
		}
		_, ok, err := table.Customers.Lookup(ctx, "1234")
		if err != nil {
			return oops.New(err)
		}
		if err := verify.That(ok)(is.Value(`false`)); err != nil {
			return oops.New(err)
		}
		return nil
	}); err != nil {
		return oops.New(err)
	}
	return nil
}

func (table dataTestSuite) testBuffer(ctx context.Context) error {
	return nil
}
