package data

import (
	"context"
)

// Map is key-value store.
type Map[K comparable, V any] interface {
	// Del deletes a key, the operation is idempotent. It has HTTP
	// DELETE semantics.
	Del(context.Context, K) error
	// All returns a result iterator over all key-value pairs.
	All(context.Context) Results[K, V]
	// Mod modifies the value using the given patch. If the value does
	// not exist, it will be created.
	Mod(context.Context, K, PatchBuilder[V]) error
	// Has returns true if the key exists.
	Has(context.Context, K) (bool, error)
	// Get returns the value of the given key. If the value does not
	// exist, the zero value will be returned.
	Get(context.Context, K) (V, error)
	// Set a key overwriting any existing value at the given key. This
	// operation is idempotent. It has HTTP PUT semantics.
	Set(context.Context, K, V) error
	// Len returns the number of key-value pairs in the map.
	Len(context.Context) (int, error)
}

// NewMap returns a new map with the given key-value pairs.
func NewMap[K comparable, V any](m map[K]V) Map[K, V] {
	var src = New[K, V]()
	for k, v := range m {
		src.Insert(context.Background(), k, v)
	}
	return src
}

// Del deletes a key, the operation is idempotent. It has HTTP
// DELETE semantics.
func (data Source[K, V]) Del(ctx context.Context, key K) error {
	return data.Delete(ctx, key)
}

// Mod modifies the value using the given patch. If the value does
// not exist, it will be created.
func (data Source[K, V]) Mod(ctx context.Context, key K, patch PatchBuilder[V]) error {
	return data.Mutate(ctx, key, patch)
}

// All returns a result iterator over all key-value pairs.
func (data Source[K, V]) All(ctx context.Context) Results[K, V] {
	return data.Search(ctx, nil)
}

// Get returns the value of the given key. If the value does not
// exist, the second return value will be false.
func (data Source[K, V]) Get(ctx context.Context, key K) (V, error) {
	v, _, err := data.Lookup(ctx, key)
	return v, err
}

// Set a key overwriting any existing value at the given key. This
// operation is idempotent. It has HTTP PUT semantics.
func (data Source[K, V]) Set(ctx context.Context, key K, val V) error {
	return data.Insert(ctx, key, val)
}
