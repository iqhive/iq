package data_test

import (
	"context"
	"testing"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/is"
	"pkg.iqhive.com/iq/verify"
)

type TestWalker struct {
	t         *testing.T
	i         int
	expecting []string
}

func AssertWalk(t *testing.T, expecting ...string) *TestWalker {
	return &TestWalker{t: t, expecting: expecting}
}

func (walker *TestWalker) Lookup(path string) error {
	if path != walker.expecting[walker.i] {
		walker.t.Errorf("Expected %s, got %s", walker.expecting, path)
	}
	walker.i++
	return nil
}

func (walker *TestWalker) Search(path string, more bool, each func(string) error) error {
	if path != walker.expecting[walker.i] {
		walker.t.Errorf("Expected %s, got %s", walker.expecting, path)
	}
	walker.i++

	if more {
		return each("X")
	}
	return nil
}

func TestWalk(t *testing.T) {
	type dbOrder struct {
		Product string
	}
	type dbOrderID struct {
		CustomerUUID string
		OrderID      string
	}

	var location1 = data.Location[dbOrderID]("customers/{CustomerUUID}/orders/{OrderID}")

	query1 := data.QueryBuilder[dbOrderID, dbOrder](func(index *dbOrderID, value *dbOrder) data.Query {
		return data.Query{
			data.Where(&index.CustomerUUID).Equals("123"),
		}
	})
	if err := location1.Walk(query1.Build(), AssertWalk(t, "customers/123/orders/")); err != nil {
		t.Error(err)
	}

	var location2 = data.Location[string]("customers")

	query2 := data.QueryBuilder[string, dbOrder](func(index *string, value *dbOrder) data.Query {
		return data.Query{
			data.Where(index).Equals("123"),
		}
	})
	if err := location2.Walk(query2.Build(), AssertWalk(t, "customers/123")); err != nil {
		t.Error(err)
	}

	var location3 = data.Location[dbOrderID]("customers/{CustomerUUID}/orders/{OrderID}")

	query3 := data.QueryBuilder[dbOrderID, dbOrder](func(index *dbOrderID, value *dbOrder) data.Query {
		return data.Query{
			data.Where(&index.OrderID).Equals("123"),
		}
	})
	if err := location3.Walk(query3.Build(), AssertWalk(t, "customers/", "customers/X/orders/123")); err != nil {
		t.Error(err)
	}
}

func TestPath(t *testing.T) {

	var location1 = data.Location[string]("customers/{Customer=%v}/orders/static")

	if err := verify.That(location1.Path("testing"))(is.Value(`customers/testing/orders/static`)); err != nil {
		t.Error(err)
	}
}

func TestPointers(t *testing.T) {
	type ValueWithPointers struct {
		Pointer *int
	}

	var table = data.New[string, ValueWithPointers]()

	query := data.QueryBuilder[string, ValueWithPointers](func(index *string, value *ValueWithPointers) data.Query {
		return data.Query{
			data.Where(index).Equals("testing"),
		}
	})

	query.Build()

	table.Insert(context.Background(), "testing", ValueWithPointers{})
}

func TestNested(t *testing.T) {
	var ctx = context.Background()

	type Customer struct {
		Name string
		Tags data.Log[string]
	}

	var table = data.New[string, Customer]()
	table.Insert(ctx, "123", Customer{
		Name: "Test",
		Tags: data.NewLog[string]("one", "two", "three"),
	})

	customer, ok, err := table.Lookup(ctx, "123")
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fatal("expected ok")
	}
	if customer.Name != "Test" {
		t.Fatal("expected Test")
	}
	for value := range customer.Tags.Stream(ctx, nil) {
		tag, err := value.Get()
		if err != nil {
			t.Fatal(err)
		}
		switch tag {
		case "one", "two", "three":
		default:
			t.Fatal("unexpected tag")
		}
	}
}
