package data

import (
	"context"
	"testing"
)

func TestSource(t *testing.T) {
	var ctx = context.Background()
	var suite = dataTestSuite{
		Customers: New[testCustomerID, testCustomer](),
	}
	if err := suite.test(ctx); err != nil {
		t.Fatal(err)
	}
}

func TestFilter(t *testing.T) {
	var ctx = context.Background()

	var table = New[string, string]().Filter(func(index, value *string) Query {
		return Query{
			Match(index).HasPrefix("test:"),
		}
	})
	if err := table.Insert(ctx, "test:1", "one"); err != nil {
		t.Fatal(err)
	}
	if err := table.Insert(ctx, "whoops", "two"); err == nil {
		t.Fatal("expected error")
	}

	val, ok, err := table.Lookup(ctx, "test:1")
	if err != nil {
		t.Fatal(err)
	}
	if !ok {
		t.Fatal("expected ok")
	}
	if val != "one" {
		t.Fatal("expected one")
	}

	val, ok, err = table.Lookup(ctx, "whoops")
	if err != nil {
		t.Fatal(err)
	}
	if ok {
		t.Fatal("expected not ok")
	}
	if val != "" {
		t.Fatal("expected empty")
	}

}
