/*
Package join provides SQL JOIN semantics for data.Maps.

	type Customer struct {
		Name string
	}
	type Order struct {
		Customer string
		Price    cash.Money
	}

	var customers = data.Make[string, Customer](0)
	customers.Insert(ctx, "alice", Customer{"Alice"})

	var orders = data.Make[string, Order](0)
	orders.Insert(ctx, "order1", Order{"alice", 100})
	orders.Insert(ctx, "order2", Order{"bob", 200})

	var customerToOrder = func(_ *string, customer *Customer) *string {
		return nil
	}
	var orderToCustomer = func(_ *string, order *Order) *string {
		return &order.Customer
	}
	var pairs = join.NewPair(customerToOrder, orderToCustomer)

	var query = func(_ *string, customer *Customer) data.Query {
		return data.Query{
			data.Where(&customer.Name).Equals("Alice"),
		}
	}

	for result := range pairs.Search(ctx, customers, orders, query, nil) {
		_, value, err := result.Get()
		if err != nil {
			t.Fatal(err)
		}
		customer, order := value.Split()
		fmt.Println(customer, order)
	}
*/
package join

import (
	"context"

	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/tuple"
)

type Relationship int

const (
	OneToOne Relationship = iota + 1
	OneToMany
	ManyToOne
	ManyToMany
)

// Pair describes the relationship between two maps.
type Pair[IndexA, ValueA, IndexB, ValueB comparable] struct {
	aToB func(*IndexA, *ValueA) *IndexB
	bToA func(*IndexB, *ValueB) *IndexA
}

func NewPair[IndexA, ValueA, IndexB, ValueB comparable](aToB func(*IndexA, *ValueA) *IndexB, bToA func(*IndexB, *ValueB) *IndexA) Pair[IndexA, ValueA, IndexB, ValueB] {
	return Pair[IndexA, ValueA, IndexB, ValueB]{aToB, bToA}
}

func (p Pair[IndexA, ValueA, IndexB, ValueB]) Relationship() Relationship {
	var zeroIndexA IndexA
	var zeroIndexB IndexB
	var zeroValueA ValueA
	var zeroValueB ValueB

	toB := p.aToB(&zeroIndexA, &zeroValueA)
	toA := p.bToA(&zeroIndexB, &zeroValueB)

	switch {
	case toB != nil && toA != nil:
		return OneToOne
	case toB != nil && toA == nil:
		return ManyToOne
	case toB == nil && toA != nil:
		return OneToMany
	}
	return 0
}

// Search performs an inner join search.
func (pair Pair[I1, V1, I2, V2]) Search(ctx context.Context, a data.Source[I1, V1], b data.Source[I2, V2], query1 data.QueryBuilder[I1, V1], query2 data.QueryBuilder[I2, V2]) data.Results[tuple.Pair[I1, I2], tuple.Pair[V1, V2]] {
	var results = make(data.Results[tuple.Pair[I1, I2], tuple.Pair[V1, V2]])
	go func() {

		results1 := b.Search(ctx, query2)

		for result := range results1 {
			index, value, err := result.Get()
			if err != nil {
				results.Close(ctx, err)
				return
			}

			a_index := pair.bToA(&index, &value)

			av, ok, err := a.Lookup(ctx, *a_index)
			if err != nil {
				results.Close(ctx, err)
				return
			}
			if !ok {
				continue
			}

			results.Send(ctx, tuple.NewPair(*a_index, index), tuple.NewPair(av, value))
		}

		results.Close(ctx, nil)
	}()
	return results
}
