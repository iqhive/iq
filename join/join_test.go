package join_test

import (
	"context"
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/cash"
	"pkg.iqhive.com/iq/data"
	"pkg.iqhive.com/iq/join"
)

func TestJoin(t *testing.T) {
	ctx := context.Background()

	type Customer struct {
		Name string
	}
	type Order struct {
		Customer string
		Price    cash.Money
	}

	var customers = data.New[string, Customer]()
	customers.Insert(ctx, "alice", Customer{"Alice"})

	var orders = data.New[string, Order]()
	orders.Insert(ctx, "order1", Order{"alice", 100})
	orders.Insert(ctx, "order2", Order{"bob", 200})

	var customerToOrder = func(_ *string, customer *Customer) *string {
		return nil
	}
	var orderToCustomer = func(_ *string, order *Order) *string {
		return &order.Customer
	}
	var pairs = join.NewPair(customerToOrder, orderToCustomer)

	var query = func(_ *string, customer *Customer) data.Query {
		return data.Query{
			data.Where(&customer.Name).Equals("Alice"),
		}
	}

	for result := range pairs.Search(ctx, customers, orders, query, nil) {
		_, value, err := result.Get()
		if err != nil {
			t.Fatal(err)
		}
		customer, order := value.Split()
		fmt.Println(customer, order)
	}
}
