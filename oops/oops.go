/*
Package oops provides error handling functions with support for tracing and
actionability.

The idea of 'oops' is to propagate useful errors up the call stack.
This is achieved by universal adoption of tracing and actionable error messages.
Errors are designed to be reported to an agent who can resolve the error.
Some examples of agents are:

  - A client, user or caller (upstream).
  - A developer, system administrator and/or distributor (internal).
  - A thirdparty (downstream).

Any error that occurs can be reported to one or more of the agents above. It
is also important that the error contains enough information so that the it
can be resolved by the agent.

When you return errors constructed by this package, they are automatically
wrapped with a trace and assigned an agent (determined by the constructor).

# Use Cases

An error occurs within your function.

	err := DoSomething()
	if err != nil {
		return oops.Wrap(err)
	}

The caller forgets to include required information.

	return please.Provide("[the required information]")

The caller provides an invalid field.

	return please.Provide("a valid [field]")

An error occurs when communicating with a thirdparty service/module.

	err := thirdparty_client.DoSomething()
	if err != nil {
		return oops.From("thirdparty", err)
	}

An error occurs when communicating with a service/module
that you control.

	err := somesvc.DoSomething()
	if err != nil {
		return oops.Tag(err, "somesvc")
	}

Missing configuration, or a known issue that a developer or administrator
needs to resolve.

	if config.IsMissing() {
		return oops.Fixme("configuration is required to launch this software")
	}

Thirdparty response doesn't match expectation. Useful for determining possible
misunderstanding between asssumptions.

	resp := thirdparty_client.DoSomething()
	if resp != "expected result" {
		return oops.Expected("thirdparty", "expected result", resp)
	}
*/
package oops

// STORY as a developer looking at my deployed software, I want to see the full trace for an error
// when it is logged.

// STORY as a developer, I want to be able to add detailed information to an error and have it
// available in the logs but not visible to the end user.

import (
	"fmt"
	"net/http"
	"runtime"

	"pkg.iqhive.com/iq/xray"
)

// unspecified agent sentinal.
var unspecified string

// trace is aliased as is the recommendation
// from the traceable package.
type trace = xray.Trace

type Error error

// ErrorWithAgent can be used to replace the builtin error type in
// order to enforce traceable and actionable errors to be
// returned from functions within your codebase. Consider
// making this a convention and/or guideline in order to
// receive the most value from this package.
type ErrorWithAgent interface {
	error

	xray.Error

	//Agent returns the agent(s) that the Error string should
	//be passed to. Non-nil results correspond to the agents.
	//If the value is an empty string then the exact agent is
	//unspecified. All other string values are considered to
	//be a specific agent name. The exact distinction between
	//agent kinds is broken down in the package documentation.
	Agent() (upstream, internal, downstream *string)
}

type wrappedError struct {
	trace
	error

	upstream, internal, downstream *string
}

func (e *wrappedError) Unwrap() error {
	return e.error
}

func (e *wrappedError) Agent() (upstream, internal, downstream *string) {
	return e.upstream, e.internal, e.downstream
}

// New is an alias for Wrap.
func New(err error) error {
	if err == nil {
		return nil
	}

	var w = wrappedError{
		trace: xray.NewTrace(runtime.Caller(1)),
		error: err,
	}

	if actionable, ok := err.(ErrorWithAgent); ok {
		w.upstream, w.internal, w.downstream = actionable.Agent()
		if w.upstream != nil {
			w.upstream = nil //You are the caller.
		}
	}

	return &w
}

/*
Wrap wraps the error with a trace.

	if err != nil {
		return err //If you do this, you lose your trace.
	}

	if err != nil {
		return oops.Wrap(err) //You can do this instead.
	}

Oops will look at the error and may change the Agent.
For example, if the error is a caller/user/client error
then oops will assume that you are the caller and will
assign the error to the developer.

Use please.Fix to preserve the original agent assignment.
*/
func Wrap(err error) ErrorWithAgent {
	if err == nil {
		return nil
	}

	var w = wrappedError{
		trace: xray.NewTrace(runtime.Caller(1)),
		error: err,
	}

	if actionable, ok := err.(ErrorWithAgent); ok {
		w.upstream, w.internal, w.downstream = actionable.Agent()
		if w.upstream != nil {
			w.upstream = nil //You are the caller.
		}
	}

	return &w
}

type importedError struct {
	trace
	error

	internal, downstream *string
}

func (e *importedError) Unwrap() error {
	return e.error
}

func (e *importedError) Agent() (upstream, internal, downstream *string) {
	return nil, e.internal, e.downstream
}

// From returns an error that indicates that the
// error passed to this function was source from
// the given thirdparty. It may assigned either
// to the developer, or the thirdparty, depending
// on the error.
func From(thirdparty string, err error) ErrorWithAgent {
	if err == nil {
		return nil
	}

	var w = importedError{
		trace: xray.NewTrace(runtime.Caller(1)),
		error: err,
	}

	//Determine who needs to be assigned.
	if actionable, ok := err.(ErrorWithAgent); ok {
		us, them, deps := actionable.Agent()
		switch {
		case us == nil && (them != nil || deps != nil):
			w.downstream = &thirdparty //assigned to them.
		case us != nil && (them == nil && deps == nil):
			w.internal = &unspecified //assigned to us.
		default:
			w.downstream = &thirdparty //assign both
			w.internal = &unspecified
		}
	}

	return &w
}

type brokenExpectationsError struct {
	trace

	downstream, expectation string
}

func (e *brokenExpectationsError) Agent() (upstream, internal, downstream *string) {
	return nil, nil, &e.downstream
}

func (e *brokenExpectationsError) Error() string {
	return e.expectation
}

// Expected returns an Error assigned to the thirdparty
// indicating than an expectation or assumption on a
// response from a thirdparty was different than what
// was expected.
func Expected(thirdparty, expectation string, reality interface{}) ErrorWithAgent {
	return &brokenExpectationsError{
		trace:       xray.NewTrace(runtime.Caller(1)),
		downstream:  thirdparty,
		expectation: fmt.Sprintf("expected "+expectation+" but got %v", reality),
	}
}

type acccessDeniedError struct {
	trace
}

func (e *acccessDeniedError) StatusHTTP() int {
	return http.StatusForbidden
}

func (e *acccessDeniedError) Agent() (upstream, internal, downstream *string) {
	return &unspecified, nil, nil
}

func (e *acccessDeniedError) Error() string {
	return "access denied"
}

// AccessDenined returns an Error indicating that the caller
// does not have access to run the function.
func AccessDenied() ErrorWithAgent {
	return &acccessDeniedError{
		trace: xray.NewTrace(runtime.Caller(1)),
	}
}

type developerError struct {
	trace

	solution string
}

func (*developerError) Agent() (upstream, internal, downstream *string) {
	return nil, &unspecified, nil
}

func (e *developerError) Error() string {
	return e.solution
}

// Fixme returns an error that indicates that the error is a known error
// with a known solution. The solution should begin with a verb.
func Fixme(solution string, args ...interface{}) ErrorWithAgent {
	return &developerError{
		trace:    xray.NewTrace(runtime.Caller(1)),
		solution: fmt.Sprintf(solution, args...),
	}
}

type taggedError struct {
	trace
	error

	tag string

	internal, downstream *string
}

func (e *taggedError) Unwrap() error {
	return e.error
}

func (e *taggedError) Agent() (upstream, internal, downstream *string) {
	return nil, &e.tag, e.downstream
}

// Tag returns an Error that has been wrapped and tagged with
// the specified tag. Useful for categorisation of errors.
// Assume that the tag string could be exposed to
func Tag(err error, tag string) ErrorWithAgent {
	if err == nil {
		return nil
	}

	var w = taggedError{
		trace: xray.NewTrace(runtime.Caller(1)),
		error: err,
		tag:   tag,
	}

	w.internal = &w.tag

	//could be thirdparty issue, overrides the tag.
	if actionable, ok := err.(ErrorWithAgent); ok {
		_, _, w.downstream = actionable.Agent()
		if w.downstream != nil {
			w.internal = nil
		}
	}

	return &w
}

type sorryError struct {
	trace
	error

	message string
}

func (e *sorryError) Unwrap() error {
	return e.error
}

func (e *sorryError) StatusHTTP() int {
	return http.StatusInternalServerError
}

func (e *sorryError) Agent() (upstream, internal, downstream *string) {
	return &unspecified, &unspecified, nil
}

func (e *sorryError) Error() string {
	return e.message
}

// Sorry returns an error that is not really actionable but
// you want to upstream to know some more information about.
// If the first argument is an error, it will be wrapped and
// will be removed from the formatting arguments.
// ie.
//
//	oops.Sorry("something went wrong", err)
func Sorry(format string, args ...interface{}) ErrorWithAgent {

	var err error
	if len(args) > 0 {
		if e, ok := args[0].(error); ok {
			err = e
			args = args[1:]
		}
	}
	return &sorryError{
		error:   err,
		trace:   xray.NewTrace(runtime.Caller(1)),
		message: fmt.Sprintf(format, args...),
	}
}

type missingError struct {
	trace
	error

	message string
}

func (e *missingError) StatusHTTP() int {
	return http.StatusNotFound
}

func (e *missingError) Is(err error) bool {
	fmt.Printf("%T", err)

	check, ok := err.(*missingError)
	if !ok {
		httpErr, ok := err.(interface {
			StatusHTTP() int
		})
		if ok && httpErr.StatusHTTP() == http.StatusNotFound {
			return true
		}
	}
	return ok && check.message == e.message
}

func (e *missingError) Unwrap() error {
	return e.error
}

func (e *missingError) Agent() (upstream, internal, downstream *string) {
	return &unspecified, &unspecified, nil
}

func (e *missingError) Error() string {
	return e.message
}

// NotFound returns an Error indicating that the resource
// requested by the caller was not found.
func NotFound() ErrorWithAgent {
	return &missingError{
		trace:   xray.NewTrace(runtime.Caller(1)),
		message: "not found",
	}
}

// Missing returns an Error indicating that the resource
// requested by the caller was not found.
func Missing(resource string) ErrorWithAgent {
	return &missingError{
		trace:   xray.NewTrace(runtime.Caller(1)),
		message: resource + " not found",
	}
}

type notImplemented struct {
	trace
}

func (e *notImplemented) Is(err error) bool {
	_, ok := err.(*notImplemented)
	if !ok {
		httpErr, ok := err.(interface {
			StatusHTTP() int
		})
		if ok && httpErr.StatusHTTP() == http.StatusNotImplemented {
			return true
		}
	}
	return ok
}

func (e *notImplemented) StatusHTTP() int {
	return http.StatusNotImplemented
}

func (e *notImplemented) Agent() (upstream, internal, downstream *string) {
	return &unspecified, &unspecified, nil
}

func (e *notImplemented) Error() string {
	return "not implemented"
}

// NotImplemented returns an Error indicating that the
// requested function is not implemented.
func NotImplemented() ErrorWithAgent {
	return &notImplemented{
		trace: xray.NewTrace(runtime.Caller(1)),
	}
}
