// Package story provides methods for working with a 'story book' where the solutions for
// user stories are organised into a set of guided testable steps.
package story

import (
	"context"
	"fmt"
	"reflect"
	"runtime/debug"
	"strings"
	"testing"
	"time"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/oops"
	"pkg.iqhive.com/iq/queue"
	"pkg.iqhive.com/iq/xray"
)

// Name identifies a story within a [Book].
type Name string

// Book can be embedded inside of a structure in order to indicate that it is a
// story book. Each method on the structure with the signature `func(context.Context) error`
// will be added to the book as a story.
type Book struct {
	Chapters map[Name][]Solution

	OnTimeChanged queue.Topic[struct{}]

	time time.Time

	cursor struct {
		name Name
		page int
		step int

		depth uint
	}
}

func (book *Book) getBook() *Book {
	return book
}

func (book *Book) Time() time.Time {
	if book.time.IsZero() {
		book.time = time.Now()
	}
	return book.time
}

func (book *Book) Wait(ctx context.Context, duration time.Duration) error {
	book.time = book.time.Add(duration)
	return book.OnTimeChanged.Send(ctx, struct{}{})
}

func (book *Book) WaitDays(ctx context.Context, days int) error {
	book.time = book.time.AddDate(0, 0, days)
	return book.OnTimeChanged.Send(ctx, struct{}{})
}

func (book *Book) WaitWeeks(ctx context.Context, weeks int) error {
	book.time = book.time.AddDate(0, 0, weeks*7)
	return book.OnTimeChanged.Send(ctx, struct{}{})
}

func (book *Book) WaitMonths(ctx context.Context, months int) error {
	book.time = book.time.AddDate(0, months, 0)
	return book.OnTimeChanged.Send(ctx, struct{}{})
}

func (book *Book) WaitYears(ctx context.Context, years int) error {
	book.time = book.time.AddDate(years, 0, 0)
	return book.OnTimeChanged.Send(ctx, struct{}{})
}

// Pages is a struct with an embedded [Book] that can be read.
type Pages interface {
	getBook() *Book
}

func (book *Book) trace(spec api.Specification) {
	for i, old := range spec.Functions {
		old := old.Copy()
		fn := &spec.Functions[i]
		fn.Set(func(ctx context.Context, keys []api.Key, args []reflect.Value) (results []reflect.Value, err error) {
			book.cursor.depth++
			defer func() {
				book.cursor.depth--
			}()

			solution := &book.Chapters[book.cursor.name][book.cursor.page]
			cursorStep := book.cursor.step
			solution.Steps = append(solution.Steps, Step{})
			book.cursor.step++

			results, err = old.FuncValue.Call(ctx, nil, func(v []reflect.Value) error {
				copy(v, args)
				return nil
			})

			solution = &book.Chapters[book.cursor.name][book.cursor.page]
			step := &solution.Steps[cursorStep]

			step.Call = fn
			step.Args = args
			step.Rets = results
			step.Fail = err
			step.Depth = book.cursor.depth

			return
		})
	}
	for _, section := range spec.Sections {
		book.trace(section)
	}
}

// Read returns a book of stories for the given pages.
func Read[T Pages](constructor func() T) (*Book, error) {
	return Test(nil, constructor)

}

// Test that all ready stories are passing.
func Test[T Pages](t *testing.T, constructor func() T) (*Book, error) {
	if t != nil {
		t.Helper()
	}
	template := constructor()

	book := template.getBook()
	book.Chapters = make(map[Name][]Solution)

	for i := 0; i < reflect.TypeOf(template).NumMethod(); i++ {

		isolated := constructor()

		rtype := reflect.TypeOf(isolated)
		rvalue := reflect.ValueOf(isolated)

		// setup API capture
		for i := 0; i < rtype.Elem().NumField(); i++ {
			if !rtype.Elem().Field(i).IsExported() {
				continue
			}
			impl, ok := rvalue.Elem().Field(i).Addr().Interface().(api.WithSpecification)
			if ok {
				spec, err := api.SpecificationOf(impl)
				if err != nil {
					return nil, oops.New(err)
				}
				book.trace(spec)
			}
		}

		method := rtype.Method(i)

		if !strings.HasPrefix(method.Name, "Test") {
			continue
		}

		writer, ok := rvalue.Method(i).Interface().(func(context.Context) error)
		if !ok {
			continue
		}

		trim := Name(strings.TrimPrefix(method.Name, "Test"))
		name := Name(strings.TrimRight(string(trim), "0123456789"))

		if len(trim) == len(name) {
			return nil, fmt.Errorf("please add a numeric ID to the end of %s", method.Name)
		}

		book.Chapters[name] = append(book.Chapters[name], Solution{
			ID: string(trim[len(name):]),
		})
		book.cursor.name = name
		book.cursor.page = len(book.Chapters[name]) - 1
		book.cursor.step = 0
		book.cursor.depth = 0

		fn := func(t *testing.T) {
			if t != nil {
				t.Parallel()
				t.Helper()
			}

			defer func() {
				if t != nil {
					t.Helper()
				}

				if err := recover(); err != nil {
					solution := book.Chapters[name][book.cursor.page]
					solution.Error = fmt.Errorf("%v %s", err, string(debug.Stack()))
					book.Chapters[name][book.cursor.page] = solution
				}
			}()

			if err := writer(context.Background()); err != nil {
				solution := book.Chapters[name][book.cursor.page]
				solution.Error = err
				if solution.Ready && t != nil {
					fmt.Println(err)
					t.Log(xray.Sprint(err))
					t.Fail()
				}
				book.Chapters[name][book.cursor.page] = solution
			}
		}

		if t != nil {
			t.Run(string(method.Name), fn)
		} else {
			fn(nil)
		}
	}

	return book, nil
}

// Ready marks the story as completed, if [Test] is called on a storybook containing
// this story, the test will fail if this story does not return a nil error.
func (book *Book) Ready() {
	if book.cursor.name == "" {
		return
	}
	if book.Chapters == nil {
		book.Chapters = make(map[Name][]Solution)
	}
	solutions := book.Chapters[book.cursor.name]
	solutions[book.cursor.page].Ready = true
	book.Chapters[book.cursor.name] = solutions
}

// Story sets the description for the current story, ie.
// "as a user, I want to write a story, so that I can share my experiences".
func (book *Book) Story(text string) {
	if book.cursor.name == "" {
		return
	}
	if book.Chapters == nil {
		book.Chapters = make(map[Name][]Solution)
	}
	solutions := book.Chapters[book.cursor.name]
	solutions[book.cursor.page].Story = text
	book.Chapters[book.cursor.name] = solutions
}

// Guide adds documentation to the story.
func (book *Book) Guide(format string, args ...any) {
	if book.cursor.name == "" {
		return
	}

	solution := &book.Chapters[book.cursor.name][book.cursor.page]
	if len(solution.Steps) == 0 {
		solution.Steps = append(solution.Steps, Step{})
	}
	step := &solution.Steps[book.cursor.step]
	if step.Text == "" {
		step.Text = fmt.Sprintf(format, args...)
	} else {
		solution.Steps = append(solution.Steps, Step{
			Text: fmt.Sprintf(format, args...),
		})
		book.cursor.step++
	}
}

type Solution struct {
	ID string

	Story string
	Steps []Step
	Error error
	Ready bool
}

type Step struct {
	Text string

	Call *api.Function
	Args []reflect.Value
	Rets []reflect.Value
	Fail error

	Depth uint
}
