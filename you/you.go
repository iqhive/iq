// Package you provides more direct solutions than the please package, where a user needs to
// take certain prerequisite actions before the error can be resolved.
package you

import (
	"errors"
	"fmt"
	"runtime"

	"pkg.iqhive.com/iq/xray"
)

type trace = xray.Trace

// pleaseError is a user-visible error.
type youError struct {
	trace

	toFix  error
	please string
}

func (e *youError) Error() string {
	return fmt.Sprintf("please %v", e.please)
}

func (e *youError) Unwrap() error {
	return e.toFix
}

func (e *youError) StatusHTTP() int {
	return 409
}

type literal string

// Should indicates that the returned error is caused by
// a malformed or invalid context, state or environment
// (perhaps because operations are being made in the wrong
// order). You must include a description of what needs to
// happen first in order to avoid this error.
//
// ie. "connect to the VPN"
// ie. "verify your email address"
func Should(description literal, args ...interface{}) error {
	var err error
	if len(args) > 0 {
		if e, ok := args[0].(error); ok {
			err = e
			args = args[1:]
		}
	}

	if err == nil {
		err = errors.New("invalid context")
	}

	return &youError{
		trace:  xray.NewTrace(runtime.Caller(1)),
		toFix:  err,
		please: fmt.Sprintf(string(description), args...),
	}
}
