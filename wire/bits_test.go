package wire_test

import (
	"encoding/binary"
	"testing"

	"pkg.iqhive.com/iq/wire"
)

func TestBits(t *testing.T) {
	var bits wire.Bits

	bits.Append(true)
	if s := bits.String(); s != "1" {
		t.Errorf("got %s, want 1", s)
	}

	bits.Append(true)
	if s := bits.String(); s != "11" {
		t.Errorf("got %s, want 11", s)
	}

	bits.Append(false)
	if s := bits.String(); s != "110" {
		t.Errorf("got %s, want 110", s)
	}

	bits.Set(2, true)
	if s := bits.String(); s != "111" {
		t.Errorf("got %s, want 111", s)
	}

	bits = wire.Bits{}
	bits.Append16(0b1011000000000000)
	if s := bits.String(); s != "0000000010110000" {
		t.Errorf("got %s, want 0000000010110000", s)
	}

	num := bits.Ingest16()
	if num != 0b1011000000000000 {
		t.Errorf("got %b, want 1011000000000000", num)
	}

	bits = wire.Bits{}
	bits.Append32(0b10110000000000001011000000100000)
	if s := bits.String(); s != "00100000101100000000000010110000" {
		t.Errorf("got %s, want 00100000101100000000000010110000", s)
	}

	num32 := bits.Ingest32()
	if num32 != 0b10110000000000001011000000100000 {
		t.Errorf("got %b, want 0b10110000000000001011000000100000", num)
	}
}

func BenchmarkWireAppend(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var bits wire.Bits
		bits.Append16(uint16(0b1011000000000000))
	}
}

func BenchmarkEncodingBinary(b *testing.B) {
	for i := 0; i < b.N; i++ {
		var buf [2]byte
		binary.LittleEndian.PutUint16(buf[:], uint16(0b1011000000000000))
	}
}
