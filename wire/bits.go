package wire

import (
	"strings"
)

// Bits is a sequence of bits, without any particular alignment.
type Bits struct {
	offset uint8
	cutoff uint8
	buffer []byte
}

// Get returns the value of the bit at the given index.
func (s *Bits) Get(index int) bool {
	index += int(s.offset)
	return s.buffer[index/8]&(1<<uint(7-index%8)) != 0
}

func (bits *Bits) Len() int {
	return len(bits.buffer)*8 - int(bits.cutoff)
}

// String returns the slice as a binary sequence of '0's and '1's.
func (bits *Bits) String() string {
	var buf strings.Builder
	for i := 0; i < bits.Len(); i++ {
		if bits.Get(i) {
			buf.WriteByte('1')
		} else {
			buf.WriteByte('0')
		}
	}
	return buf.String()
}

// Set sets the value of the bit at the given index.
func (s Bits) Set(index int, one bool) {
	index += int(s.offset)
	if one {
		s.buffer[index/8] |= 1 << (7 - index%8)
	} else {
		s.buffer[index/8] &= ^(1 << (7 - index%8))
	}
}

func (bits *Bits) Append(one bool) {
	if bits.cutoff > 0 {
		bits.Set(bits.Len(), one)
		bits.cutoff--
		return
	}
	if one {
		bits.buffer = append(bits.buffer, 1<<7)
	} else {
		bits.buffer = append(bits.buffer, 0)
	}
	bits.cutoff = 7
}

func (bits *Bits) Append8(b uint8) error {
	if bits.offset+bits.cutoff == 0 {
		bits.buffer = append(bits.buffer, b)
		return nil
	}
	for i := 0; i < 8; i++ {
		bits.Append(b&(1<<uint(7-i)) != 0)
	}
	return nil
}

func (s *Bits) Append16(value uint16) {
	s.Append8(byte(value))
	s.Append8(byte(value >> 8))
}

func (s *Bits) Append32(value uint32) {
	s.Append8(byte(value))
	s.Append8(byte(value >> 8))
	s.Append8(byte(value >> 16))
	s.Append8(byte(value >> 24))
}

func (s *Bits) Append64(value uint64) {
	s.Append8(byte(value))
	s.Append8(byte(value >> 8))
	s.Append8(byte(value >> 16))
	s.Append8(byte(value >> 24))
	s.Append8(byte(value >> 32))
	s.Append8(byte(value >> 40))
	s.Append8(byte(value >> 48))
	s.Append8(byte(value >> 56))
}

// Ingest removes the first bit and
// returns true if it was a 1.
func (bits *Bits) Ingest() bool {
	one := bits.Get(0)
	if bits.offset == 7 {
		bits.offset = 0
		bits.buffer = bits.buffer[1:]
	} else {
		bits.offset++
	}
	return one
}

// Ingest8 removes the first 8 bits and
// returns them as a uint8.
func (bits *Bits) Ingest8() uint8 {
	var b uint8
	for i := 0; i < 8; i++ {
		b <<= 1
		if bits.Ingest() {
			b |= 1
		}
	}
	return b
}

func (bits *Bits) Ingest16() uint16 {
	return uint16(bits.Ingest8()) | uint16(bits.Ingest8())<<8
}

func (bits *Bits) Ingest32() uint32 {
	return uint32(bits.Ingest8()) | uint32(bits.Ingest8())<<8 | uint32(bits.Ingest8())<<16 | uint32(bits.Ingest8())<<24
}

func (bits *Bits) Ingest64() uint64 {
	return uint64(bits.Ingest8()) | uint64(bits.Ingest8())<<8 | uint64(bits.Ingest8())<<16 | uint64(bits.Ingest8())<<24 | uint64(bits.Ingest8())<<32 | uint64(bits.Ingest8())<<40 | uint64(bits.Ingest8())<<48 | uint64(bits.Ingest8())<<56
}
