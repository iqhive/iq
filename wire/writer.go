// Package wire provides buffered bit-level I/O for reading and writing binary protocols.
package wire

import (
	"errors"
	"io"
	"unsafe"

	"pkg.iqhive.com/iq/wire/internal/mirror"
)

const defaultBufSize = 4096

type Encoder struct {
	out io.Writer
	buf []byte
	err error
}

func NewEncoder(w io.Writer) Encoder {
	return Encoder{
		out: w,
		buf: make([]byte, 0, defaultBufSize),
	}
}

func (e *Encoder) Err() error {
	return e.err
}

func (w *Encoder) Reset() {
	w.buf = w.buf[:0:cap(w.buf)]
}

func (w *Encoder) canBufferBytes(n int) bool {
	return len(w.buf)+n < cap(w.buf)
}

func (w *Encoder) Flush() error {
	if len(w.buf) == 0 {
		return nil
	}
	_, err := w.out.Write(w.buf)
	if err != nil {
		w.err = err
	}
	w.buf = w.buf[:0:cap(w.buf)]
	return w.err
}

func (e *Encoder) flush() {
	_, err := e.out.Write(e.buf)
	if err != nil {
		e.err = err
	}
	e.buf = e.buf[:0:cap(e.buf)]
}

func (e *Encoder) EncodeBytes(data []byte) {
	for {
		n := copy(e.buf[len(e.buf):cap(e.buf)], data)
		e.buf = e.buf[:len(e.buf)+n]
		if n == len(data) {
			break
		}
		data = data[n:]
		_, err := e.out.Write(e.buf)
		if err != nil {
			e.err = err
		}
		e.buf = e.buf[:0:cap(e.buf)]
	}
}

func (e *Encoder) EncodeU8(b uint8) {
	if len(e.buf)+1 >= cap(e.buf) {
		e.Flush()
	}
	e.buf = append(e.buf, b)
}

func (w *Encoder) EncodeU16(v uint16) {
	w.EncodeU8(byte(v))
	w.EncodeU8(byte(v >> 8))
}

func (w *Encoder) EncodeU32(v uint32) {
	w.EncodeU8(byte(v))
	w.EncodeU8(byte(v >> 8))
	w.EncodeU8(byte(v >> 16))
	w.EncodeU8(byte(v >> 24))
}

func (w *Encoder) EncodeU64(v uint64) {
	w.EncodeU8(byte(v))
	w.EncodeU8(byte(v >> 8))
	w.EncodeU8(byte(v >> 16))
	w.EncodeU8(byte(v >> 24))
	w.EncodeU8(byte(v >> 32))
	w.EncodeU8(byte(v >> 40))
	w.EncodeU8(byte(v >> 48))
	w.EncodeU8(byte(v >> 56))
}

func (encoder *Encoder) EncodeString(s string) {
	encoder.EncodeBytes(unsafe.Slice(unsafe.StringData(s), len(s)))
}

func (encoder *Encoder) Encode(value any) error {
	mvalue := mirror.Reflect(value)
	vtype := mvalue.Type()

	if vtype.HasPointers() {
		return errors.New("pointer types are not supported yet")
	}
	return errors.New("not implemented")
}
