package proto

import (
	"google.golang.org/protobuf/proto"
	"pkg.iqhive.com/iq/wire/internal/pb"
)

// MessageOf returns a proto message from the given argument it implements the absolute bare minumum of
// proto.Message so that the marshal functions in this package can be used in the grpc package.
// (the google grpc module requires arguments of a function to implement proto.Message).
func MessageOf(i interface{}) (proto.Message, error) {
	return pb.MessageOf(i)
}

// Marshal marshals the given Go value to the protocol buffer
// encoding of the value.
func Marshal(v interface{}) ([]byte, error) {
	return pb.Marshal(v)
}

func Unmarshal(data []byte, value interface{}) error {
	return pb.Unmarshal(data, value)
}
