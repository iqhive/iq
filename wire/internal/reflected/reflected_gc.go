//go:build gc

// Package reflected provides faster value access for reflection.
package reflected

import (
	"reflect"
	"unsafe"
)

type any = interface{}

type Value struct {
	typ *rtype
	val unsafe.Pointer
}

type FieldOffset struct {
	of     *rtype //parent type, used to verify offset.
	typ    *rtype
	offset uintptr
}

func FieldOffsetOf(of reflect.Type, index []int) FieldOffset {
	ofIntf := *(*unsafeInterface)(unsafe.Pointer(&of))
	field := of.FieldByIndex(index)
	typIntf := *(*unsafeInterface)(unsafe.Pointer(&field.Type))
	return FieldOffset{
		of:     (*rtype)(ofIntf.ptr),
		typ:    (*rtype)(typIntf.ptr),
		offset: field.Offset,
	}
}

var rtypeType = reflect.TypeOf(reflect.TypeOf(""))

func (val *Value) Type() reflect.Type {
	header := *(*unsafeInterface)(unsafe.Pointer(&rtypeType))
	header.ptr = unsafe.Pointer(val.typ)
	return *(*reflect.Type)(unsafe.Pointer(&header))
}

func (val *Value) set(to any) {
	header := (*unsafeInterface)(unsafe.Pointer(&to))
	val.typ = (*rtype)(header.typ)
	kind := reflect.Kind(val.typ.kind & kindMask)
	if kind < reflect.Array || kind > reflect.Struct {
		kind = reflect.String
	}
	val.val = header.ptr
}

func (val *Value) get(kind reflect.Kind) unsafeInterface {
	if reflect.Kind(val.typ.kind&kindMask) != kind {
		panic("invalid kind")
	}
	return unsafeInterface{
		typ: unsafe.Pointer(val.typ),
		ptr: val.val,
	}
}

func (v *Value) Kind() reflect.Kind {
	return reflect.Kind(v.typ.kind & kindMask)
}

func (v *Value) NumField() int {
	header := v.get(reflect.Struct)
	rtype := (*structType)(header.typ)
	return len(rtype.fields)
}

func (v *Value) FieldName(i int) string {
	header := v.get(reflect.Struct)
	rtype := (*structType)(header.typ)
	return rtype.fields[i].name.name()
}

func (v *Value) FieldTag(i int) reflect.StructTag {
	header := v.get(reflect.Struct)
	rtype := (*structType)(header.typ)
	return reflect.StructTag(rtype.fields[i].name.tag())
}

func (v *Value) SliceLen() int {
	return (*reflect.SliceHeader)(v.get(reflect.Slice).ptr).Len
}

func (v *Value) SliceIsNil() bool {
	header := v.get(reflect.Slice)
	slice := (*reflect.SliceHeader)(header.ptr)
	return slice.Data == 0
}

func (v *Value) InterfaceIsNil() bool {
	return (*unsafeInterface)(v.get(reflect.Interface).ptr).typ == nil
}

func (v *Value) Interface() interface{} {
	return *(*interface{})(unsafe.Pointer(v.get(reflect.Interface).ptr))
}

func (v *Value) SliceIndex(i int) any {
	header := v.get(reflect.Slice)
	slice := (*reflect.SliceHeader)(header.ptr)
	elem := (*elemType)(header.typ).elem
	return *(*any)(unsafe.Pointer(&unsafeInterface{
		typ: unsafe.Pointer(elem),
		ptr: unsafe.Pointer(unsafe.Add(unsafe.Pointer(slice.Data), uintptr(i)*elem.size)),
	}))
}

func (v *Value) ArrayLen() int {
	return int((*arrayType)(v.get(reflect.Array).typ).len)
}

func (v *Value) ArrayIndex(i int) any {
	header := v.get(reflect.Array)
	elem := (*arrayType)(header.typ).elem
	return *(*any)(unsafe.Pointer(&unsafeInterface{
		typ: unsafe.Pointer(elem),
		ptr: unsafe.Pointer(unsafe.Add(unsafe.Pointer(header.ptr), uintptr(i)*elem.size)),
	}))
}

func (v *Value) MapIsNil() bool {
	header := v.get(reflect.Map)
	mapVal := *(*any)(unsafe.Pointer(&header))
	return reflect.ValueOf(mapVal).IsNil()
}

func (v *Value) MapRange() *reflect.MapIter {
	header := v.get(reflect.Map)
	header.ptr = *(*unsafe.Pointer)(header.ptr)
	mapVal := *(*any)(unsafe.Pointer(&header))
	return reflect.ValueOf(mapVal).MapRange()
}

func (v *Value) FieldTagGet(i int, key string) string {
	tag := v.FieldTag(i)
	for tag != "" {
		// Skip leading space.
		i := 0
		for i < len(tag) && tag[i] == ' ' {
			i++
		}
		tag = tag[i:]
		if tag == "" {
			break
		}

		// Scan to colon. A space, a quote or a control character is a syntax error.
		// Strictly speaking, control chars include the range [0x7f, 0x9f], not just
		// [0x00, 0x1f], but in practice, we ignore the multi-byte control characters
		// as it is simpler to inspect the tag's bytes than the tag's runes.
		i = 0
		for i < len(tag) && tag[i] > ' ' && tag[i] != ':' && tag[i] != '"' && tag[i] != 0x7f {
			i++
		}
		if i == 0 || i+1 >= len(tag) || tag[i] != ':' || tag[i+1] != '"' {
			break
		}
		name := string(tag[:i])
		tag = tag[i+1:]

		// Scan quoted string to find value.
		i = 1
		for i < len(tag) && tag[i] != '"' {
			if tag[i] == '\\' {
				i++
			}
			i++
		}
		if i >= len(tag) {
			break
		}
		qvalue := string(tag[:i+1])
		tag = tag[i+1:]

		if key == name {
			return qvalue[1 : len(qvalue)-1]
		}
	}
	return ""
}

func (v *Value) Field(i int) any {
	header := v.get(reflect.Struct)
	field := (*structType)(header.typ).fields[i]
	return *(*any)(unsafe.Pointer(&unsafeInterface{
		typ: field.typ,
		ptr: unsafe.Add(header.ptr, field.offset),
	}))
}

func (v *Value) FieldByOffset(offset FieldOffset) any {
	header := v.get(reflect.Struct)
	if (*rtype)(header.typ) != offset.of {
		panic("reflect: FieldByOffset of mismatched types")
	}
	// if indirect, make it direct
	if offset.typ.kind&kindMask == uint8(reflect.Ptr) {
		return *(*any)(unsafe.Pointer(&unsafeInterface{
			typ: unsafe.Pointer(offset.typ),
			ptr: *(*unsafe.Pointer)(unsafe.Add(header.ptr, offset.offset)),
		}))
	}
	return *(*any)(unsafe.Pointer(&unsafeInterface{
		typ: unsafe.Pointer(offset.typ),
		ptr: unsafe.Add(header.ptr, offset.offset),
	}))
}

func (v *Value) Deref() any {
	header := v.get(reflect.Ptr)
	rtype := (*elemType)(header.typ)

	if rtype.elem.kind&kindMask != uint8(reflect.Ptr) {
		return *(*any)(unsafe.Pointer(&unsafeInterface{
			typ: unsafe.Pointer(rtype.elem),
			ptr: header.ptr,
		}))
	}
	return *(*any)(unsafe.Pointer(&unsafeInterface{
		typ: unsafe.Pointer(rtype.elem),
		ptr: *(*unsafe.Pointer)(header.ptr),
	}))
}

func (v *Value) IsNil() bool {
	return v.get(reflect.Ptr).ptr == nil
}

func ValueOf(v any) (*Value, reflect.Kind) {
	var val Value

	val.set(v)
	return &val, val.Kind()
}

// BasicValueOf returns the basic value of v
// as an interface. If the value is
// a not a basic type, Get returns
// one of the exported types from
// this package.
func BasicValueOf(v any) any {
	header := (*unsafeInterface)(unsafe.Pointer(&v))

	if v := &basicValues[(*rtype)(header.typ).kind&kindMask]; *v != nil {
		return *(*any)(unsafe.Pointer(&unsafeInterface{
			typ: (*unsafeInterface)(unsafe.Pointer(v)).typ,
			ptr: header.ptr,
		}))
	}

	return v
}

func IsZero(v any) bool {
	header := (*unsafeInterface)(unsafe.Pointer(&v))
	if header.ptr == nil {
		return true
	}
	rtype := (*rtype)(header.typ)
	for i := uintptr(0); i < rtype.size; i++ { //check byte by byte
		if *(*byte)(unsafe.Add(header.ptr, i)) != 0 {
			return false
		}
	}
	return true
}

var basicValues = [...]interface{}{
	nil,
	bool(false),
	int(0),
	int8(0),
	int16(0),
	int32(0),
	int64(0),
	uint(0),
	uint8(0),
	uint16(0),
	uint32(0),
	uint64(0),
	uintptr(0),
	float32(0),
	float64(0),
	complex64(0),
	complex128(0),
	nil,
	nil,
	nil,
	nil,
	nil,
	nil,
	nil,
	string(""),
	nil,
	unsafe.Pointer(nil),
}
