package reflected

func IntOf(i any) int64 {
	switch v := i.(type) {
	case int:
		return int64(v)
	case int8:
		return int64(v)
	case int16:
		return int64(v)
	case int32:
		return int64(v)
	case int64:
		return v
	default:
		panic("not an int")
	}
}

func UintOf(i any) uint64 {
	switch v := i.(type) {
	case uint:
		return uint64(v)
	case uint8:
		return uint64(v)
	case uint16:
		return uint64(v)
	case uint32:
		return uint64(v)
	case uint64:
		return v
	default:
		panic("not a uint")
	}
}

func FloatOf(f any) float64 {
	switch v := f.(type) {
	case float32:
		return float64(v)
	case float64:
		return v
	default:
		panic("not a float")
	}
}

func ComplexOf(c any) complex128 {
	switch v := c.(type) {
	case complex64:
		return complex128(v)
	case complex128:
		return v
	default:
		panic("not a complex")
	}
}
