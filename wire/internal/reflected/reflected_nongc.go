//go:build !gc

//Package reflected provides faster value access for reflection.
package reflected

import (
	"reflect"
	"unsafe"
)

type any = interface{}

type Value struct {
	kind reflect.Kind

	val [9]reflect.Value
}

func (v Value) Kind() reflect.Kind {
	return v.kind
}

func (v Value) NumField() int {
	return v.val[reflect.Struct-reflect.Array].NumField()
}

func (v Value) FieldName(i int) string {
	return v.val[reflect.Struct-reflect.Array].Type().Field(i).Name
}

func (v Value) Field(i int) any {
	return basicValueOf(v.val[reflect.Struct-reflect.Array].Field(i))
}

func (v Value) Deref() any {
	return basicValueOf(v.val[reflect.Ptr-reflect.Array].Elem())
}

func (v Value) IsNil() bool {
	return v.val[reflect.Ptr-reflect.Array].IsNil()
}

func ValueOf(v any) (val Value) {
	rvalue := v.(reflect.Value)
	val.kind = rvalue.Kind()
	if val.kind < reflect.Array || val.kind > reflect.Struct {
		val.val[reflect.String-reflect.Array] = rvalue
	} else {
		val.val[val.kind-reflect.Array] = rvalue
	}
	return val
}

//Value returns the basic value of v
//as an interface. If the value is
//a not a basic type, Get returns
//one of the exported types from
//this package.
func BasicValueOf(v any) any {
	if rvalue, ok := v.(reflect.Value); ok {
		return basicValueOf(rvalue)
	}
	return basicValueOf(reflect.ValueOf(v))
}

func basicValueOf(v reflect.Value) any {
	switch v.Kind() {
	case reflect.Bool:
		return v.Bool()
	case reflect.Int:
		return int(v.Int())
	case reflect.Int8:
		return int8(v.Int())
	case reflect.Int16:
		return int16(v.Int())
	case reflect.Int32:
		return int32(v.Int())
	case reflect.Int64:
		return int64(v.Int())
	case reflect.Uint:
		return uint(v.Uint())
	case reflect.Uint8:
		return uint8(v.Uint())
	case reflect.Uint16:
		return uint16(v.Uint())
	case reflect.Uint32:
		return uint32(v.Uint())
	case reflect.Uint64:
		return uint64(v.Uint())
	case reflect.Uintptr:
		return uintptr(v.Uint())
	case reflect.Float32:
		return float32(v.Float())
	case reflect.Float64:
		return float64(v.Float())
	case reflect.Complex64:
		return complex64(v.Complex())
	case reflect.Complex128:
		return complex128(v.Complex())
	case reflect.Array:
		return v
	case reflect.Chan:
		return v
	case reflect.Func:
		return v
	case reflect.Interface:
		return v
	case reflect.Map:
		return v
	case reflect.Ptr:
		return v
	case reflect.Slice:
		return v
	case reflect.String:
		return string(v.String())
	case reflect.Struct:
		return v
	case reflect.UnsafePointer:
		return unsafe.Pointer(v.Pointer())
	default:
		return nil
	}
}
