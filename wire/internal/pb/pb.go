package pb

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"google.golang.org/protobuf/encoding/protowire"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protodesc"
	"google.golang.org/protobuf/reflect/protoreflect"
	"google.golang.org/protobuf/reflect/protoregistry"
	"google.golang.org/protobuf/types/descriptorpb"
	"google.golang.org/protobuf/types/dynamicpb"
)

/*

	HOW TO OPTIMISE THIS CODE

	Since we are using reflection to convert between Go and proto types, this may be slow.
	This can be made faster by precompiling various information needed for the conversion
	for each type. See the jit package for more information about how to go about this.

	This will give a certain level of speedup, but the best speedup will be found in
	writing the proto binary format directly without relying on the google.golang.org/protobuf
	module. If this were to be done, this package would likely outperform Google's implementation.
*/

func index(msg *dynamicpb.Message, desc protoreflect.FieldDescriptor) protoreflect.Value {
	if desc.IsList() {
		return msg.Mutable(desc)
	}
	return msg.Get(desc)
}

var descriptionComplex64 = descriptorpb.DescriptorProto{
	Name: proto.String("complex64"),
	Field: []*descriptorpb.FieldDescriptorProto{
		{
			Name:   proto.String("real"),
			Type:   descriptorpb.FieldDescriptorProto_TYPE_FLOAT.Enum(),
			Number: proto.Int32(int32(1)),
		},
		{
			Name:   proto.String("imag"),
			Type:   descriptorpb.FieldDescriptorProto_TYPE_FLOAT.Enum(),
			Number: proto.Int32(int32(2)),
		},
	},
}

var descriptionComplex128 = descriptorpb.DescriptorProto{
	Name: proto.String("complex128"),
	Field: []*descriptorpb.FieldDescriptorProto{
		{
			Name:   proto.String("real"),
			Type:   descriptorpb.FieldDescriptorProto_TYPE_DOUBLE.Enum(),
			Number: proto.Int32(int32(1)),
		},
		{
			Name:   proto.String("imag"),
			Type:   descriptorpb.FieldDescriptorProto_TYPE_DOUBLE.Enum(),
			Number: proto.Int32(int32(2)),
		},
	},
}

func messageNameOf(field string, rtype reflect.Type) string {
	if rtype.Kind() == reflect.Map {
		switch rtype.Key().Kind() {
		case reflect.Int, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint32, reflect.Uint64, reflect.Uintptr, reflect.String:
			return field + "Entry"
		default:
			return strings.NewReplacer("[", "_", "]", "_", ".", "_").Replace(rtype.String())
		}
	}

	return strings.NewReplacer("[", "_", "]", "_", ".", "_").Replace(rtype.String())
}

type registration struct {
	index   int
	numbers []protowire.Number
}

// Environment is the proto environment that a
// given set of messages sits in. Think of it as
// a virtual proto file where all of your messages
// sit.
type Environment struct {
	sync.Mutex

	done  int
	files *protoregistry.Files

	registered   map[reflect.Type]registration
	messageTypes []*descriptorpb.DescriptorProto
}

// protoFieldOf maps a Go type to a proto field.
// types allow adding new named types to the proto environment.
func (env *Environment) protoFieldOf(name string, rtype reflect.Type, description *descriptorpb.DescriptorProto) (*descriptorpb.FieldDescriptorProto, error) {
	var field descriptorpb.FieldDescriptorProto

	for rtype.Kind() == reflect.Ptr {
		rtype = rtype.Elem()
	}

	switch rtype.Kind() {
	case reflect.Bool:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_BOOL.Enum()

	case reflect.Int8, reflect.Int16, reflect.Int32:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_INT32.Enum()

	case reflect.Int64, reflect.Int:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_INT64.Enum()

	case reflect.Uint8, reflect.Uint16, reflect.Uint32:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_UINT32.Enum()

	case reflect.Uint64, reflect.Uint, reflect.Uintptr:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_UINT64.Enum()

	case reflect.Float32:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_FLOAT.Enum()

	case reflect.Float64:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_DOUBLE.Enum()

	case reflect.String:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_STRING.Enum()

	//Complex types are defined in the builtinFile.
	case reflect.Complex64:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_MESSAGE.Enum()
		field.TypeName = proto.String("complex64")
		if err := env.register(name, reflect.TypeOf(complex64(0))); err != nil {
			return nil, fmt.Errorf("failed to register proto type: %w", err)
		}

	case reflect.Complex128:
		field.Type = descriptorpb.FieldDescriptorProto_TYPE_MESSAGE.Enum()
		field.TypeName = proto.String("complex128")
		if err := env.register(name, reflect.TypeOf(complex128(0))); err != nil {
			return nil, fmt.Errorf("failed to register proto type: %w", err)
		}

	//Add repeated label for arrays and slices.
	case reflect.Array, reflect.Slice:
		if rtype == reflect.TypeOf([]byte{}) {
			field.Type = descriptorpb.FieldDescriptorProto_TYPE_BYTES.Enum()
			break
		}

		elem, err := env.protoFieldOf(name, rtype.Elem(), description)
		if err != nil {
			return nil, fmt.Errorf("failed to determine proto field for %v: %w", rtype.Elem(), err)
		}
		elem.Label = descriptorpb.FieldDescriptorProto_LABEL_REPEATED.Enum()
		return elem, nil

	case reflect.Struct:

		field.Type = descriptorpb.FieldDescriptorProto_TYPE_MESSAGE.Enum()
		field.TypeName = proto.String(messageNameOf(name, rtype))
		if err := env.register(name, rtype); err != nil {
			return nil, fmt.Errorf("failed to register proto type %T: %w", rtype, err)
		}

	case reflect.Pointer:
		return env.protoFieldOf(name, rtype.Elem(), description)

	case reflect.Map:

		field.Type = descriptorpb.FieldDescriptorProto_TYPE_MESSAGE.Enum()
		field.TypeName = proto.String(messageNameOf(name, rtype))
		field.Label = descriptorpb.FieldDescriptorProto_LABEL_REPEATED.Enum()

		desc, _, err := env.descriptionOf(name, rtype)
		if err != nil {
			return nil, fmt.Errorf("failed to get description of %T: %w", rtype, err)
		}

		if desc.Options == nil {
			if err := env.register(name, rtype); err != nil {
				return nil, fmt.Errorf("failed to register proto type %T: %w", rtype, err)
			}
		} else {
			description.NestedType = append(description.NestedType, desc)
		}

	default:

		return nil, fmt.Errorf("cannot convert Go type %v to proto type", rtype)
	}

	return &field, nil
}

func (env *Environment) descriptionOf(name string, rtype reflect.Type) (*descriptorpb.DescriptorProto, []protowire.Number, error) {

	var description descriptorpb.DescriptorProto
	var numbers []protowire.Number
	var fields []*descriptorpb.FieldDescriptorProto

	switch rtype.Kind() {

	case reflect.Complex64:
		return &descriptionComplex64, nil, nil

	case reflect.Complex128:
		return &descriptionComplex128, nil, nil

	//For basic types, we construct a trivial message with a single field with the number 1.
	//The message name is the name of the Go type, the field is 'value'.
	case reflect.Bool, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
		reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
		reflect.Uintptr, reflect.Float32, reflect.Float64:

		pfield, err := env.protoFieldOf(name, rtype, &description)
		if err != nil {
			return nil, nil, err
		}

		pfield.Name = proto.String("value")
		pfield.Number = proto.Int32(int32(1))

		fields = append(fields, pfield)

	case reflect.Map:

		//For maps, we construct a trivial message with a key and value field
		//(proto map syntax is just a sugar for this very structure).
		//ie see descriptorpb.MessageOptions.MapEntry

		keyField, err := env.protoFieldOf(name, rtype.Key(), &description)
		if err != nil {
			return nil, nil, err
		}

		keyField.Name = proto.String("key")
		keyField.Number = proto.Int32(int32(1))

		mapField, err := env.protoFieldOf(name, rtype.Elem(), &description)
		if err != nil {
			return nil, nil, err
		}

		mapField.Name = proto.String("value")
		mapField.Number = proto.Int32(int32(2))

		description.Name = proto.String(messageNameOf(name, rtype))
		description.Field = []*descriptorpb.FieldDescriptorProto{
			keyField,
			mapField,
		}

		//proto only supports certain types as 'MapKey' fields.
		//we can just use a repeated field to represent other kinds
		//of Go maps.
		switch *keyField.Type {

		//case boolType, int32Type, int64Type, uint32Type, uint64Type, stringType:
		case descriptorpb.FieldDescriptorProto_TYPE_BOOL,
			descriptorpb.FieldDescriptorProto_TYPE_INT32,
			descriptorpb.FieldDescriptorProto_TYPE_INT64,
			descriptorpb.FieldDescriptorProto_TYPE_UINT32,
			descriptorpb.FieldDescriptorProto_TYPE_UINT64,
			descriptorpb.FieldDescriptorProto_TYPE_STRING:

			description.Options = &descriptorpb.MessageOptions{
				MapEntry: proto.Bool(true),
			}
		}

		return &description, []protowire.Number{1, 2}, nil

	case reflect.Pointer:
		return env.descriptionOf(name, rtype.Elem())

	//Structs can easily map onto proto messages.
	case reflect.Struct:

		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)

			tag, ok := field.Tag.Lookup("pb")
			if !ok {
				return nil, nil, errors.New("all struct fields must have a pb tag")
			}

			number, err := strconv.Atoi(tag)
			if err != nil {
				return nil, nil, errors.New("all struct fields must have numerical pb tags")
			}

			numbers = append(numbers, protowire.Number(number))

			pfield, err := env.protoFieldOf(field.Name, field.Type, &description)
			if err != nil {
				return nil, nil, fmt.Errorf("failed to determine proto field for %v: %w", field.Type, err)
			}

			pfield.Name = proto.String(field.Name)
			pfield.Number = proto.Int32(int32(number))

			fields = append(fields, pfield)
		}

	default:
		return nil, nil, fmt.Errorf("cannot describe Go type %v as proto type", rtype)

	}

	description.Name = proto.String(messageNameOf(name, rtype))
	description.Field = fields
	return &description, numbers, nil
}

func (env *Environment) register(name string, rtype reflect.Type) error {
	for rtype.Kind() == reflect.Ptr {
		rtype = rtype.Elem()
	}

	if _, ok := env.registered[rtype]; ok {
		return nil
	}

	desc, numbers, err := env.descriptionOf(name, rtype)
	if err != nil {
		return fmt.Errorf("failed to determine proto description for %v: %w", rtype, err)
	}

	if env.registered == nil {
		env.registered = make(map[reflect.Type]registration)
	}

	env.registered[rtype] = registration{
		index:   len(env.messageTypes),
		numbers: numbers,
	}
	env.messageTypes = append(env.messageTypes, desc)

	return nil
}

// Register registers the type of the provided interface.
// Future calls to MessageOf with values of this type will be faster.
func (env *Environment) Register(i interface{}) error {
	env.Lock()
	defer env.Unlock()

	return env.register("", reflect.TypeOf(i))
}

func (env *Environment) get(name string, rtype reflect.Type) (protoreflect.MessageDescriptor, []protowire.Number, error) {
	reg := env.registered[rtype]

	if env.files == nil || env.done != len(env.messageTypes) {
		files, err := protodesc.NewFiles(&descriptorpb.FileDescriptorSet{
			File: []*descriptorpb.FileDescriptorProto{
				{
					Package:     proto.String("pb"),
					Name:        proto.String("pb.proto"),
					Syntax:      proto.String("proto3"),
					MessageType: env.messageTypes,
				},
			},
		})
		if err != nil {
			return nil, nil, fmt.Errorf("failed to create virtual proto files: %w", err)
		}

		env.files = files
		env.done = len(env.messageTypes)
	}

	desc, err := env.files.FindDescriptorByName(protoreflect.FullName("pb." + messageNameOf(name, rtype)))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to find descriptor for %v: %w", protoreflect.FullName("pb."+messageNameOf(name, rtype)), err)
	}

	return desc.(protoreflect.MessageDescriptor), reg.numbers, nil
}

// MessageOf returns a proto message from the given argument.
func (env *Environment) MessageOf(i interface{}) (proto.Message, error) {
	env.Lock()
	defer env.Unlock()

	//It may already be a proto-generated type.
	if msg, ok := i.(proto.Message); ok {
		return msg, nil
	}

	rtype := reflect.TypeOf(i)
	rvalue := reflect.ValueOf(i)

	for rtype.Kind() == reflect.Ptr {
		rtype = rtype.Elem()
		rvalue = rvalue.Elem()
	}

	if err := env.register("", rtype); err != nil {
		return nil, fmt.Errorf("failed to register proto type: %w", err)
	}

	description, numbers, err := env.get("", rtype)
	if err != nil {
		return nil, fmt.Errorf("failed to get proto type for %v: %w", rtype, err)
	}

	msg := dynamicpb.NewMessage(description)

	for i := 0; i < rtype.NumField(); i++ {
		desc := description.Fields().ByNumber(numbers[i])

		value, err := env.protoValueOf(rtype.Name()+"."+rtype.Field(i).Name, msg, desc, rvalue.Field(i))
		if err != nil {
			return nil, fmt.Errorf("failed to convert %v value to proto value: %w", rvalue.Field(i).Type(), err)
		}

		msg.Set(desc, value)
	}

	return msg, nil
}

// goValueOf maps a protoreflect.Value to a Go value.
func (env *Environment) goValueOf(name string, hint reflect.Type, val protoreflect.Value) (reflect.Value, error) {

	var rvalue = reflect.New(hint).Elem()

	switch kind := hint.Kind(); kind {
	case reflect.Ptr:
		elem, err := env.goValueOf(name, hint.Elem(), val)
		if err != nil {
			return rvalue, err
		}
		rvalue := reflect.New(hint.Elem())
		rvalue.Elem().Set(elem)
		return rvalue, nil

	case reflect.Bool:
		rvalue.SetBool(val.Bool())
		return rvalue, nil

	case reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int, reflect.Int64:
		rvalue.SetInt(val.Int())
		return rvalue, nil

	case reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint, reflect.Uint64, reflect.Uintptr:
		rvalue.SetUint(val.Uint())
		return rvalue, nil

	case reflect.Float32, reflect.Float64:
		rvalue.SetFloat(val.Float())
		return rvalue, nil

	case reflect.String:
		rvalue.SetString(val.String())
		return rvalue, nil

	case reflect.Complex64, reflect.Complex128:
		desc, err := env.files.FindDescriptorByName(protoreflect.FullName("pb." + kind.String()))
		if err != nil {
			return rvalue, fmt.Errorf("failed to find descriptor %v: %w", kind.String(), err)
		}
		fields := desc.(protoreflect.MessageDescriptor).Fields()

		msg := val.Message()
		rvalue.SetComplex(complex(msg.Get(fields.ByNumber(1)).Float(), msg.Get(fields.ByNumber(2)).Float()))
		return rvalue, nil

	case reflect.Slice:

		if rvalue.Type() == reflect.TypeOf([]byte{}) {
			rvalue.SetBytes(val.Bytes())
			return rvalue, nil
		}

		var list = val.List()

		var slice = reflect.MakeSlice(hint, list.Len(), list.Len())

		for i := 0; i < list.Len(); i++ {
			val, err := env.goValueOf(name, hint.Elem(), list.Get(i))
			if err != nil {
				return slice, fmt.Errorf("failed to get goValueOf %v: %w", hint.Elem(), err)
			}
			slice.Index(i).Set(val)
		}

		return slice, nil

	case reflect.Array:

		var list = val.List()

		var array = reflect.New(hint).Elem()

		for i := 0; i < array.Len(); i++ {
			val, err := env.goValueOf(name, hint.Elem(), list.Get(i))
			if err != nil {
				return array, fmt.Errorf("failed to get goValueOf %v: %w", hint, err)
			}
			array.Index(i).Set(val)
		}

		return array, nil

	case reflect.Struct:

		desc, numbers, err := env.get(name, hint)
		if err != nil {
			return rvalue, fmt.Errorf("failed to get proto type for %v: %w", hint, err)
		}

		fields := desc.Fields()

		msg := val.Message()

		for i := 0; i < hint.NumField(); i++ {

			val, err := env.goValueOf(hint.Field(i).Name, hint.Field(i).Type, msg.Get(fields.ByNumber(numbers[i])))
			if err != nil {
				return reflect.Value{}, fmt.Errorf("failed to get goValueOf %v: %w", hint.Field(i).Type, err)
			}
			rvalue.Field(i).Set(val)
		}

		return rvalue, nil

	case reflect.Map:

		var rmap = reflect.MakeMap(hint)

		switch hint.Key().Kind() {
		case reflect.Int, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint32, reflect.Uint64, reflect.Uintptr, reflect.String:
			break
		default:
			var list = val.List()

			desc, _, err := env.get(name, hint)
			if err != nil {
				return rvalue, fmt.Errorf("failed to get proto type for %v: %w", hint, err)
			}

			fields := desc.Fields()

			for i := 0; i < list.Len(); i++ {
				elem := list.Get(i).Message()

				key, err := env.goValueOf(name, hint.Key(), elem.Get(fields.ByNumber(1)))
				if err != nil {
					return rmap, err
				}
				value, err := env.goValueOf(name, hint.Elem(), elem.Get(fields.ByNumber(2)))
				if err != nil {
					return rmap, err
				}

				rmap.SetMapIndex(key, value)
			}

			return rmap, nil
		}

		var mapping = val.Map()

		var rangeErr error

		mapping.Range(func(mk protoreflect.MapKey, v protoreflect.Value) bool {
			key, err := env.goValueOf(name, hint.Key(), mk.Value())
			if err != nil {
				rangeErr = err
				return false
			}
			value, err := env.goValueOf(name, hint.Elem(), v)
			if err != nil {
				rangeErr = err
				return false
			}
			rmap.SetMapIndex(key, value)
			return true
		})

		return rmap, rangeErr

	default:
		return reflect.Value{}, fmt.Errorf("cannot convert proto value %v to Go type %v", val, hint)
	}
}

// protoValueOf maps a Go value to a protoreflect.Value.
func (env *Environment) protoValueOf(name string, msg *dynamicpb.Message, desc protoreflect.FieldDescriptor, rvalue reflect.Value) (protoreflect.Value, error) {
	switch kind := rvalue.Type().Kind(); kind {
	case reflect.Bool:
		return protoreflect.ValueOfBool(rvalue.Bool()), nil

	case reflect.Int8, reflect.Int16, reflect.Int32:
		return protoreflect.ValueOfInt32(int32(rvalue.Int())), nil
	case reflect.Int, reflect.Int64:
		return protoreflect.ValueOfInt64(rvalue.Int()), nil

	case reflect.Uint8, reflect.Uint16, reflect.Uint32:
		return protoreflect.ValueOfUint32(uint32(rvalue.Uint())), nil
	case reflect.Uint, reflect.Uint64:
		return protoreflect.ValueOfUint64(rvalue.Uint()), nil

	case reflect.Float32:
		return protoreflect.ValueOfFloat32(float32(rvalue.Float())), nil
	case reflect.Float64:
		return protoreflect.ValueOfFloat64(rvalue.Float()), nil

	case reflect.String:
		return protoreflect.ValueOfString(rvalue.String()), nil

	case reflect.Complex64:

		msg := msg.NewField(desc).Message()

		cplxDesc, err := env.files.FindDescriptorByName(protoreflect.FullName("pb." + kind.String()))
		if err != nil {
			return protoreflect.ValueOfMessage(msg), err
		}

		msg.Set(cplxDesc.(protoreflect.MessageDescriptor).Fields().ByNumber(1), protoreflect.ValueOfFloat32(float32(real(rvalue.Complex()))))
		msg.Set(cplxDesc.(protoreflect.MessageDescriptor).Fields().ByNumber(2), protoreflect.ValueOfFloat32(float32((imag(rvalue.Complex())))))

		return protoreflect.ValueOfMessage(msg), nil

	case reflect.Complex128:

		msg := msg.NewField(desc).Message()

		cplxDesc, err := env.files.FindDescriptorByName(protoreflect.FullName("pb." + kind.String()))
		if err != nil {
			return protoreflect.ValueOfMessage(msg), err
		}

		msg.Set(cplxDesc.(protoreflect.MessageDescriptor).Fields().ByNumber(1), protoreflect.ValueOfFloat64(real(rvalue.Complex())))
		msg.Set(cplxDesc.(protoreflect.MessageDescriptor).Fields().ByNumber(2), protoreflect.ValueOfFloat64(imag(rvalue.Complex())))

		return protoreflect.ValueOfMessage(msg), nil

	case reflect.Struct:
		messageDesc, _, err := env.get(name, rvalue.Type())
		if err != nil {
			return protoreflect.ValueOfMessage(msg), err
		}

		message := dynamicpb.NewMessage(messageDesc)

		messageDesc, numbers, err := env.get(name, rvalue.Type())
		if err != nil {
			return protoreflect.ValueOfMessage(msg), err
		}

		fields := messageDesc.Fields()

		for i := 0; i < rvalue.NumField(); i++ {

			value, err := env.protoValueOf(rvalue.Type().Field(i).Name, msg, desc, rvalue.Field(i))
			if err != nil {
				return protoreflect.ValueOfMessage(msg), err
			}

			message.Set(fields.ByNumber(numbers[i]), value)
		}

		return protoreflect.ValueOfMessage(message), nil

	case reflect.Map:

		//Does map have a unsupported proto key?
		if desc.IsList() {
			list := msg.NewField(desc).List()

			iter := rvalue.MapRange()
			for iter.Next() {

				messageDesc, _, err := env.get(name, rvalue.Type())
				if err != nil {
					return protoreflect.ValueOfMessage(msg), err
				}

				msg := dynamicpb.NewMessage(messageDesc)

				fields := messageDesc.Fields()

				key, err := env.protoValueOf(name, msg, fields.ByNumber(1), iter.Key())
				if err != nil {
					return protoreflect.ValueOfList(list), err
				}

				val, err := env.protoValueOf(name, msg, fields.ByNumber(2), iter.Value())
				if err != nil {
					return protoreflect.ValueOfList(list), err
				}

				msg.Set(fields.ByNumber(1), key)
				msg.Set(fields.ByNumber(2), val)

				list.Append(protoreflect.ValueOfMessage(msg))
			}

			return protoreflect.ValueOfList(list), nil
		}

		mapping := msg.NewField(desc).Map()

		iter := rvalue.MapRange()
		for iter.Next() {
			key, err := env.protoValueOf(name, msg, desc, iter.Key())
			if err != nil {
				return protoreflect.ValueOfMap(mapping), err
			}

			val, err := env.protoValueOf(name, msg, desc, iter.Value())
			if err != nil {
				return protoreflect.ValueOfMap(mapping), err
			}

			//TODO handle types that proto doesn't "support" as map keys.
			//this can be done by falling back to slice handling.
			mapping.Set(key.MapKey(), val)
		}

		return protoreflect.ValueOfMap(mapping), nil

	case reflect.Slice, reflect.Array:

		if rvalue.Type() == reflect.TypeOf([]byte{}) {
			return protoreflect.ValueOfBytes(rvalue.Bytes()), nil
		}

		list := msg.NewField(desc).List()

		for i := 0; i < rvalue.Len(); i++ {
			val, err := env.protoValueOf(name, msg, desc, rvalue.Index(i))
			if err != nil {
				return protoreflect.ValueOfList(list), err
			}

			list.Append(val)
		}

		return protoreflect.ValueOfList(list), nil

	default:
		return protoreflect.Value{}, fmt.Errorf("cannot use Go type %v as proto value", rvalue.Type())
	}
}

// Recorder allows a proto decoding to be recorded
// and then played back.
type Recorder struct {
	env   *Environment
	files *protoregistry.Files

	Message proto.Message
	dynamic *dynamicpb.Message

	description protoreflect.MessageDescriptor
	numbers     []protowire.Number

	rtype  reflect.Type
	rvalue reflect.Value
}

func (env *Environment) NewRecorder(i interface{}) (*Recorder, error) {
	env.Lock()
	defer env.Unlock()

	//It may already be a proto-generated type.
	//In which case, we don't need to do anything.
	if msg, ok := (i.(proto.Message)); ok {
		return &Recorder{Message: msg}, nil
	}

	rtype := reflect.TypeOf(i)
	rvalue := reflect.ValueOf(i)

	if rtype.Kind() != reflect.Ptr {
		return nil, fmt.Errorf("must be pointer")
	}

	rtype = rtype.Elem()
	rvalue = rvalue.Elem()

	if err := env.register("", rtype); err != nil {
		return nil, err
	}

	description, numbers, err := env.get("", rtype)
	if err != nil {
		return nil, err
	}

	msg := dynamicpb.NewMessage(description)

	return &Recorder{env, env.files, msg, msg, description, numbers, rtype, rvalue}, nil
}

// Close closes the recorder, writing back the value to the value passed to NewRecorder.
func (r Recorder) Close() error {
	if r.dynamic == nil {
		//a concrete proto value was provided.
		return nil
	}

	for i := 0; i < r.rtype.NumField(); i++ {

		field := r.description.Fields().ByNumber(r.numbers[i])

		if r.dynamic.Has(field) {

			val, err := r.env.goValueOf(r.rtype.Field(i).Name, r.rtype.Field(i).Type, index(r.dynamic, field))
			if err != nil {
				return fmt.Errorf("failed to get goValueOf %v: %w", r.rtype.Field(i).Type, err)
			}

			r.rvalue.Field(i).Set(val)
		}
	}

	return nil
}
