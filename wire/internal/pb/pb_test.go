package pb_test

import (
	"fmt"
)

type SPID int32

func (s *SPID) String() string {
	return "SPID: " + fmt.Sprint(int32(*s))
}

type Nested struct {
	Value string `pb:"1"`
}

type HistogramRequest struct {
	SPID SPID `pb:"1"`

	Columns []string `pb:"2"`

	Complex complex64 `pb:"3"`

	Nested Nested `pb:"4"`

	Mapping map[string]string `pb:"5"`

	SpecialMapping map[Nested]string `pb:"6"`
}

type HistogramResponse struct {
	Data int32 `pb:"1"`
}
