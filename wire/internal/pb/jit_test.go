package pb_test

import (
	"fmt"
	"reflect"
	"testing"

	"pkg.iqhive.com/iq/wire/internal/pb"

	pubsubpb "google.golang.org/genproto/googleapis/pubsub/v1"

	"google.golang.org/protobuf/proto"
)

// Unmarshal parses the Protocol Buffer encoded data and stores the result in the value pointed to by v.
// Struct fields must contain a pb tag with the wire number.
func safeUnmarshal(v interface{}, data []byte) error {
	rec, err := env.NewRecorder(v)
	if err != nil {
		return err
	}

	if err := proto.Unmarshal(data, rec.Message); err != nil {
		return err
	}

	if err := rec.Close(); err != nil {
		return err
	}

	return nil
}

var env pb.Environment

func testcase(t *testing.T, val interface{}) {
	t.Helper()

	b, err := pb.Marshal(reflect.ValueOf(val).Elem().Interface())
	if err != nil {
		t.Fatalf("error marshalling %v", err)
	}

	//fmt.Println(b)

	var copy = reflect.New(reflect.TypeOf(val).Elem())

	if err := safeUnmarshal(copy.Interface(), b); err != nil {

		fmt.Println(b)
		fmt.Println("should be:")
		msg, _ := env.MessageOf(val)
		fmt.Println(proto.Marshal(msg))

		t.Fatalf("error unmarshalling %v", err)
	}

	if !reflect.DeepEqual(reflect.ValueOf(val).Elem().Interface(), copy.Elem().Interface()) {
		fmt.Println(b)
		t.Fatalf("%v != %v", val, copy.Elem().Interface())
	}

	copy = reflect.New(reflect.TypeOf(val).Elem())

	if err := pb.Unmarshal(b, copy.Interface()); err != nil {

		fmt.Println(b)

		t.Fatalf("error unmarshalling %v", err)
	}

	if !reflect.DeepEqual(reflect.ValueOf(val).Elem().Interface(), copy.Elem().Interface()) {
		fmt.Println(b)

		t.Fatalf("%v != %v", val, copy.Elem().Interface())
	}
}

func TestJIT(t *testing.T) {

	type Message struct {
		Value string `pb:"1"`
	}
	testcase(t, &Message{"Hello World"})

	type MultipleMessage struct {
		ID   int64  `pb:"1"`
		Name string `pb:"2"`
	}
	testcase(t, &MultipleMessage{22, "Hello World"})

	type SliceMessage struct {
		IDs []int `pb:"1"`
	}
	testcase(t, &SliceMessage{[]int{1, 2, 3}})

	type ArrayMessage struct {
		IDs [3]int `pb:"1"`
	}
	testcase(t, &ArrayMessage{[3]int{1, 2, 3}})

	type NestedMessage struct {
		Nested MultipleMessage `pb:"1"`
	}
	testcase(t, &NestedMessage{MultipleMessage{22, "Hello World"}})

	type NestedPointerMessage struct {
		IDs *MultipleMessage `pb:"1"`
	}
	testcase(t, &NestedPointerMessage{&MultipleMessage{22, "Hello World"}})

	type MappedMessage struct {
		Map map[string]int `pb:"1"`
	}
	testcase(t, &MappedMessage{Map: map[string]int{"hello": 22}})

	type AcknowledgeRequest struct {
		Subscription string   `pb:"1"`
		AckIds       []string `pb:"2"`
	}
	testcase(t, &AcknowledgeRequest{
		Subscription: "hello world",
		AckIds:       []string{"Hello World"},
	})

	type GetUsersRequest struct {
		Ids []int64 `json:"ids" pb:"1"`
	}
	testcase(t, &GetUsersRequest{
		Ids: []int64{1, 2, 3, 4},
	})

	type User struct {
		ID   int64  `json:"id" pb:"1"`
		Name string `json:"name" pb:"2"`
	}
	type GetUsersResponse struct {
		Users []User `json:"users" pb:"1"`
	}
	testcase(t, &GetUsersResponse{
		Users: []User{
			{ID: 1, Name: "aaaaaa"},
			{ID: 2, Name: "bbbbbb"},
			{ID: 3, Name: "cccccc"},
		},
	})

	BenchmarkMessageMarshaled, _ = pb.Marshal(&BenchmarkMessage)
}

var BenchmarkMessage = pubsubpb.CreateSchemaRequest{
	Parent: "Hello",
	Schema: &pubsubpb.Schema{
		Name:       "hello",
		Type:       pubsubpb.Schema_AVRO,
		Definition: "string",
	},
	SchemaId: "hello",
}

var BenchmarkMessageMarshaled []byte
var BenchmarkMessageUnmarshaled pubsubpb.CreateSchemaRequest

func BenchmarkMarshalJIT(b *testing.B) {
	for i := 0; i < b.N; i++ {
		pb.Marshal(&BenchmarkMessage)
	}
}

func BenchmarkMarshalGoogle(b *testing.B) {
	for i := 0; i < b.N; i++ {
		proto.Marshal(&BenchmarkMessage)
	}
}

func BenchmarkUnmarshalJIT(b *testing.B) {
	BenchmarkMessageUnmarshaled = pubsubpb.CreateSchemaRequest{}
	for i := 0; i < b.N; i++ {
		pb.Unmarshal(BenchmarkMessageMarshaled, &BenchmarkMessageUnmarshaled)
	}
	if BenchmarkMessageUnmarshaled.Schema == nil {
		b.Fatal("unmarshalled schema is nil")
	}
	if BenchmarkMessageUnmarshaled.Schema.Type != pubsubpb.Schema_AVRO {
		b.Fatal("wrong type")
	}
}

func BenchmarkUnmarshalGoogle(b *testing.B) {
	BenchmarkMessageUnmarshaled = pubsubpb.CreateSchemaRequest{}
	for i := 0; i < b.N; i++ {
		proto.Unmarshal(BenchmarkMessageMarshaled, &BenchmarkMessageUnmarshaled)
	}
}
