package pb_test

import (
	"testing"

	"pkg.iqhive.com/iq/wire/internal/pb"
)

func TestDecodeSimple(t *testing.T) {
	var simple = 22

	b, err := pb.Marshal(simple)
	if err != nil {
		t.Fatal(err)
	}

	var decoded int
	if err := pb.Unmarshal(b, &decoded); err != nil {
		t.Fatal(err)
	}

	if decoded != simple {
		t.Fatalf("decoded value does not match %v != %v", simple, decoded)
	}
}

func TestDecodeStruct(t *testing.T) {
	var simple struct {
		Message string `pb:"1"`
	}
	var decoded = simple

	simple.Message = "Hello world"

	b, err := pb.Marshal(simple)
	if err != nil {
		t.Fatal(err)
	}

	if err := pb.Unmarshal(b, &decoded); err != nil {
		t.Fatal(err)
	}

	if decoded != simple {
		t.Fatalf("decoded value does not match %v != %v", simple, decoded)
	}
}
