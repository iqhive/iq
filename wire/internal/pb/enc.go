package pb

import (
	"errors"
	"fmt"
	"math"
	"reflect"
	"strconv"
	"unsafe"

	"google.golang.org/protobuf/encoding/protowire"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/reflect/protoreflect"
	"google.golang.org/protobuf/runtime/protoiface"
)

/*
	Inspired by https://github.com/goccy/go-json

	This encoder prebuilds a series of instructions for encoding
	a Go type to the protocol buffer format. Similar to a how a
	VM operates.

	After the instructions have been built for the type, subsequent
	encodings of the same type will be very fast. Likely beating
	Go protoc generated code.
*/

// fastProtoMessage implements the absolute bare minumum of proto.Message
// so that the marshal functions in this package can be used in the grpc package.
// (the google grpc module requires arguments of a function to implement proto.Message).
type fastProtoMessage struct {
	protoreflect.Message
	protoiface.MessageV1

	value interface{}
}

var _ proto.Message = &fastProtoMessage{}

func (f *fastProtoMessage) ProtoReflect() protoreflect.Message {
	return f
}

func (f *fastProtoMessage) Reset() {}

func (f *fastProtoMessage) ProtoMethods() *protoiface.Methods {
	return &protoiface.Methods{
		CheckInitialized: func(cii protoiface.CheckInitializedInput) (protoiface.CheckInitializedOutput, error) {
			return protoiface.CheckInitializedOutput{}, nil
		},
		Marshal: func(mi protoiface.MarshalInput) (protoiface.MarshalOutput, error) {
			buf, err := Marshal(f.value)
			if err != nil {
				return protoiface.MarshalOutput{}, err
			}

			return protoiface.MarshalOutput{Buf: buf}, nil
		},
		Unmarshal: func(mi protoiface.UnmarshalInput) (protoiface.UnmarshalOutput, error) {
			if err := Unmarshal(mi.Buf, f.value); err != nil {
				return protoiface.UnmarshalOutput{}, err
			}

			return protoiface.UnmarshalOutput{}, nil
		},
	}
}

// MessageOf returns a proto message from the given argument it implements the absolute bare minumum of
// proto.Message so that the marshal functions in this package can be used in the grpc package.
// (the google grpc module requires arguments of a function to implement proto.Message).
func MessageOf(i interface{}) (proto.Message, error) {
	return &fastProtoMessage{value: i}, nil
}

// encoders maps Go types to an unsafe pb.Encoder that will encode them.
var encoders = make(map[reflect.Type]Encoder)

// Marshal marshals the given Go value to the protocol buffer
// encoding of the value.
func Marshal(v interface{}) ([]byte, error) {
	var encoder Encoder
	var err error
	var ok bool

	rtype := reflect.TypeOf(v)

	//the map is kind of slow, but what else can we do?
	if encoder, ok = encoders[rtype]; !ok {
		encoder, err = NewEncoder(rtype)
		if err != nil {
			return nil, err
		}

		//Is it worth optimising this when two types have the same encoder?
		//We can save memory this way.
		encoders[rtype] = encoder
	}

	//fmt.Println(encoder)

	return encoder.Encode(nil, v), nil
}

// TODO remove this when unsafe.Add is added to the standard library.
func unsafeAdd(ptr unsafe.Pointer, offset uint32) unsafe.Pointer {
	return unsafe.Pointer(uintptr(ptr) + uintptr(offset))
}

// compile returns an encoder (a slice of instructions) for the given type.
// the result of this function is unsafe to persist outside of private memory
// and should always be created at runtime.
func (e Encoder) compile(number uint32, rtype reflect.Type, offset uint32) (encoder Encoder, err error) {
	var instruction Instruction
	var tag protowire.Type

	switch rtype.Kind() {
	case reflect.Struct:
		var encoder = Encoder{
			Instruction{
				opcode: opTag,
				flag:   uint8(protowire.BytesType),
				offset: uint32(number),
			},
			Instruction{
				opcode: opStruct,
				offset: offset,
			},
		}

		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)

			if !field.IsExported() || field.Type.Size() == 0 {
				continue
			}

			ftag, ok := field.Tag.Lookup("pb")
			if !ok {

				//We can use the protoc tag if it exists.
				ptag, ok := field.Tag.Lookup("protobuf")
				if !ok {
					return nil, errors.New("all struct fields must have a pb tag")
				}

				_, ftag, _ = splitIntoThree(ptag, ",")
			}

			number, err := strconv.Atoi(ftag)
			if err != nil {
				return nil, errors.New("all struct fields must have numerical pb tags")
			}

			instructions, err := e.compile(uint32(number), field.Type, uint32(field.Offset))
			if err != nil {
				return nil, fmt.Errorf("struct field %v failed: %w", field.Name, err)
			}

			encoder = append(encoder, instructions...)
		}

		encoder = append(encoder, Instruction{
			opcode: opRecalcStruct,
		})

		return encoder, nil

	case reflect.Map:
		key := rtype.Key()
		elem := rtype.Elem()

		var instructions []Instruction

		keyInstructions, err := e.compile(1, key, 0)
		if err != nil {
			return nil, err
		}
		instructions = append(instructions, Instruction{opcode: opMapKey})
		instructions = append(instructions, keyInstructions...)

		elemInstructions, err := e.compile(2, elem, 0)
		if err != nil {
			return nil, err
		}

		instructions = append(instructions, Instruction{opcode: opMapValue})
		instructions = append(instructions, elemInstructions...)

		return append(Encoder{
			{opcode: opTag, flag: uint8(protowire.BytesType), offset: uint32(number)},
			{opcode: opMap, offset: offset, count: uint16(len(instructions)), value: rtype},
		}, instructions...), nil

	case reflect.Slice, reflect.Array:
		elem := rtype.Elem()

		instructions, err := e.compile(number, elem, 0)
		if err != nil {
			return nil, err
		}

		instructions = append(instructions, Instruction{
			opcode: opAddressMove,
			offset: uint32(elem.Size()),
		})

		var size uint16 = 0

		var op = opSlice
		if rtype.Kind() == reflect.Array {
			op = opArray
			size = uint16(rtype.Len())
		}

		var encoder Encoder

		switch elem.Kind() {
		case reflect.Bool, reflect.Int8, reflect.Uint8, reflect.Int16, reflect.Uint16,
			reflect.Int32, reflect.Uint32, reflect.Int64, reflect.Uint64, reflect.Int, reflect.Uint,
			reflect.Uintptr:
			op += offsetPacked
			tag = protowire.BytesType
			instructions = instructions[1:]
			encoder = append(encoder, Instruction{opcode: opTag, flag: uint8(tag), offset: uint32(number)})
		}

		encoder = append(encoder, Instruction{opcode: op, offset: offset, count: uint16(len(instructions)), flag: uint8(size)})
		encoder = append(encoder, instructions...)

		return encoder, nil

	case reflect.Ptr:
		elem := rtype.Elem()

		instructions, err := e.compile(number, elem, 0)
		if err != nil {
			return nil, err
		}

		return append(Encoder{
			{opcode: opPointer, offset: offset, count: uint16(len(instructions))},
		}, instructions...), nil

	case reflect.String:
		tag = protowire.BytesType
		instruction.opcode = opString

	case reflect.Bool, reflect.Uint8, reflect.Int8, reflect.Int, reflect.Uint,
		reflect.Uintptr, reflect.Int16, reflect.Uint16, reflect.Int32, reflect.Uint32,
		reflect.Int64, reflect.Uint64:

		tag = protowire.VarintType
		switch s := rtype.Size(); s {
		case 1, 2, 4, 8:
			instruction.opcode = opVarint
			instruction.flag = uint8(rtype.Size() * 8)
		default:
			return nil, fmt.Errorf("unsupported %v size %v", rtype, unsafe.Sizeof(uintptr(0)))
		}

	case reflect.Complex64:
		tag = protowire.BytesType
		instruction.opcode = opComplex64
	case reflect.Complex128:
		tag = protowire.BytesType
		instruction.opcode = opComplex128

	case reflect.Float32:
		tag = protowire.Fixed32Type
		instruction.opcode = opFixed32
	case reflect.Float64:
		tag = protowire.Fixed64Type
		instruction.opcode = opFixed64

	default:
		switch rtype {
		case reflect.TypeOf([]byte{}):
			tag = protowire.BytesType
			instruction.opcode = opBytes
		default:
			return nil, fmt.Errorf("unsupported type: %v", rtype)
		}
	}

	instruction.offset = offset

	return Encoder{Instruction{opcode: opTag, flag: uint8(tag), offset: uint32(number)}, instruction}, nil
}

func NewEncoder(rtype reflect.Type) (Encoder, error) {
	var encoder Encoder

	if rtype.Kind() == reflect.Ptr {
		rtype = rtype.Elem()
	}

	encoder, err := encoder.compile(1, rtype, 0)
	if err != nil {
		return nil, err
	}

	return encoder, nil
}

type Encoder []Instruction

// malloc returns the size of the protocol buffer for message. Enables us to allocate once when marshalling a message.
func (encoder Encoder) malloc(amount int, addr unsafe.Pointer, mapState *reflect.MapIter) (int, uint32, unsafe.Pointer) {
	instructions := encoder

	var old = amount

	for pc := uint32(0); pc < uint32(len(encoder)); pc++ {
		instruction := encoder[pc]

		switch op := instruction.opcode; op {
		case opStruct:
			//somehow encode the length *sigh* but we only know the length if we
			//encode the structure first.
			var add uint32
			amount, add, _ = encoder[pc+1:].malloc(amount, unsafeAdd(addr, instruction.offset), mapState)
			pc += add

		case opMapValue:
			amount++
			addr = addrOf(mapState.Value())

		case opMapKey:
			if !mapState.Next() {
				return amount, pc, addr
			}
			amount++
			addr = addrOf(mapState.Key())

		case opRecalcStruct:

			amount += protowire.SizeVarint(uint64(amount - old))

			return amount, pc, addr

		case opTag:
			amount++

		case opString:
			l := len(*(*string)(unsafeAdd(addr, instruction.offset)))
			amount += protowire.SizeVarint(uint64(l))
			amount += l

		case opBytes:
			amount += len(*(*[]byte)(unsafeAdd(addr, instruction.offset)))

		case opPointer:
			ptr := *(*unsafe.Pointer)(unsafeAdd(addr, instruction.offset))
			if ptr != nil {
				amount, _, _ = Encoder(instructions[pc+1:pc+uint32(instruction.count)+1]).
					malloc(amount, ptr, mapState)
			}
			pc += uint32(instruction.count)
			continue

		case opSlicePacked:
			val := (*reflect.SliceHeader)(unsafeAdd(addr, instruction.offset))

			amount += protowire.SizeVarint(uint64(val.Len))
			fallthrough

		case opSlice:
			val := (*reflect.SliceHeader)(unsafeAdd(addr, instruction.offset))

			var offset = unsafe.Pointer(val.Data)
			var loop = instructions[pc+1 : pc+uint32(instruction.count)+1]
			for i := 0; i < val.Len; i++ {
				if i == int(instruction.flag)-1 {
					loop = loop[:len(loop)-1]
				}
				amount, _, offset = Encoder(loop).
					malloc(amount, offset, mapState)
			}

			pc += uint32(instruction.count)
			continue

		case opArrayPacked:
			amount += protowire.SizeVarint(uint64(instruction.flag))
			fallthrough

		case opArray:

			var offset = unsafeAdd(addr, instruction.offset)
			var loop = instructions[pc+1 : pc+uint32(instruction.count)+1]
			for i := 0; i < int(instruction.flag); i++ {
				if i == int(instruction.flag)-1 {
					loop = loop[:len(loop)-1]
				}
				amount, _, offset = Encoder(loop).
					malloc(amount, offset, mapState)
			}
			pc += uint32(instruction.count)
			continue

		case opAddressMove:
			addr = unsafeAdd(addr, instruction.offset)

		case opMap:
			val := reflect.NewAt(instruction.value.(reflect.Type), unsafeAdd(addr, instruction.offset)).Elem()

			for i := 0; i < val.Len(); i++ {
				old := amount
				amount, _, _ = Encoder(instructions[pc+1:pc+uint32(instruction.count)+1]).
					malloc(amount, nil, val.MapRange())

				length := amount - old
				amount += protowire.SizeVarint(uint64(length))
			}
			pc += uint32(instruction.count)
			continue

		case opVarint:
			switch instruction.flag {
			case 8:
				amount += protowire.SizeVarint(uint64(*(*uint8)(unsafeAdd(addr, instruction.offset))))
			case 16:
				amount += protowire.SizeVarint(uint64(*(*uint16)(unsafeAdd(addr, instruction.offset))))
			case 32:
				amount += protowire.SizeVarint(uint64(*(*uint32)(unsafeAdd(addr, instruction.offset))))
			case 64:
				amount += protowire.SizeVarint(uint64(*(*uint64)(unsafeAdd(addr, instruction.offset))))
			}

		case opFixed32:
			amount += 4

		case opFixed64:
			amount += 8

		case opComplex64:
			amount += 8

		case opComplex128:
			amount += 16
		}
	}

	return amount, uint32(len(encoder)), addr
}

func (encoder Encoder) encode(b []byte,
	addr unsafe.Pointer,
	structState int,
	mapState *reflect.MapIter,
) ([]byte, uint32, unsafe.Pointer) {
	instructions := encoder

	//tag stores the length of the last tag made.
	var tag int

	for pc := uint32(0); pc < uint32(len(instructions)); pc++ {

		instruction := instructions[pc]

		switch op := instruction.opcode; op {

		case opTag:
			b4 := len(b)
			b = protowire.AppendTag(b, protowire.Number(instruction.offset), protowire.Type(instruction.flag))
			tag = len(b) - b4

		case opStruct:
			//somehow encode the length *sigh* but we only know the length if we
			//encode the structure first.
			var add uint32
			b, add, _ = encoder[pc+1:].encode(b, unsafeAdd(addr, instruction.offset), len(b), mapState)
			pc += add + 1

		case opMapValue:
			addr = addrOf(mapState.Value())

		case opMapKey:
			if !mapState.Next() {
				return b, pc, addr
			}

			addr = addrOf(mapState.Key())

			if structState == 0 {
				pc++
				continue
			}

		case opRecalcStruct:
			//This can probably be optimised.
			if structState != -1 {

				//calculate the length of the current structure
				//and insert it at the beginning.
				length := len(b) - structState
				size := protowire.SizeVarint(uint64(length))
				b = append(b, make([]byte, size)...)
				copy(b[structState+size:], b[structState:])
				protowire.AppendVarint(b[:structState], uint64(length))
			}
			return b, pc, addr

		case opString:
			if instruction.opcode != opString {
				panic("invalid opcode")
			}
			b = protowire.AppendString(b, *(*string)(unsafeAdd(addr, instruction.offset)))

		case opBytes:
			b = protowire.AppendBytes(b, *(*[]byte)(unsafeAdd(addr, instruction.offset)))

		case opPointer:
			ptr := *(*unsafe.Pointer)(unsafeAdd(addr, instruction.offset))
			if ptr != nil {
				b, _, _ = Encoder(instructions[pc+1:pc+uint32(instruction.count)+1]).
					encode(b, ptr, structState, mapState)
			}
			pc += uint32(instruction.count)
			continue

		case opSlicePacked:
			val := (*reflect.SliceHeader)(unsafeAdd(addr, instruction.offset))

			b = protowire.AppendVarint(b, uint64(val.Len))
			fallthrough

		case opSlice:
			val := (*reflect.SliceHeader)(unsafeAdd(addr, instruction.offset))

			var offset = unsafe.Pointer(val.Data)
			var loop = instructions[pc+1 : pc+uint32(instruction.count)+1]
			for i := 0; i < val.Len; i++ {
				if i == val.Len-1 {
					loop = loop[:len(loop)-1]
				}
				b, _, offset = Encoder(loop).
					encode(b, offset, structState, mapState)
			}

			pc += uint32(instruction.count)
			continue

		case opArrayPacked:
			b = protowire.AppendVarint(b, uint64(instruction.flag))
			fallthrough

		case opArray:

			var offset = unsafeAdd(addr, instruction.offset)
			var loop = instructions[pc+1 : pc+uint32(instruction.count)+1]
			for i := 0; i < int(instruction.flag); i++ {
				if i == int(instruction.flag)-1 {
					loop = loop[:len(loop)-1]
				}
				b, _, offset = Encoder(loop).
					encode(b, offset, structState, mapState)
			}
			pc += uint32(instruction.count)
			continue

		case opAddressMove:
			addr = unsafeAdd(addr, instruction.offset)

		case opMap:
			val := reflect.NewAt(instruction.value.(reflect.Type), unsafeAdd(addr, instruction.offset)).Elem()

			for i := 0; i < val.Len(); i++ {
				structState := len(b)
				b, _, _ = Encoder(instructions[pc+1:pc+uint32(instruction.count)+1]).
					encode(b, nil, structState, val.MapRange())

				length := len(b) - structState
				size := protowire.SizeVarint(uint64(length))
				b = append(b, make([]byte, size)...)
				copy(b[structState+size:], b[structState:])
				protowire.AppendVarint(b[:structState], uint64(length))
			}

			//untag this value.
			if val.Len() == 0 {
				b = b[:len(b)-tag]
			}

			pc += uint32(instruction.count)
			continue

		case opVarint:
			switch instruction.flag {
			case 8:
				b = protowire.AppendVarint(b, uint64(*(*uint8)(unsafeAdd(addr, instruction.offset))))
			case 16:
				b = protowire.AppendVarint(b, uint64(*(*uint16)(unsafeAdd(addr, instruction.offset))))
			case 32:
				b = protowire.AppendVarint(b, uint64(*(*uint32)(unsafeAdd(addr, instruction.offset))))
			case 64:
				b = protowire.AppendVarint(b, uint64(*(*uint64)(unsafeAdd(addr, instruction.offset))))
			}

		case opFixed32:
			b = protowire.AppendFixed32(b, *(*uint32)(unsafeAdd(addr, instruction.offset)))

		case opFixed64:
			b = protowire.AppendFixed64(b, *(*uint64)(unsafeAdd(addr, instruction.offset)))

		case opComplex64:
			number := *(*complex64)(unsafeAdd(addr, instruction.offset))

			b = protowire.AppendVarint(b, 1+4+1+4) //size in bytes
			b = protowire.AppendTag(b, protowire.Number(1), protowire.Fixed32Type)
			b = protowire.AppendFixed32(b, math.Float32bits(real(number)))
			b = protowire.AppendTag(b, protowire.Number(2), protowire.Fixed32Type)
			b = protowire.AppendFixed32(b, math.Float32bits(imag(number)))

		case opComplex128:
			number := *(*complex128)(unsafeAdd(addr, instruction.offset))

			b = protowire.AppendVarint(b, 1+8+1+8) //size in bytes
			b = protowire.AppendTag(b, protowire.Number(1), protowire.Fixed64Type)
			b = protowire.AppendFixed64(b, math.Float64bits(real(number)))
			b = protowire.AppendTag(b, protowire.Number(2), protowire.Fixed64Type)
			b = protowire.AppendFixed64(b, math.Float64bits(imag(number)))

		default:
			panic("unsupported opcode: " + fmt.Sprint(instruction.opcode))
		}
	}

	return b, uint32(len(instructions)), addr
}

// TODO investigate feasability of using interface header.
// this would safe the overhead of a copy+heap allocation.
func addrOf(val reflect.Value) unsafe.Pointer {
	if val.Kind() == reflect.Ptr {
		return unsafe.Pointer(val.Pointer())
	}
	if val.CanAddr() {
		return unsafe.Pointer(val.UnsafeAddr())
	}
	copy := reflect.New(val.Type()).Elem()
	copy.Set(val)
	return unsafe.Pointer(copy.UnsafeAddr())
}

func (e Encoder) Encode(b []byte, val interface{}) []byte {

	//fmt.Println(e)

	if e[1].opcode == opStruct {
		e = e[2 : len(e)-1]
	}

	addr := addrOf(reflect.ValueOf(val))

	size, _, _ := e.malloc(0, addr, nil)

	//fmt.Println("guess", size)

	if cap(b) < size {
		b = make([]byte, 0, size)
	}

	b, _, _ = e.encode(b, addr, -1, nil)

	//fmt.Println("is ", len(b))

	return b
}

// Marshal marshals the given Go value to the protocol buffer
// encoding of the value.
func SizeOf(v interface{}) int {
	var encoder Encoder
	var ok bool

	rtype := reflect.TypeOf(v)

	//the map is kind of slow, but what else can we do?
	if encoder, ok = encoders[rtype]; !ok {
		encoder, _ = NewEncoder(rtype)

		//Is it worth optimising this when two types have the same encoder?
		//We can save memory this way.
		encoders[rtype] = encoder
	}

	addr := addrOf(reflect.ValueOf(v))

	size, _, _ := encoder.malloc(0, addr, nil)

	return size
}
