package pb

import (
	"fmt"
	"reflect"

	"google.golang.org/protobuf/encoding/protowire"
)

//opcode defines what an instruction will do.
type opcode uint8

//Add offsetPacked to opEncodeSlice or opEncodeArray to pack the data.
const (
	offsetPacked = 2
)

const (
	//func opNext()
	//	advance to the next operation.
	opNext opcode = iota

	//func opSkip(offset uint32)
	//	advances the program counter 'offset'
	//  within the instruction space.
	opSkip

	//func opTag(flag uint8, offset uint32)
	//  encodes a protocol buffer type tag
	//  where flag is the protowire.Type and offset is the protowire.Number
	opTag

	//func opVarint(flag uint8, offset uint32)
	//  encodes the 'flag'-bit sized value at memory+'offset as a varint.
	opVarint

	//func opString(offset uint32)
	//  encodes a Go string at memory+'offset'
	opString

	//func opBytes(offset uint32)
	//  encodes a Go []byte at memory+'offset'
	opBytes

	//func opFixed32(offset uint32)
	//  encodes the 32bit value at memory+'offset'
	opFixed32

	//func opFixed64(offset uint32)
	//  encodes the 64bit value at memory+'offset'
	opFixed64

	//func opComplex64(offset uint32)
	//  encodes a Go complex64 value at memory+'offset'
	opComplex64

	//func opComplex128(offset uint32)
	//  encodes a Go complex128 value at memory+'offset'
	opComplex128

	//func opPointer(offset uint32, count uint16)
	//  dereferences the pointer at memory+'offset'
	//  and encodes the next 'count' instructions at
	//  the pointed memory location.
	opPointer

	//func opSlice(offset uint32, count uint16)
	//  dereferences the Go slice at memory+'offset'
	//  and encodes the next 'count' instructions at
	//  the slices memory location. These intructions
	//  are looped for every element in the slice.
	opSlice

	//func opArray(offset uint32, count uint16, flag uint8)
	//  encodes the array at memory+'offset' by repeating
	//  next 'count' instructions for the array size 'flag'.
	opArray

	opSlicePacked //same as opEncodeSlice but packs the data.
	opArrayPacked //same as opEncodeArray but packs the data.

	//func opMap(count uint16, value reflect.Type)
	//  dereferences the Go map at memory+'offset' and encodes the
	//  next 'count' instructions for each element in the map.
	//  These instructions should contain opEncodeMapKey and opEncodeMapValue.
	opMap

	//func opMapKey(flag uint8)
	//  loads the current map's next key and encodes it as field 1
	//  with the given protowire.Type 'flag'.
	opMapKey

	//func opMapValue(flag uint8)
	//  loads the current map's next value and encodes it as field 2
	//  with the given protowire.Type 'flag'.
	opMapValue

	//func opStruct(offset uint32)
	//  marks the encoding of a nested message, which closes on
	//  the next matching opRecalcStruct.
	opStruct

	//func opRecalcStruct()
	//  marks the end of the most recent unended opEncodeStruct.
	opRecalcStruct

	//func opAddressMove(offset uint32)
	//	moves the address pointer to memory+'offset'
	//  useful at the end of an opEncodeSlice or opEncodeArray.
	//  (so that the memory offset increases for each element)
	opAddressMove
)

//Instruction is a single instruction in the encoding process.
//Each opcode will interpret the fields of the instruction
//differently so refer to the opcode's documentation.
type Instruction struct {
	opcode

	flag   byte
	count  uint16
	offset uint32
	value  interface{}
}

//String returns a human readable representation of an instruction.
func (i Instruction) String() string {

	protoTypeOf := func(t uint8) string {
		var name string = "Unknown"
		switch protowire.Type(t) {
		case protowire.BytesType:
			name = "Bytes"
		case protowire.Fixed32Type:
			name = "Fixed32"
		case protowire.Fixed64Type:
			name = "Fixed64"
		case protowire.VarintType:
			name = "Varint"
		}
		return name
	}

	switch i.opcode {
	case opNext:
		return fmt.Sprintf("Next()")

	case opSkip:
		return fmt.Sprintf("Skip(%v)", i.offset)

	case opTag:
		return fmt.Sprintf("Tag%v(%v)", protoTypeOf(i.flag), i.offset)

	case opString:
		return fmt.Sprintf("String(%v)", i.offset)
	case opBytes:
		return fmt.Sprintf("Bytes(%v)", i.offset)

	case opVarint:
		return fmt.Sprintf("Varint%v(%v)", i.flag, i.offset)

	case opFixed32:
		return fmt.Sprintf("Fixed32(%v)", i.offset)
	case opFixed64:
		return fmt.Sprintf("Fixed64(%v)", i.offset)

	case opComplex64:
		return fmt.Sprintf("opComplex64(%v)", i.offset)
	case opComplex128:
		return fmt.Sprintf("opComplex128(%v)", i.offset)

	case opSlice:
		return fmt.Sprintf("Slice(%v, %v)", i.offset, i.count)

	case opArray:
		return fmt.Sprintf("Array%v(%v, %v)", i.flag, i.offset, i.count)

	case opSlicePacked:
		return fmt.Sprintf("SlicePacked(%v, %v)", i.offset, i.count)
	case opArrayPacked:
		return fmt.Sprintf("ArrayPacked%v(%v, %v)", i.flag, i.offset, i.count)

	case opStruct:
		return fmt.Sprintf("Struct(%v, %v)", i.offset, i.count)
	case opRecalcStruct:
		return "RecalcStruct()"

	case opMap:
		rtype := i.value.(reflect.Type)
		return fmt.Sprintf("Map(%v, %v, %v)", i.offset, rtype, i.count)

	case opMapKey:
		return fmt.Sprintf("MapKey%v()", protoTypeOf(i.flag))

	case opMapValue:
		return fmt.Sprintf("MapValue%v()", protoTypeOf(i.flag))

	case opAddressMove:
		return fmt.Sprintf("AddressMove(%v)", i.offset)

	case opPointer:
		return fmt.Sprintf("Pointer(%v, %v)", i.offset, i.count)

	default:
		return "Unknown"
	}
}
