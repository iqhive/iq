package pb

import (
	"errors"
	"fmt"
	"io"
	"math"
	"reflect"
	"strconv"
	"strings"
	"unsafe"

	"google.golang.org/protobuf/encoding/protowire"
)

// splitIntoThree splits a string into three strings, based on
// the given separator.
func splitIntoThree(str string, separator string) (string, string, string) {
	switch splits := strings.SplitN(str, separator, 3); len(splits) {
	case 1:
		return splits[0], "", ""
	case 2:
		return splits[0], splits[1], ""
	default:
		return splits[0], splits[1], splits[2]
	}
}

// JumpTable for field numbers higher than 15.
type JumpTable map[uint32]uint32

// decoders maps Go types to an unsafe pb.Decoder that will decode them.
var decoders = make(map[reflect.Type]Decoder)

// Unmarshal unmarshals into the given Go value from the protocol buffer
// encoded data.
func Unmarshal(data []byte, value interface{}) error {
	var decoder Decoder
	var err error
	var ok bool

	rvalue := reflect.ValueOf(value)
	for rvalue.Kind() == reflect.Ptr {
		rvalue = rvalue.Elem()
	}
	rtype := rvalue.Type()

	//the map is kind of slow, but what else can we do?
	if decoder, ok = decoders[rtype]; !ok {
		decoder, err = NewDecoder(rtype)
		if err != nil {
			return err
		}

		//Is it worth optimising this when two types have the same encoder?
		//We can save memory this way.
		decoders[rtype] = decoder
	}

	return decoder.decodeMessage(data, 0, protowire.BytesType, addrOf(rvalue))
}

// Decoder is a virtual machine for decoding protocol buffers.
// Each instruction operates on a sub-slice of protocol buffer message.
type Decoder []Instruction

// NewDecoder contructs a protocol buffer Decoder for the given Go type.
func NewDecoder(rtype reflect.Type) (Decoder, error) {

	var dec Decoder

	for rtype.Kind() == reflect.Ptr {
		rtype = rtype.Elem()
	}

	//Add struct header if the type is not a struct.
	if rtype.Kind() != reflect.Struct {
		dec = Decoder{
			{
				opcode: opStruct,
				count:  1,
			},
		}
	}

	instructions, err := dec.compile(len(dec), rtype, 0)
	if err != nil {
		return nil, err
	}

	if rtype.Kind() == reflect.Struct {
		return instructions, nil
	}

	dec = append(dec, instructions...)

	return dec, nil
}

// compile compiles a decoder for the given Go type and returns it. If offset is provided
// then the instruction will operate on memory+offset.
func (dec *Decoder) compile(size int, rtype reflect.Type, offset uint32) (Decoder, error) {

	var dependencies Decoder
	var instruction Instruction

	switch rtype.Kind() {
	case reflect.Bool, reflect.Uint8, reflect.Int8, reflect.Int, reflect.Uint,
		reflect.Uintptr, reflect.Int16, reflect.Uint16, reflect.Int32, reflect.Uint32,
		reflect.Int64, reflect.Uint64:

		switch s := rtype.Size(); s {
		case 1, 2, 4, 8:
			instruction.opcode = opVarint
			instruction.flag = uint8(rtype.Size() * 8)
		default:
			return nil, fmt.Errorf("unsupported %v size %v", rtype, unsafe.Sizeof(uintptr(0)))
		}
	case reflect.String:
		instruction.opcode = opString

	case reflect.Float32:
		instruction.opcode = opFixed32

	case reflect.Float64:
		instruction.opcode = opFixed64

	case reflect.Complex64:
		instruction.opcode = opComplex64

	case reflect.Complex128:
		instruction.opcode = opComplex128

	case reflect.Slice:
		instruction.opcode = opSlice
		instruction.value = rtype

		instructions, err := dec.compile(size+len(dependencies), rtype.Elem(), 0)
		if err != nil {
			return nil, err
		}

		dependencies = append(dependencies, instructions...)

	case reflect.Map:
		instruction.opcode = opMap
		instruction.value = rtype

		keyInstructions, err := dec.compile(size+len(dependencies), rtype.Key(), 0)
		if err != nil {
			return nil, err
		}

		valInstructions, err := dec.compile(size+len(dependencies), rtype.Elem(), 0)
		if err != nil {
			return nil, err
		}

		//Optimise the map header just like a struct but we can make better assumptions.
		//Since we only expect two fields.
		switch {
		case len(keyInstructions) == 1:
			dependencies = append(dependencies, keyInstructions...)
			dependencies = append(dependencies, valInstructions...)
		case len(valInstructions) == 1:
			dependencies = append(dependencies, Instruction{
				opcode: opSkip,
				offset: uint32(len(valInstructions) + 1),
			})
			dependencies = append(dependencies, valInstructions...)
			dependencies = append(dependencies, keyInstructions...)
		default:
			dependencies = append(dependencies,
				Instruction{
					opcode: opSkip,
					offset: 2,
				},
				Instruction{
					opcode: opSkip,
					offset: uint32(len(keyInstructions) + 1),
				},
			)
			dependencies = append(dependencies, keyInstructions...)
			dependencies = append(dependencies, valInstructions...)
		}

	case reflect.Array:
		instruction.opcode = opArray
		instruction.value = rtype

		instructions, err := dec.compile(size+len(dependencies), rtype.Elem(), 0)
		if err != nil {
			return nil, err
		}

		dependencies = append(dependencies, instructions...)

	case reflect.Ptr:
		instruction.opcode = opPointer
		instruction.value = rtype.Elem()

		instructions, err := dec.compile(size+len(dependencies), rtype.Elem(), 0)
		if err != nil {
			return nil, err
		}

		dependencies = append(dependencies, instructions...)

	//Struct begins with a jumptable header that maps each proto field to
	//the offset and length containing the instructions needed to decode that
	//field, if there are any protowire numbers higher than 16, then a jumptable
	//is used to map those numbers to the instructions. The length of this header
	//is stored in the opStruct.
	case reflect.Struct:
		instruction.opcode = opStruct

		//protowire extracts the protowire number from the field's structtag.
		protowire := func(tag reflect.StructTag) (int, error) {
			ftag, ok := tag.Lookup("pb")
			if !ok {

				//We can use the protoc tag if it exists.
				ptag, ok := tag.Lookup("protobuf")
				if !ok {
					return 0, errors.New("all struct fields must have a pb tag")
				}

				_, ftag, _ = splitIntoThree(ptag, ",")
			}

			number, err := strconv.Atoi(ftag)
			if err != nil {
				return 0, errors.New("all struct fields must have numerical pb tags")
			}

			return number, nil
		}

		var used = make(map[int]bool)
		var highest = 0

		//First figure out how the header will be structured.
		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)

			if field.PkgPath != "" || field.Type.Size() == 0 {
				continue
			}

			n, err := protowire(field.Tag)
			if err != nil {
				return nil, err
			}

			used[n] = true
			if n > highest {
				highest = n
			}
		}

		var lowerThan16Count int
		for i := 0; i < 16; i++ {
			if used[i] {
				lowerThan16Count++
			}
		}

		switch {
		case highest <= 16:
			instruction.count = uint16(highest)
		case highest > 16 && lowerThan16Count/2 >= 8:
			instruction.count = uint16(16)
		}

		jumptable := make(JumpTable)

		if highest > 16 {
			instruction.value = jumptable
		}

		dependencies = make([]Instruction, instruction.count)

		//The header has been allocated so compile the instructions for each field.
		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)

			if field.PkgPath != "" || field.Type.Size() == 0 {
				continue
			}

			n, err := protowire(field.Tag)
			if err != nil {
				return nil, err
			}

			if n <= int(instruction.count) {
				dependencies[n-1].opcode = opSkip
				dependencies[n-1].offset = uint32(len(dependencies)) - uint32(instruction.count) + (uint32(instruction.count) - uint32(n)) + 1
			} else {
				jumptable[uint32(n)] = uint32(size + len(dependencies))
			}

			instructions, err := dec.compile(size+len(dependencies), field.Type, uint32(field.Offset))
			if err != nil {
				return nil, fmt.Errorf("struct field %v failed: %w", field.Name, err)
			}

			switch len(instructions) {
			case 1:
				//if there is only a single instruction, it can be stuffed
				//directly into the header.
				dependencies[n-1] = instructions[0]
			default:
				dependencies = append(dependencies, instructions...)
			}
		}

	default:
		switch rtype {
		case reflect.TypeOf([]byte{}):
			instruction.opcode = opBytes

		default:
			return nil, fmt.Errorf("unsupported type %v for protobuffer decoding", rtype)
		}
	}

	instruction.offset = offset

	return append(Decoder{instruction}, dependencies...), nil
}

func (dec Decoder) decodeMessage(b []byte, pc int, ptype protowire.Type, addr unsafe.Pointer) (err error) {
	var instruction = dec[pc]

	if instruction.opcode == opSkip {
		pc += int(instruction.offset)
		instruction = dec[pc]
	}

	if instruction.opcode == opNext {
		return nil
	}

	switch instruction.opcode {
	case opStruct:
		if ptype != protowire.BytesType {
			return errors.New("opStruct: cannot parse invalid wire-format data")
		}

		//parse length
		if pc != 0 {
			_, n := protowire.ConsumeVarint(b)
			b = b[n:]
		}

		for len(b) > 0 {
			var n, t, l = protowire.ConsumeTag(b)
			if l < 0 {
				switch err := protowire.ParseError(l); err {
				case io.ErrUnexpectedEOF:
					return nil
				default:
					return err
				}
			}
			b = b[l:]

			l = protowire.ConsumeFieldValue(n, t, b)
			if l < 0 {
				return protowire.ParseError(l)
			}

			val := b[:l]
			b = b[l:]

			var jump int

			//jump to the correct instruction.
			switch {
			case uint16(n) <= instruction.count:
				jump = pc + int(n)
			default:
				table, ok := instruction.value.(JumpTable)
				if !ok {
					continue
				}
				jump = pc + int(table[uint32(n)])
			}

			err = dec.decodeMessage(val, jump, t, unsafeAdd(addr, instruction.offset))
			if err != nil {
				return err
			}
		}

		return nil

	case opMap:
		if ptype != protowire.BytesType {
			return errors.New("opMap: cannot parse invalid wire-format data")
		}

		//skip length
		_, l := protowire.ConsumeVarint(b)
		b = b[l:]

		var T = instruction.value.(reflect.Type)

		var key = reflect.New(T.Key())
		var elem = reflect.New(T.Elem())

		for len(b) > 0 {
			var n, t, l = protowire.ConsumeTag(b)
			if l < 0 {
				return protowire.ParseError(l)
			}
			b = b[l:]

			l = protowire.ConsumeFieldValue(n, t, b)
			if l < 0 {
				return protowire.ParseError(l)
			}

			val := b[:l]
			b = b[l:]

			switch n {
			case 1:
				err = dec.decodeMessage(val, pc+1, t, unsafe.Pointer(key.Pointer()))
				if err != nil {
					return err
				}
			case 2:
				err = dec.decodeMessage(val, pc+2, t, unsafe.Pointer(elem.Pointer()))
				if err != nil {
					return err
				}
			}

		}

		rmap := reflect.NewAt(T, unsafeAdd(addr, instruction.offset))
		if rmap.Elem().IsNil() {
			rmap.Elem().Set(reflect.MakeMap(T))
		}

		rmap.Elem().SetMapIndex(key.Elem(), elem.Elem())

	case opSlice:
		t := instruction.value.(reflect.Type)

		ptr := reflect.NewAt(t, unsafeAdd(addr, instruction.offset))
		slice := ptr.Elem()

		//is it a packed field?
		switch t.Elem().Kind() {
		case reflect.Bool, reflect.Int8, reflect.Uint8, reflect.Int16, reflect.Uint16,
			reflect.Int32, reflect.Uint32, reflect.Int64, reflect.Uint64, reflect.Int, reflect.Uint,
			reflect.Uintptr:
			if ptype == protowire.BytesType {
				length, n := protowire.ConsumeVarint(b)
				b = b[n:]

				slice.Set(reflect.MakeSlice(t, int(length), int(length)))

				for i := uint64(0); i < length; i++ {
					_, n := protowire.ConsumeVarint(b)

					err = dec.decodeMessage(b, pc+1, protowire.VarintType, unsafe.Pointer(slice.Index(int(i)).UnsafeAddr()))
					if err != nil {
						return err
					}

					b = b[n:]
				}

				return nil
			}
		}

		slice.Set(reflect.Append(slice, reflect.Zero(t.Elem())))

		err = dec.decodeMessage(b, pc+1, ptype, unsafe.Pointer(slice.Index(slice.Len()-1).UnsafeAddr()))
		if err != nil {
			return err
		}

	case opArray:
		t := instruction.value.(reflect.Type)

		ptr := reflect.NewAt(t, unsafeAdd(addr, instruction.offset))
		array := ptr.Elem()

		//is it a packed field?
		switch t.Elem().Kind() {
		case reflect.Bool, reflect.Int8, reflect.Uint8, reflect.Int16, reflect.Uint16,
			reflect.Int32, reflect.Uint32, reflect.Int64, reflect.Uint64, reflect.Int, reflect.Uint,
			reflect.Uintptr:
			if ptype == protowire.BytesType {
				length, n := protowire.ConsumeVarint(b)
				b = b[n:]

				for i := uint64(0); i < length; i++ {
					_, n := protowire.ConsumeVarint(b)

					err = dec.decodeMessage(b, pc+1, protowire.VarintType, unsafe.Pointer(array.Index(int(i)).UnsafeAddr()))
					if err != nil {
						return err
					}

					b = b[n:]
				}

				return nil
			}
		}

		return errors.New("unpacked structured arrays are not supported")

	case opPointer:
		ptr := *(*unsafe.Pointer)(unsafeAdd(addr, instruction.offset))
		if ptr == nil { //allocate a new pointer if it is nil.
			ptr = unsafe.Pointer(reflect.New(instruction.value.(reflect.Type)).Pointer())
			*(*unsafe.Pointer)(unsafeAdd(addr, instruction.offset)) = ptr
		}
		return dec.decodeMessage(b, pc+1, ptype, ptr)

	case opVarint:
		if ptype != protowire.VarintType {
			return errors.New("opVarint: cannot parse invalid wire-format data")
		}

		value, n := protowire.ConsumeVarint(b)
		if n < 0 {
			return errors.New("opVarint: cannot parse invalid wire-format data")
		}

		switch instruction.flag {
		case 8:
			*(*uint8)(unsafeAdd(addr, instruction.offset)) = uint8(value)
		case 16:
			*(*uint16)(unsafeAdd(addr, instruction.offset)) = uint16(value)
		case 32:
			*(*uint32)(unsafeAdd(addr, instruction.offset)) = uint32(value)
		case 64:
			*(*uint64)(unsafeAdd(addr, instruction.offset)) = uint64(value)
		}
	case opString:
		if ptype != protowire.BytesType {
			return errors.New("opString: cannot parse invalid wire-format data")
		}

		value, n := protowire.ConsumeString(b)
		if n < 0 {
			return errors.New("opString: cannot parse invalid wire-format data")
		}

		*(*string)(unsafeAdd(addr, instruction.offset)) = value

	case opBytes:
		if ptype != protowire.BytesType {
			return errors.New("opBytes: cannot parse invalid wire-format data")
		}

		value, n := protowire.ConsumeBytes(b)
		if n < 0 {
			return errors.New("opBytes: cannot parse invalid wire-format data")
		}

		*(*[]byte)(unsafeAdd(addr, instruction.offset)) = value

	case opFixed32:
		if ptype != protowire.Fixed32Type {
			return errors.New("opFixed32: cannot parse invalid wire-format data")
		}

		value, n := protowire.ConsumeFixed32(b)
		if n < 0 {
			return errors.New("opFixed32: cannot parse invalid wire-format data")
		}

		*(*uint32)(unsafeAdd(addr, instruction.offset)) = value

	case opFixed64:
		value, n := protowire.ConsumeFixed64(b)
		if n < 0 {
			return errors.New("opFixed64: cannot parse invalid wire-format data")
		}

		*(*uint64)(unsafeAdd(addr, instruction.offset)) = value

	case opComplex64:
		if ptype != protowire.BytesType {
			return errors.New("opComplex64: cannot parse invalid wire-format data")
		}

		var real, imag float32

		msg, n := protowire.ConsumeBytes(b)
		if n < 0 {
			return errors.New("opComplex64: cannot parse invalid wire-format data")
		}

		for len(msg) > 0 {
			number, ptype, n := protowire.ConsumeTag(msg)
			if n == -1 {
				return errors.New("opComplex64: cannot parse invalid wire-format data")
			}
			msg = msg[n:]

			value, n := protowire.ConsumeFixed32(msg)
			if n < 0 {
				return errors.New("opComplex64: cannot parse invalid wire-format data")
			}
			msg = msg[n:]

			switch number {
			case 1:
				if ptype != protowire.Fixed32Type {
					return errors.New("opComplex64: cannot parse invalid wire-format data")
				}
				real = math.Float32frombits(value)
			case 2:
				if ptype != protowire.Fixed32Type {
					return errors.New("opComplex64: cannot parse invalid wire-format data")
				}
				imag = math.Float32frombits(value)
			}
		}

		*(*complex64)(unsafeAdd(addr, instruction.offset)) = complex(real, imag)

	case opComplex128:
		if ptype != protowire.BytesType {
			return errors.New("opComplex128: cannot parse invalid wire-format data")
		}

		var real, imag float64

		msg, n := protowire.ConsumeBytes(b)
		if n < 0 {
			return errors.New("opComplex128: cannot parse invalid wire-format data")
		}

		for len(msg) > 0 {
			number, ptype, n := protowire.ConsumeTag(msg)
			if n == -1 {
				return errors.New("opComplex128: cannot parse invalid wire-format data")
			}
			msg = msg[n:]

			if ptype != protowire.Fixed64Type {
				return errors.New("opComplex128: cannot parse invalid wire-format data")
			}

			value, n := protowire.ConsumeFixed64(msg)
			if n < 0 {
				return errors.New("opComplex128: cannot parse invalid wire-format data")
			}
			msg = msg[n:]

			switch number {
			case 1:
				if ptype != protowire.Fixed64Type {
					return errors.New("opComplex128: cannot parse invalid wire-format data")
				}
				real = math.Float64frombits(value)
			case 2:
				if ptype != protowire.Fixed64Type {
					return errors.New("opComplex128: cannot parse invalid wire-format data")
				}
				imag = math.Float64frombits(value)
			}
		}

		*(*complex128)(unsafeAdd(addr, instruction.offset)) = complex(real, imag)

	default:
		return fmt.Errorf("unknown opcode %d", instruction.opcode)
	}

	return nil
}
