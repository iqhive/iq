package xml_test

import (
	"fmt"
	"testing"

	go_xml "encoding/xml"

	"pkg.iqhive.com/iq/wire/internal/talk/xml"
	"pkg.iqhive.com/iq/wire/proto"
)

type Object struct {
	Name string
	Bool bool
	Num  int
	Dec  float64

	A         [2]int
	Something *int32
	B         []int

	Null *int64
}

type MapObject struct {
	Map map[string]string
}

func TestMap(t *testing.T) {
	var v MapObject
	v.Map = map[string]string{
		"foo": "bar",
	}
	b, err := xml.Marshal(&v)
	if err != nil {
		t.Fatal(err)
	}
	if string(b) != `<MapObject><Map><key>foo</key><val>bar</val></Map></MapObject>` {
		t.Fatalf("unexpected output: %s", b)
	}
}

var v Object

func TestEncoder(t *testing.T) {

	v.Name = "foo"
	v.Bool = true
	v.Num = 42
	v.Dec = 3.14159
	v.A = [2]int{1, 2}
	v.B = []int{1, 2, 3}
	v.Something = new(int32)
	v.Null = nil

	std_xml, err := go_xml.Marshal(v)
	fmt.Println(string(std_xml), err)

	b, err := xml.Marshal(&v)
	if err != nil {
		t.Fatal(err)
	}
	if string(b) != string(std_xml) {
		t.Fatalf("unexpected output: %s\nexpected %v", string(b), string(std_xml))
	}

}

func BenchmarkJitEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		xml.Marshal(&v)
	}
}

func BenchmarkGoEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		go_xml.Marshal(&v)
	}
}

func BenchmarkProtoEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		proto.Marshal(&v)
	}
}

func assertEncoding(t *testing.T, a interface{}, encoded string) {
	t.Helper()

	b, err := xml.Marshal(a)
	if err != nil {
		t.Fatal(err)
	}
	if string(b) != encoded {
		t.Fatalf("unexpected output: %s\nexpected %v", string(b), encoded)
	}
}

func TestNameTags(t *testing.T) {
	type NamedWithTag struct {
		_ xml.Name `xml:"foo"`

		Value string
	}
	assertEncoding(t, &NamedWithTag{Value: "bar"}, `<foo><Value>bar</Value></foo>`)

	type NamedWithXMLNameTag struct {
		XMLName go_xml.Name `xml:"foo"`

		Value string
	}
	assertEncoding(t, &NamedWithXMLNameTag{Value: "bar"}, `<foo><Value>bar</Value></foo>`)

	type NamedWithXMLNameValue struct {
		XMLName go_xml.Name

		Value string
	}
	assertEncoding(t, &NamedWithXMLNameValue{XMLName: go_xml.Name{Space: "", Local: "foo"}, Value: "bar"}, `<foo><Value>bar</Value></foo>`)

	type NamedByParentTag struct {
		Child struct {
			Value string
		} `xml:"foo"`
	}
	assertEncoding(t, &NamedByParentTag{Child: struct{ Value string }{"bar"}}, `<NamedByParentTag><foo><Value>bar</Value></foo></NamedByParentTag>`)

	type NamedByParentField struct {
		Child struct {
			Value string
		}
	}
	assertEncoding(t, &NamedByParentField{Child: struct{ Value string }{"bar"}}, `<NamedByParentField><Child><Value>bar</Value></Child></NamedByParentField>`)

	var NamedByType int = 22
	assertEncoding(t, &NamedByType, `<int>22</int>`)
}

func TestAttr(t *testing.T) {
	type WithStaticAttr struct {
		_ xml.Attr `key="value"`

		Value string
	}
	assertEncoding(t, &WithStaticAttr{Value: "bar"}, `<WithStaticAttr key="value"><Value>bar</Value></WithStaticAttr>`)

	type WithFieldAttr struct {
		Key string `xml:"key,attr"`

		Value string
	}
	assertEncoding(t, &WithFieldAttr{Key: "value", Value: "bar"}, `<WithFieldAttr key="value"><Value>bar</Value></WithFieldAttr>`)
}

func TestNesting(t *testing.T) {
	type Nested struct {
		Value string
	}
	type Nesting struct {
		Nested Nested
	}
	assertEncoding(t, &Nesting{Nested: Nested{Value: "bar"}}, `<Nesting><Nested><Value>bar</Value></Nested></Nesting>`)
}
