package xml

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"reflect"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"

	"pkg.iqhive.com/iq/api"
	"pkg.iqhive.com/iq/wire/internal/reflected"
)

//NOTE this file is mostly copied from encoding/json with minor modifications.

// tagOptions is the string following a comma in a struct field's "json"
// tag, or the empty string. It does not include the leading comma.
type tagOptions string

// parseTag splits a struct field's json tag into its name and
// comma-separated options.
func parseTag(tag string) (string, tagOptions) {
	if idx := strings.Index(tag, ","); idx != -1 {
		return tag[:idx], tagOptions(tag[idx+1:])
	}
	return tag, tagOptions("")
}

// Contains reports whether a comma-separated list of options
// contains a particular substr flag. substr must be surrounded by a
// string boundary or commas.
func (o tagOptions) Contains(optionName string) bool {
	if len(o) == 0 {
		return false
	}
	s := string(o)
	for s != "" {
		var next string
		i := strings.Index(s, ",")
		if i >= 0 {
			s, next = s[:i], s[i+1:]
		}
		if s == optionName {
			return true
		}
		s = next
	}
	return false
}

const (
	caseMask     = ^byte(0x20) // Mask to ignore case in ASCII.
	kelvin       = '\u212a'
	smallLongEss = '\u017f'
)

// foldFunc returns one of four different case folding equivalence
// functions, from most general (and slow) to fastest:
//
// 1) bytes.EqualFold, if the key s contains any non-ASCII UTF-8
// 2) equalFoldRight, if s contains special folding ASCII ('k', 'K', 's', 'S')
// 3) asciiEqualFold, no special, but includes non-letters (including _)
// 4) simpleLetterEqualFold, no specials, no non-letters.
//
// The letters S and K are special because they map to 3 runes, not just 2:
//   - S maps to s and to U+017F 'ſ' Latin small letter long s
//   - k maps to K and to U+212A 'K' Kelvin sign
//
// See https://play.golang.org/p/tTxjOc0OGo
//
// The returned function is specialized for matching against s and
// should only be given s. It's not curried for performance reasons.
func foldFunc(s []byte) func(s, t []byte) bool {
	nonLetter := false
	special := false // special letter
	for _, b := range s {
		if b >= utf8.RuneSelf {
			return bytes.EqualFold
		}
		upper := b & caseMask
		if upper < 'A' || upper > 'Z' {
			nonLetter = true
		} else if upper == 'K' || upper == 'S' {
			// See above for why these letters are special.
			special = true
		}
	}
	if special {
		return equalFoldRight
	}
	if nonLetter {
		return asciiEqualFold
	}
	return simpleLetterEqualFold
}

// equalFoldRight is a specialization of bytes.EqualFold when s is
// known to be all ASCII (including punctuation), but contains an 's',
// 'S', 'k', or 'K', requiring a Unicode fold on the bytes in t.
// See comments on foldFunc.
func equalFoldRight(s, t []byte) bool {
	for _, sb := range s {
		if len(t) == 0 {
			return false
		}
		tb := t[0]
		if tb < utf8.RuneSelf {
			if sb != tb {
				sbUpper := sb & caseMask
				if 'A' <= sbUpper && sbUpper <= 'Z' {
					if sbUpper != tb&caseMask {
						return false
					}
				} else {
					return false
				}
			}
			t = t[1:]
			continue
		}
		// sb is ASCII and t is not. t must be either kelvin
		// sign or long s; sb must be s, S, k, or K.
		tr, size := utf8.DecodeRune(t)
		switch sb {
		case 's', 'S':
			if tr != smallLongEss {
				return false
			}
		case 'k', 'K':
			if tr != kelvin {
				return false
			}
		default:
			return false
		}
		t = t[size:]

	}
	return len(t) <= 0
}

// asciiEqualFold is a specialization of bytes.EqualFold for use when
// s is all ASCII (but may contain non-letters) and contains no
// special-folding letters.
// See comments on foldFunc.
func asciiEqualFold(s, t []byte) bool {
	if len(s) != len(t) {
		return false
	}
	for i, sb := range s {
		tb := t[i]
		if sb == tb {
			continue
		}
		if ('a' <= sb && sb <= 'z') || ('A' <= sb && sb <= 'Z') {
			if sb&caseMask != tb&caseMask {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

// simpleLetterEqualFold is a specialization of bytes.EqualFold for
// use when s is all ASCII letters (no underscores, etc) and also
// doesn't contain 'k', 'K', 's', or 'S'.
// See comments on foldFunc.
func simpleLetterEqualFold(s, t []byte) bool {
	if len(s) != len(t) {
		return false
	}
	for i, b := range s {
		if b&caseMask != t[i]&caseMask {
			return false
		}
	}
	return true
}

type structFields struct {
	tag  string // tag name for this type.
	dtag reflected.FieldOffset

	fields     []field
	attributes []field

	nameIndex map[string]int
}

// A field represents a single field found in a struct.
type field struct {
	name    string
	literal string

	nameBytes []byte                 // []byte(name)
	equalFold func(s, t []byte) bool // bytes.EqualFold or equivalent

	nameNonEsc  string // `"` + name + `":`
	nameEscHTML string // `"` + HTMLEscape(name) + `":`

	marshaler bool

	tag       bool
	index     []int
	typ       reflect.Type
	omitEmpty bool

	offset reflected.FieldOffset
}

// byIndex sorts field by index sequence.
type byIndex []field

func (x byIndex) Len() int { return len(x) }

func (x byIndex) Swap(i, j int) { x[i], x[j] = x[j], x[i] }

func (x byIndex) Less(i, j int) bool {
	for k, xik := range x[i].index {
		if k >= len(x[j].index) {
			return false
		}
		if xik != x[j].index[k] {
			return xik < x[j].index[k]
		}
	}
	return len(x[i].index) < len(x[j].index)
}

func isValidTag(s string) bool {
	if s == "" {
		return false
	}
	for _, c := range s {
		switch {
		case strings.ContainsRune("!#$%&()*+-./:;<=>?@[]^_{|}~ ", c):
			// Backslash and quote chars are reserved, but
			// otherwise any punctuation chars are allowed
			// in a tag name.
		case !unicode.IsLetter(c) && !unicode.IsDigit(c):
			return false
		}
	}
	return true
}

// typeFields returns a list of fields that JSON should recognize for the given type.
// The algorithm is breadth-first search over the set of structs to include - the top struct
// and then any reachable anonymous structs.
func typeFields(t reflect.Type) (result structFields) {

	//parseNameTag xml package's XMLName tag format of the form
	//"name" or "namespace-URL name"
	parseNameTag := func(tag string) (element, namespace string) {
		if tag == "" {
			return
		}
		tag = strings.Split(tag, ",")[0]

		if strings.Contains(tag, " ") {
			parts := strings.SplitN(tag, " ", 2)
			element = parts[0]
			namespace = parts[1]
		} else {
			element = tag
		}
		return
	}

	//isAttribute returns true if the struct field is an
	//XML attribute.
	isAttribute := func(field reflect.StructField) bool {
		if field.Type == reflect.TypeOf(Attr{}) {
			return true
		}
		tag := api.Tags(field.Tag).Get("xml")
		return tag.Has("attr")
	}

	// Anonymous fields to explore at the current level and the next.
	current := []field{}
	next := []field{{typ: t}}

	// Count of queued names for current level and the next.
	var count, nextCount map[reflect.Type]int

	// Types already visited at an earlier level.
	visited := map[reflect.Type]bool{}

	// Buffer to run HTMLEscape on field names.
	var nameEscBuf bytes.Buffer

	for len(next) > 0 {
		current, next = next, current[:0]
		count, nextCount = nextCount, map[reflect.Type]int{}

		for _, f := range current {
			if visited[f.typ] {
				continue
			}
			visited[f.typ] = true

			// Scan f.typ for fields to include.
			for i := 0; i < f.typ.NumField(); i++ {
				sf := f.typ.Field(i)
				ft := sf.Type

				index := make([]int, len(f.index)+1)
				copy(index, f.index)
				index[len(f.index)] = i

				//XML name logic.
				if result.tag == "" && result.dtag == (reflected.FieldOffset{}) {
					if sf.Type == reflect.TypeOf(Name{}) {
						element, namespace := parseNameTag(sf.Tag.Get("xml"))
						if element != "" {
							result.tag = element
							if namespace != "" {
								result.tag += " xmlns=\"" + namespace + "\""
							}
						} else {
							result.dtag = reflected.FieldOffsetOf(t, append(index, 1))
						}
						continue
					}
				}

				// XML static attributes.
				if sf.Name == "_" && sf.Type == reflect.TypeOf(Attr{}) && len(sf.Tag) > 0 {
					result.attributes = append(result.attributes, field{
						literal: string(sf.Tag),
					})
					if count[f.typ] > 1 {
						// If there were multiple instances, add a second,
						// so that the annihilation code will see a duplicate.
						// It only cares about the distinction between 1 or 2,
						// so don't bother generating any more copies.
						result.attributes = append(result.attributes, result.attributes[len(result.attributes)-1])
					}
					continue
				}

				if sf.Anonymous {
					t := sf.Type
					if t.Kind() == reflect.Ptr {
						t = t.Elem()
					}
					if !sf.IsExported() && t.Kind() != reflect.Struct {
						// Ignore embedded fields of unexported non-struct types.
						continue
					}
					// Do not ignore embedded fields of unexported struct types
					// since they may have exported fields.
				} else if !sf.IsExported() {
					// Ignore unexported non-embedded fields.
					continue
				}
				tag := sf.Tag.Get("xml")
				if tag == "-" {
					continue
				}
				name, opts := parseTag(tag)
				if !isValidTag(name) {
					name = ""
				}

				if ft.Name() == "" && ft.Kind() == reflect.Ptr {
					// Follow pointer.
					ft = ft.Elem()
				}

				// Record found field and index sequence.
				if name != "" || !sf.Anonymous || ft.Kind() != reflect.Struct {
					tagged := name != ""

					if name == "" {
						name = sf.Name
					}

					field := field{
						name: name,

						tag:       tagged,
						index:     index,
						typ:       ft,
						marshaler: ft.Implements(reflect.TypeOf((*xml.Marshaler)(nil)).Elem()),
						omitEmpty: opts.Contains("omitempty"),
						offset:    reflected.FieldOffsetOf(f.typ, index),
					}

					field.nameBytes = []byte(field.name)
					field.equalFold = foldFunc(field.nameBytes)

					// Build nameEscHTML and nameNonEsc ahead of time.
					nameEscBuf.Reset()
					nameEscBuf.WriteString(`"`)
					json.HTMLEscape(&nameEscBuf, field.nameBytes)
					nameEscBuf.WriteString(`":`)
					field.nameEscHTML = nameEscBuf.String()
					field.nameNonEsc = `"` + field.name + `":`

					if isAttribute(sf) {
						result.attributes = append(result.attributes, field)
						if count[f.typ] > 1 {
							// If there were multiple instances, add a second,
							// so that the annihilation code will see a duplicate.
							// It only cares about the distinction between 1 or 2,
							// so don't bother generating any more copies.
							result.attributes = append(result.attributes, result.attributes[len(result.attributes)-1])
						}
					} else {
						result.fields = append(result.fields, field)
						if count[f.typ] > 1 {
							// If there were multiple instances, add a second,
							// so that the annihilation code will see a duplicate.
							// It only cares about the distinction between 1 or 2,
							// so don't bother generating any more copies.
							result.fields = append(result.fields, result.fields[len(result.fields)-1])
						}
					}

					continue
				}

				// Record new anonymous struct to explore in next round.
				nextCount[ft]++
				if nextCount[ft] == 1 {
					next = append(next, field{name: ft.Name(), index: index, typ: ft})
				}
			}
		}
	}

	sort.Slice(result.fields, func(i, j int) bool {
		x := result.fields
		// sort field by name, breaking ties with depth, then
		// breaking ties with "name came from json tag", then
		// breaking ties with index sequence.
		if x[i].name != x[j].name {
			return x[i].name < x[j].name
		}
		if len(x[i].index) != len(x[j].index) {
			return len(x[i].index) < len(x[j].index)
		}
		if x[i].tag != x[j].tag {
			return x[i].tag
		}
		return byIndex(x).Less(i, j)
	})
	sort.Slice(result.attributes, func(i, j int) bool {
		x := result.attributes
		// sort field by name, breaking ties with depth, then
		// breaking ties with "name came from json tag", then
		// breaking ties with index sequence.
		if x[i].name != x[j].name {
			return x[i].name < x[j].name
		}
		if len(x[i].index) != len(x[j].index) {
			return len(x[i].index) < len(x[j].index)
		}
		if x[i].tag != x[j].tag {
			return x[i].tag
		}
		return byIndex(x).Less(i, j)
	})

	// Delete all fields that are hidden by the Go rules for embedded fields,
	// except that fields with XML tags are promoted.

	// The fields are sorted in primary order of name, secondary order
	// of field index length. Loop over names; for each name, delete
	// hidden fields by choosing the one dominant field that survives.
	out := result.fields[:0]
	for advance, i := 0, 0; i < len(result.fields); i += advance {
		// One iteration per name.
		// Find the sequence of fields with the name of this first field.
		fi := result.fields[i]
		name := fi.name
		for advance = 1; i+advance < len(result.fields); advance++ {
			fj := result.fields[i+advance]
			if fj.name != name {
				break
			}
		}
		if advance == 1 { // Only one field with this name
			out = append(out, fi)
			continue
		}
		dominant, ok := dominantField(result.fields[i : i+advance])
		if ok {
			out = append(out, dominant)
		}
	}
	// The fields are sorted in primary order of name, secondary order
	// of field index length. Loop over names; for each name, delete
	// hidden fields by choosing the one dominant field that survives.
	out2 := result.attributes[:0]
	for advance, i := 0, 0; i < len(result.attributes); i += advance {
		// One iteration per name.
		// Find the sequence of fields with the name of this first field.
		fi := result.attributes[i]
		name := fi.name
		for advance = 1; i+advance < len(result.attributes); advance++ {
			fj := result.attributes[i+advance]
			if fj.name != name {
				break
			}
		}
		if advance == 1 { // Only one field with this name
			out2 = append(out2, fi)
			continue
		}
		dominant, ok := dominantField(result.attributes[i : i+advance])
		if ok {
			out2 = append(out2, dominant)
		}
	}

	result.fields = out
	result.attributes = out2
	sort.Sort(byIndex(result.fields))
	sort.Sort(byIndex(result.attributes))

	result.nameIndex = make(map[string]int, len(result.fields))
	for i, field := range result.fields {
		result.nameIndex[field.name] = i
	}

	return result
}

// dominantField looks through the fields, all of which are known to
// have the same name, to find the single field that dominates the
// others using Go's embedding rules, modified by the presence of
// JSON tags. If there are multiple top-level fields, the boolean
// will be false: This condition is an error in Go and we skip all
// the fields.
func dominantField(fields []field) (field, bool) {
	// The fields are sorted in increasing index-length order, then by presence of tag.
	// That means that the first field is the dominant one. We need only check
	// for error cases: two fields at top level, either both tagged or neither tagged.
	if len(fields) > 1 && len(fields[0].index) == len(fields[1].index) && fields[0].tag == fields[1].tag {
		return field{}, false
	}
	return fields[0], true
}
