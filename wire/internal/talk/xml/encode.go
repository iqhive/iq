// Package xml provides an efficient JSON marshaler with support for constant attributes.
package xml

import (
	"encoding"
	"encoding/json"
	"encoding/xml"
	"errors"
	"io"
	"reflect"
	"strconv"
	"sync"
	"unicode/utf8"

	"pkg.iqhive.com/iq/wire/internal/reflected"
)

type Name = xml.Name
type Attr = xml.Attr

type any = interface{}

// An encodeState encodes JSON into a bytes.Buffer.
type encodeState struct {
	buf []byte
}

var encodeStatePool sync.Pool

func newEncodeState() *encodeState {
	if v := encodeStatePool.Get(); v != nil {
		e := v.(*encodeState)
		e.buf = e.buf[:0:cap(e.buf)]
		return e
	}
	return &encodeState{}
}

type Encoder struct {
	w io.Writer
}

func NewEncoder(w io.Writer) *Encoder {
	return &Encoder{w}
}

func (e Encoder) Encode(v interface{}) error {
	if v == nil {
		return nil
	}

	if e.w == nil {
		return errors.New("xml: nil destination for serialized object")
	}

	b, err := Marshal(v)
	if err != nil {
		return err
	}

	_, err = e.w.Write(b)
	return err
}

/*
Marshal the given value to XML.
Faster than encoding/json.Marshal
however you may not pass recursive
values.

The name for the XML element is taken from, in order of preference:

  - the tag on an untitled field of type Name
  - the tag on the XMLName field, if the data is a struct
  - the value of the XMLName field of type xml.Name
  - the tag of the struct field used to obtain the data
  - the name of the struct field used to obtain the data
  - the name of the marshaled type
*/
func Marshal(v any) ([]byte, error) {
	var e = newEncodeState()
	if _, err := e.marshal(v, reflect.TypeOf(v).Name(), 0); err != nil {
		return nil, err
	}
	var b = make([]byte, len(e.buf))
	copy(b, e.buf)
	encodeStatePool.Put(e)
	return b, nil
}

var fieldCache = make(map[reflect.Type]structFields)
var fieldCacheMutex sync.RWMutex

type marshalFlags uint8

const (
	skipXMLMarshaler marshalFlags = 1 << iota
	skipTextMarshaler
	isAttribute
)

const hex = "0123456789abcdef"

// Decide whether the given rune is in the XML Character Range, per
// the Char production of https://www.xml.com/axml/testaxml.htm,
// Section 2.2 Characters.
// ported from encoding/xml
func isInCharacterRange(r rune) (inrange bool) {
	return r == 0x09 ||
		r == 0x0A ||
		r == 0x0D ||
		r >= 0x20 && r <= 0xD7FF ||
		r >= 0xE000 && r <= 0xFFFD ||
		r >= 0x10000 && r <= 0x10FFFF
}

// string is ported from encoding/xml
func (e *encodeState) string(s string) {
	const (
		escQuot = "&#34;" // shorter than "&quot;"
		escApos = "&#39;" // shorter than "&apos;"
		escAmp  = "&amp;"
		escLT   = "&lt;"
		escGT   = "&gt;"
		escTab  = "&#x9;"
		escNL   = "&#xA;"
		escCR   = "&#xD;"
		escFFFD = "\uFFFD" // Unicode replacement character
	)
	const escapeNewline = true

	var esc string
	last := 0
	for i := 0; i < len(s); {
		r, width := utf8.DecodeRuneInString(s[i:])
		i += width
		switch r {
		case '"':
			esc = escQuot
		case '\'':
			esc = escApos
		case '&':
			esc = escAmp
		case '<':
			esc = escLT
		case '>':
			esc = escGT
		case '\t':
			esc = escTab
		case '\n':
			if !escapeNewline {
				continue
			}
			esc = escNL
		case '\r':
			esc = escCR
		default:
			if !isInCharacterRange(r) || (r == 0xFFFD && width == 1) {
				esc = escFFFD
				break
			}
			continue
		}
		e.buf = append(e.buf, s[last:i-width]...)
		e.buf = append(e.buf, esc...)
		last = i
	}
	e.buf = append(e.buf, s[last:]...)
}

// base64 is an efficient base64 encoder, adapted from encoding/base64.
func (m *encodeState) base64(b []byte) {

	const std = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

	if len(b) == 0 {
		m.buf = append(m.buf, "null"...)
		return
	}

	m.buf = append(m.buf, '"')

	offset := len(m.buf)
	m.buf = append(m.buf, make([]byte, (len(b)+2)/3*4)...)

	di, si := 0, 0
	n := (len(b) / 3) * 3
	for si < n {
		// Convert 3x 8bit source bytes into 4 bytes
		val := uint(b[si+0])<<16 | uint(b[si+1])<<8 | uint(b[si+2])

		m.buf[offset+di+0] = std[val>>18&0x3F]
		m.buf[offset+di+1] = std[val>>12&0x3F]
		m.buf[offset+di+2] = std[val>>6&0x3F]
		m.buf[offset+di+3] = std[val&0x3F]

		si += 3
		di += 4
	}

	remain := len(b) - si
	if remain == 0 {
		m.buf = append(m.buf, '"')
		return
	}
	// Add the remaining small block
	val := uint(b[si+0]) << 16
	if remain == 2 {
		val |= uint(b[si+1]) << 8
	}

	m.buf[offset+di+0] = std[val>>18&0x3F]
	m.buf[offset+di+1] = std[val>>12&0x3F]

	switch remain {
	case 2:
		m.buf[offset+di+2] = std[val>>6&0x3F]
		m.buf[offset+di+3] = byte('=')
	case 1:
		m.buf[offset+di+2] = byte('=')
		m.buf[offset+di+3] = byte('=')
	}

	m.buf = append(m.buf, '"')
}

func (m *encodeState) open(tag string, flags marshalFlags) {
	if flags&isAttribute == 0 {
		m.buf = append(m.buf, '<')
	}
	m.buf = append(m.buf, tag...)
	if flags&isAttribute == 0 {
		m.buf = append(m.buf, '>')
	} else {
		m.buf = append(m.buf, '=', '"')
	}
}

func (m *encodeState) start(tag string, flags marshalFlags) {
	m.buf = append(m.buf, '<')
	m.buf = append(m.buf, tag...)
}

func (m *encodeState) close(tag string, flags marshalFlags) {
	if flags&isAttribute != 0 {
		m.buf = append(m.buf, '"')
		return
	}

	m.buf = append(m.buf, "</"...)
	m.buf = append(m.buf, tag...)
	m.buf = append(m.buf, '>')
}

// marshal is an efficient reflection-based XML marshaller for Go types.
// function is purposely large in order to reduce the cost of function calls
// ie. don't split it up.
func (m *encodeState) marshal(val any, tag string, flags marshalFlags) (repeatFlags marshalFlags, err error) {

	//checking for interface implementation is very
	//slow compared to other operations. So we use flags
	//to keep track of whether we need to check this or
	//not. The flags help to avoid checking each array
	//element and struct field.
	/*if marshalFlags(flags)&skipXMLMarshaler == 0 {
		if marshaler, ok := val.(xml.Marshaler); ok {
			b, err := marshaler.MarshalXML()
			if err != nil {
				return 0, err
			}
			m.buf = append(m.buf, b...)
			return 0, nil
		}
		flags &= skipXMLMarshaler
	}*/

	if tag == "" {
		tag = reflect.TypeOf(val).Name()
	}

	//we can type-switch on the result of this call
	// (all basic values are normalised to their
	//  builtin basic equivalents)
	switch v := reflected.BasicValueOf(val).(type) {
	case bool:
		m.open(tag, flags)
		if v {
			m.buf = append(m.buf, `true`...)
		} else {
			m.buf = append(m.buf, `false`...)
		}
		m.close(tag, flags)
	case int, int8, int16, int32, int64:
		m.open(tag, flags)
		m.buf = strconv.AppendInt(m.buf, reflected.IntOf(v), 10)
		m.close(tag, flags)
	case uint, uint8, uint16, uint32, uint64:
		m.open(tag, flags)
		m.buf = strconv.AppendUint(m.buf, reflected.UintOf(v), 10)
		m.close(tag, flags)
	case uintptr:
		m.open(tag, flags)
		m.buf = strconv.AppendUint(m.buf, uint64(v), 10)
		m.close(tag, flags)
	case float32, float64:
		m.open(tag, flags)
		m.buf = strconv.AppendFloat(m.buf, reflected.FloatOf(v), 'f', -1, 32)
		m.close(tag, flags)
	case complex64, complex128:
		m.open(tag, flags)
		m.buf = append(m.buf, strconv.FormatComplex(reflected.ComplexOf(v), 'f', -1, 128)...)
		m.close(tag, flags)
	case string:
		m.open(tag, flags)
		m.string(v)
		m.close(tag, flags)
	case []byte:
		m.open(tag, flags)
		m.base64(v)
		m.close(tag, flags)
	default:
		switch v, kind := reflected.ValueOf(v); kind {
		case reflect.Array:
			var elemFlags = flags
			for i := 0; i < v.ArrayLen(); i++ {
				elemFlags, err = m.marshal(v.ArrayIndex(i), tag, elemFlags)
				if err != nil {
					return 0, err
				}
			}
		case reflect.Map:
			if !v.MapIsNil() {
				m.open(tag, flags)
				for iter := v.MapRange(); iter.Next(); {
					if _, err := m.marshal(iter.Key().Interface(), "key", 0); err != nil {
						return 0, err
					}
					if _, err := m.marshal(iter.Value().Interface(), "val", 0); err != nil {
						return 0, err
					}
				}
				m.close(tag, flags)
			}
		case reflect.Interface:
			if !v.InterfaceIsNil() {
				if _, err := m.marshal(v.Interface(), tag, 0); err != nil {
					return 0, err
				}
			}
		case reflect.Ptr:
			if !v.IsNil() {
				return m.marshal(v.Deref(), tag, 0)
			}
		case reflect.Slice:
			if !v.SliceIsNil() {
				var elemFlags = flags
				for i := 0; i < v.SliceLen(); i++ {
					elemFlags, err = m.marshal(v.SliceIndex(i), tag, elemFlags)
					if err != nil {
						return 0, err
					}
				}
			}
		case reflect.Struct:
			//TODO handle zero-sized structs differently.
			if v.Type().Size() == 0 {
				return flags, nil
			}

			//this cache is inlined to save on
			//the function call, it is not cheap
			//but faster than recomputing the
			//field information which is very slow.
			t := v.Type()
			fieldCacheMutex.RLock()
			fields, ok := fieldCache[t]
			fieldCacheMutex.RUnlock()
			if !ok {
				fields = typeFields(t)
				fieldCacheMutex.Lock()
				fieldCache[t] = fields
				fieldCacheMutex.Unlock()
			}

			if fields.tag != "" {
				tag = fields.tag
			}
			if fields.dtag != (reflected.FieldOffset{}) {
				tag = v.FieldByOffset(fields.dtag).(string)
			}

			m.start(tag, flags)
			for i := range fields.attributes {
				field := &fields.attributes[i]

				m.buf = append(m.buf, ' ')
				if field.literal != "" {
					m.buf = append(m.buf, field.literal...)
					continue
				}

				//We support omitempty for
				//all Go types, even structs.
				if field.omitEmpty {
					if reflected.IsZero(v.Field(i)) {
						continue
					}
				}

				//field stores whether or not the field type
				//implements json.Marshaler. Allowing us to
				//skip the implementation check with a flag.
				if !field.marshaler {
					if _, err := m.marshal(v.FieldByOffset(field.offset), field.name, skipXMLMarshaler|isAttribute); err != nil {
						return 0, err
					}
				} else {
					m.buf = append(m.buf, '"')
					s, err := v.FieldByOffset(field.offset).(encoding.TextMarshaler).MarshalText()
					if err != nil {
						return 0, err
					}
					m.buf = append(m.buf, s...)
					m.buf = append(m.buf, '"')
				}
			}
			m.buf = append(m.buf, '>')

			for i := range fields.fields {
				field := &fields.fields[i]

				//We support omitempty for
				//all Go types, even structs.
				if field.omitEmpty {
					if reflected.IsZero(v.Field(i)) {
						continue
					}
				}

				//field stores whether or not the field type
				//implements json.Marshaler. Allowing us to
				//skip the implementation check with a flag.
				if !field.marshaler {
					if _, err := m.marshal(v.FieldByOffset(field.offset), field.name, skipXMLMarshaler); err != nil {
						return 0, err
					}
				} else {
					err := v.FieldByOffset(field.offset).(xml.Marshaler).MarshalXML(nil, xml.StartElement{})
					if err != nil {
						return 0, err
					}
				}
			}
			m.close(tag, flags)
		default:
			return 0, &json.UnsupportedTypeError{Type: v.Type()}
		}
	}
	return flags, nil
}
