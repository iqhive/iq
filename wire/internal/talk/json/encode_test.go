package json_test

import (
	go_json "encoding/json"
	go_xml "encoding/xml"

	"testing"

	gojson "github.com/goccy/go-json"
	"pkg.iqhive.com/iq/wire/internal/talk/json"
	"pkg.iqhive.com/iq/wire/internal/talk/xml"
	"pkg.iqhive.com/iq/wire/proto"
)

type Object struct {
	A     int `json:"biglongvaluename,omitempty"`
	Name  string
	Slice [3]string
	Omit  bool `json:",omitempty"`
	Test  []byte
	//Map   map[string]string
	Any interface{}
}

func assertEncoding(t *testing.T, a interface{}, encoded string) {
	b, err := json.Marshal(a)
	if err != nil {
		t.Fatal(err)
	}
	if string(b) != encoded {
		t.Fatalf("unexpected output: %s\nexpected %v", string(b), encoded)
	}
}

func TestBasic(t *testing.T) {
	assertEncoding(t, &Object{1, "foo", [3]string{"a", "b", "c"}, false, []byte{1, 2, 3}, "hello"}, `{"biglongvaluename":1,"Name":"foo","Slice":["a","b","c"],"Test":"AQID","Any":"hello"}`)
}

func BenchmarkTalkEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		json.Marshal(&v)
	}
}

func BenchmarkTalkXMLEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		xml.Marshal(&v)
	}
}

func BenchmarkGoXMLEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		go_xml.Marshal(&v)
	}

}

func BenchmarkGoJSONEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		gojson.Marshal(&v)
	}
}

func BenchmarkProtoEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		proto.Marshal(&v)
	}
}

func BenchmarkGoEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		go_json.Marshal(&v)
	}
}
