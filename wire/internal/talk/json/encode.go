// Package json provides an efficient JSON marshaler with better support for interfaces, unknown fields.
package json

import (
	"encoding/json"
	"reflect"
	"strconv"
	"sync"
	"unicode/utf8"

	"pkg.iqhive.com/iq/wire/internal/reflected"
)

type any = interface{}

// An encodeState encodes JSON into a bytes.Buffer.
type encodeState struct {
	buf []byte
}

var encodeStatePool sync.Pool

func newEncodeState() *encodeState {
	if v := encodeStatePool.Get(); v != nil {
		e := v.(*encodeState)
		e.buf = e.buf[:0:cap(e.buf)]
		return e
	}
	return &encodeState{}
}

// Marshal the given value to JSON.
// Faster than encoding/json.Marshal
// however you may not pass recursive
// values. Does not escape HTML.
//
// It supports omitempty and string
// tags, however omitempty applies to
// all Go values, including structs.
func Marshal(v any) ([]byte, error) {
	var e = newEncodeState()
	if _, err := e.marshal(v, 0); err != nil {
		return nil, err
	}
	var b = make([]byte, len(e.buf))
	copy(b, e.buf)
	encodeStatePool.Put(e)
	return b, nil
}

var fieldCache = make(map[reflect.Type]structFields)
var fieldCacheMutex sync.RWMutex

type marshalFlags uint8

const (
	skipJSONMarshaler marshalFlags = 1 << iota
)

const hex = "0123456789abcdef"

// string is ported from encoding/json
// support has been dropped for htmlEscape.
func (e *encodeState) string(s string) {
	e.buf = append(e.buf, '"')
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if !(b <= 31 || b == '"' || b == '\\') {
				i++
				continue
			}
			if start < i {
				e.buf = append(e.buf, s[start:i]...)
			}
			e.buf = append(e.buf, '\\')
			switch b {
			case '\\', '"':
				e.buf = append(e.buf, b)
			case '\n':
				e.buf = append(e.buf, 'n')
			case '\r':
				e.buf = append(e.buf, 'r')
			case '\t':
				e.buf = append(e.buf, 't')
			default:
				// This encodes bytes < 0x20 except for \t, \n and \r.
				// If escapeHTML is set, it also escapes <, >, and &
				// because they can lead to security holes when
				// user-controlled strings are rendered into JSON
				// and served to some browsers.
				e.buf = append(e.buf, `u00`...)
				e.buf = append(e.buf, hex[b>>4])
				e.buf = append(e.buf, hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRuneInString(s[i:])
		if c == utf8.RuneError && size == 1 {
			if start < i {
				e.buf = append(e.buf, s[start:i]...)
			}
			e.buf = append(e.buf, `\ufffd`...)
			i += size
			start = i
			continue
		}
		// U+2028 is LINE SEPARATOR.
		// U+2029 is PARAGRAPH SEPARATOR.
		// They are both technically valid characters in JSON strings,
		// but don't work in JSONP, which has to be evaluated as JavaScript,
		// and can lead to security holes there. It is valid JSON to
		// escape them, so we do so unconditionally.
		// See http://timelessrepo.com/json-isnt-a-javascript-subset for discussion.
		if c == '\u2028' || c == '\u2029' {
			if start < i {
				e.buf = append(e.buf, s[start:i]...)
			}
			e.buf = append(e.buf, `\u202`...)
			e.buf = append(e.buf, hex[c&0xF])
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		e.buf = append(e.buf, s[start:]...)
	}
	e.buf = append(e.buf, '"')
}

// base64 is an efficient base64 encoder, adapted from encoding/base64.
func (m *encodeState) base64(b []byte) {

	const std = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

	if len(b) == 0 {
		m.buf = append(m.buf, "null"...)
		return
	}

	m.buf = append(m.buf, '"')

	offset := len(m.buf)
	m.buf = append(m.buf, make([]byte, (len(b)+2)/3*4)...)

	di, si := 0, 0
	n := (len(b) / 3) * 3
	for si < n {
		// Convert 3x 8bit source bytes into 4 bytes
		val := uint(b[si+0])<<16 | uint(b[si+1])<<8 | uint(b[si+2])

		m.buf[offset+di+0] = std[val>>18&0x3F]
		m.buf[offset+di+1] = std[val>>12&0x3F]
		m.buf[offset+di+2] = std[val>>6&0x3F]
		m.buf[offset+di+3] = std[val&0x3F]

		si += 3
		di += 4
	}

	remain := len(b) - si
	if remain == 0 {
		m.buf = append(m.buf, '"')
		return
	}
	// Add the remaining small block
	val := uint(b[si+0]) << 16
	if remain == 2 {
		val |= uint(b[si+1]) << 8
	}

	m.buf[offset+di+0] = std[val>>18&0x3F]
	m.buf[offset+di+1] = std[val>>12&0x3F]

	switch remain {
	case 2:
		m.buf[offset+di+2] = std[val>>6&0x3F]
		m.buf[offset+di+3] = byte('=')
	case 1:
		m.buf[offset+di+2] = byte('=')
		m.buf[offset+di+3] = byte('=')
	}

	m.buf = append(m.buf, '"')
}

// marshal is an efficient reflection-based JSON marshaller for Go types.
// function is purposely large in order to reduce the cost of function calls
// ie. don't split it up.
func (m *encodeState) marshal(val any, flags marshalFlags) (repeatFlags marshalFlags, err error) {

	//checking for interface implementation is very
	//slow compared to other operations. So we use flags
	//to keep track of whether we need to check this or
	//not. The flags help to avoid checking each array
	//element and struct field.
	if marshalFlags(flags)&skipJSONMarshaler == 0 {
		if marshaler, ok := val.(json.Marshaler); ok {
			b, err := marshaler.MarshalJSON()
			if err != nil {
				return 0, err
			}
			m.buf = append(m.buf, b...)
			return 0, nil
		}
		flags &= skipJSONMarshaler
	}

	//we can type-switch on the result of this call
	// (all basic values are normalised to their
	//  builtin basic equivalents)
	switch v := reflected.BasicValueOf(val).(type) {
	case bool:
		if v {
			m.buf = append(m.buf, `true`...)
		} else {
			m.buf = append(m.buf, `false`...)
		}
	case int, int8, int16, int32, int64:
		m.buf = strconv.AppendInt(m.buf, reflected.IntOf(v), 10)
	case uint, uint8, uint16, uint32, uint64:
		m.buf = strconv.AppendUint(m.buf, reflected.UintOf(v), 10)
	case uintptr:
		m.buf = strconv.AppendUint(m.buf, uint64(v), 10)
	case float32, float64:
		m.buf = strconv.AppendFloat(m.buf, reflected.FloatOf(v), 'f', -1, 32)
	case complex64, complex128:
		m.buf = append(m.buf, '"')
		m.buf = append(m.buf, strconv.FormatComplex(reflected.ComplexOf(v), 'f', -1, 128)...)
		m.buf = append(m.buf, '"')
	case string:
		m.string(v)
	case []byte:
		m.base64(v)
	default:
		switch v, kind := reflected.ValueOf(v); kind {
		case reflect.Array:
			var next byte = '['
			var elemFlags = flags
			for i := 0; i < v.ArrayLen(); i++ {
				m.buf = append(m.buf, next)
				elemFlags, err = m.marshal(v.ArrayIndex(i), elemFlags)
				if err != nil {
					return 0, err
				}
				next = ','
			}
			m.buf = append(m.buf, ']')
		case reflect.Map:
			if v.MapIsNil() {
				m.buf = append(m.buf, `null`...)
			} else {
				var next byte = '{'
				for iter := v.MapRange(); iter.Next(); {
					m.buf = append(m.buf, next)
					if _, err := m.marshal(iter.Key().Interface(), 0); err != nil {
						return 0, err
					}
					m.buf = append(m.buf, ':')
					if _, err := m.marshal(iter.Value().Interface(), 0); err != nil {
						return 0, err
					}
					next = ','
				}
				m.buf = append(m.buf, '}')
			}
		case reflect.Interface:
			if v.InterfaceIsNil() {
				m.buf = append(m.buf, `null`...)
			} else {
				if _, err := m.marshal(v.Interface(), 0); err != nil {
					return 0, err
				}
			}
		case reflect.Ptr:
			if v.IsNil() {
				m.buf = append(m.buf, `null`...)
			} else {
				return m.marshal(v.Deref(), 0)
			}
		case reflect.Slice:
			if v.SliceIsNil() {
				m.buf = append(m.buf, `null`...)
			} else {
				var next byte = '['
				var elemFlags = flags
				for i := 0; i < v.SliceLen(); i++ {
					m.buf = append(m.buf, next)
					elemFlags, err = m.marshal(v.SliceIndex(i), elemFlags)
					if err != nil {
						return 0, err
					}
					next = ','
				}
				m.buf = append(m.buf, ']')
			}
		case reflect.Struct:
			//TODO handle zero-sized structs differently.
			if v.Type().Size() == 0 {
				return flags, nil
			}

			//this cache is inlined to save on
			//the function call, it is not cheap
			//but faster than recomputing the
			//field information which is very slow.
			t := v.Type()
			fieldCacheMutex.RLock()
			fields, ok := fieldCache[t]
			fieldCacheMutex.RUnlock()
			if !ok {
				fields = typeFields(t)
				fieldCacheMutex.Lock()
				fieldCache[t] = fields
				fieldCacheMutex.Unlock()
			}

			var next byte = '{'
			for i := range fields.list {
				field := &fields.list[i]

				//We support omitempty for
				//all Go types, even structs.
				if field.omitEmpty {
					if reflected.IsZero(v.Field(i)) {
						continue
					}
				}

				m.buf = append(m.buf, next)
				m.buf = append(m.buf, field.nameNonEsc...)
				if field.quoted {
					m.buf = append(m.buf, '"')
				}

				//field stores whether or not the field type
				//implements json.Marshaler. Allowing us to
				//skip the implementation check with a flag.
				if !field.marshaler {
					if _, err := m.marshal(v.FieldByOffset(field.offset), skipJSONMarshaler); err != nil {
						return 0, err
					}
				} else {
					b, err := v.FieldByOffset(field.offset).(json.Marshaler).MarshalJSON()
					if err != nil {
						return 0, err
					}
					m.buf = append(m.buf, b...)
				}

				if field.quoted {
					m.buf = append(m.buf, '"')
				}
				next = ','
			}
			m.buf = append(m.buf, '}')
		default:
			return 0, &json.UnsupportedTypeError{Type: v.Type()}
		}
	}
	return flags, nil
}
