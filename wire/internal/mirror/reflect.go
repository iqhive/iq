//go:build !gc

package mirror

import "reflect"

type Type = reflect.Type

func Reflect(value any) (Type, Value) {
	if already, ok := value.(Value); ok {
		return already.val.Type(), already
	}
	return reflect.TypeOf(value), Value{reflect.ValueOf(value)}
}

type Value struct {
	val reflect.Value
}

func (v Value) Bool() bool {
	return v.val.Bool()
}

func (v Value) IsZero() bool {
	return v.val.IsZero()
}

func (v Value) Int() int {
	return int(v.val.Int())
}

func (v Value) Int8() int8 {
	return int8(v.val.Int())
}

func (v Value) Int16() int16 {
	return int16(v.val.Int())
}

func (v Value) Int32() int32 {
	return int32(v.val.Int())
}

func (v Value) Int64() int64 {
	return v.val.Int()
}

func (v Value) Uint() uint {
	return uint(v.val.Uint())
}

func (v Value) Uint8() uint8 {
	return uint8(v.val.Uint())
}

func (v Value) Uint16() uint16 {
	return uint16(v.val.Uint())
}

func (v Value) Uint32() uint32 {
	return uint32(v.val.Uint())
}

func (v Value) Uint64() uint64 {
	return v.val.Uint()
}

func (v Value) Uintptr() uintptr {
	return uintptr(v.val.Uint())
}

func (v Value) Float32() float32 {
	return float32(v.val.Float())
}

func (v Value) Float64() float64 {
	return v.val.Float()
}

func (v Value) ArrayLen() int {
	return v.val.Len()
}

func (v Value) ArrayIndex(i int) Value {
	return Value{v.val.Index(i)}
}

func (v Value) InterfaceIsNil() bool {
	return v.val.IsNil()
}

func (v Value) Interface() any {
	return v.val.Interface()
}

func (v Value) MapIsNil() bool {
	return v.val.IsNil()
}

func (v Value) MapRange() *reflect.MapIter {
	return v.val.MapRange()
}

func (v Value) IsNil() bool {
	return v.val.IsNil()
}

func (v Value) Deref() Value {
	return Value{v.val.Elem()}
}

func (v Value) SliceIsNil() bool {
	return v.val.IsNil()
}

func (v Value) SliceLen() int {
	return v.val.Len()
}

func (v Value) SliceIndex(i int) Value {
	return Value{v.val.Index(i)}
}

func (v Value) String() string {
	return v.val.String()
}

func (v Value) Field(i int) Value {
	return Value{v.val.Field(i)}
}

func (v Value) FieldByOffset(offset []int) Value {
	return Value{v.val.FieldByIndex(offset)}
}

func (v Value) Bytes() []byte {
	return v.val.Bytes()
}
