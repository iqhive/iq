//go:build gc && go1.20

package mirror

import (
	"reflect"
	"unsafe"
)

func Reflect(value any) Value {
	return *(*Value)(unsafe.Pointer(&value))
}

type Type = *rtype

func (t *rtype) Kind() reflect.Kind {
	return reflect.Kind(t.kind & kindMask)
}

func (t *rtype) Uncommon() bool {
	return t.tflag&tflagUncommon != 0
}

func (t *rtype) Size() uintptr {
	return t.size
}

func (t *rtype) HasPointers() bool {
	return t.ptrdata != 0
}

func (t *rtype) SliceElem() Type {
	if t.Kind() != reflect.Slice {
		panic("not a slice")
	}
	return (*sliceType)(unsafe.Pointer(t)).elem
}

func (t *rtype) NumField() int {
	if t.Kind() != reflect.Struct {
		panic("not a struct")
	}
	return len((*structType)(unsafe.Pointer(t)).fields)
}

type Field struct {
	Name   string
	Type   Type
	Tag    Tag
	Offset FieldOffset
}

func (t *rtype) Field(i int) Field {
	if t.Kind() != reflect.Struct {
		panic("not a struct")
	}

	field := (*structType)(unsafe.Pointer(t)).fields[i]

	var name, tag string

	n := field.name
	if n.bytes != nil {
		i, l := n.readVarint(1)
		name = unsafe.String(n.data(1+i, "non-empty string"), l)
		i2, l2 := n.readVarint(1 + i + l)
		tag = unsafe.String(n.data(1+i+l+i2, "non-empty string"), l2)
	}

	return Field{
		Name: name,
		Type: field.typ,
		Tag:  Tag(tag),
		Offset: FieldOffset{
			of:     t,
			typ:    field.typ,
			offset: field.offset,
		},
	}
}

type Value struct {
	typ Type
	ptr unsafe.Pointer
}

func (v Value) Any() any {
	return *(*any)(unsafe.Pointer(&v))
}

func (v Value) Kind() reflect.Kind {
	return reflect.Kind(v.typ.kind & kindMask)
}

func (v Value) Type() Type {
	return v.typ
}

func (v Value) Bool() bool {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Bool {
		return *(*bool)(v.ptr)
	}
	return false
}

func (val Value) IsZero() bool {
	if val.ptr == nil {
		return true
	}
	rtype := (*rtype)(val.typ)
	for i := uintptr(0); i < rtype.size; i++ { //check byte by byte
		if *(*byte)(unsafe.Add(val.ptr, i)) != 0 {
			return false
		}
	}
	return true
}

func (v Value) Int() int {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Int {
		return *(*int)(v.ptr)
	}
	return 0
}

func (v Value) Int8() int8 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Int8 {
		return *(*int8)(v.ptr)
	}
	return 0
}

func (v Value) Int16() int16 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Int16 {
		return *(*int16)(v.ptr)
	}
	return 0
}

func (v Value) Int32() int32 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Int32 {
		return *(*int32)(v.ptr)
	}
	return 0
}

func (v Value) Int64() int64 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Int64 {
		return *(*int64)(v.ptr)
	}
	return 0
}

func (v Value) Uint() uint {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Uint {
		return *(*uint)(v.ptr)
	}
	return 0
}

func (v Value) Uint8() uint8 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Uint8 {
		return *(*uint8)(v.ptr)
	}
	return 0
}

func (v Value) Uint16() uint16 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Uint16 {
		return *(*uint16)(v.ptr)
	}
	return 0
}

func (v Value) Uint32() uint32 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Uint32 {
		return *(*uint32)(v.ptr)
	}
	return 0
}

func (v Value) Uint64() uint64 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Uint64 {
		return *(*uint64)(v.ptr)
	}
	return 0
}

func (v Value) Uintptr() uintptr {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Uintptr {
		return *(*uintptr)(v.ptr)
	}
	return 0
}

func (v Value) Float32() float32 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Float32 {
		return *(*float32)(v.ptr)
	}
	return 0
}

func (v Value) Float64() float64 {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Float64 {
		return *(*float64)(v.ptr)
	}
	return 0
}

func (v Value) ArrayLen() int {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Array {
		return int((*arrayType)(v.ptr).len)
	}
	return 0
}

func (v Value) ArrayIndex(i int) Value {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Array {
		tt := (*arrayType)(unsafe.Pointer(v.typ))
		if uint(i) >= uint(tt.len) {
			panic("reflect: array index out of range")
		}
		return Value{
			typ: tt.elem,
			ptr: unsafe.Add(v.ptr, uintptr(i)*tt.elem.size),
		}
	}
	panic("not an array")
}

func (v Value) InterfaceIsNil() bool {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Interface {
		return (*Value)(v.ptr).typ == nil
	}
	return false
}

func (v Value) Interface() any {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Interface {
		return *(*interface{})(unsafe.Pointer(v.ptr))
	}
	return nil
}

func (v Value) InterfaceDeref() Value {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Interface {
		return *(*Value)(unsafe.Pointer(v.ptr))
	}
	return Value{}
}

func (v Value) MapIsNil() bool {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Map {
		return reflect.ValueOf(*(*any)(unsafe.Pointer(&v.ptr))).IsNil()
	}
	return false
}

func (v Value) MapRange() *reflect.MapIter {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Map {
		return reflect.ValueOf(*(*any)(unsafe.Pointer(&v.ptr))).MapRange()
	}
	return nil
}

func (v Value) IsNil() bool {
	if reflect.Kind(v.typ.kind&kindMask) == reflect.Ptr {
		return v.ptr == nil
	}
	return false
}

func (v Value) Deref() Value {
	if v.Kind() != reflect.Ptr {
		panic("not a pointer")
	}
	rtype := (*ptrType)(unsafe.Pointer(v.typ))
	if !ifaceIndir(v.typ) {
		return Value{
			typ: rtype.elem,
			ptr: v.ptr,
		}
	}
	return Value{
		typ: rtype.elem,
		ptr: *(*unsafe.Pointer)(v.ptr),
	}
}

func (v Value) SliceIsNil() bool {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Slice {
		panic("not a slice")
	}
	slice := (*reflect.SliceHeader)(v.ptr)
	return slice.Data == 0
}

func (v Value) SliceLen() int {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Slice {
		panic("not a slice")
	}
	return (*reflect.SliceHeader)(v.ptr).Len
}

func (v Value) SliceIndex(i int) Value {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Slice {
		panic("not a slice")
	}
	slice := (*reflect.SliceHeader)(v.ptr)
	elem := (*sliceType)(unsafe.Pointer(v.typ)).elem
	return Value{
		typ: elem,
		ptr: unsafe.Pointer(unsafe.Add(unsafe.Pointer(slice.Data), uintptr(i)*elem.size)),
	}
}

func (v Value) String() string {
	if v.Kind() == reflect.String {
		return *(*string)(v.ptr)
	}
	panic("not a string")
}

func (v Value) Field(i int) Value {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Struct {
		panic("not a struct")
	}

	field := (*structType)(unsafe.Pointer(v.typ)).fields[i]
	return Value{
		typ: Type(field.typ),
		ptr: unsafe.Add(v.ptr, field.offset),
	}
}

type FieldOffset struct {
	of     *rtype //parent type, used to verify offset.
	typ    *rtype
	offset uintptr
}

func (v Value) FieldByOffset(offset FieldOffset) Value {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Struct {
		panic("not a struct")
	}
	if (*rtype)(v.typ) != offset.of {
		panic("reflect: FieldByOffset of mismatched types")
	}
	return Value{
		typ: offset.typ,
		ptr: unsafe.Add(v.ptr, offset.offset),
	}
}

func (v Value) Bytes() []byte {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Slice {
		panic("not a slice")
	}
	return *(*[]byte)(v.ptr)
}

func (v Value) SetBool(b bool) {
	if reflect.Kind(v.typ.kind&kindMask) != reflect.Bool {
		panic("not a bool")
	}
	*(*bool)(v.ptr) = b
}
