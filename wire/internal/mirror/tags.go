package mirror

type Tag string

// Get the named struct tag 'key' from the tag string.
// ie `key:"main,flag1,flag2"`
func (tag Tag) Get(pkg string) (primary string, remaining string, ok bool) {
	tagString := string(tag)

	var inQuotes bool
	var start int
	var found bool
	var foundPrimary bool

	for i := 0; i < len(tagString); i++ {
		switch tagString[i] {
		case ':':
			if tagString[start:i] == pkg {
				start = i + 2
				found = true
			}
		case '"':
			if inQuotes && found {
				if !foundPrimary {
					primary = tagString[start:i]
				} else {
					remaining = tagString[start:i]
				}
				return primary, remaining, true
			}
			inQuotes = !inQuotes
		case ',':
			if found && inQuotes {
				primary = tagString[start:i]
				foundPrimary = true
				start = i
			}
		case '\n':
			return "", "", false
		case ' ':
			if !found {
				start = i + 1
			}
		}
	}
	return "", "", false
}
