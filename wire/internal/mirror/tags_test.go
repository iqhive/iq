package mirror_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/wire/internal/mirror"
)

func TestTag(t *testing.T) {
	fmt.Println(mirror.Tag(`json:"name,omitempty"`).Get("json"))
	fmt.Println(mirror.Tag(`json:"name"`).Get("json"))
	fmt.Println(mirror.Tag(`json:",omitempty"`).Get("json"))
	fmt.Println(mirror.Tag(`pb:"1" json:"name"`).Get("json"))
}
