package mirror_test

import (
	"fmt"
	"testing"

	"pkg.iqhive.com/iq/wire/internal/mirror"
)

type Vector3 struct {
	X, Y, Z float64
}

var Vector3Field Vector3

type Nested struct {
	A string
	B Vector3
}

var NestedField Nested

func TestMirror(t *testing.T) {
	mirror.Register(&Vector3Field, &NestedField)

	fmt.Println(mirror.FieldOf(&Vector3Field.Z).Name)
	fmt.Println(mirror.FieldOf(&NestedField.A).Name)
	fmt.Println(mirror.FieldOf(&NestedField.B).Name)
	fmt.Println(mirror.TypeOf(&NestedField.B.X))
}
