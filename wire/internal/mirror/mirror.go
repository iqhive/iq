// Package mirror provides a useful mirror for reflecting fields.
package mirror

import (
	"reflect"
)

type mirror struct {
	reflect.StructField

	root reflect.Type
	path []reflect.StructField
}

var mapping = make(map[interface{}]mirror)

// FieldOf returns the reflect.StructField of the given field.
func FieldOf(field interface{}) reflect.StructField {
	if T := reflect.TypeOf(field); T.Kind() != reflect.Ptr {
		panic("mirror.FieldOf: please provide a pointer type, take the & address of the " + T.String())
	}

	if m, ok := mapping[field]; ok {
		return m.StructField
	}

	panic("mirror.FieldOf: field must be registered with mirror.Register")
}

// TypeOf returns the type of the mirror that contains the given field.
// This is the top-level type not the parent type.
func TypeOf(field interface{}) reflect.Type {
	if T := reflect.TypeOf(field); T.Kind() != reflect.Ptr {
		panic("mirror.TypeOf: please provide a pointer type, take the & address of the " + T.String())
	}

	if m, ok := mapping[field]; ok {
		return m.root
	}

	panic("mirror.TypeOf: field must be registered with mirror.Register")
}

// FieldsOf returns the fields traversing from the parent to this field.
// Does not include itself, if the slice is nil, then this field is a direct field of
// the mirror iteself.
func FieldsOf(field interface{}) []reflect.StructField {
	if T := reflect.TypeOf(field); T.Kind() != reflect.Ptr {
		panic("mirror.FieldsOf: please provide a pointer type, take the & address of the " + T.String())
	}

	if m, ok := mapping[field]; ok {
		return m.path
	}

	panic("mirror.FieldsOf: field must be registered with mirror.Register")
}

// Register makes a mirror out of the given type.
// You should cache this value somewhere and
// name it, so that it is obvious to use as
// a mirror for functions expecting fields.
// If the given value is not a pointer to a
// struct, Register panics.
func Register(mirrors ...interface{}) {

	var register func(root reflect.Type, i interface{}, path []reflect.StructField)
	register = func(root reflect.Type, i interface{}, path []reflect.StructField) {
		T := reflect.TypeOf(i).Elem()
		V := reflect.ValueOf(i).Elem()

		for i := 0; i < T.NumField(); i++ {
			var m = mirror{
				StructField: T.Field(i),
				root:        root,
				path:        path,
			}

			if !T.Field(i).IsExported() {
				continue
			}

			var key interface{}

			//Allow converted pointers to basic types, ie (*int)(&MyVariableWithCustomIntType)
			switch T.Field(i).Type.Kind() {
			case reflect.Int32:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*int32{}).Elem()).Interface()
			case reflect.Int64:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*int64{}).Elem()).Interface()
			case reflect.Int:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*int{}).Elem()).Interface()
			case reflect.Uint32:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*uint32{}).Elem()).Interface()
			case reflect.Uint64:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*uint64{}).Elem()).Interface()
			case reflect.Uint:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*uint{}).Elem()).Interface()
			case reflect.String:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*string{}).Elem()).Interface()
			case reflect.Bool:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*bool{}).Elem()).Interface()
			case reflect.Float32:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*float32{}).Elem()).Interface()
			case reflect.Float64:
				key = V.Field(i).Addr().Convert(reflect.TypeOf([0]*float64{}).Elem()).Interface()
			}

			if key != nil {
				if _, ok := mapping[key]; !ok {
					mapping[key] = m
				}
			}

			var raw = V.Field(i).Addr().Interface()
			if _, ok := mapping[raw]; !ok {
				mapping[raw] = m
			}

			if V.Field(i).Type().Kind() == reflect.Struct {
				register(root, V.Field(i).Addr().Interface(), append(path, T.Field(i)))
			}
		}
	}

	for _, mirror := range mirrors {
		T := reflect.TypeOf(mirror)
		if T.Kind() != reflect.Ptr || T.Elem().Kind() != reflect.Struct {
			panic("mirror.Make: please provide a pointer to a struct, not a " + T.String())
		}

		register(T, mirror, nil)
	}
}
