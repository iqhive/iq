package json_test

import (
	"bytes"
	go_json "encoding/json"
	go_xml "encoding/xml"

	gojson "github.com/goccy/go-json"
	talkjson "pkg.iqhive.com/iq/wire/internal/talk/json"
	talkxml "pkg.iqhive.com/iq/wire/internal/talk/xml"
	hivejson "pkg.iqhive.com/iq/wire/json"

	"testing"

	"pkg.iqhive.com/iq/wire/proto"
)

type Object struct {
	A     int `json:"biglongvaluename,omitempty"`
	Name  string
	Slice [3]string
	Omit  bool `json:",omitempty"`
	Test  []byte
	//Map   map[string]string
	Any interface{}
}

func assertEncoding(t *testing.T, a interface{}, encoded string) {
	var buf bytes.Buffer
	var enc = hivejson.NewEncoder(&buf)
	if err := enc.Encode(a); err != nil {
		t.Fatal(err)
	}
	if buf.String() != encoded {
		t.Fatalf("\n unexpected: %s\n   expected: %v", buf.String(), encoded)
	}
}

func TestBasic(t *testing.T) {
	assertEncoding(t, &Object{1, "foo", [3]string{"a", "b", "c"}, false, []byte{1, 2, 3}, "hello"}, `{"biglongvaluename":1,"Name":"foo","Slice":["a","b","c"],"Test":"AQID","Any":"hello"}`)
}

func BenchmarkTalkEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		talkjson.Marshal(&v)
	}
}

func BenchmarkWireEncoder(b *testing.B) {
	var buf bytes.Buffer
	var enc = hivejson.NewEncoder(&buf)
	var v Object
	v.Name = "foo"

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		enc.Encode(&v)
		enc.Reset()
		buf.Reset()
	}
}

func BenchmarkTalkXMLEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		talkxml.Marshal(&v)
	}
}

func BenchmarkGoXMLEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		go_xml.Marshal(&v)
	}

}

func BenchmarkGoJSONEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		gojson.Marshal(&v)
	}
}

func BenchmarkProtoEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		proto.Marshal(&v)
	}
}

func BenchmarkGoEncoder(b *testing.B) {
	var v Object
	v.Name = "foo"
	for i := 0; i < b.N; i++ {
		go_json.Marshal(&v)
	}
}
