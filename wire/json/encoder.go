package json

import (
	"encoding/json"
	"io"
	"reflect"
	"strconv"
	"strings"
	"unicode/utf8"

	"pkg.iqhive.com/iq/wire"
	"pkg.iqhive.com/iq/wire/internal/mirror"
)

type Encoder struct {
	out wire.Encoder
}

func NewEncoder(w io.Writer) Encoder {
	return Encoder{
		out: wire.NewEncoder(w),
	}
}

func (enc *Encoder) Err() error {
	return enc.out.Err()
}

func (enc *Encoder) Reset() {
	enc.out.Reset()
}

func (enc *Encoder) itoa(i int64) {
	formatBits(&enc.out, uint64(i), 10, i < 0)
}

func (enc *Encoder) utoa(i uint64) {
	formatBits(&enc.out, uint64(i), 10, false)
}

func (enc *Encoder) ftoa(i float64) {
	var buf []byte
	buf = strconv.AppendFloat(buf, i, 'f', -1, 64)
	enc.out.EncodeBytes(buf)
}

const hex = "0123456789abcdef"

// string is ported from encoding/json
// support has been dropped for htmlEscape.
func (e *Encoder) string(s string) {
	e.out.EncodeU8('"')
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if !(b <= 31 || b == '"' || b == '\\') {
				i++
				continue
			}
			if start < i {
				e.out.EncodeString(s[start:i])
			}
			e.out.EncodeU8('\\')
			switch b {
			case '\\', '"':
				e.out.EncodeU8(b)
			case '\n':
				e.out.EncodeU8('n')
			case '\r':
				e.out.EncodeU8('r')
			case '\t':
				e.out.EncodeU8('t')
			default:
				// This encodes bytes < 0x20 except for \t, \n and \r.
				// If escapeHTML is set, it also escapes <, >, and &
				// because they can lead to security holes when
				// user-controlled strings are rendered into JSON
				// and served to some browsers.
				e.out.EncodeString(`u00`)
				e.out.EncodeU8(hex[b>>4])
				e.out.EncodeU8(hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRuneInString(s[i:])
		if c == utf8.RuneError && size == 1 {
			if start < i {
				e.out.EncodeString(s[start:i])
			}
			e.out.EncodeString(`\ufffd`)
			i += size
			start = i
			continue
		}
		// U+2028 is LINE SEPARATOR.
		// U+2029 is PARAGRAPH SEPARATOR.
		// They are both technically valid characters in JSON strings,
		// but don't work in JSONP, which has to be evaluated as JavaScript,
		// and can lead to security holes there. It is valid JSON to
		// escape them, so we do so unconditionally.
		// See http://timelessrepo.com/json-isnt-a-javascript-subset for discussion.
		if c == '\u2028' || c == '\u2029' {
			if start < i {
				e.out.EncodeString(s[start:i])
			}
			e.out.EncodeString(`\u202`)
			e.out.EncodeU8(hex[c&0xF])
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		e.out.EncodeString(s[start:])
	}
	e.out.EncodeU8('"')
}

// base64 is an efficient base64 encoder, adapted from encoding/base64.
func (m *Encoder) base64(b []byte) {

	const std = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

	if len(b) == 0 {
		m.out.EncodeString(`"null"`)
		return
	}

	m.out.EncodeU8('"')

	// Encode full quanta.
	n := len(b) / 3 * 3
	for i := 0; i < n; i += 3 {
		// We could use binary.BigEndian.Uint32 here, but it's overkill.
		x := uint32(b[i+0])<<16 | uint32(b[i+1])<<8 | uint32(b[i+2])
		m.out.EncodeU8(std[x>>18])
		m.out.EncodeU8(std[x>>12&0x3f])
		m.out.EncodeU8(std[x>>6&0x3f])
		m.out.EncodeU8(std[x&0x3f])
	}

	remain := len(b) - n
	if remain == 0 {
		m.out.EncodeU8('"')
		return
	}

	// Encode partial quanta.
	switch len(b) - n {
	case 1:
		x := uint32(b[n+0])
		m.out.EncodeU8(std[x>>2])
		m.out.EncodeU8(std[x<<4&0x3f])
		m.out.EncodeString("==")
	case 2:
		x := uint32(b[n+0])<<8 | uint32(b[n+1])
		m.out.EncodeU8(std[x>>10])
		m.out.EncodeU8(std[x>>4&0x3f])
		m.out.EncodeU8(std[x<<2&0x3f])
		m.out.EncodeU8('=')
	}

	m.out.EncodeU8('"')
}

func (enc *Encoder) Encode(value any) error {
	if _, err := enc.encode(mirror.Reflect(value), false); err != nil {
		return err
	}
	return enc.out.Flush()
}

func (enc *Encoder) encode(rvalue mirror.Value, skipMarshaler bool) (hasMarshaler bool, err error) {
	rtype := rvalue.Type()

	if err := enc.Err(); err != nil {
		return skipMarshaler, err
	}

	if rtype.Uncommon() && !skipMarshaler {
		if impl, ok := rvalue.Any().(json.Marshaler); ok {
			b, err := impl.MarshalJSON()
			if err != nil {
				return skipMarshaler, err
			}
			enc.out.EncodeBytes(b)
			return true, nil
		}
	}

	switch rtype.Kind() {
	case reflect.Bool:
		if rvalue.Bool() {
			enc.out.EncodeString("true")
		} else {
			enc.out.EncodeString("false")
		}
	case reflect.Int:
		enc.itoa(int64(rvalue.Int()))
	case reflect.Int8:
		enc.itoa(int64(rvalue.Int8()))
	case reflect.Int16:
		enc.itoa(int64(rvalue.Int16()))
	case reflect.Int32:
		enc.itoa(int64(rvalue.Int32()))
	case reflect.Int64:
		enc.itoa(rvalue.Int64())
	case reflect.Uint:
		enc.utoa(uint64(rvalue.Uint()))
	case reflect.Uint8:
		enc.utoa(uint64(rvalue.Uint8()))
	case reflect.Uint16:
		enc.utoa(uint64(rvalue.Uint16()))
	case reflect.Uint32:
		enc.utoa(uint64(rvalue.Uint32()))
	case reflect.Uint64:
		enc.utoa(rvalue.Uint64())
	case reflect.Uintptr:
		enc.utoa(uint64(rvalue.Uintptr()))
	case reflect.Float32:
		enc.ftoa(float64(rvalue.Float32()))
	case reflect.Float64:
		enc.ftoa(float64(rvalue.Float64()))
	case reflect.Array:
		var next byte = '['
		for i := 0; i < rvalue.ArrayLen(); i++ {
			enc.out.EncodeU8(next)
			if hasMarshaler, err = enc.encode(rvalue.ArrayIndex(i), !hasMarshaler); err != nil {
				return
			}
			next = ','
		}
		enc.out.EncodeU8(']')
	case reflect.Interface:
		if rvalue.InterfaceIsNil() {
			enc.out.EncodeString("null")
		} else {
			if _, err = enc.encode(rvalue.InterfaceDeref(), false); err != nil {
				return
			}
		}
	case reflect.Map:
		if rvalue.MapIsNil() {
			enc.out.EncodeString("null")
		} else {
			var next byte = '{'
			for iter := rvalue.MapRange(); iter.Next(); {
				enc.out.EncodeU8(next)
				if _, err = enc.encode(mirror.Reflect(iter.Key().Interface()), false); err != nil {
					return
				}
				enc.out.EncodeU8(':')
				if _, err = enc.encode(mirror.Reflect(iter.Value().Interface()), false); err != nil {
					return
				}
				next = ','
			}
			enc.out.EncodeU8('}')
		}
	case reflect.Ptr:
		if rvalue.IsNil() {
			enc.out.EncodeString("null")
		} else {
			return enc.encode(rvalue.Deref(), false)
		}
	case reflect.Slice:
		if rtype.SliceElem().Kind() == reflect.Uint8 {
			enc.base64(rvalue.Bytes())
			return true, nil
		}
		if rvalue.SliceIsNil() {
			enc.out.EncodeString("null")
		} else {
			var next byte = '['
			for i := 0; i < rvalue.SliceLen(); i++ {
				enc.out.EncodeU8(next)
				if hasMarshaler, err = enc.encode(rvalue.SliceIndex(i), !hasMarshaler); err != nil {
					return
				}
				next = ','
			}
			enc.out.EncodeU8(']')
		}
	case reflect.String:
		enc.string(rvalue.String())
	case reflect.Struct:
		if rtype.Size() == 0 {
			enc.out.EncodeString("{}")
			return
		}

		var next byte = '{'
		for i := 0; i < rtype.NumField(); i++ {
			field := rtype.Field(i)

			var omitempty bool
			var quoted bool

			name := field.Name
			if field.Tag != "" {
				main, flags, ok := field.Tag.Get("json")
				if ok {
					if main == "-" {
						continue
					}
					if main != "" {
						name = main
					}
					if strings.Contains(flags, ",omitempty") {
						omitempty = true
					}
					if strings.Contains(flags, ",quoted") {
						quoted = true
					}
				}
			}

			//We support omitempty for
			//all Go types, even structs.
			if omitempty {
				if rvalue.Field(i).IsZero() {
					continue
				}
			}

			enc.out.EncodeU8(next)

			enc.out.EncodeU8('"')
			enc.out.EncodeString(name)
			enc.out.EncodeU8('"')

			if quoted {
				enc.out.EncodeU8('"')
			}
			enc.out.EncodeU8(':')

			if _, err = enc.encode(rvalue.Field(i), false); err != nil {
				return
			}

			if quoted {
				enc.out.EncodeU8('"')
			}
			next = ','
		}
		enc.out.EncodeU8('}')

	default:
		return hasMarshaler, &json.UnsupportedTypeError{}
	}
	return
}
