/*
Package wire is a draft design for a possible IQ Hive serialisation format.

The wire format is designed to enable fast communication between systems
that share an underlying memory model.

This is achieved through the use of a header that dynamically
specifies the structure of the message. A message is formatted
as follows:

	[Architecture Bits] (Message Schema) [Message Header] [Message Body] [Additional Message Memory]

The architecture bits are a 32 bit integer that specify flags
indicating endianness, C strings VS Go strings and other global
parameters such as numerical compression, datetime format and whether
any pointers could be (or are) recursive and their size.

The message schema is optional and should usually be omitted,
however if it is present, it will contain information about
the names, tags and description of each field. Useful for debugging or
long-term storage where the original schema could be forgotten.
Schema format TBD.

The message header is a sequence of fields (ie. numbered fields / slots)
each field has a wire number and a data-type value. A field may also be marked
as an empty padding field. This sequence of fields will be serialised as
close to the original memory representation as possible so that reading
and writing of the message is fast. A field heading always starts with
a single byte, the most significant three bits of which specify the
size of the data in that field:

	000 -> padding of exactly N bytes where N is the wire number of the this field.

	001 -> 8 Bit field size.
	010 -> 16 Bit field size.
	011 -> 32 Bit field size.
	100 -> 64 Bit field size.

	101 -> Field marks the beginning of nested message (resets the numbering scheme until the next 111).
	110 -> Field marked as an array of exactly N elements where N is the wire number of the this field.
	111 -> Closes the previous nested 101 message.

The next five bits of the mesasge header are used to identify the number
of the box/field. If this number is larger than 32, then the five bits
are left as zeros and the next byte is used to identify the number. This
process is repeated until a number is found.

The message body contains the actual data of the message, as defined
by the header. If the header defines a pointer type, then the pointer
is a byte-offset into the entire message ie. usually into the additional
message memory.

A box message is intepreted based on the recieving type however overflows
will raise an error, so it is important to keep consistent types between
systems.
*/
package wire
