package wire_test

import (
	"bytes"
	"testing"

	"pkg.iqhive.com/iq/wire"
)

func TestWriter(t *testing.T) {
	var buf bytes.Buffer

	var w = wire.NewEncoder(&buf)

	w.EncodeU8('{')
	w.EncodeBytes([]byte("Hello, World!"))
	w.EncodeU8('}')
	w.EncodeBytes([]byte(" nice! "))

	if err := w.Flush(); err != nil {
		t.Fatal(err)
	}
	if got, want := buf.String(), "{Hello, World!} nice! "; got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}
